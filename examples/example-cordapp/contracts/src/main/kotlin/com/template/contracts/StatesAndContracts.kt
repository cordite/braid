/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.template.contracts

import net.corda.core.contracts.*
import net.corda.core.identity.AbstractParty
import net.corda.core.transactions.LedgerTransaction

open class ExampleContract : Contract {
  companion object {

    val ID: ContractClassName = ExampleContract::class.java.name as ContractClassName
  }

  // A transaction is considered valid if the verify() function of the contract of each of the transaction's input
  // and output states does not throw an exception.
  override fun verify(tx: LedgerTransaction) {
  }

  // Used to indicate the transaction's intent.
  interface Commands : CommandData {
    class Create : Commands
  }
}

// *********
// * State *
// *********
@BelongsToContract(ExampleContract::class)
data class ExampleState(val data: String, override val participants: List<AbstractParty>) : ContractState