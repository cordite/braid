/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.template.flows.flows

import co.paralleluniverse.fibers.Suspendable
import com.template.contracts.ExampleContract
import com.template.contracts.ExampleState
import net.corda.core.contracts.Command
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.StartableByRPC
import net.corda.core.flows.StartableByService
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
@StartableByService
class IssueExampleState(private val data: String) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    val notary = serviceHub.networkMapCache.notaryIdentities[0]
    val state = ExampleState(data, listOf(serviceHub.myInfo.legalIdentities.first()))
    val command = Command(ExampleContract.Commands.Create(), ourIdentity.owningKey)
    val txb = TransactionBuilder(notary)
      .addOutputState(state, ExampleContract.ID)
      .addCommand(command)
    val signedTx = serviceHub.signInitialTransaction(txb)
    val secureHash = subFlow(FinalityFlow(signedTx, emptyList())).id
    return waitForLedgerCommit(secureHash)
  }
}