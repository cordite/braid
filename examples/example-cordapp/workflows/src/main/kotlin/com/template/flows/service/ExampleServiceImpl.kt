/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.template.flows.service

import com.template.contracts.ExampleState
import com.template.flows.flows.EchoFlow
import com.template.flows.flows.IssueExampleState
import io.cordite.braid.corda.BraidCordaContext
import net.corda.core.concurrent.CordaFuture
import net.corda.core.contracts.StateAndRef
import net.corda.core.identity.AbstractParty
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.contextLogger
import rx.Observable
import javax.ws.rs.QueryParam

@Suppress("unused") // bound at runtime by braid
class ExampleServiceImpl(braidContext: BraidCordaContext) : ExampleService {

  companion object {

    private val log = contextLogger()
  }

  private val serviceHub: AppServiceHub = braidContext.serviceHub

  override fun listenForExampleUpdates(): Observable<Vault.Update<ExampleState>> {
    return serviceHub.vaultService.trackBy(ExampleState::class.java).updates
  }

  override fun issueExampleState(@QueryParam("data") data: String): CordaFuture<SignedTransaction> {
    return serviceHub.startFlow(IssueExampleState(data)).returnValue
  }

  override fun getExamples(pageSize: Int, page: Int): List<StateAndRef<ExampleState>> {
    return serviceHub.vaultService.queryBy(ExampleState::class.java, PageSpecification(page, pageSize)).states
  }

  override fun echo(text: String, party: AbstractParty): CordaFuture<String> {
    return serviceHub.startFlow(EchoFlow(text, party)).returnValue
  }
}