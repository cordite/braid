/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.template.flows.service

import com.template.contracts.ExampleState
import io.cordite.braid.core.annotation.BraidMethod
import io.cordite.braid.core.annotation.BraidService
import io.cordite.braid.core.constants.BraidConstants.BRAID_AUTHORIZATION_SCHEME_NAME
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import net.corda.core.concurrent.CordaFuture
import net.corda.core.contracts.StateAndRef
import net.corda.core.identity.AbstractParty
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.DEFAULT_PAGE_SIZE
import net.corda.core.transactions.SignedTransaction
import rx.Observable
import javax.ws.rs.*

@BraidService(
  name = "myService",
  description = "my own service"
) // this name and description is used both for the JSON-RPC and REST protocols
@Path("/my-service") // base path for this service
@SecurityRequirement(name = BRAID_AUTHORIZATION_SCHEME_NAME)
interface ExampleService {

  @BraidMethod(
    value = "listens for template updates in the vault",
    returnType = Vault.Update::class
  )
  @GET
  @Path("/example/updates")
  fun listenForExampleUpdates(): Observable<Vault.Update<ExampleState>>

  @POST
  @Path("/example/")
  @Operation(summary = "issue an example state")
  fun issueExampleState(@QueryParam("data") data: String): CordaFuture<SignedTransaction>

  @GET
  @Path("/example/")
  @Operation(summary = "get example states in the database")
  fun getExamples(
    @QueryParam("page-size")
    @DefaultValue(DEFAULT_PAGE_SIZE.toString())
    pageSize: Int,

    @QueryParam("page")
    @DefaultValue("1")
    page: Int
  ): List<StateAndRef<ExampleState>>

  @POST
  @Path("/echo")
  @Operation(summary = "echo a text on a different party")
  fun echo(@QueryParam("text") text: String, @QueryParam("party") party: AbstractParty): CordaFuture<String>
}