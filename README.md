# Braid

[![pipeline status](https://gitlab.com/cordite/braid/badges/master/pipeline.svg)](https://gitlab.com/cordite/braid/commits/master)

![logo](docs/logo-small.png) 

 
## Introduction

A Kotlin communication library, implementing REST, JSON-RPC and a streaming-over-sockets extension to JSON-RPC.

The library can be used in any Kotlin / Java project using [braid-standalone-server](braid-standalone-server/README.md).

It can also be incorporated in [Corda](https://corda.net) Cordapps using [braid-corda](braid-corda/README.md). 

> To get started quickly with braid and Corda see the [Kotlin Cordapp template project with braid](https://gitlab.com/cordite/cordapp-template-kotlin-with-braid).

These both use the [core](braid-core/README.md) module. 

### Consuming Services 

The REST endpoint can be consumed using the `swagger.json` emitted by the server.
The socket JSON-RPC endpoint can be consumed using the following languages and runtimes:

* [javascript](braid-client-js) in a browser or NodeJS
* [any JVM language](braid-client)

### Protocol

If you want to implement your own client, or are just interested in how Braid works, the 
protocol is defined **[here](./braid-core/README.md)**.

## Examples

* [`example-cordapp`](examples/example-cordapp) - an example Corda cordapp, with customised authentication, streaming
* [`example-server`](examples/example-server) - a simple standalone server 
* [another cordapp](https://github.com/joeldudleyr3/pigtail) - another example by [Joel Dudley](https://twitter.com/joeldudley6)


## Building locally

You will need:

* Maven 3.6.x
* Node JS 12.18.3 or greater.

The build for all modules (Kotlin, Javascript, Cordapps etc) is orchestrated with maven.

First install the BFT-Smart dependency required by Corda. This is necessary because the depedency in R3's artifactory does not contain a pom.xml file. 

```
mvn install:install-file -Dfile=./lib/bft-smart.jar -DgroupId=com.github.bft-smart -DartifactId=library -Dversion=master-v1.1-beta-g6215ec8-87 -Dpackaging=jar -DgeneratePom=true
```

To compile, test and install the dependencies:

```bash
mvn clean install
```

The project can be loaded into any IDE that can read Maven POMs.

## Publishing / Deploying Artifacts

1. Navigate to the [Gitlab Tags](https://gitlab.com/cordite/braid/-/tags).
2. Create a new version tag in the form `v<major>.<minor>.<patch>[-<pre-release>]`.
3. In [Gitlab CI](https://gitlab.com/cordite/braid/-/pipelines), navigate to the tag's pipeline. There should be a manual
task called `Release`. Start it.
4. Log into [https://oss.sonatype.org/](https://oss.sonatype.org/).
5. Go to the `Staging Repositories` tab, and search for `cordite`, locating the current staged release.
6. Close the release and release it.
7. Make a cup of tea, think about the universe, while Sonatype takes its sweet time to push the files to 
[Maven Central](https://search.maven.org/search?q=g:io.cordite.braid).

