/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.jsonrpc

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.security.AuthenticatedSocket
import io.cordite.braid.core.service.ServiceExecutor
import io.cordite.braid.core.socket.SockJSSocketWrapper
import io.cordite.braid.core.socket.Socket
import io.cordite.braid.core.socket.TypedSocket
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.ext.auth.AuthProvider
import io.vertx.ext.web.handler.sockjs.SockJSSocket

/**
 * Binds a [SockJSSocket] to [service] using the JsonRpc processing pipeline
 */
class SockJSSocketServiceBinder(
  val braidConfig: BraidContext,
  val service: ServiceExecutor,
  val vertx: Vertx,
  val authProvider: AuthProvider? = null,
  private val threadPoolSize: Int = 1
) {

  fun bind(socket: SockJSSocket) {
    val sockWrapper = bindSocketAdapter(socket)
    val rpcSocket = TypedSocket.create<JsonRPCRequest, JsonRPCResponse>(braidConfig)
    sockWrapper.addListener(rpcSocket)
    val mount = JsonRpcMounter(service, vertx)
    rpcSocket.addListener(mount)
  }

  /**
   * Takes the [SockJSSocket] [socket] and wraps it with an adapter to make it into a [Socket]
   * Includes an [AuthenticatedSocket] in the pipeline, if an [AuthProvider] is provied
   */
  private fun bindSocketAdapter(socket: SockJSSocket): Socket<Buffer, Buffer> {
    val sockJSWrapper = SockJSSocketWrapper.create(socket, vertx, threadPoolSize)

    return if (authProvider == null) {
      sockJSWrapper
    } else {
      // we tag on the authenticator on the pipeline and return that
      val authenticatedSocket = AuthenticatedSocket.create(braidConfig, authProvider)
      sockJSWrapper.addListener(authenticatedSocket)
      authenticatedSocket
    }
  }
}