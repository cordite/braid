/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.swaggermodel.inspector

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.jsontype.TypeSerializer
import com.fasterxml.jackson.databind.ser.ContainerSerializer
import com.fasterxml.jackson.databind.ser.std.ArraySerializerBase

class ArraySerializerBaseInspector(src: ArraySerializerBase<Any>, private val mapper: ObjectMapper) :
  ArraySerializerBase<Any>(src) {

  val jacksonType: JavaType?
    get() {
      return when {
        _property != null && _property.type != null -> _property.type
        handledType() != null -> mapper.constructType(handledType())
        else -> null
      }
    }

  override fun getContentType(): JavaType {
    error("Should not be called")
  }

  override fun getContentSerializer(): JsonSerializer<*> {
    error("Should not be called")
  }

  override fun hasSingleElement(value: Any?): Boolean {
    error("Should not be called")
  }

  override fun _withValueTypeSerializer(vts: TypeSerializer?): ContainerSerializer<*> {
    error("Should not be called")
  }

  override fun _withResolved(prop: BeanProperty?, unwrapSingle: Boolean?): JsonSerializer<*> {
    error("Should not be called")
  }

  override fun serializeContents(value: Any?, jgen: JsonGenerator?, provider: SerializerProvider?) {
    error("Should not be called")
  }
}