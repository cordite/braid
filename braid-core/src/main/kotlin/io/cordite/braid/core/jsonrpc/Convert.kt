/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.jsonrpc

import io.cordite.braid.core.context.BraidContext
import java.lang.reflect.Parameter
import kotlin.reflect.KParameter
import kotlin.reflect.KType
import kotlin.reflect.jvm.jvmErasure

object Converter {

  fun convert(braidContext: BraidContext, value: Any?, parameter: KParameter) =
    convert(braidContext, value, parameter.type.jvmErasure.javaObjectType)

  fun convert(braidContext: BraidContext, value: Any?, parameter: Parameter) =
    convert(braidContext, value, parameter.type)

  fun convert(braidCon: BraidContext, value: Any?, type: KType) =
    convert(braidCon, value, type.jvmErasure.javaObjectType)

  fun convert(braidContext: BraidContext, value: Any?, clazz: Class<*>): Any? {
    return when (value) {
      null -> null
      else -> {
        braidContext.mapper.convertValue(value, clazz)
      }
    }
  }
}
