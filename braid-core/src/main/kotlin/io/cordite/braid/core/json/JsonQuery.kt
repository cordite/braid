/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.json

import io.cordite.braid.core.context.BraidContext
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import kotlin.reflect.KClass

/**
 * Simple functions to query JSON strings, [JsonObject] and [JsonArray] using
 * <a href="http://tools.ietf.org/html/draft-ietf-appsawg-json-pointer-03">JSON Pointer</a> syntax
 */
object JsonQuery {

  /**
   * JSON Pointer escape code for the character '~'
   */
  const val TILDA = "~0"

  /**
   * JSON Pointer escape code for the character '/
   */
  const val SLASH = "~1"

  /**
   * Escape strings that contain JSON Pointer special characters
   * e.g. for method paths `/my-method/path`
   */
  fun escape(value: String): String = value.replace("~", TILDA).replace("/", SLASH)

  // INLINE functions

  inline fun <reified T : Any> String.query(braidContext: BraidContext, expression: String): T? {
    return query(braidContext, this, expression, T::class)
  }

  inline fun <reified T : Any> JsonObject.query(braidContext: BraidContext, expression: String): T? {
    return query(braidContext, this, expression, T::class)
  }

  inline fun <reified T : Any> JsonArray.query(braidContext: BraidContext, expression: String): T? {
    return query(braidContext, this, expression, T::class)
  }

  // Static functions

  @Suppress("UNCHECKED_CAST")
  fun <T : Any> query(braidContext: BraidContext, json: String, expression: String, returnType: KClass<T>): T? {
    val jsonNode = braidContext.mapper.readTree(json).at(expression)
    return when (jsonNode) {
      null -> null
      else -> {
        val str = braidContext.mapper.writeValueAsString(jsonNode)
        when (returnType) {
          JsonObject::class -> JsonObject(str) as T
          JsonArray::class -> JsonArray(str) as T
          String::class -> {
            if (str.startsWith("\"") && str.endsWith("\"")) {
              braidContext.decodeValue(str, String::class) as T
            } else {
              str as T
            }
          }
          else -> braidContext.decodeValue(str, returnType.java)
        }
      }
    }
  }

  fun <T : Any> query(braidContext: BraidContext, json: JsonObject, expression: String, returnType: KClass<T>): T? {
    return query(braidContext, json.toString(), expression, returnType)
  }

  fun <T : Any> query(braidContext: BraidContext, json: JsonArray, expression: String, returnType: KClass<T>): T? {
    return query(braidContext, json.toString(), expression, returnType)
  }
}