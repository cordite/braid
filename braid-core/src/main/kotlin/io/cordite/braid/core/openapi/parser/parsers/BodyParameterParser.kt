/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.parser.parsers

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.parser.ParameterParser
import io.cordite.braid.core.openapi.parser.ParseResult
import io.netty.buffer.ByteBuf
import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.RoutingContext
import java.nio.ByteBuffer
import javax.ws.rs.HeaderParam
import javax.ws.rs.PathParam
import javax.ws.rs.QueryParam
import javax.ws.rs.core.Context
import kotlin.reflect.KCallable
import kotlin.reflect.KParameter
import kotlin.reflect.full.isSubclassOf

class BodyParameterParser(braidContext: BraidContext, routingContext: RoutingContext) :
  ParameterParser(braidContext, routingContext) {

  companion object {

    val ignoredParamAnnotations = listOf(
      HeaderParam::class,
      PathParam::class,
      QueryParam::class,
      Context::class
    )
  }

  override fun <R> internalParse(
    callable: KCallable<R>,
    parameter: KParameter,
    parameterIndex: Int
  ): ParseResult {
    val foundAnnotations = braidContext.annotationsCache.allMatchingAnnotations(callable, parameterIndex) {
      ignoredParamAnnotations.contains(it.annotationClass)
    }

    if (foundAnnotations.isNotEmpty()) return ParseResult.SKIP

    val body = routingContext.body
    val type = parameter.getType()
    val value = when {
      type.isSubclassOf(Buffer::class) -> {
        body
      }
      type.isSubclassOf(ByteArray::class) -> {
        body.bytes
      }
      type.isSubclassOf(ByteBuf::class) -> {
        body.byteBuf
      }
      type.isSubclassOf(ByteBuffer::class) -> {
        ByteBuffer.wrap(body.bytes)
      }
      type == String::class -> {
        body.toString()
      }
      else -> {
        deserialize(parameter.type, body.toString())
      }
    }
    return wrapResult(value, parameter)
  }

}