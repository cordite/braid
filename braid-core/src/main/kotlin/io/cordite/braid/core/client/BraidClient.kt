/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.client

import io.cordite.braid.core.client.invocations.Invocations
import io.cordite.braid.core.context.BraidContext
import io.vertx.core.http.HttpClientOptions
import java.io.Closeable
import java.lang.reflect.InvocationHandler
import java.lang.reflect.Method
import java.lang.reflect.Proxy

open class BraidClient<ServiceType>(
  protected val config: BraidClientConfig<ServiceType>,
  exceptionHandler: (Throwable) -> Unit = Invocations.defaultSocketExceptionHandler(),
  closeHandler: () -> Unit = Invocations.defaultSocketCloseHandler(),
  clientOptions: HttpClientOptions = Invocations.defaultClientHttpOptions
) : Closeable, InvocationHandler {

  private val invocations =
    Invocations.create(config, exceptionHandler, closeHandler, clientOptions)

  companion object {

    fun <ServiceType> createClient(
      config: BraidClientConfig<ServiceType>,
      exceptionHandler: (Throwable) -> Unit = Invocations.defaultSocketExceptionHandler(),
      closeHandler: (() -> Unit) = Invocations.defaultSocketCloseHandler(),
      clientOptions: HttpClientOptions = Invocations.defaultClientHttpOptions
    ): BraidClient<ServiceType> {
      return BraidClient(config, exceptionHandler, closeHandler, clientOptions)
    }

    fun <ServiceType> createClient(
      serviceClass: Class<ServiceType>,
      port: Int,
      host: String = "localhost",
      authCredentials: Any? = null,
      braidContext: BraidContext
    ): BraidClient<ServiceType> {
      return BraidClient(
        config = BraidClientConfig(
          serviceClass = serviceClass,
          port = port,
          host = host,
          trustAll = true,
          verifyHost = false,
          authCredentials = authCredentials,
          braidContext = braidContext
        )
      )
    }
  }

  fun activeRequestsCount(): Int {
    return invocations.activeRequestsCount
  }

  @Suppress("UNCHECKED_CAST")
  fun bind(): ServiceType {
    return Proxy.newProxyInstance(config.serviceClass.classLoader, arrayOf(config.serviceClass), this) as ServiceType
  }

  override fun close() {
    invocations.close()
  }

  override fun invoke(proxy: Any, method: Method, args: Array<out Any?>?): Any? {
    return invocations.invoke(
      method.name,
      method.genericReturnType,
      args ?: arrayOfNulls<Any>(0)
    )
  }
}
