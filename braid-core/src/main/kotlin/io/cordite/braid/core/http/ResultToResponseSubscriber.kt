/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.http

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.logging.loggerFor
import io.vertx.ext.web.RoutingContext
import rx.Subscriber
import java.lang.reflect.InvocationTargetException

class ResultToResponseSubscriber<T>(
  val braidContext: BraidContext,
  private val routingContext: RoutingContext,
  private val isSingleResult: Boolean
) :
  Subscriber<T>() {

  companion object {

    private val log = loggerFor<ResultToResponseSubscriber<*>>()
  }

  private var firstItem = true

  init {
    if (!isSingleResult) {
      routingContext.response().isChunked = true
    }
  }

  override fun onCompleted() {
    if (!routingContext.response().closed()) {
      try {
        routingContext.response().end()
      } catch (err: Throwable) {
        log.error("failed to close http response", err)
      }
    }
  }

  override fun onError(error: Throwable) {
    if (routingContext.response().closed()) return
    try {
      val e = if (error is InvocationTargetException) error.targetException else error
      routingContext.endWithObject(braidContext, e)
    } catch (ex: Throwable) {
      log.error("failed to write error", ex)
    }
  }

  override fun onNext(value: T) {
    if (routingContext.response().closed()) return
    try {
      if (!firstItem) routingContext.writeUtf8("\n")
      routingContext.writeObject(braidContext, value)
    } catch (err: Throwable) {
      log.error("failed to write item", err)
    }
    firstItem = false
  }

}
