/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.json

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.util.StdConverter
import java.math.BigDecimal
import java.time.Duration

@JsonSerialize(converter = DurationToStringConverter::class)
@JsonDeserialize(converter = StringToDurationConverter::class)
abstract class DurationMixin

class DurationToStringConverter : StdConverter<Duration, String>() {

  override fun convert(value: Duration): String {
    return value.toString()
  }
}

class StringToDurationConverter : StdConverter<String, Duration>() {

  override fun convert(value: String): Duration {
    return Duration.parse(value)
  }
}