/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@file:Suppress("MemberVisibilityCanBePrivate")

package io.cordite.braid.core.openapi.utils

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.swaggermodel.ParameterAnnotationConverter
import io.cordite.braid.core.reflection.getGenericParameterType
import io.netty.buffer.ByteBuf
import io.swagger.v3.core.util.AnnotationsUtils
import io.swagger.v3.oas.models.ExternalDocumentation
import io.swagger.v3.oas.models.Operation
import io.swagger.v3.oas.models.examples.Example
import io.swagger.v3.oas.models.headers.Header
import io.swagger.v3.oas.models.links.Link
import io.swagger.v3.oas.models.media.*
import io.swagger.v3.oas.models.parameters.*
import io.swagger.v3.oas.models.responses.ApiResponse
import io.swagger.v3.oas.models.responses.ApiResponses
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.swagger.v3.oas.models.servers.Server
import io.swagger.v3.oas.models.tags.Tag
import io.vertx.core.buffer.Buffer
import rx.Observable
import java.lang.reflect.GenericArrayType
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.nio.ByteBuffer
import javax.ws.rs.CookieParam
import javax.ws.rs.HeaderParam
import javax.ws.rs.PathParam
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType.*
import kotlin.reflect.*
import kotlin.reflect.full.valueParameters
import kotlin.reflect.jvm.javaType
import kotlin.reflect.jvm.jvmErasure
import io.swagger.v3.oas.annotations.ExternalDocumentation as ExternalDocumentationAnnotation
import io.swagger.v3.oas.annotations.Operation as OperationAnnotation
import io.swagger.v3.oas.annotations.extensions.Extension as ExtensionAnnotation
import io.swagger.v3.oas.annotations.headers.Header as HeaderAnnotation
import io.swagger.v3.oas.annotations.links.Link as LinkAnnotation
import io.swagger.v3.oas.annotations.links.LinkParameter as LinkParameterAnnotation
import io.swagger.v3.oas.annotations.media.ArraySchema as ArraySchemaAnnotation
import io.swagger.v3.oas.annotations.media.Content as ContentAnnotation
import io.swagger.v3.oas.annotations.media.Encoding as EncodingAnnotation
import io.swagger.v3.oas.annotations.media.ExampleObject as ExampleAnnotation
import io.swagger.v3.oas.annotations.media.Schema as SchemaAnnotation
import io.swagger.v3.oas.annotations.parameters.RequestBody as RequestBodyAnnotation
import io.swagger.v3.oas.annotations.responses.ApiResponse as ApiResponseAnnotation
import io.swagger.v3.oas.annotations.security.SecurityRequirement as SecurityRequirementAnnotation
import io.swagger.v3.oas.annotations.servers.Server as ServerAnnotation
import io.swagger.v3.oas.annotations.tags.Tag as TagAnnotation

class EndPointUtilities(val braidContext: BraidContext) {
  companion object {

    const val DEFAULT_DESCRIPTION = "description not given"

    // other data access functions
    @Suppress("MemberVisibilityCanBePrivate")
    val Schema<*>.fullyQualifiedRef: String
      get() = `$ref` ?: AnnotationsUtils.COMPONENTS_REF + name

    fun Schema<*>.toRefSchema(): Schema<*> {
      return when (this.type) {
        "object" -> Schema<Any>().`$ref`(fullyQualifiedRef)
        else -> this
      }
    }
  }

  fun getContent(parameter: KParameter): Content {
    val schema = braidContext.getOpenAPISchemaReference(parameter.type)
    return getContent(parameter.type, schema)
  }

  fun getContent(type: KType): Content {
    val schema = braidContext.getOpenAPISchemaReference(type)
    return getContent(type, schema)
  }

  private fun getContent(type: KType, schema: Schema<*>?): Content {
    return Content().apply {
      this.addMediaType(getMediaType(type), MediaType().apply {
        this.schema = schema
      })
    }
  }

  fun getContent(type: KClass<*>): Content {
    return getContent(type.java)
  }

  fun getContent(type: Type): Content {
    val schema = braidContext.getOpenAPISchema(type)?.toRefSchema()
    return getContent(type, schema)
  }

  private fun getContent(type: Type, schema: Schema<*>?): Content {
    return Content().apply {
      this.addMediaType(getMediaType(type), MediaType().apply {
        this.schema = schema
      })
    }
  }

  fun typeWithoutAsyncWrapper(type: KType): KType {
    return if (braidContext.asyncResultAdapters.isAsyncResultType(type.jvmErasure.java)) {
      type.arguments.last().type!!
    } else {
      type
    }
  }

  fun typeWithoutAsyncWrapper(type: Type): Type {
    return when (type) {
      is ParameterizedType -> {
        when {
          braidContext.asyncResultAdapters.isAsyncResultType(type.rawType as Class<*>) -> type.actualTypeArguments[0]
          else -> type
        }
      }
      else -> type
    }
  }

  private val binaryTypes =
    listOf(Buffer::class.java, ByteArray::class.java, ByteBuffer::class.java, ByteBuf::class.java)

  fun isBinary(type: Type): Boolean {
    return when (type) {
      is Class<*> -> {
        binaryTypes.any {
          it.isAssignableFrom(type)
        }
      }
      else -> false
    }
  }

  fun isArrayLike(type: Type): Boolean {
    return when {
      (type is ParameterizedType) && (type.rawType is Class<*>) -> Collection::class.java.isAssignableFrom(type.rawType as Class<*>)
      type is Class<*> -> type.isArray || Collection::class.java.isAssignableFrom(type)
      (type is GenericArrayType) -> true
      else -> false
    }
  }

  fun getArrayLikeItemType(type: Type): Type {
    return when {
      (type is GenericArrayType) -> type.genericComponentType
      (type is ParameterizedType) && (type.rawType is Class<*>) -> type.getGenericParameterType(0)
      type is Class<*> -> {
        if (type.isArray) {
          type.componentType
        } else {
          type
        }
      }
      else -> type
    }
  }

  fun toModel(annotation: TagAnnotation): Tag {
    return Tag().let { model ->
      if (annotation.name.isNotBlank()) model.name = annotation.name
      if (annotation.description.isNotBlank()) model.description = annotation.description
      model.extensions = toModel(annotation.extensions)
      model.externalDocs = toModel(annotation.externalDocs)
      model
    }
  }

  @Suppress("USELESS_IS_CHECK", "IMPLICIT_CAST_TO_ANY")
  fun toByRefModel(annotation: SchemaAnnotation, parameter: KParameter?): Schema<*>? {
    return toModel(annotation, parameter)?.toRefSchema()
  }

  @Suppress("USELESS_IS_CHECK", "IMPLICIT_CAST_TO_ANY")
  fun toModel(annotation: SchemaAnnotation, parameter: KParameter?): Schema<*>? {
    // this horror show is because when used with kotlin, the kotlin annotation processor inserts a KClass and not a Class
    // into the implementation field
    val implementationType = when (annotation.implementation) {
      Void::class, Void::class.java -> {
        parameter?.type?.javaType ?: getImplementationType(annotation)
      }
      else -> getImplementationType(annotation)
    }

    return if (implementationType != Void::class.java) {
      braidContext.getOpenAPISchema(implementationType)?.apply {
        if (annotation.format.isNotBlank()) format = annotation.format
        if (annotation.description.isNotBlank()) description = annotation.description
        if (annotation.defaultValue.isNotBlank()) {
          // TODO: de-serialize defaultValue for complex types?
          this.setDefault(annotation.defaultValue)
        }
      }
    } else null
  }

  @Suppress("USELESS_IS_CHECK") // because of a bug in the kotlin annotations processor, we get a Class<*> field storing a KClass<*>
  fun getImplementationType(annotation: SchemaAnnotation): Type {
    return when (annotation.implementation) {
      is KClass<*> -> annotation.implementation.java
      else -> annotation.implementation as Type
    }
  }

  @Suppress("MemberVisibilityCanBePrivate", "unused")
  fun toModel(annotation: ArraySchemaAnnotation, parameter: KParameter?): ArraySchema? {
    return if (AnnotationsUtils.hasArrayAnnotation(annotation)) {
      ArraySchema().let { model ->
        if (annotation.maxItems != Int.MIN_VALUE) model.maxItems = annotation.maxItems
        if (annotation.minItems != Int.MAX_VALUE) model.minItems = annotation.minItems
        model.uniqueItems = model.uniqueItems
        model.items = toByRefModel(annotation.schema, parameter)
        model
      }
    } else null
  }

  @Suppress("MemberVisibilityCanBePrivate")
  fun toModel(annotation: HeaderAnnotation, kParameter: KParameter?): Pair<String, Header> {
    return annotation.name to Header().let { model ->
      if (annotation.description.isNotBlank()) model.description = annotation.description
      model.schema = toByRefModel(annotation.schema, kParameter)
      model.required = annotation.required
      model.deprecated = annotation.deprecated
      if (annotation.ref.isNotBlank()) model.`$ref` = annotation.ref
      model
    }
  }

  @Suppress("MemberVisibilityCanBePrivate")
  fun toModel(annotations: Array<HeaderAnnotation>, kParameter: KParameter?): Map<String, Header>? {
    if (annotations.isEmpty()) return null
    return annotations.map { toModel(it, kParameter) }.toMap().toMutableMap()
  }

  @Suppress("MemberVisibilityCanBePrivate", "UselessCallOnNotNull")
  fun toModel(annotation: EncodingAnnotation, kParameter: KParameter?): Pair<String, Encoding>? {
    if (annotation.name.isNullOrBlank()) return null
    return annotation.name to Encoding().let { model ->
      model.contentType = annotation.contentType
      model.style = Encoding.StyleEnum.fromString(annotation.style)
      model.explode = annotation.explode
      model.allowReserved = annotation.allowReserved
      model.headers = toModel(annotation.headers, kParameter)
      model
    }
  }

  @Suppress("MemberVisibilityCanBePrivate")
  fun toModel(annotations: Array<ContentAnnotation>, kParameter: KParameter?): Content? {
    if (annotations.isEmpty()) return null
    return Content().let { model ->
      annotations.forEach { contentAnnotation ->
        val media = MediaType().let { media ->
          media.schema = toByRefModel(contentAnnotation.schema, kParameter)
          media.encoding = contentAnnotation.encoding.mapNotNull { toModel(it, kParameter) }.toMap().notEmptyOrNull()?.toMutableMap()
          media
        }
        model.addMediaType(contentAnnotation.mediaType, media)
      }
      model
    }
  }

  fun toModel(annotations: Array<ExtensionAnnotation>): Map<String, Any>? {
    if (annotations.isEmpty()) return null
    return AnnotationsUtils.getExtensions(*annotations)
  }

  @Suppress("MemberVisibilityCanBePrivate")
  fun toModel(annotation: ExampleAnnotation): Pair<String, Example>? {
    if (annotation.value.isBlank()) return null
    return annotation.name to Example().let { model ->
      if (annotation.summary.isNotBlank()) model.summary = annotation.summary
      if (annotation.value.isNotBlank()) model.value = annotation.value
      if (annotation.ref.isNotBlank()) model.`$ref` = annotation.ref
      if (annotation.externalValue.isNotBlank()) model.externalValue = annotation.externalValue
      if (annotation.description.isNotBlank()) model.description = annotation.description
      model.extensions = toModel(annotation.extensions)
      if (annotation.ref.isNotBlank()) model.`$ref` = annotation.ref
      model
    }
  }

  fun toModel(annotations: Array<ExampleAnnotation>): MutableMap<String, Example>? {
    if (annotations.isEmpty()) return null
    return annotations.mapNotNull { toModel(it) }.let {
      when {
        it.isEmpty() -> null
        else -> it.toMap().toMutableMap()
      }
    }
  }

  @Suppress("MemberVisibilityCanBePrivate")
  fun toModel(annotation: RequestBodyAnnotation, kParameter: KParameter?): RequestBody? {
    if (annotation.content.isEmpty()) return null
    return RequestBody().let { model ->
      if (annotation.description.isNotBlank()) model.description = annotation.description
      model.content = toModel(annotation.content, kParameter)
      model.required = annotation.required
      model.extensions = toModel(annotation.extensions)
      if (annotation.ref.isNotBlank()) model.`$ref` = annotation.ref
      model
    }
  }

  fun toModel(annotation: ServerAnnotation): Server? {
    return if (annotation.url.isNotBlank() || annotation.description.isNotBlank() || annotation.extensions.isNotEmpty()) {
      return Server().let { model ->
        if (annotation.url.isNotBlank()) model.url = annotation.url
        if (annotation.description.isNotBlank()) model.description = annotation.description
        model.extensions = toModel(annotation.extensions)
        model
      }
    } else null
  }

  fun toModel(annotations: Array<LinkParameterAnnotation>): Map<String, String>? {
    if (annotations.isEmpty()) return null
    return annotations.map {
      it.name to it.expression
    }.toMap().toMutableMap()
  }

  fun toModel(annotations: Array<LinkAnnotation>): Map<String, Link>? {
    if (annotations.isEmpty()) return null
    return annotations.map { linkAnnotation ->
      linkAnnotation.name to Link().let { link ->
        if (linkAnnotation.operationRef.isNotBlank()) link.operationRef = linkAnnotation.operationRef
        if (linkAnnotation.operationId.isNotBlank()) link.operationId = linkAnnotation.operationId
        if (linkAnnotation.description.isNotBlank()) link.description = linkAnnotation.description
        if (linkAnnotation.requestBody.isNotBlank()) {
          val processedLinkRequestBody = try {
            braidContext.mapper.readTree(linkAnnotation.requestBody)
          } catch (err: Throwable) {
            linkAnnotation.requestBody
          }
          link.requestBody = processedLinkRequestBody
          link.server = toModel(linkAnnotation.server)
          link.parameters = toModel(linkAnnotation.parameters)
        }
        link
      }
    }.toMap().toMutableMap()
  }

  fun toModel(annotation: ApiResponseAnnotation, kParameter: KParameter?): Pair<String, ApiResponse> {
    return annotation.responseCode to ApiResponse().let { model ->
      if (annotation.description.isNotBlank()) {
        model.description = annotation.description
      } else {
        model.description = DEFAULT_DESCRIPTION
      }
      model.headers = toModel(annotation.headers, kParameter)
      model.links = toModel(annotation.links)
      model.content = toModel(annotation.content, kParameter)
      model.extensions = toModel(annotation.extensions)
      if (annotation.ref.isNotBlank()) model.`$ref` = annotation.ref
      model
    }
  }

  fun toModel(annotation: Array<ApiResponseAnnotation>, kParameter: KParameter?): ApiResponses? {
    if (annotation.isEmpty()) return null
    return ApiResponses().let { responsesModel ->
      annotation.forEach { annotation ->
        val (name, response) = this.toModel(annotation, kParameter)
        responsesModel.addApiResponse(name, response)
        if (name == "default") {
          responsesModel.default = response
        }
      }
      responsesModel
    }
  }

  fun toModel(annotation: ExternalDocumentationAnnotation): ExternalDocumentation? {
    if (annotation.url.isBlank()) return null
    return ExternalDocumentation().let { model ->
      if (annotation.description.isNotBlank()) model.description = annotation.description
      if (annotation.url.isNotBlank()) model.url = annotation.url
      model.extensions = toModel(annotation.extensions)
      model
    }
  }

  @Suppress("SENSELESS_COMPARISON")
  fun toModel(annotation: OperationAnnotation, handler: KCallable<*>): Operation? {
    return if (!annotation.hidden)
      Operation().let { model ->
        model.tags = annotation.tags.toMutableList()
        if (annotation.summary.isNotBlank()) model.summary = annotation.summary
        if (annotation.description.isNotBlank()) model.description = annotation.description
        model.externalDocs = toModel(annotation.externalDocs)
        if (annotation.operationId.isNotBlank()) model.operationId = annotation.operationId
        model.requestBody = toModel(annotation.requestBody, null)
        model.parameters = annotation.parameters
          .filter { it.name != null }
          .map { parameterAnnotation ->
            check(parameterAnnotation.name != null) { "parameter annotation must have a name $annotation" }
            val kParameter = handler.valueParameters.find { it.name == parameterAnnotation.name }
            check(kParameter == null) { "parameter with name ${parameterAnnotation.name} not found. See $annotation on method $handler" }
            ParameterAnnotationConverter(braidContext, this).toModel(parameterAnnotation, kParameter, null)
          }
        model.responses = toModel(annotation.responses, null)
        model.extensions = toModel(annotation.extensions)
        model
      } else {
      null
    }
  }

  @Suppress("UselessCallOnNotNull")
  fun toModel(annotation: SecurityRequirementAnnotation): SecurityRequirement? {
    if (annotation.name.isNullOrBlank()) return null
    return SecurityRequirement().apply {
      addList(annotation.name, annotation.scopes.toMutableList())
    }
  }

  fun toModel(annotations: List<SecurityRequirementAnnotation>): List<SecurityRequirement>? {
    if (annotations.isEmpty()) return null
    return annotations.mapNotNull { toModel(it) }
  }

  fun getMediaType(type: KType): String {
    return getMediaType(type.javaType)
  }

  fun getMediaType(type: Type): String {
    val actualType = typeWithoutAsyncWrapper(type)
    return when {
      isBinary(actualType) -> APPLICATION_OCTET_STREAM
      actualType == String::class.java -> TEXT_PLAIN
      else -> APPLICATION_JSON
    }
  }

  fun isEmptyResponseType(actualReturnType: KType): Boolean {
    return actualReturnType.javaType == Unit::class.java ||
      actualReturnType.javaType == Void::class.java ||
      actualReturnType.javaType.typeName == "void"
  }

  fun createNonEmptyResponse(returnType: KType): ApiResponse {
    return ApiResponse().let { model ->
      model.description = DEFAULT_DESCRIPTION
      model.content = getContent(returnType)
      val rawReturnType = rawType(returnType.javaType)
      if (rawReturnType is Class<*> && Observable::class.java.isAssignableFrom(rawReturnType)) {
        model.addExtension("x-braid-stream", "true")
      }
      model
    }
  }

  fun rawType(type: Type): Type {
    return when (type) {
      is ParameterizedType -> type.rawType
      is GenericArrayType -> Array<Any>::class.java
      else -> type
    }
  }

  fun createEmptyResponse(): ApiResponse = ApiResponse().description(DEFAULT_DESCRIPTION)!!

  fun createResponseOrEmpty(returnType: KType): ApiResponse {
    val actualReturnType = typeWithoutAsyncWrapper(returnType)
    return if (isEmptyResponseType(actualReturnType)) {
      createEmptyResponse()
    } else {
      createNonEmptyResponse(returnType)
    }
  }

  // These copy data from the source to the destination, if the destination doesn't already have a value set

  fun fill(destination: Server, source: Server) {
    fillField(destination::setDescription, destination.description, source.description)
    fillField(destination::setExtensions, destination.extensions, source.extensions)
    fillField(destination::setUrl, destination.url, source.url)
    fillField(destination::setVariables, destination.variables, source.variables)
  }

  fun fill(destination: ApiResponse, source: ApiResponse) {
    fillField(destination::`set$ref`, destination.`$ref`, source.`$ref`)
    fillField(
      destination::setDescription,
      if (destination.description == DEFAULT_DESCRIPTION) null else destination.description,
      source.description
    )
    fillField(destination::setHeaders, destination.headers, source.headers)
    fillField(destination::setContent, destination.content, source.content)
    fillField(destination::setLinks, destination.links, source.links)
    fillField(destination::setExtensions, destination.extensions, source.extensions)
  }

  @Suppress("DuplicatedCode")
  fun fill(destination: Parameter, source: Parameter) {
    fillField(destination::setName, destination.name, source.name)
    fillField(destination::setIn, destination.`in`, source.`in`)
    fillField(destination::setDescription, destination.description, source.description)
    destination.required = source.required
    fillField(destination::setDeprecated, destination.deprecated, source.deprecated)
    fillField(destination::setAllowEmptyValue, destination.allowEmptyValue, source.allowEmptyValue)
    fillField(destination::`set$ref`, destination.`$ref`, source.`$ref`)
    fillObjectField(destination::setStyle, destination.style, source.style)
    fillField(destination::setExplode, destination.explode, source.explode)
    fillField(destination::setAllowReserved, destination.allowReserved, source.allowReserved)
    fillField(destination::setSchema, destination.schema, source.schema)
    fillField(destination::setExamples, destination.examples, source.examples)
    fillObjectField(destination::setExample, destination.example, source.example)
    fillField(destination::setContent, destination.content, source.content)
    fillField(destination::setExtensions, destination.extensions, source.extensions)
  }

  fun fill(destination: RequestBody, source: RequestBody) {
    fillField(destination::setDescription, destination.description, source.description)
    fillField(destination::setContent, destination.content, source.content)
    fillField(destination::setRequired, destination.required, source.required)
    fillField(destination::setExtensions, destination.extensions, source.extensions)
  }

  @Suppress("DuplicatedCode")
  fun fill(destination: Operation, source: Operation) {
    fillField(destination::setTags, destination.tags, source.tags)
    fillField(destination::setSummary, destination.summary, source.summary)
    fillField(destination::setDescription, destination.description, source.description)
    fillField(destination::setExternalDocs, destination.externalDocs, source.externalDocs)
    fillField(destination::setOperationId, destination.operationId, source.operationId)
    fillField(destination::setParameters, destination.parameters, source.parameters)
    fillField(destination::setRequestBody, destination.requestBody, source.requestBody)
    fillField(destination::setResponses, destination.responses, source.responses)
    fillField(destination::setCallbacks, destination.callbacks, source.callbacks)
    fillField(destination::setDeprecated, destination.deprecated, source.deprecated)
    fillField(destination::setSecurity, destination.security, source.security)
    fillField(destination::setServers, destination.servers, source.servers)
    fillField(destination::setExtensions, destination.extensions, source.extensions)
  }

  @Suppress("DuplicatedCode")
  private fun fillField(setter: KFunction1<RequestBody, Unit>, destination: RequestBody?, source: RequestBody?) {
    when {
      source == null && destination == null -> return
      source != null && destination == null -> setter(source)
      source == null && destination != null -> setter(destination)
      else -> {
        source!!
        destination!!
        fillField(destination::setDescription, destination.description, source.description)
        fillField(destination::setContent, destination.content, source.content)
        fillField(destination::setContent, destination.content, source.content)
        fillField(destination::setRequired, destination.required, source.required)
        fillField(destination::setExtensions, destination.extensions, source.extensions)
        fillField(destination::`set$ref`, destination.`$ref`, source.`$ref`)
      }
    }
  }

  private fun fillField(setter: KFunction1<ExternalDocumentation, Unit>, destination: ExternalDocumentation?, source: ExternalDocumentation?) {
    @Suppress("DuplicatedCode")
    if (source == null && destination == null) return
    if (source == null && destination != null) {
      setter(destination); return
    }
    if (destination == null && source != null) {
      setter(source); return
    }
  }


  @Suppress("DuplicatedCode", "DuplicatedCode")
  private fun fillField(
    setter: KFunction1<Schema<*>, Unit>,
    destination: Schema<*>?,
    source: Schema<*>?
  ) {
    if (source == null && destination == null) return
    if (source == null && destination != null) {
      setter(destination); return
    }
    if (destination == null && source != null) {
      setter(source); return
    }
    destination!!
    source!!
    val dst = Schema<Any>()
    fillField(dst::setDefault, destination.default, source.default)
    fillField(dst::setName, destination.name, source.name)
    fillField(dst::setTitle, destination.title, source.title)
    fillField(dst::setMultipleOf, destination.multipleOf, source.multipleOf)
    fillField(dst::setMaximum, destination.maximum, source.maximum)
    fillField(dst::setExclusiveMaximum, destination.exclusiveMaximum, source.exclusiveMaximum)
    fillField(dst::setMinimum, destination.minimum, source.minimum)
    fillField(dst::setExclusiveMinimum, destination.exclusiveMinimum, source.exclusiveMinimum)
    fillField(dst::setMaxLength, destination.maxLength, source.maxLength)
    fillField(dst::setMinLength, destination.minLength, source.minLength)
    fillField(dst::setPattern, destination.pattern, source.pattern)
    fillField(dst::setMaxItems, destination.maxItems, source.maxItems)
    fillField(dst::setMinItems, destination.minItems, source.minItems)
    fillField(dst::setUniqueItems, destination.uniqueItems, source.uniqueItems)
    fillField(dst::setMaxProperties, destination.maxProperties, source.maxProperties)
    fillField(dst::setMinProperties, destination.minProperties, source.minProperties)
    fillField(dst::setRequired, destination.required, source.required)
    fillField(dst::setType, destination.type, source.type)
    fillField(dst::setNot, destination.not, source.not)
    fillField(dst::setProperties, destination.properties, source.properties)
    fillObjectField(dst::setAdditionalProperties, destination.additionalProperties, source.additionalProperties)
    fillField(dst::setDescription, destination.description, source.description)
    fillField(dst::setFormat, destination.format, source.format)
    fillField(dst::`set$ref`, destination.`$ref`, source.`$ref`)
    fillField(dst::setNullable, destination.nullable, source.nullable)
    fillField(dst::setReadOnly, destination.readOnly, source.readOnly)
    fillField(dst::setWriteOnly, destination.writeOnly, source.writeOnly)
    fillObjectField(dst::setExample, destination.example, source.example)
    fillObjectField(dst::setExternalDocs, destination.externalDocs, source.externalDocs)
    fillField(dst::setDeprecated, destination.deprecated, source.deprecated)
    fillObjectField(dst::setXml, destination.xml, source.xml)
    fillField(dst::setExtensions, destination.extensions, source.extensions)
    fillField(dst::setExtensions, destination.extensions, source.extensions)
    fillField(dst::setEnum, destination.enum, source.enum)
    fillObjectField(dst::setDiscriminator, destination.discriminator, source.discriminator)
    setter(dst)
  }

  private fun fillField(setter: KFunction1<Any, Unit>, destination: Any?, source: Any?) {
    if (destination != null) {
      setter(destination)
    } else if (source != null) {
      setter(source)
    }
  }

  private fun <K : Any, V : Any> fillField(
    setter: KFunction1<Map<K, V>, Unit>,
    destination: Map<K, V>?,
    source: Map<K, V>?
  ) {
    if (source == null && destination == null) return
    val dst = destination?.toMutableMap() ?: mutableMapOf()
    source?.forEach { (k, v) ->
      if (!dst.containsKey(k)) {
        dst[k] = v
      }
    }
    setter(dst)
  }

  private fun <FieldType : Number> fillField(
    setter: KFunction1<FieldType?, Unit>,
    destination: FieldType?,
    source: FieldType?
  ) {
    if (destination == null && source != null) {
      setter(source)
    } else {
      setter(destination)
    }
  }

  private fun fillField(
    setter: KFunction1<Boolean?, Unit>,
    destination: Boolean?,
    source: Boolean?
  ) {
    if (destination == null && source == null) return
    if (destination == null && source != null) {
      setter(source)
    } else {
      setter(destination)
    }
  }

  private fun <T> fillField(
    setter: KFunction1<List<T>?, Unit>,
    destination: List<T>?,
    source: List<T>?
  ) {
    if (destination == null && source == null) return
    val lst = destination?.toMutableSet() ?: mutableSetOf()
    source?.forEach {
      lst.add(it)
    }
    setter(lst.toMutableList())
  }

  private fun <K : Any, V : Any, T : LinkedHashMap<K, V>> fillField(
    setter: KFunction1<T?, Unit>,
    destination: T?,
    source: T?
  ) {
    if (source == null && destination == null) return
    if (source != null && destination == null) {
      setter(source); return
    }
    if (destination != null && source == null) {
      setter(destination); return
    }
    source!!
    destination!!
    source.forEach { (k, v) ->
      destination[k] = v
    }
    setter(destination)
  }

  private fun fillField(
    setter: KFunction1<String, Unit>,
    destination: String?,
    source: String?
  ) {
    if (destination == null && source == null) return

    if (destination.isNullOrBlank()) {
      if (source != null) {
        setter(source)
      }
    } else {
      setter(destination!!)
    }
  }

  private fun fillField(
    setter: KFunction1<Content, Unit>,
    destination: Content?,
    source: Content?
  ) {
    if (source == null && destination == null) return

    val dst = Content()
    destination?.forEach { (k, v) -> dst[k] = v }
    source?.forEach { (k, v) ->
      if (!dst.containsKey(k)) {
        dst[k] = v
      }
    }
    setter(dst)
  }

  private fun <T : Any> fillObjectField(setter: KFunction1<T?, Unit>, destination: T?, source: T?) {
    if (destination == null && source != null) {
      setter(source)
    }
  }

  fun <T : Annotation> getParameterAnnotation(
    handler: KCallable<*>,
    kParameter: KParameter,
    annotationClass: KClass<T>
  ): T? {
    return braidContext.webAnnotationExtractor.parameterAnnotation(handler, kParameter, annotationClass)
  }

  fun getJaxRSParameterAnnotation(handler: KCallable<*>, kParameter: KParameter): Annotation? {
    return getParameterAnnotation(handler, kParameter, QueryParam::class)
      ?: getParameterAnnotation(handler, kParameter, PathParam::class)
      ?: getParameterAnnotation(handler, kParameter, HeaderParam::class)
      ?: getParameterAnnotation(handler, kParameter, CookieParam::class)
  }

  fun <K, V> Map<K, V>.notEmptyOrNull(): Map<K, V>? {
    return when {
      isEmpty() -> null
      else -> this
    }
  }

}

fun SchemaAnnotation?.isNotVoid(): Boolean {
  return this != null && this.implementation != Void::class.java && this.implementation != Void::class
}
