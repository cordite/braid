/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.swaggermodel

import io.cordite.braid.core.logging.loggerFor
import io.swagger.v3.core.jackson.TypeNameResolver
import io.swagger.v3.core.jackson.TypeNameResolver.Options.SKIP_API_MODEL
import io.swagger.v3.oas.annotations.media.Schema
import org.apache.commons.lang3.StringUtils

object FullyQualifiedTypeNameResolver : TypeNameResolver() {

  private val log = loggerFor<FullyQualifiedTypeNameResolver>()
  override fun nameForClass(cls: Class<*>, options: MutableSet<Options>): String {
    return cls.openApiName(options)
  }

  fun Class<*>.openApiName(options: Set<Options> = emptySet()): String {
    if (options.contains(SKIP_API_MODEL)) {
      return name.makeOpenApiCompliant()
    }
    val model = getAnnotation(Schema::class.java)
    val modelName = if (model == null) null else StringUtils.trimToNull(model.name)
    val actualName = modelName ?: name.makeOpenApiCompliant() // map JVM type names to an acceptable form for OpenAPI
    if (log.isTraceEnabled) {
      log.trace("resolved class $this to name $actualName")
    }
    return actualName
  }

  fun String.makeOpenApiCompliant(): String {
    return this.replace("$", "_")
  }
}


