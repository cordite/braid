/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.reflection

import kotlin.reflect.KCallable
import kotlin.reflect.KParameter
import kotlin.reflect.full.valueParameters

/**
 * This class is used to wrap a [KCallable] ([innerCallable]) without a [KCallable.instanceParameter] with a given
 * [instance].
 */
data class KCallableWithInstance<R>(internal val instance: Any, internal val innerCallable: KCallable<R>) :
  KCallable<R> by innerCallable {

  companion object {

    fun <R> KCallable<R>.withInstance(instance: Any): KCallable<R> = KCallableWithInstance(instance, this)
  }

  override fun call(vararg args: Any?): R {
    return innerCallable.call(instance, *args)
  }

  override fun callBy(args: Map<KParameter, Any?>): R {
    val modifiedArgs = args + (innerCallable.parameters[0] to instance)
    return innerCallable.callBy(modifiedArgs)
  }

  override val parameters: List<KParameter>
    get() = innerCallable.valueParameters
}