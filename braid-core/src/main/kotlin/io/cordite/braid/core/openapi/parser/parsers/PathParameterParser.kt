/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.parser.parsers

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.parser.ParameterParser
import io.cordite.braid.core.openapi.parser.ParseResult
import io.cordite.braid.core.openapi.parser.RestParametersParser.Companion.description
import io.vertx.ext.web.RoutingContext
import javax.ws.rs.DefaultValue
import javax.ws.rs.PathParam
import kotlin.reflect.KCallable
import kotlin.reflect.KParameter

class PathParameterParser(
  braidContext: BraidContext,
  routingContext: RoutingContext
) :
  ParameterParser(braidContext, routingContext) {

  override fun <R> internalParse(
    callable: KCallable<R>,
    parameter: KParameter,
    parameterIndex: Int
  ): ParseResult {
    val annotation = braidContext.webAnnotationExtractor.parameterAnnotation(callable, parameterIndex, PathParam::class)
      ?: return ParseResult.SKIP
    val pathParameterName = annotation.value
    val payload = routingContext.pathParam(pathParameterName)

    val defaultValueAnnotation =
      braidContext.webAnnotationExtractor.parameterAnnotation(callable, parameter, DefaultValue::class)

    return if (payload == null && defaultValueAnnotation == null) {
      ParseResult.failed("method ${routingContext.description()} requires path parameter '$pathParameterName'")
    } else {
      val parsed = deserialize(parameter.type, payload)
      wrapResult(parsed, parameter)
    }

  }
}