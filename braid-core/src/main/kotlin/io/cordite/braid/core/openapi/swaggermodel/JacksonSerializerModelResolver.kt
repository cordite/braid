/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.swaggermodel

import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ser.std.MapSerializer
import com.fasterxml.jackson.databind.type.MapType
import com.fasterxml.jackson.databind.type.SimpleType
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.openapi.swaggermodel.inspector.SerializerInspector
import io.cordite.braid.core.openapi.utils.EndPointUtilities.Companion.toRefSchema
import io.swagger.v3.core.converter.AnnotatedType
import io.swagger.v3.core.converter.ModelConverter
import io.swagger.v3.core.converter.ModelConverterContext
import io.swagger.v3.core.jackson.ModelResolver
import io.swagger.v3.core.jackson.TypeNameResolver
import io.swagger.v3.oas.models.media.MapSchema
import io.swagger.v3.oas.models.media.ObjectSchema
import io.swagger.v3.oas.models.media.Schema
import org.apache.commons.lang3.ClassUtils
import java.lang.reflect.Type

/**
 * This class works out, for swagger, what the real type that a presented type maps to according to Jackson
 */
class JacksonSerializerModelResolver(mapper: ObjectMapper, typenameResolver: TypeNameResolver) :
  ModelResolver(mapper, typenameResolver) {

  companion object {

    private val log = loggerFor<JacksonSerializerModelResolver>()

    private val ignoredPackages: Set<String> = setOf(
      "javax.",
      "io.netty",
      "io.vertx",
      "java.",
      "rx"
    )
  }

  override fun resolve(
    annotatedType: AnnotatedType?,
    context: ModelConverterContext,
    next: MutableIterator<ModelConverter>?
  ): Schema<*>? {
    if (annotatedType == null) return super.resolve(annotatedType, context, next)

    val handledType = mapToAnnotatedTypeHandledByJackson(annotatedType)
    return when {
      isIgnored(handledType) -> null
      handledType.type is MapType -> resolveMapType(handledType, annotatedType, context, next)
      handledType.type == Object::class.java -> ObjectSchema()
      handledType == annotatedType -> super.resolve(annotatedType, context, next)
      else -> resolveJacksonMappedType(handledType, context, next, annotatedType)
    }
  }

  private fun resolveJacksonMappedType(
    handledType: AnnotatedType,
    context: ModelConverterContext,
    next: MutableIterator<ModelConverter>?,
    annotatedType: AnnotatedType
  ): Schema<*>? {
    val schema = super.resolve(handledType, context, next)
    if (schema != null && schema.type == "object") {
      val type: JavaType = when (annotatedType.type) {
        is JavaType -> annotatedType.type as JavaType
        else -> _mapper.constructType(annotatedType.type)
      }
      val typeName = _typeName(type)
      context.defineModel(typeName, schema, annotatedType, null)
    }
    return schema
  }

  private fun resolveMapType(
    handledType: AnnotatedType,
    annotatedType: AnnotatedType,
    context: ModelConverterContext,
    next: MutableIterator<ModelConverter>?
  ): Schema<*>? {
    val mapType = handledType.type as MapType
    val valueSchema = super.resolve(
      AnnotatedType(mapType.contentType).apply { ctxAnnotations = annotatedType.ctxAnnotations },
      context,
      next
    )
    return MapSchema().additionalProperties(valueSchema.toRefSchema())
  }

  fun isIgnored(annotatedType: AnnotatedType): Boolean {
    val type = annotatedType.type
    return when (type) {
      is Class<*> -> {
        val isPrimitiveType =
          String::class.java.isAssignableFrom(type) || type == Object::class.java || ClassUtils.isPrimitiveOrWrapper(
            type
          )
        !isPrimitiveType && ignoredPackages.any { type.canonicalName.startsWith(it) }
      }
      else -> false
    }
  }

  /**
   * Using Jackson we locate the actual mapped type using the pre-configured serializers
   */
  fun mapToAnnotatedTypeHandledByJackson(annotatedType: AnnotatedType): AnnotatedType {
    val javaTypeAsSeenBySwagger = annotatedType.type
    val jacksonJavaType = _mapper.constructType(javaTypeAsSeenBySwagger)

    // look up the serializer in Jackson
    val serializerProvider = _mapper.serializerProviderInstance
    val jsonSerializer = serializerProvider.findTypedValueSerializer(jacksonJavaType, true, null)
    val jacksonType = SerializerInspector(jsonSerializer, jacksonJavaType, _mapper).jacksonType
    val simplifiedType = if (jacksonType is SimpleType && jacksonType.bindings.isEmpty) {
      jacksonType.rawClass as Type
    } else {
      jacksonType
    }
    if (log.isDebugEnabled) {
      log.debug("converted $javaTypeAsSeenBySwagger to $jacksonType")
    }
    return AnnotatedType(simplifiedType).apply {
      name = annotatedType.name
      parent = annotatedType.parent
      isSkipOverride = annotatedType.isSkipOverride
      isSchemaProperty = annotatedType.isSchemaProperty
      ctxAnnotations = annotatedType.ctxAnnotations
      propertyName = annotatedType.propertyName
      resolveAsRef(annotatedType.isResolveAsRef)
      jsonViewAnnotation(annotatedType.jsonViewAnnotation)
      skipSchemaName(annotatedType.isSkipSchemaName)
      skipJsonIdentity(annotatedType.isSkipJsonIdentity)
    }
  }

  private fun getTypeOfMapSerializer(jsonSerializer: MapSerializer): MapType {
    // in that case find we need to find the types that Jackson handles for K and V of the map
    // there is a problem here which we need to resolve in the future
    // the keySerializer.handledType()  are both erased types ... 🤯
    // this is the best we can do without forcing MapSerializer._keyType to be public and reflecting it
    val mappedKeyType = mapToAnnotatedTypeHandledByJackson(AnnotatedType(jsonSerializer.keySerializer.handledType()))
    val mappedValueType = mapToAnnotatedTypeHandledByJackson(AnnotatedType(jsonSerializer.contentType))

    val syntheticType = _mapper.typeFactory.constructMapType(
      Map::class.java,
      _mapper.typeFactory.constructType(mappedKeyType.type),
      _mapper.typeFactory.constructType(mappedValueType.type)
    )
    return syntheticType
  }
}

