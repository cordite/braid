/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.parser

import com.fasterxml.jackson.databind.JavaType
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.logging.loggerFor
import io.vertx.ext.web.RoutingContext
import java.lang.reflect.Type
import javax.ws.rs.DefaultValue
import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.KType
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.jvm.javaType

abstract class ParameterParser(
  val braidContext: BraidContext,
  val routingContext: RoutingContext
) {

  companion object {

    protected val log = loggerFor<ParameterParser>()
    protected const val QUOTE = "\""
  }

  fun <R> parse(callable: KCallable<R>, parameter: KParameter, parameterIndex: Int): ParseResult {
    return try {
      internalParse(callable, parameter, parameterIndex)
    } catch (err: Throwable) {
      val msg = "failed to parse parameter ${parameter.kind} for route ${routingContext.request().method()} " +
        "${routingContext.request().path()}: ${err.message}"

      if (log.isDebugEnabled) {
        log.debug(msg, err)
      }

      ParseResult.failed(msg)
    }
  }

  protected abstract fun <R> internalParse(
    callable: KCallable<R>,
    parameter: KParameter,
    parameterIndex: Int
  ): ParseResult

  protected fun parseDefault(parameter: KParameter): ParseResult {
    val annotation = parameter.findAnnotation<DefaultValue>() ?: return ParseResult.SKIP
    val result = deserialize(parameter.type, annotation.value)
    return ParseResult.succeeded(result)
  }

  protected fun deserialize(kType: KType, payload: String?): Any? {
    return deserialize(kType.javaType, payload)
  }

  protected fun deserialize(type: Type, payload: String?): Any? {
    val javaType = braidContext.mapper.constructType(type)
    return deserialize(javaType, payload)
  }

  protected fun deserialize(javaType: JavaType, payload: String?): Any? {
    return when (payload) {
      null -> null
      else -> {
        val serializerInstance = braidContext.mapper.serializerProviderInstance
        val serializer = serializerInstance.findTypedValueSerializer(javaType, true, null)
        val normalisedPayload = when (serializer.handledType()) {
          String::class.java -> payload.ensureQuoted()
          else -> payload
        }
        braidContext.decodeValue<Any>(normalisedPayload, javaType)
      }
    }
  }

  protected fun wrapResult(value: Any?, parameter: KParameter): ParseResult {
    return when (value) {
      null -> {
        when {
          parameter.findAnnotation<DefaultValue>() != null -> parseDefault(parameter)
          parameter.type.isMarkedNullable -> ParseResult.succeeded(value)
          else -> ParseResult.SKIP
        }
      }
      else -> ParseResult.succeeded(value)
    }
  }

  protected fun String.ensureQuoted(): String {
    val trimmed = trim()
    return if (!trimmed.startsWith(QUOTE) || !trimmed.endsWith(QUOTE)) {
      "${QUOTE}$trimmed${QUOTE}"
    } else {
      trimmed
    }
  }

  protected fun KParameter.getType(): KClass<*> {
    return when (type.classifier) {
      is KClass<*> -> {
        type.classifier as KClass<*>
      }
      else -> throw RuntimeException("parameter doesn't have a class type")
    }
  }
}