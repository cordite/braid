/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.annotation

import kotlin.reflect.KClass

/**
 * Decorate a class or interface as being a braid service
 * This will allow braid to instantiate it and bind it to both REST and JSON-RPC
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class BraidService(
  val description: String = "",
  val name: String = "",
  val category: String = ""
)

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class BraidMethod(
  val value: String = "",
  val description: String = "",
  val returnType: KClass<*> = Any::class
)

/**
 * Explicitly to ignore methods for JSON-RPC Binding
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class JsonRpcIgnore

/**
 * Services decorate with this as well as [BraidService] will not
 * be automatically bound
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class BraidDoNotAutoBind
