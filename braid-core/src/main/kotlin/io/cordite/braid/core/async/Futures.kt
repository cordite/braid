/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.async

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.logging.loggerFor
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Future.succeededFuture
import io.vertx.core.Promise
import io.vertx.core.Promise.promise
import io.vertx.core.buffer.Buffer
import rx.Observable
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicInteger

private val log = loggerFor<Future<*>>()

fun <T> Future<T>.getOrThrow(): T {
  val latch = CountDownLatch(1)

  this.setHandler {
    latch.countDown()
  }

  latch.await()
  if (this.failed()) {
    throw this.cause()
  }
  return this.result()
}

fun <T> Future<T>.then(fn: (T) -> Unit): Future<T> {
  val promise = promise<T>()
  val future = promise.future()
  setHandler { asyncResult ->
    try {
      if (!future.isComplete) {
        when {
          asyncResult.succeeded() -> {
            val r = asyncResult.result()
            fn(r)
            promise.tryComplete(r)
          }
          asyncResult.failed() -> {
            promise.tryFail(asyncResult.cause())
          }
        }
      }
    } catch (err: Throwable) {
      promise.tryFail(err)
    }
  }
  return future
}

fun <T> Future<T>.catch(fn: (Throwable) -> Unit): Future<T> {
  val promise = promise<T>()
  val future = promise.future()

  setHandler { asyncResult ->
    try {
      if (!future.isComplete) {
        when {
          asyncResult.failed() -> {
            val cause = asyncResult.cause()
            fn(cause)
            promise.tryFail(cause)
          }
          asyncResult.succeeded() -> {
            promise.tryComplete(asyncResult.result())
          }
        }
      }
    } catch (err: Throwable) {
      promise.tryFail(err)
    }
  }
  return future
}

fun <T> Future<T>.finally(fn: (AsyncResult<T>) -> Unit): Future<T> {
  val promise = promise<T>()
  val future = promise.future()
  setHandler { asyncResult ->
    try {
      if (!future.isComplete) {
        fn(asyncResult)
        when {
          asyncResult.succeeded() -> promise.tryComplete(asyncResult.result())
          asyncResult.failed() -> promise.tryFail(asyncResult.cause())
        }
      }
    } catch (err: Throwable) {
      promise.tryFail(err)
    }
  }
  return future
}

@JvmName("allTyped")
fun <T> all(vararg futures: Future<T>): Future<List<T>> {
  return futures.toList().all()
}

@Suppress("UNCHECKED_CAST")
fun all(vararg futures: Future<*>): Future<List<*>> {
  return (futures.toList() as List<Future<Any>>).all() as Future<List<*>>
}

fun <T> List<Future<T>>.all(): Future<List<T>> {
  if (this.isEmpty()) return succeededFuture(emptyList())
  val results = mutableMapOf<Int, T>()
  val pResult = promise<List<T>>()
  val fResult = pResult.future()
  val countdown = AtomicInteger(this.size)
  this.forEachIndexed { index, future ->
    future.setHandler { ar ->
      when {
        fResult.isComplete -> {
          // we received a result after the future was completed.
          // this shouldn't happen but we have to guard for result sources that
          // don't comply to the handler spec
        }
        ar.succeeded() -> {
          results[index] = ar.result()
          if (countdown.decrementAndGet() == 0) {
            pResult.tryComplete(results.entries.sortedBy { it.key }.map { it.value })
          }
        }
        else -> {
          // we've got a failed future - report it
          pResult.tryFail(ar.cause())
        }
      }
    }
  }
  return fResult
}

inline fun <T> withPromise(fn: (Promise<T>) -> Unit): Promise<T> {
  val result = promise<T>()
  fn(result)
  return result
}

inline fun <reified T> Future<Buffer>.mapToObject(braidContext: BraidContext): Future<T> =
  this.map { braidContext.decodeValue(it, T::class.java) }

fun <T> Future<T>.mapUnit(): Future<Unit> = this.map { Unit }
fun <T> Future<T>.mapVoid(): Future<Void> = this.map { null }

fun <T> Future<T>.assertFails(): Future<Unit> {
  val promise = promise<Unit>()
  val future = promise.future()
  setHandler { asyncResult ->
    if (!future.isComplete) {
      when {
        asyncResult.succeeded() -> promise.tryFail("did not fail")
        else -> promise.tryComplete(Unit)
      }
    }
  }
  return future
}

fun <T> Observable<T>.toFuture(): Future<T> {
  val promise = promise<T>()
  this.single().subscribe({ value ->
    if (!promise.tryComplete(value)) {
      log.warn("Observable completed but apparently for the second time")
    }
  }, { cause ->
    if (!promise.tryFail(cause)) {
      log.warn("Observable completed with error but apparently for the second time")
    }
  })
  return promise.future()
}
