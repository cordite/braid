/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.service

import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.annotation.JsonRpcIgnore

/**
 * This is an optional interface for Braid service to implement if the service n
 * eeds more control for configuring its execution. Each implementor is passed a
 * [BraidConfig] that it can use to bind itself to Braid's transports.
 * NB your class *may* have a single argument constructor that takes an [BraidContext]
 */
interface ConfigurableBraidService {

  /**
   * Used to customise the braid config - usually you'll just register yourself as a service
   */
  @JsonRpcIgnore
  fun configureWith(config: BraidConfig): BraidConfig
}

