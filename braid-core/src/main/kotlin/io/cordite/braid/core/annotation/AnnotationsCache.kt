/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.annotation

import io.cordite.braid.core.reflection.KCallableWithInstance
import java.lang.reflect.Constructor
import java.lang.reflect.Method
import kotlin.reflect.KCallable
import kotlin.reflect.KClass

class AnnotationsCache {

  private val cache = mutableMapOf<ClassLoader, ClassLoaderAnnotationCache>()

  fun <R> allMatchingAnnotations(
    callable: KCallable<R>,
    matcher: (Annotation) -> Boolean = { true }
  ): List<Annotation> {
    val trueCallable = if (callable is KCallableWithInstance) callable.innerCallable else callable
    return getCache().allMatchingAnnotations(trueCallable, matcher)
  }

  fun <R> allMatchingAnnotations(
    callable: KCallable<R>,
    parameterIndex: Int,
    matcher: (Annotation) -> Boolean = { true }
  ): List<Annotation> {
    val trueCallable = if (callable is KCallableWithInstance) callable.innerCallable else callable
    return getCache().allMatchingAnnotations(trueCallable, parameterIndex, matcher)
  }

  @Suppress("UNCHECKED_CAST")
  fun <A : Annotation, R> findAnnotation(callable: KCallable<R>, parameterIndex: Int, annotationClass: KClass<A>): A? {
    val trueCallable = if (callable is KCallableWithInstance) callable.innerCallable else callable
    val annotation =
      getCache().allMatchingAnnotations(trueCallable, parameterIndex) { it.annotationClass == annotationClass }
        .firstOrNull() ?: return null
    return annotation as A
  }

  @Suppress("UNCHECKED_CAST")
  fun <A : Annotation, R> findAnnotation(callable: KCallable<R>, annotationClass: KClass<A>): A? {
    val trueCallable = if (callable is KCallableWithInstance) callable.innerCallable else callable
    val annotation =
      getCache().allMatchingAnnotations(trueCallable) { it.annotationClass == annotationClass }.firstOrNull()
        ?: return null
    return annotation as A
  }

  @Suppress("UNCHECKED_CAST")
  fun <A : Annotation> findAnnotation(objClass: KClass<*>, annotationClass: KClass<A>): A? {
    return findAnnotation(objClass.java, annotationClass.java)
  }

  @Suppress("UNCHECKED_CAST")
  fun <A : Annotation, R> findAnnotation(objClass: Class<R>, annotationClass: Class<A>): A? {
    return findAnnotationWithTarget(objClass, annotationClass)?.let { (_, annotation) -> annotation }
  }

  @Suppress("UNCHECKED_CAST")
  fun <A : Annotation, R> findAnnotationWithTarget(objClass: Class<R>, annotationClass: Class<A>): Pair<Class<*>, A>? {
    // TODO: cache this?
    val chain = sequenceOf(
      { objClass.getAnnotation(annotationClass)?.let { objClass to it } },
      {
        when {
          objClass.superclass != null -> findAnnotationWithTarget(objClass.superclass, annotationClass)
          else -> null
        }
      },
      {
        objClass.interfaces.asSequence().map { findAnnotationWithTarget(it, annotationClass) }.filterNotNull()
          .firstOrNull()
      }
    )
    return chain.map { it() }.filterNotNull().firstOrNull()
  }

  fun <A : Annotation> findAnnotation(method: Method, annotationClass: Class<A>): A? {
    // TODO: cache this?
    val chain = sequenceOf(
      { method.getAnnotation(annotationClass) },
      {
        try {
          method.declaringClass.superclass?.getMethod(method.name, *method.parameterTypes)
            ?.let {
              findAnnotation(it, annotationClass)
            }

        } catch (err: NoSuchMethodException) {
          null
        }
      },
      {
        method.declaringClass.getAnnotation(annotationClass)
      },
      {
        method.declaringClass.interfaces.asSequence().map { findAnnotation(it, annotationClass) }.filterNotNull()
          .firstOrNull()
      }
    )
    return chain.map { it() }.filterNotNull().firstOrNull()
  }

  fun <A : Annotation> findAnnotation(constructor: Constructor<*>, annotationClass: Class<A>): A? {
    // we lazily walk the hierarchy looking for the required annotation class
    // TODO: cache these as well?
    val chain = sequenceOf(
      {
        constructor.getAnnotation(annotationClass)
      },
      {
        try {
          constructor.declaringClass.superclass?.getConstructor(*constructor.parameterTypes)
            ?.let {
              findAnnotation(it, annotationClass)
            }
        } catch (err: NoSuchMethodException) {
          null
        }
      },
      {
        constructor.declaringClass.getAnnotation(annotationClass)
      },
      {
        constructor.declaringClass.interfaces.asSequence().map { findAnnotation(it, annotationClass) }.filterNotNull()
          .firstOrNull()
      }
    )
    return chain.map { it() }.filterNotNull().firstOrNull()
  }

  private fun getCache(): ClassLoaderAnnotationCache {
    return cache.computeIfAbsent(Thread.currentThread().contextClassLoader) {
      ClassLoaderAnnotationCache()
    }
  }
}