/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.service

import io.cordite.braid.core.annotation.AnnotationsCache
import io.cordite.braid.core.annotation.BraidService
import io.cordite.braid.core.string.toKebabCase

interface ServiceNaming {
  companion object {

    fun create(annotationsCache: AnnotationsCache): ServiceNaming = ServiceNamingImpl(annotationsCache)
  }

  fun getServiceName(service: Any): String
  fun getServiceName(serviceClass: Class<*>): String
  fun getServiceCategory(service: Any): String
  fun getServiceCategory(serviceClass: Class<*>): String
}

class ServiceNamingImpl(private val annotationsCache: AnnotationsCache) : ServiceNaming {

  override fun getServiceName(service: Any): String {
    return getServiceName(service.javaClass)
  }

  override fun getServiceName(serviceClass: Class<*>): String {
    val annotationWithTarget = annotationsCache.findAnnotationWithTarget(serviceClass, BraidService::class.java)
    val annotation = annotationWithTarget?.second
    val target = annotationWithTarget?.first ?: serviceClass
    return when {
      annotation == null || annotation.name.isBlank() -> target.simpleName.decapitalize()
      else -> annotation.name
    }
  }

  override fun getServiceCategory(service: Any): String {
    return getServiceCategory(service.javaClass)
  }

  override fun getServiceCategory(serviceClass: Class<*>): String {
    val annotationWithTarget = annotationsCache.findAnnotationWithTarget(serviceClass, BraidService::class.java)
    val annotation = annotationWithTarget?.second
    val target = annotationWithTarget?.first ?: serviceClass
    return when {
      annotation == null || annotation.category.isBlank() -> getServiceName(target).toKebabCase()
      else -> annotation.category
    }
  }
}