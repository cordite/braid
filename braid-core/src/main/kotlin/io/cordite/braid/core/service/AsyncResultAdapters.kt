/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.service

import rx.Observable


data class AsyncResultAdapters(private val asyncResultAdapters: List<AsyncResultAdapter<*>>) {
  companion object {

    val DEFAULT_ASYNC_RESULT_ADAPTERS: AsyncResultAdapters = AsyncResultAdapters(
      listOf(VertxFutureAsyncResultAdapter, ObservableAsyncResultAdapter)
    )
  }

  @Suppress("UNCHECKED_CAST")
  fun <T> findAsyncResultAdapter(result: T?): AsyncResultAdapter<T> {
    return when (result) {
      null -> DefaultAsyncResultAdapter
      else -> {
        asyncResultAdapters.firstOrNull() {
          it.handledClass.isInstance(result)
        } ?: DefaultAsyncResultAdapter
      }
    } as AsyncResultAdapter<T>
  }

  @Suppress("UNCHECKED_CAST")
  fun adapt(requestId: Long, result: Any?): Observable<Any> {
    val adapter = when (result) {
      null -> DefaultAsyncResultAdapter
      else -> {
        asyncResultAdapters.firstOrNull() {
          it.handledClass.isInstance(result)
        } ?: DefaultAsyncResultAdapter
      }
    } as AsyncResultAdapter<Any>
    return adapter.adapt(requestId, result)
  }

  fun isAsyncResultType(java: Class<out Any>): Boolean {
    val adapter = asyncResultAdapters.firstOrNull { adapter ->
      adapter.handledClass.isAssignableFrom(java)
    }

    return adapter != null
  }

  operator fun <T> plus(asyncResultAdapter: AsyncResultAdapter<T>): AsyncResultAdapters {
    return copy(asyncResultAdapters = asyncResultAdapters + asyncResultAdapter)
  }

  operator fun <T> plus(asyncResultAdapters: List<AsyncResultAdapter<T>>): AsyncResultAdapters {
    return copy(asyncResultAdapters = this.asyncResultAdapters + asyncResultAdapters)
  }
}