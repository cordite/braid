/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.service

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.annotation.JsonRpcIgnore
import io.cordite.braid.core.jsonrpc.JsonRPCRequest
import io.cordite.braid.core.jsonrpc.Params
import io.cordite.braid.core.jsonschema.toDescriptor
import io.cordite.braid.core.logging.loggerFor
import rx.Observable
import rx.Subscriber
import java.lang.reflect.InvocationTargetException
import kotlin.reflect.KFunction
import kotlin.reflect.KVisibility
import kotlin.reflect.full.functions
import kotlin.reflect.full.valueParameters
import kotlin.reflect.jvm.javaType

class ConcreteServiceExecutor private constructor (
  private val braidContext: BraidContext,
  private val service: Any
) : ServiceExecutor {

  companion object {

    private val log = loggerFor<ConcreteServiceExecutor>()

  }

  data class Factory(private val service: Any): ServiceExecutorFactory<ConcreteServiceExecutor> {
    override fun create(braidContext: BraidContext): ConcreteServiceExecutor {
      return ConcreteServiceExecutor(braidContext, service)
    }
  }

  override fun invoke(request: JsonRPCRequest): Observable<Any> {
    return Observable.unsafeCreate<Any> { subscriber ->
      try {
        val parameters = request.parameters(braidContext)
        val candidates = candidateMethods(request)
        val result = candidates
          .asSequence() // lazy iteration through the candidate lists
          .convertParametersAndFilter(parameters)
          .map { (method, params) -> invokeMethod(method, params) }
          .firstOrNull()
          ?: throwMethodDoesNotExist(request)
        handleResult(request.id, result, subscriber)
      } catch (err: InvocationTargetException) {
        log.error("failed to invoke target for $request", err)
        subscriber.onError(err.targetException)
      } catch (err: Throwable) {
        log.error("failed to invoke $request", err)
        subscriber.onError(err)
      }
    }
  }

  private fun <R> invokeMethod(method: KFunction<R>, params: Array<Any?>): ResultOrFailure<R>? {
    if (log.isTraceEnabled) {
      log.trace(
        "invoking ${method.asSimpleString()} with ${
        params.joinToString(
          ","
        ) { it.toString() }
        }"
      )
    }
    return try {
      val result = ResultOrFailure.Result(method.call(service, *params))
      if (log.isTraceEnabled) {
        log.trace(
          "invoked ${method.asSimpleString()} with ${params.joinToString(",") { it.toString() }}"
        )
      }
      result
    } catch (err: IllegalArgumentException) {
      if (log.isTraceEnabled) {
        log.trace("method $method not appropriate for parameters $params")
      }
      null
    } catch (err: Throwable) {
      ResultOrFailure.Failure(err)
    }
  }

  private fun KFunction<*>.asSimpleString(): String {
    val params = this.valueParameters.joinToString(",") { "${it.name}: ${it.type.javaType.typeName}" }
    return "$name($params)"
  }

  private fun candidateMethods(request: JsonRPCRequest): List<KFunction<*>> {
    val params = request.parameters(braidContext)
    return service::class.functions
      .filter(request::matchesName)
      .filter(this::isPublic)
      .filter { it.valueParameters.size == params.count }
      .map { it to params.computeScore(it) }
      .filter { (_, score) -> score > 0 }
      .sortedByDescending { (_, score) -> score }
      .also {
        if (log.isTraceEnabled) {
          log.trace("scores for candidate methods for {}:", request)
          it.forEach {
            println("${it.second}: ${it.first.asSimpleString()}")
          }
        }
      }
      .map { (fn, _) -> fn }
  }

  @Throws
  private fun throwMethodDoesNotExist(request: JsonRPCRequest): Nothing {
    throw MethodDoesNotExist("failed to find a method that matches ${request.method}(${request.parameters(braidContext)})")
  }

  private fun Sequence<KFunction<*>>.convertParametersAndFilter(parameters: Params): Sequence<Pair<KFunction<*>, Array<Any?>>> {
    // attempt to convert the parameters
    return map { method ->
      method to try {
        parameters.mapParams(method)
      } catch (err: Throwable) {
        null
      }
    }
      // filter out parameters that didn't match
      .filter { (_, params) -> params != null }
      .map { (method, params) -> method to params!! }
  }

  private fun handleResult(requestId: Long, resultOrFailure: ResultOrFailure<*>, subscriber: Subscriber<Any>) {
    when (resultOrFailure) {
      is ResultOrFailure.Result -> braidContext.asyncResultAdapters.adapt(requestId, resultOrFailure.result)
        .subscribe(subscriber)
      is ResultOrFailure.Failure -> subscriber.onError(resultOrFailure.error)
    }
  }

  private fun isPublic(method: KFunction<*>) = method.visibility == KVisibility.PUBLIC

  override fun getMethodDescriptors(): List<MethodDescriptor> {
    return service::class.members
      .filter {
        it is KFunction<*> &&
          (it.visibility == KVisibility.PUBLIC)
      }
      .filter {
        !it.name.contains("$")
      }
      .filter {
        braidContext.annotationsCache.findAnnotation(it, JsonRpcIgnore::class) == null
      }
      .map {
        it.toDescriptor(braidContext, braidContext.asyncResultAdapters)
      }
  }
}

private sealed class ResultOrFailure<out R> {
  data class Result<R>(val result: R) : ResultOrFailure<R>()
  data class Failure<R>(val error: Throwable) : ResultOrFailure<R>()
}

