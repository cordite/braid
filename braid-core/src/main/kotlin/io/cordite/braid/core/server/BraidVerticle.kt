/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.server

import io.cordite.braid.core.async.executeAsync
import io.cordite.braid.core.async.mapVoid
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.http.setupAllowAnyCORS
import io.cordite.braid.core.http.setupOptionsMethod
import io.cordite.braid.core.http.withCompatibleWebsockets
import io.cordite.braid.core.jsonrpc.JsonRpcSocketBinder
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.openapi.RestMounter
import io.cordite.braid.core.router.Routers
import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.http.HttpServer
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.LoggerFormat
import io.vertx.ext.web.handler.LoggerHandler
import java.net.URL

class BraidVerticle(
  private val braidContext: BraidContext,
  private val config: BraidConfig
) : AbstractVerticle() {

  companion object {

    private val log = loggerFor<BraidVerticle>()
  }

  @Suppress("DEPRECATION")
  override fun start(startFuture: Future<Void>) {
    log.info("starting with ${braidContext.encode(config)}")
    vertx
      .executeAsync<Router> { it.tryComplete(setupRouter()) }
      .compose { router -> setupWebserver(router) }
      .mapVoid()
      .onSuccess {
        startFuture.tryComplete(it)
      }
      .onFailure {
        startFuture.tryFail(it)
      }
  }

  override fun stop(stopPromise: Promise<Void>) {
    Routers.remove(this.vertx, config.port)
    stopPromise.tryComplete()
  }

  private fun setupWebserver(router: Router): Future<HttpServer> {
    val promise = Promise.promise<HttpServer>()
    vertx.createHttpServer(config.createHttpServerOptions().withCompatibleWebsockets())
      .requestHandler(router)
      .listen(config.port) { httpServerAsyncResult ->
        if (httpServerAsyncResult.failed()) {
          log.error("failed to start server: ${httpServerAsyncResult.cause().message}")
        }
        promise.handle(httpServerAsyncResult)
      }
    return promise.future()
  }

  private fun setupRouter(): Router {
    val router = Routers.create(vertx, config.port)
    router.route().handler(LoggerHandler.create(LoggerFormat.SHORT))
    router.route().handler(BodyHandler.create())
    router.setupAllowAnyCORS()
    router.setupOptionsMethod()
    router.setupSockJSHandler(config)
    val moddedConfig = config.amendRestConfig {
      val host = URL(hostAndPortUri).host
      val updatedHostAndPort = "${config.protocol}://$host:${config.port}"
      withHostAndPortUri(updatedHostAndPort)
    }
    RestMounter.mount(braidContext, router, vertx, moddedConfig)
    return router
  }

  private fun Router.setupSockJSHandler(config: BraidConfig): Router {
    JsonRpcSocketBinder.bind(braidContext, config, this)
    return this
  }
}