/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.parser.parsers

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.parser.ParameterParser
import io.cordite.braid.core.openapi.parser.ParseResult
import io.cordite.braid.core.openapi.parser.ParseResult.Companion.failed
import io.cordite.braid.core.openapi.parser.RestParametersParser.Companion.description
import io.cordite.braid.core.openapi.swaggermodel.FullyQualifiedTypeNameResolver
import io.cordite.braid.core.openapi.swaggermodel.JacksonSerializerModelResolver
import io.cordite.braid.core.reflection.isArrayLike
import io.cordite.braid.core.reflection.isJsonPrimitive
import io.swagger.v3.core.converter.AnnotatedType
import io.vertx.core.json.JsonArray
import io.vertx.ext.web.RoutingContext
import javax.ws.rs.DefaultValue
import javax.ws.rs.HeaderParam
import kotlin.reflect.KCallable
import kotlin.reflect.KParameter
import kotlin.reflect.jvm.javaType

class HeaderParameterParser(
  braidContext: BraidContext,
  routingContext: RoutingContext
) : ParameterParser(braidContext, routingContext) {

  override fun <R> internalParse(
    callable: KCallable<R>,
    parameter: KParameter,
    parameterIndex: Int
  ): ParseResult {
    val annotation = braidContext.webAnnotationExtractor.parameterAnnotation(callable, parameter, HeaderParam::class)
      ?: return ParseResult.SKIP

    val headerName = annotation.value
    // split up the header values using the OpenAPI 'simple' regime
    val values = routingContext.request().headers().getAll(headerName)

    val result = deserialize(values, parameter)
    val defaultValueAnnotation =
      braidContext.webAnnotationExtractor.parameterAnnotation(callable, parameter, DefaultValue::class)

    return when {
      result != null || parameter.type.isMarkedNullable -> wrapResult(result, parameter)
      defaultValueAnnotation != null -> wrapResult(deserialize(parameter.type, defaultValueAnnotation.value), parameter)
      else -> failed("method ${routingContext.description()} requires header '$headerName'")
    }
  }

  private fun deserialize(values: List<String>, parameter: KParameter): Any? {
    if (values.isEmpty()) return null
    val mappedType =
      JacksonSerializerModelResolver(
        braidContext.mapper,
        FullyQualifiedTypeNameResolver
      ).mapToAnnotatedTypeHandledByJackson(
        AnnotatedType(parameter.type.javaType).schemaProperty(true)
      )
    return when {
      mappedType.type.isJsonPrimitive() -> deserialize(parameter.type, values.first())
      mappedType.type.isArrayLike() -> {
        val list = values.flatMap { it.split(',') }
        deserialize(parameter.type, JsonArray(list).encode())
      }
      else -> { // an object
        val items = values.first().split(',')
        when {
          items.size == 1 -> {
            // we don't have any pair-wise elements so just deserialize as normal
            deserialize(parameter.type, values.first())
          }
          (items.size % 2) != 0 -> {
            error("more than one item in the header but not even number to allow a pair-wise deserialization")
          }
          else -> {
            val payloadAsJson = braidContext.encode(items.windowed(2, 2).map { it[0] to it[1] }.toMap())
            deserialize(parameter.type, payloadAsJson)
          }
        }
      }
    }
  }
}


