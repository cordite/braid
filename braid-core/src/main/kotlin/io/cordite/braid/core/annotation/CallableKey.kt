/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.annotation

import kotlin.jvm.internal.FunctionReference
import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.full.instanceParameter
import kotlin.reflect.full.valueParameters

data class CallableKey(
  val instanceType: KClass<*>?,
  val name: String,
  val parameters: List<ParameterKey>,
  val returnType: KType
) {

  constructor(callable: KCallable<*>) : this(callable.getReceiverType(), callable.name, callable.valueParameters.map {
    ParameterKey(
      it
    )
  }, callable.returnType)

  companion object {

    fun <R> KCallable<R>.getReceiverType(): KClass<*>? {
      val classifier = instanceParameter?.type?.classifier
      return when {
        classifier != null && classifier is KClass<*> -> classifier
        this is FunctionReference && this.boundReceiver != null -> this.boundReceiver::class
        else -> null
      }
    }
  }
}

fun <R> KCallable<R>.createKey() = CallableKey(this)
