/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.swaggermodel.inspector

import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ser.std.ObjectArraySerializer

class ObjectArraySerializerInspector(src: ObjectArraySerializer, private val mapper: ObjectMapper) :
  ObjectArraySerializer(src, null) {
  val jacksonType: JavaType?
    get() {
      return when {
        _elementType != null -> mapper.typeFactory.constructArrayType(_elementType)
        handledType() != null -> mapper.constructType(handledType())
        else -> null
      }
    }
}