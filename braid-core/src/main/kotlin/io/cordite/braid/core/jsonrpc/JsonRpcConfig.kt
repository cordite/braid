/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.jsonrpc

import com.fasterxml.jackson.annotation.JsonIgnore
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.service.ConcreteServiceExecutor
import io.cordite.braid.core.service.ServiceExecutorFactory

data class JsonRpcConfig(
  val rootPath: String = DEFAULT_JSON_RPC_ROOT_PATH,
  @JsonIgnore
  val serviceExecutorFactories: Map<String, ServiceExecutorFactory<*>> = emptyMap()
) {

  companion object {

    private val log = loggerFor<JsonRpcConfig>()
    const val DEFAULT_JSON_RPC_ROOT_PATH = "/api"
  }

  @get:JsonIgnore
  val serviceNames: Set<String> get() = serviceExecutorFactories.keys

  @Suppress("MemberVisibilityCanBePrivate")
  fun withRootPath(rootPath: String): JsonRpcConfig {
    return this.copy(rootPath = rootPath)
  }

  @Suppress("MemberVisibilityCanBePrivate")
  fun withService(name: String, service: Any): JsonRpcConfig {
    if (serviceExecutorFactories.containsKey(name)) {
      log.warn("services already contains '${name}'. all other service bindings being ignored")
      return this
    }

    val factory = when (service) {
      is ServiceExecutorFactory<*> -> service // if the service is actually a service executor factory, use it
      else -> ConcreteServiceExecutor.Factory(service) // otherwise, wrap it
    }
    val map = serviceExecutorFactories.toMutableMap()
    map[name] = factory
    return copy(serviceExecutorFactories = map)

  }
}