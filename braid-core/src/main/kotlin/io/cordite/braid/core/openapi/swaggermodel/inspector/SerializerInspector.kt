/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.swaggermodel.inspector

import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ser.BeanSerializer
import com.fasterxml.jackson.databind.ser.impl.StringCollectionSerializer
import com.fasterxml.jackson.databind.ser.impl.TypeWrappedSerializer
import com.fasterxml.jackson.databind.ser.std.ArraySerializerBase
import com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase
import com.fasterxml.jackson.databind.ser.std.MapSerializer
import com.fasterxml.jackson.databind.ser.std.ObjectArraySerializer

class SerializerInspector(
  private val serializer: JsonSerializer<*>,
  private val swaggerProvidedJacksonType: JavaType,
  private val mapper: ObjectMapper
) {
  val jacksonType: JavaType
    get() {
      @Suppress("UNCHECKED_CAST")
      val proposedType = when (serializer) {
        is MapSerializer -> MapSerializerInspector(serializer, mapper).jacksonType
        is BeanSerializer -> BeanSerializerInspector(serializer, mapper).jacksonType
        is TypeWrappedSerializer -> TypeWrappedSerializerInspector(
          serializer,
          swaggerProvidedJacksonType,
          mapper
        ).jacksonType
        is ObjectArraySerializer -> ObjectArraySerializerInspector(serializer, mapper).jacksonType
        is AsArraySerializerBase -> AsArraySerializerBaseInspector(
          serializer as AsArraySerializerBase<Any>,
          mapper
        ).jacksonType
        is ArraySerializerBase -> ArraySerializerBaseInspector(
          serializer as ArraySerializerBase<Any>,
          mapper
        ).jacksonType
        is StringCollectionSerializer -> {
          swaggerProvidedJacksonType
        }
        else -> {
          when {
            serializer.handledType() != null -> mapper.constructType(serializer.handledType())
            else -> null
          }
        }
      }
      return proposedType ?: swaggerProvidedJacksonType
    }
}
