/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.jaxrs.runtime

//import io.cordite.braid.core.config.BraidConfig
//import javax.ws.rs.core.*
//import javax.ws.rs.ext.RuntimeDelegate
//
//class BraidRuntimeDelegate(braidConfig: BraidConfig) : RuntimeDelegate() {
//  init {
//    val delegateClass = RuntimeDelegate::class.java
//    val cachedDelegateFiled = delegateClass.declaredFields.first { it.name == "cachedDelegate" }
//    cachedDelegateFiled.isAccessible = true
//    cachedDelegateFiled.set(null, this)
//  }
//
//  override fun createUriBuilder(): UriBuilder {
//    TODO("Not yet implemented")
//  }
//
//  override fun createResponseBuilder(): Response.ResponseBuilder {
//    TODO("Not yet implemented")
//  }
//
//  override fun createVariantListBuilder(): Variant.VariantListBuilder {
//    TODO("Not yet implemented")
//  }
//
//  override fun <T : Any?> createEndpoint(application: Application?, endpointType: Class<T>?): T {
//    TODO("Not yet implemented")
//  }
//
//  override fun <T : Any?> createHeaderDelegate(type: Class<T>?): HeaderDelegate<T> {
//    TODO("Not yet implemented")
//  }
//
//  override fun createLinkBuilder(): Link.Builder {
//    TODO("Not yet implemented")
//  }
//
//}
//
