/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.config

//import io.cordite.braid.core.rest.jaxrs.runtime.BraidRuntimeDelegate
import com.fasterxml.jackson.annotation.JsonIgnore
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.http.HttpServerConfig
import io.cordite.braid.core.jsonrpc.JsonRpcConfig
import io.cordite.braid.core.logging.BraidLoggerInit
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.openapi.config.RestConfig
import io.cordite.braid.core.resources.Resources
import io.cordite.braid.core.server.BraidServer
import io.cordite.braid.core.service.ConfigurableBraidService
import io.cordite.braid.core.service.ServiceExecutorFactory
import io.vertx.core.Future
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject

/**
 * Configuration class for Braid
 * @param port - port that the service is bound on
 * @param httpServerConfig - these options control all HTTP transport concerns e.g. TLS
 */
data class BraidConfig(
  val port: Int = 8080,
  @JsonIgnore
  val serviceExecutorFactories: Map<String, ServiceExecutorFactory<*>> = emptyMap(),
  val httpServerConfig: HttpServerConfig = HttpServerConfig(),
  val restConfig: RestConfig = RestConfig(),
  val jsonRpcConfig: JsonRpcConfig = JsonRpcConfig()
) {

  companion object {

    private val log = loggerFor<BraidConfig>()

    init {
      BraidLoggerInit.init()
    }

    @Suppress("unused")
    @JvmStatic
    fun fromResource(braidContext: BraidContext, resourcePath: String): BraidConfig? {
      val fullConfig = try {
        val file = Resources.getResource(resourcePath).readText(Charsets.UTF_8)
        JsonObject(file)
      } catch (err: Throwable) {
        val msg = "could not find config resource $resourcePath"
        log.warn(msg)
        null
      }
      return if (fullConfig != null) {
        braidContext.decodeValue(fullConfig.encode(), BraidConfig::class.java)
      } else {
        null
      }
    }
  }

  @get:JsonIgnore
  val protocol: String
    get() = if (httpServerConfig.tlsEnabled) "https" else "http"

  @get:JsonIgnore
  val server: String
    get() = "${protocol}://localhost:$port"

//  private val jaxRuntimeDelegate = BraidRuntimeDelegate(this)

  fun createHttpServerOptions(): HttpServerOptions = httpServerConfig.build()

  fun withPort(port: Int) = this.copy(port = port)

  fun withHttpServerConfig(httpServerConfig: HttpServerConfig) = copy(httpServerConfig = httpServerConfig)

  fun withService(braidContext: BraidContext, service: Any) =
    withService(braidContext, braidContext.serviceNaming.getServiceName(service), service)

  @Suppress("MemberVisibilityCanBePrivate")
  fun withService(braidContext: BraidContext, name: String, service: Any): BraidConfig {
    val jsonRpcModified = amendJsonRpcConfig { withService(name, service) }
    return when (service) {
      is ConfigurableBraidService -> service.configureWith(jsonRpcModified)
      else -> jsonRpcModified.amendRestConfig { withService(braidContext, service) }
    }
  }

  @Suppress("unused")
  fun withThreadPoolSize(threadCount: Int): BraidConfig {
    return this.copy(httpServerConfig = httpServerConfig.copy(threadPoolSize = threadCount))
  }

  fun start(braidContext: BraidContext): Future<BraidServer> {
    return braidContext.createServer(this)
  }

  private fun withRestConfig(restConfig: RestConfig): BraidConfig {
    return this.copy(restConfig = restConfig)
  }

  private fun withJsonRpcConfig(jsonRpcConfig: JsonRpcConfig): BraidConfig {
    return this.copy(jsonRpcConfig = jsonRpcConfig)
  }

  fun amendRestConfig(fn: RestConfig.() -> RestConfig): BraidConfig {
    val newRestConfig = restConfig.fn()
    return withRestConfig(newRestConfig)
  }

  fun amendJsonRpcConfig(fn: JsonRpcConfig.() -> JsonRpcConfig): BraidConfig {
    val newJsonRpcConfig = jsonRpcConfig.fn()
    return withJsonRpcConfig(newJsonRpcConfig)
  }

  override fun toString(): String {
    return Json.encode(this)
  }
}


