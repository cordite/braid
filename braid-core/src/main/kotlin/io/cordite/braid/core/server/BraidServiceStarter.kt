/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.server

import io.cordite.braid.core.annotation.BraidDoNotAutoBind
import io.cordite.braid.core.annotation.BraidService
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.service.ConfigurableBraidService
import java.lang.reflect.Modifier

class BraidServiceStarter(
  private val braidContext: BraidContext,
  private val braidConfig: BraidConfig
) {

  companion object {

    private val log = loggerFor<BraidServiceStarter>()

  }

  /**
   * Start all classes that implement [ConfigurableBraidService] and bind them to the [BraidConfig]
   */
  fun startAllBraidServicesAndBindConfig(): BraidConfig {
    val braidServices = createAllBraidServices()
    return braidServices.fold(braidConfig) { acc, service ->
      acc.withService(braidContext, service)
    }
  }

  private fun createAllBraidServices(): List<Any> {
    val classes = findAllBraidServiceClasses()
    return classes.mapNotNull { serviceClass ->
      val constructorAndParams = serviceClass.constructors.asSequence()
        .mapNotNull { constructor ->
          try {
            constructor to braidContext.bindParameters(constructor.parameters)
          } catch (err: Throwable) {
            log.warn("cannot bind to constructor $constructor: ${err.message}")
            null
          }
        }.firstOrNull()
        ?: error("Failed to start service class ${serviceClass.name}. Could find a suitable constructor to bind")
      val (constructor, params) = constructorAndParams
      constructor.newInstance(*params)
    }
  }

  private fun findAllBraidServiceClasses(): List<Class<out Any>> {
    val res = braidContext.classScanResult
    val annotatedClasses = res.getClassesWithAnnotation(BraidService::class.qualifiedName)
      .flatMap {
        when {
          it.isInterface -> it.classesImplementing
          else -> listOf(it)
        }
      }
      .filter { !Modifier.isAbstract(it.modifiers) }
    val implementingClasses = res.getClassesImplementing(ConfigurableBraidService::class.qualifiedName).map { it }
    return (annotatedClasses + implementingClasses)
      .distinct()
      .map { Class.forName(it.name) }
      .filter { clazz ->
        braidContext.annotationsCache.findAnnotation(clazz, BraidDoNotAutoBind::class.java) == null
      }
  }
}

fun BraidConfig.findAndStartAllBraidServices(braidContext: BraidContext): BraidConfig {
  return BraidServiceStarter(braidContext, this).startAllBraidServicesAndBindConfig()
}
