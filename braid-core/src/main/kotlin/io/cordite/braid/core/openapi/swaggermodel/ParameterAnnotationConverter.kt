/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.swaggermodel

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.utils.EndPointUtilities
import io.cordite.braid.core.openapi.utils.EndPointUtilities.Companion.toRefSchema
import io.cordite.braid.core.openapi.utils.isNotVoid
import io.swagger.v3.core.util.AnnotationsUtils
import io.swagger.v3.oas.annotations.enums.Explode
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.enums.ParameterStyle
import io.swagger.v3.oas.models.media.Content
import io.swagger.v3.oas.models.media.MediaType
import io.swagger.v3.oas.models.parameters.*
import javax.ws.rs.CookieParam
import javax.ws.rs.HeaderParam
import javax.ws.rs.PathParam
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import kotlin.reflect.KParameter
import io.swagger.v3.oas.annotations.Parameter as ParameterAnnotation
import io.swagger.v3.oas.annotations.media.ArraySchema as ArraySchemaAnnotation
import io.swagger.v3.oas.annotations.media.Schema as SchemaAnnotation

class ParameterAnnotationConverter(
  private val braidContext: BraidContext,
  private val endpointUtilities: EndPointUtilities
) {

  fun toModel(
    annotation: ParameterAnnotation,
    kParameter: KParameter?,
    jaxRsParamAnnotation: Annotation?
  ): Parameter {

    // create the right type of parameter model, either on the annotation itself, or the JaxRS annotation if available
    val parameter = when (annotation.`in`) {
      ParameterIn.QUERY -> QueryParameter()
      ParameterIn.HEADER -> HeaderParameter()
      ParameterIn.PATH -> PathParameter()
      ParameterIn.COOKIE -> CookieParameter()
      else -> {
        when (jaxRsParamAnnotation) {
          is QueryParam -> QueryParameter()
          is HeaderParam -> HeaderParameter()
          is PathParam -> PathParameter()
          is CookieParam -> CookieParameter()
          else -> Parameter()
        }
      }
    }

    // extract the jaxrs name if provided
    val jaxRSName = when (jaxRsParamAnnotation) {
      is QueryParam -> jaxRsParamAnnotation.value
      is HeaderParam -> jaxRsParamAnnotation.value
      is PathParam -> jaxRsParamAnnotation.value
      is CookieParam -> jaxRsParamAnnotation.value
      else -> null
    }

    // set the name on the parameter, if available
    when {
      annotation.name.isNotBlank() -> {
        parameter.name = annotation.name
      }
      jaxRSName != null -> {
        parameter.name = jaxRSName
      }
      kParameter != null -> {
        parameter.name = kParameter.name
      }
    }

    // set various parameter fields

    if (annotation.description.isNotBlank()) parameter.description = annotation.description
    if (parameter !is PathParameter) {
      parameter.required = annotation.required
    }
    parameter.example = when {
      annotation.example.isBlank() -> null
      else -> annotation.example
    }
    parameter.examples = endpointUtilities.toModel(annotation.examples)
    parameter.deprecated = annotation.deprecated
    parameter.allowEmptyValue = annotation.allowEmptyValue
    if (annotation.style != ParameterStyle.DEFAULT) {
      parameter.style = Parameter.StyleEnum.valueOf(annotation.style.name)
    }
    parameter.explode = when (annotation.explode) {
      Explode.TRUE -> true
      else -> false
    }
    parameter.allowReserved = annotation.allowReserved
    parameter.extensions = endpointUtilities.toModel(annotation.extensions)

    // now let's figure out what schema we have and what we can do with it
    // swagger-ui is very sensitive to this and will actually throw exceptions if the schema definition and location
    // in the hierarchy does not match its interpretation of the spec

    // first get the schema annotation - there are two locations
    val schemaAnnotation = when {
      annotation.schema.isNotVoid() -> annotation.schema
      annotation.content.firstOrNull()?.schema.isNotVoid() -> annotation.content.first().schema
      AnnotationsUtils.hasArrayAnnotation(annotation.content.firstOrNull()?.array) -> annotation.content.first().array
      else -> null
    }

    // now construct a full schema model
    val fullModel = when (schemaAnnotation) {
      null -> {
        when (kParameter) {
          null -> null
          else -> braidContext.getOpenAPISchema(kParameter.type)
        }
      }
      is ArraySchemaAnnotation -> endpointUtilities.toModel(schemaAnnotation, kParameter)
      is SchemaAnnotation -> endpointUtilities.toModel(schemaAnnotation, kParameter)
      else -> null
    }

    // where we place the schema is sensitive if the schema is complex i.e. "object" or not
    if (fullModel != null && schemaAnnotation != null) {
      when {
        fullModel.type != "object" -> {
          parameter.schema = fullModel
        }
        fullModel.type == "object" -> {
          parameter.content = Content().apply {
            this.addMediaType(APPLICATION_JSON, MediaType().schema(fullModel.toRefSchema()))
          }
        }
      }
    }
    return parameter
  }
}