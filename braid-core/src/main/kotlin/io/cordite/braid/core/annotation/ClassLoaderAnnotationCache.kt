/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.annotation

import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.full.allSupertypes
import kotlin.reflect.full.instanceParameter
import kotlin.reflect.full.valueParameters

class ClassLoaderAnnotationCache {

  /**
   * map of all supers for a callable
   */
  private val superDefinitions = mutableMapOf<CallableKey, List<KCallable<*>>>()

  /**
   * map from callable to annotations
   */
  private val callableAnnotations = mutableMapOf<CallableKey, List<Annotation>>()

  /**
   * map from pair(callable, parameterIndex) -> annotations
   */
  private val parameterAnnotations = mutableMapOf<ParameterReference, List<Annotation>>()

  fun <R> allMatchingAnnotations(
    callable: KCallable<R>,
    parameterIndex: Int,
    matcher: (Annotation) -> Boolean = { true }
  ): List<Annotation> {
    val key = ParameterReference(callable, parameterIndex)
    return parameterAnnotations.computeIfAbsent(key) { _ ->
      val superDefinitions = callable.findSuperDefinitions().asSequence()
      val seq = callable.valueParameters[parameterIndex].annotations.asSequence() +
        superDefinitions.flatMap { superCallable -> allMatchingAnnotations(superCallable, parameterIndex).asSequence() }
      seq.toList().distinct()
    }.filter(matcher)
  }

  fun <R> allMatchingAnnotations(
    callable: KCallable<R>,
    matcher: (Annotation) -> Boolean = { true }
  ): List<Annotation> {
    val key = callable.createKey()
    return callableAnnotations.computeIfAbsent(key) {
      val superDefinitions = callable.findSuperDefinitions()
      val allNonDistinct = callable.annotations + superDefinitions.flatMap { allMatchingAnnotations(it) }
      allNonDistinct.distinct()
    }.filter(matcher)
  }

  private fun <R> KCallable<R>.findSuperDefinitions(): List<KCallable<R>> {
    val key = createKey()
    @Suppress("UNCHECKED_CAST")
    return superDefinitions.computeIfAbsent(key) {
      val classifier = instanceParameter?.type?.classifier
      when (classifier) {
        is KClass<*> -> {
          val superClasses = classifier.allSupertypes.map { it.classifier }.filterIsInstance<KClass<*>>()
          superClasses.flatMap { it.findDefinitions(this).toList() }
        }
        else -> emptyList()
      }
    } as List<KCallable<R>>
  }

  private fun <T : Any, R> KClass<T>.findDefinitions(callable: KCallable<R>): Sequence<KCallable<R>> {
    return members.asSequence()
      .filter { member ->
        val memberParameters = member.valueParameters
        val callableParameters = callable.valueParameters

        member.name == callable.name
          && memberParameters.map { it.createKey() }.zip(callableParameters.map { it.createKey() })
          .all { (p1, p2) -> p1 == p2 }
          && member.returnType == callable.returnType
      }
      .map {
        @Suppress("UNCHECKED_CAST")
        it as KCallable<R>
      }
  }

}


