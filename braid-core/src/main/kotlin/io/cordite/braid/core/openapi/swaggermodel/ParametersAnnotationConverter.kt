/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.swaggermodel

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.utils.EndPointUtilities
import io.swagger.v3.oas.annotations.Parameters
import io.swagger.v3.oas.models.parameters.Parameter

class ParametersAnnotationConverter(
  private val braidContext: BraidContext,
  private val endpointUtilities: EndPointUtilities
) {

  fun toModel(annotation: Parameters): List<Parameter> {
    val parameterConverter = ParameterAnnotationConverter(braidContext, endpointUtilities)
    return annotation.value.map { parameter ->
      parameterConverter.toModel(parameter, null, null)
    }
  }
}