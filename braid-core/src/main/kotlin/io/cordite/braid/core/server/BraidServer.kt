/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.server

import io.cordite.braid.core.async.mapUnit
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.logging.loggerFor
import io.vertx.core.Future
import io.vertx.core.Future.succeededFuture
import io.vertx.core.Promise
import io.vertx.core.Vertx

open class BraidServer(private val braidContext: BraidContext, private val config: BraidConfig) {
  companion object {

    private val log = loggerFor<BraidServer>()
  }

  private val vertx: Vertx get() = braidContext.vertx
  private val pDeployId = Promise.promise<String>()
  private val fDeployId = pDeployId.future()
  val deployId: String?
    get() = fDeployId.result()
  var stopped = false

  fun start(): Future<BraidServer> {
    if (deployId != null) {
      log.info("server already started; no action will be taken. have you called ${BraidServer::start} more than once?")
      return succeededFuture(this)
    }
    log.info("deploying server to vertx")
    vertx.deployVerticle(BraidVerticle(braidContext, config)) {
      if (it.failed()) {
        val msg = "failed to start braid server on ${config.port}"
        log.error(msg, it.cause())
        pDeployId.tryFail(RuntimeException(msg, it.cause()))
      } else {
        log.info("Braid server started successfully on ${config.port}. Deploy ID: ${it.result()}")
        pDeployId.tryComplete(it.result())
      }
    }
    return fDeployId.map { this }
  }

  fun whenReady(): Future<String> = fDeployId

  @Suppress("MemberVisibilityCanBePrivate")
  fun stop(): Future<Unit> {
    val fStop = if (deployId != null && !stopped) {
      stopped = true
      val promise = Promise.promise<Void>()
      vertx.undeploy(deployId, promise::handle)
      promise.future().mapUnit()
    } else {
      succeededFuture(Unit)
    }
    return fStop.setHandler {
      log.info("shutting down braid server on port: ${config.port}")
      try {
        braidContext.removeServerForPort(config.port)
      } catch (err: Throwable) {
        log.error("failed to remove myself from my braid context", err)
      }
    }
  }
}


