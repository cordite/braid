/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.http

import io.cordite.braid.core.context.BraidContext
import io.vertx.core.Future
import io.vertx.core.Promise.promise
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.*
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.net.URLEncoder
import javax.ws.rs.core.MediaType.*

val HttpClientResponse.failed: Boolean
  get() = (this.statusCode() / 100) != 2

fun HttpClientResponse.body(): Future<Buffer> {
  val result = promise<Buffer>()
  try {
    this.bodyHandler { buffer ->
      result.tryComplete(buffer)
    }
  } catch (err: Throwable) {
    result.tryFail(err)
  }
  return result.future()
}

fun HttpClientResponse.bodyAsString(): Future<String> {
  return body().map { it.toString() }
}

inline fun <reified T : Any> HttpClientResponse.bodyAsJsonObject(braidContext: BraidContext): Future<T> {
  return body().map {
    try {
      braidContext.decodeValue(it, T::class.java)
    } catch (err: Throwable) {
      val log = LoggerFactory.getLogger("io.cordite.braid.core.http")
      log.error("failed to decode as json to type ${T::class.qualifiedName}", err)
      throw err
    }
  }
}

inline fun <reified T : Any> HttpClientResponse.bodyAsJsonSet(braidContext: BraidContext): Future<Set<T>> {
  return bodyAsJsonList<T>(braidContext)
    .map { it.toSet() }
}

inline fun <reified T : Any> HttpClientResponse.bodyAsJsonList(braidContext: BraidContext): Future<List<T>> {
  val bodyFuture = body()
  return when (T::class) {
    String::class -> {
      @Suppress("UNCHECKED_CAST")
      bodyFuture.map { JsonArray(it).map { item -> item.toString() } } as Future<List<T>>
    }
    Int::class -> {
      @Suppress("UNCHECKED_CAST")
      bodyFuture.map { JsonArray(it).map { item -> item.toString().toInt() } } as Future<List<T>>
    }
    Double::class -> {
      @Suppress("UNCHECKED_CAST")
      bodyFuture.map { JsonArray(it).map { item -> item.toString().toDouble() } } as Future<List<T>>
    }
    Float::class -> {
      @Suppress("UNCHECKED_CAST")
      bodyFuture.map { JsonArray(it).map { item -> item.toString().toFloat() } } as Future<List<T>>
    }
    Boolean::class -> {
      @Suppress("UNCHECKED_CAST")
      bodyFuture.map { JsonArray(it).map { item -> item.toString().toBoolean() } } as Future<List<T>>
    }
    BigDecimal::class -> {
      @Suppress("UNCHECKED_CAST")
      bodyFuture.map { JsonArray(it).map { item -> item.toString().toBigDecimal() } } as Future<List<T>>
    }
    Byte::class -> {
      @Suppress("UNCHECKED_CAST")
      bodyFuture.map { JsonArray(it).map { item -> item.toString().toByte() } } as Future<List<T>>
    }
    ByteArray::class -> {
      @Suppress("UNCHECKED_CAST")
      bodyFuture.map { JsonArray(it).map { item -> item.toString().toByteArray() } } as Future<List<T>>
    }
    Long::class -> {
      @Suppress("UNCHECKED_CAST")
      bodyFuture.map { JsonArray(it).map { item -> item.toString().toLong() } } as Future<List<T>>
    }
    Short::class -> {
      @Suppress("UNCHECKED_CAST")
      bodyFuture.map { JsonArray(it).map { item -> item.toString().toShort() } } as Future<List<T>>
    }
    else -> {
      bodyFuture
        .map {
          val decoded = JsonArray(it).map { item ->
            val reEncoded = (item as JsonObject).encode()
            @Suppress("UNCHECKED_CAST")
            braidContext.decodeValue(reEncoded, T::class.java)
          }
          decoded
        }
    }
  }
}

fun HttpClient.futureGet(
  path: String,
  headers: Map<String, Any> = emptyMap(),
  queryParams: Map<String, String> = emptyMap()
): Future<HttpClientResponse> {
  val result = promise<HttpClientResponse>()
  @Suppress("DEPRECATION")
  get(path.appendQueryParams(queryParams))
    .appendHeaders(headers)
    .exceptionHandler { result.tryFail(it.cause) }
    .handler { result.tryComplete(it) }
    .end()
  return result.future()
}

fun HttpClient.futurePut(
  braidContext: BraidContext,
  headers: Map<String, Any> = emptyMap(),
  queryParams: Map<String, String> = emptyMap(),
  path: String
) = futurePut<Unit>(braidContext, headers, queryParams, null, path)

fun <T> HttpClient.futurePut(
  braidContext: BraidContext,
  headers: Map<String, Any> = emptyMap(),
  queryParams: Map<String, String> = emptyMap(),
  body: T?,
  path: String
): Future<HttpClientResponse> {
  val result = promise<HttpClientResponse>()
  @Suppress("DEPRECATION")
  put(path.appendQueryParams(queryParams))
    .appendHeaders(headers)
    .exceptionHandler { result.tryFail(it.cause) }
    .handler { result.tryComplete(it) }
    .apply {
      writeBody(braidContext, body)
    }
  return result.future()
}

fun HttpClientResponse.verifyError(code: Int): HttpClientResponse {
  this.request().path()
  val statusClass = this.statusCode() / 100
  if (statusClass == 2) {
    val simpleMessage =
      "${request().method()} ${request().path()} response did not fail with status code ${statusCode()}"
    if (statusMessage() == null || statusMessage().isBlank()) {
      error(simpleMessage)
    } else {
      error("$simpleMessage: ${statusMessage()}")
    }
  } else {
    check(this.statusCode() == code) { "response failed with status code ${statusCode()} rather than $code" }
  }
  return this
}

fun HttpClientResponse.verifyNoError(): HttpClientResponse {
  this.request().path()
  val statusClass = this.statusCode() / 100
  if (statusClass != 2) {
    val simpleMessage = "${request().method()} ${request().path()} response failed with status code ${statusCode()}"
    if (statusMessage() == null || statusMessage().isBlank()) {
      error(simpleMessage)
    } else {
      error("$simpleMessage: ${statusMessage()}")
    }
  }
  return this
}

fun  HttpClientRequest.writeBody(body: Buffer) {
  putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_OCTET_STREAM)
  putHeader(HttpHeaders.CONTENT_LENGTH, body.length().toString())
  end(body)
}

fun HttpClientRequest.writeBody(body: String) {
  putHeader(HttpHeaders.CONTENT_TYPE, TEXT_PLAIN)
  putHeader(HttpHeaders.CONTENT_LENGTH, body.toByteArray().size.toString())
  end(body)
}

fun <T> HttpClientRequest.writeBody(braidContext: BraidContext, body: T?) {
  when (body) {
    null -> end()
    is String -> writeBody(body)
    is Buffer -> writeBody(body)
    else -> {
      val bodyJson = braidContext.encode(body)
      putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
      putHeader(HttpHeaders.CONTENT_LENGTH, bodyJson.toByteArray().size.toString())
      end(bodyJson)
    }
  }
}

fun HttpClient.futurePost(
  braidContext: BraidContext,
  headers: Map<String, Any> = emptyMap(),
  queryParams: Map<String, String> = emptyMap(),
  path: String
) = futurePost<Unit>(braidContext, headers, queryParams, null, path)

fun <T> HttpClient.futurePost(
  braidContext: BraidContext,
  headers: Map<String, Any> = emptyMap(),
  queryParams: Map<String, String> = emptyMap(),
  body: T? = null,
  path: String
): Future<HttpClientResponse> {
  val result = promise<HttpClientResponse>()
  @Suppress("DEPRECATION")
  post(path.appendQueryParams(queryParams))
    .appendHeaders(headers)
    .exceptionHandler { result.tryFail(it.cause) }
    .handler { result.tryComplete(it) }
    .apply {
      writeBody(braidContext, body)
    }
  return result.future()
}

@Suppress("UNCHECKED_CAST")
fun HttpClientRequest.appendHeaders(headers: Map<String, Any>): HttpClientRequest {
  headers.forEach {(key, value) ->
    when (value) {
      is Iterable<*> -> {
        when (value.firstOrNull()) {
          null -> {
          }
          is CharSequence -> putHeader(key, value as Iterable<CharSequence>)
          is String -> putHeader(key, value as Iterable<String>)
          else -> putHeader(key, value.map { it.toString() })
        }
      }
      is String -> putHeader(key, value)
      else -> putHeader(key, value.toString())
    }
  }
  return this
}

fun String.appendQueryParams(queryParams: Map<String, String>): String {
  if (queryParams.isEmpty()) return this
  val queryString = queryParams.map { (key, value) ->
    "$key=${URLEncoder.encode(value, "UTF-8")}"
  }.joinToString("&")
  return when {
    endsWith('?') -> "$this$queryString"
    else -> "$this?$queryString"
  }
}

@Suppress("DEPRECATION")
fun HttpClient.fetch(
  url: String,
  braidContext: BraidContext,
  method: HttpMethod = HttpMethod.GET,
  headers: Map<String, Any> = emptyMap(),
  queryParams: Map<String, String> = emptyMap(),
  body: Any? = null
): Future<HttpClientResponse> {
  val result = promise<HttpClientResponse>()
  requestAbs(method, url.appendQueryParams(queryParams))
    .appendHeaders(headers)
    .exceptionHandler {
      result.tryFail(it.cause)
    }
    .handler {
      result.tryComplete(it)
    }
    .apply {
      this.writeBody(braidContext, body)
    }
  return result.future()
}

fun HttpClientRequest.getBodyAsString(): Future<String> = this.getBodyBuffer().map { it.toString() }
fun HttpClientRequest.getBodyBuffer(): Future<Buffer> = this.toFuture().getBodyBuffer()

fun HttpClientRequest.toFuture(): Future<HttpClientResponse> {
  val result = promise<HttpClientResponse>()
  handler {
    result.tryComplete(it)
  }
  exceptionHandler {
    result.tryFail(it)
  }
  end()
  return result.future()
}

fun Future<HttpClientResponse>.getBodyBuffer(): Future<Buffer> {
  return this.compose { response ->
    when (response.statusCode() / 100 != 2) {
      true -> Future.failedFuture<Buffer>("failed with status ${response.statusCode()} - ${response.statusMessage()}")
      else -> {
        val result = promise<Buffer>()
        response.bodyHandler {
          result.tryComplete(it)
        }
        result.future()
      }
    }
  }
}
