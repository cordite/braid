/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.http

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.http.HttpServerResponseLogHelper.Companion.log
import io.cordite.braid.core.logging.BraidLoggerInit
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.openapi.jaxrs.BraidWebApplicationException
import io.netty.buffer.ByteBuf
import io.netty.handler.codec.http.HttpHeaderValues
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpHeaders
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import java.lang.reflect.InvocationTargetException
import java.nio.ByteBuffer
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.MediaType.TEXT_PLAIN

class HttpServerResponseLogHelper {
  companion object {

    val log = loggerFor<HttpServerResponseLogHelper>()

    init {
      BraidLoggerInit.init()
    }
  }
}

fun <T> RoutingContext.endWithObject(braidContext: BraidContext, value: T) {
  when {
    value is Throwable -> endWithException(value)
    braidContext.isAsyncResultType(value) -> handleAsyncResult(braidContext, value)
    else -> writeObject(braidContext, value).response().end()
  }
}

fun RoutingContext.endWithException(error: Throwable) {
  log.error("http request failed ${request().method()} ${request().path()}", error)
  val e = if (error is InvocationTargetException) error.targetException else error
  val message = e.message ?: "Undefined error"
  val statusCode = if (e is BraidWebApplicationException) e.status.statusCode else 500
  val statusMessage = when {
    e is BraidWebApplicationException && e.message != null -> e.message!!.replace(Regex("[\\n|\\r]"), " ")
    else -> "failed"
  }
  this
    .endWithUtf8(text = message, statusCode = statusCode, statusMessage = statusMessage)
}

private fun <T> RoutingContext.handleAsyncResult(braidContext: BraidContext, value: T): RoutingContext {
  val asyncAdapter = braidContext.asyncResultAdapters.findAsyncResultAdapter(value)
  val subscriber = ResultToResponseSubscriber<Any>(braidContext, this, asyncAdapter.isSingleResult)
  val observable = asyncAdapter.adapt(-1, value)
  val subscription = observable.subscribe(subscriber)
  try {
    if (!this.response().ended()) {
      this.response().closeHandler {
        subscription.unsubscribe()
      }
    }
  } catch (ise: IllegalStateException) {
    // sometimes the response object may already be closed
    log.warn("Expected illegal state exception as object may be closed: ${ise.message}")
  }
  return this
}

fun RoutingContext.endWithUtf8(
  text: String,
  contentType: String = TEXT_PLAIN,
  statusCode: Int = HttpResponseStatus.OK.code(),
  statusMessage: String = ""
): RoutingContext {
  writeUtf8(text, contentType, statusCode, statusMessage).response().end()
  return this
}

private fun RoutingContext.writeJsonArray(value: JsonArray): RoutingContext {
  val payload = value.encode()
  return writeUtf8(payload, APPLICATION_JSON)
}

private fun RoutingContext.writeJsonObject(value: JsonObject): RoutingContext {
  val payload = value.encode()
  return writeUtf8(payload, APPLICATION_JSON)
}

fun RoutingContext.writeUtf8(
  text: String,
  contentType: String = TEXT_PLAIN,
  statusCode: Int = HttpResponseStatus.OK.code(),
  statusMessage: String = ""
): RoutingContext {
  val bytes = Buffer.buffer(text.toByteArray())
  this.response()
    .putHeader(HttpHeaders.CONTENT_LENGTH, bytes.length().toString())
    .putHeader(HttpHeaders.CONTENT_TYPE, "$contentType;charset=UTF-8")
    .setStatusCode(statusCode)
    .setStatusMessage(statusMessage)
    .write(bytes)
  return this
}

private fun <T> RoutingContext.writeJson(braidContext: BraidContext, value: T): RoutingContext {
  val payload = braidContext.encode(value)
  return writeUtf8(payload, APPLICATION_JSON)
}

private fun RoutingContext.writeBuffer(value: Buffer): RoutingContext {
  this.response()
    .putHeader(HttpHeaders.CONTENT_LENGTH, value.length().toString())
    .putHeader(HttpHeaders.CONTENT_TYPE, HttpHeaderValues.APPLICATION_OCTET_STREAM)
    .write(value)
  return this
}

private fun RoutingContext.writeByteBuf(value: ByteBuf): RoutingContext {
  return writeBuffer(Buffer.buffer(value))
}

private fun RoutingContext.writeByteArray(value: ByteArray): RoutingContext {
  writeBuffer(Buffer.buffer(value))
  return this
}

private fun RoutingContext.writeByteBuffer(value: ByteBuffer): RoutingContext {
  writeByteArray(value.array())
  return this
}

fun <T> RoutingContext.writeObject(braidContext: BraidContext, value: T): RoutingContext {
  return try {
    when {
      value is String -> writeUtf8(value)
      value is Buffer -> writeBuffer(value)
      value is ByteArray -> writeByteArray(value)
      value is ByteBuffer -> writeByteBuffer(value)
      value is ByteBuf -> writeByteBuf(value)
      value is JsonArray -> writeJsonArray(value)
      value is JsonObject -> writeJsonObject(value)
      braidContext.isAsyncResultType(value) -> error("attempt to write an asynchronous type: $value")
      else -> writeJson(braidContext, value)
    }
  } catch (e: Throwable) {
    log.error("Unexpected exception during request ${request().method()} ${request().path()}", e)
    // attempt last chance cleanup of the request
    if (!this.response().closed()) {
      try {
        this.response().end()
      } catch (err: Throwable) {
        //
      }
    }
    throw e
  }
}
