/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.jsonschema

import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.module.jsonSchema.JsonSchema
import com.fasterxml.jackson.module.jsonSchema.factories.SchemaFactoryWrapper
import com.fasterxml.jackson.module.jsonSchema.types.ObjectSchema
import io.cordite.braid.core.annotation.BraidMethod
import io.cordite.braid.core.annotation.BraidService
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.reflection.getAsyncResultType
import io.cordite.braid.core.service.AsyncResultAdapters
import io.cordite.braid.core.service.MethodDescriptor
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import rx.Observable
import java.lang.reflect.Constructor
import java.lang.reflect.Method
import kotlin.reflect.KCallable
import kotlin.reflect.full.valueParameters
import kotlin.reflect.jvm.javaType

fun <R> KCallable<R>.toDescriptor(
  braidContext: BraidContext,
  asyncResultAdapters: AsyncResultAdapters
): MethodDescriptor {
  val braidMethodAnnotation = braidContext.annotationsCache.findAnnotation(this, BraidMethod::class)
  val params = json {
    val m = valueParameters.map {
      val javaType = braidContext.mapper.constructType(it.type.javaType)
      it.name!! to javaType.toJavascriptType(braidContext)
    }.toMap()
    obj(m)
  }

  val returnDescription = when {
    braidMethodAnnotation != null && braidMethodAnnotation.returnType != Any::class ->
      braidMethodAnnotation.returnType.javaObjectType.toJavascriptType(braidContext)
    else -> {
      braidContext.mapper.constructType(this.returnType.javaType.getAsyncResultType(asyncResultAdapters))
        .toJavascriptType(braidContext)
    }
  }

  if (returnType == Observable::class.java) {
    returnDescription.put("x-json-rpc-streaming", true)
  }
  val description = braidMethodAnnotation?.value ?: ""
  return MethodDescriptor(name, description, params, returnDescription)

}

fun Method.toDescriptor(braidContext: BraidContext, asyncResultAdapters: AsyncResultAdapters): MethodDescriptor {
  val braidMethodAnnotation = braidContext.annotationsCache.findAnnotation(this, BraidMethod::class.java)
  val name = this.name
  val params = json { obj(parameters.map { it.name to it.type.toJavascriptType(braidContext) }.toMap()) }

  val returnDescription = when {
    braidMethodAnnotation != null && braidMethodAnnotation.returnType != Any::class ->
      braidMethodAnnotation.returnType.javaObjectType.toJavascriptType(braidContext)
    else -> {
      braidContext.mapper.constructType(genericReturnType.getAsyncResultType(asyncResultAdapters))
        .toJavascriptType(braidContext)
    }
  }

  if (returnType == Observable::class.java) {
    returnDescription.put("x-json-rpc-streaming", true)
  }
  val description = braidMethodAnnotation?.value ?: ""
  return MethodDescriptor(name, description, params, returnDescription)
}

fun <T : Any> Constructor<T>.toDescriptor(braidContext: BraidContext): MethodDescriptor {
  val annotation = braidContext.annotationsCache.findAnnotation(this, BraidService::class.java)
  val description = annotation?.description ?: ""
  val name = this.name
  val params = json { obj(parameters.map { it.name to it.type.toJavascriptType(braidContext) }.toMap()) }
  val returnDescription = json { obj("type" to this@toDescriptor.declaringClass.simpleName) }
  return MethodDescriptor(name = name, description = description, parameters = params, returnType = returnDescription)
}

fun Class<*>.toJavascriptType(braidContext: BraidContext) = describeClass(braidContext, this)
fun JavaType.toJavascriptType(braidContext: BraidContext) = describeJavaType(braidContext, this)

fun describeClassSimple(braidContext: BraidContext, clazz: Class<*>): Any {
  return if (clazz.isPrimitive || clazz == String::class.java) {
    describeClass(braidContext, clazz)
  } else if (clazz.isArray) {
    "array"
  } else {
    clazz.simpleName
  }
}

fun describeClass(braidContext: BraidContext, clazz: Class<*>): JsonObject {
  val mapper = braidContext.mapper
  val visitor = SchemaFactoryWrapper()
  mapper.acceptJsonFormatVisitor(clazz, visitor)
  return describe(braidContext, visitor.finalSchema())
}

fun describeJavaType(braidContext: BraidContext, type: JavaType): JsonObject {
  val mapper = braidContext.mapper
  val visitor = SchemaFactoryWrapper()
  mapper.acceptJsonFormatVisitor(type, visitor)
  return describe(braidContext, visitor.finalSchema())
}

private fun describe(braidContext: BraidContext, value: JsonSchema) = describeAsObject(braidContext, value)

private fun describeAsObject(braidContext: BraidContext, value: JsonSchema): JsonObject {
  return if (value is ObjectSchema) {
    describeProperties(braidContext, value)
  } else {
    @Suppress("UNCHECKED_CAST") val result =
      braidContext.mapper.convertValue(value, Map::class.java) as Map<String, Any>
    JsonObject(result)
  }
}

private fun describeProperties(braidContext: BraidContext, value: ObjectSchema): JsonObject {
  return json {
    obj {
      val jo = this
      value.properties.forEach {
        jo.put(it.key, describeAsObject(braidContext, it.value))
      }
    }
  }
}