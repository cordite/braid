/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.http

import io.vertx.core.http.HttpHeaders.ALLOW
import io.vertx.core.http.HttpHeaders.CONTENT_TYPE
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import org.slf4j.LoggerFactory

private val log = LoggerFactory.getLogger("io.cordite.braid.core.http")

fun Router.setupAllowAnyCORS() {
  route().handler {
    // allow all origins .. TODO: set this up with configuration
    val origin = it.request().getHeader("Origin")
    if (origin != null) {
      it.response().putHeader("Access-Control-Allow-Origin", origin)
      it.response().putHeader("Access-Control-Allow-Credentials", "true")
      it.response().putHeader(
        "Access-Control-Allow-Headers",
        "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
      )
    }
    it.next()
  }
}

fun Router.setupOptionsMethod() {
  options().handler {
    it.response()
      .putHeader(
        ALLOW,
        "GET, PUT, POST, OPTIONS, CONNECT, HEAD, DELETE, CONNECT, TRACE, PATCH"
      )
      .putHeader(CONTENT_TYPE, "text/*")
      .putHeader(CONTENT_TYPE, "application/*")
      .end()
  }
}

fun RoutingContext.withErrorHandler(callback: RoutingContext.() -> Unit) {
  try {
    this.callback()
  } catch (err: Throwable) {
    this.endWithException(err)
  }
}
