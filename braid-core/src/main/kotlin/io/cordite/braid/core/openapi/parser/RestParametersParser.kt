/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.parser

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.parser.ParseResult.Status.SKIP
import io.cordite.braid.core.openapi.parser.ParseResult.Status.SUCCEEDED
import io.cordite.braid.core.openapi.parser.parsers.*
import io.cordite.braid.core.reflection.description
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.RoutingContext
import javax.ws.rs.DefaultValue
import kotlin.reflect.KCallable
import kotlin.reflect.KParameter
import kotlin.reflect.full.valueParameters

class RestParametersParser(
  val braidContext: BraidContext
) {

  companion object {

    internal fun String.nonEmptyOrNull() = when {
      isEmpty() -> null
      else -> this
    }

    internal fun RoutingContext.description() = "${request().method()} ${request().path()}"
  }

  fun <R> parseArguments(
    braidContext: BraidContext,
    routingContext: RoutingContext,
    callable: KCallable<R>
  ): Array<Any?> {
    val chain = listOf(
      ::PathParameterParser,
      ::QueryParameterParser,
      ::ContextParameterParser,
      ::HeaderParameterParser,
      ::BodyParameterParser
    ).map { it(braidContext, routingContext) }
    return callable.valueParameters.mapIndexed { parameterIndex, parameter ->
      parseParameter(
        callable,
        parameter,
        parameterIndex,
        chain,
        routingContext
      )
    }.toTypedArray()
  }

  fun <R> parseParameter(
    fn: KCallable<R>,
    parameter: KParameter,
    parameterIndex: Int,
    chain: List<ParameterParser>,
    routingContext: RoutingContext
  ): Any? {
    val parseResult = chain.asSequence()
      .map { it.parse(fn, parameter, parameterIndex) }
      .firstOrNull { it.status != SKIP } ?: ParseResult.SKIP

    return when (parseResult.status) {
      SUCCEEDED -> parseResult.value
      else -> {
        error(
          parseResult.value?.toString()
            ?: "failed to find a parser for parameter '${parameter.name}' against request ${routingContext.description()}"
        )
      }
    }
  }

  fun <R> validateIsBindable(callable: KCallable<R>, webMethod: HttpMethod) {
    val allowedBodyParameters = when (webMethod) {
      HttpMethod.POST, HttpMethod.PUT, HttpMethod.DELETE -> 1
      else -> 0
    }

    val potentialBodyParameters = callable.valueParameters.filterIndexed { index, parameter ->
      when (parameter.type.classifier) {
        RoutingContext::class -> false
        else -> {
          val paramAnnotations = braidContext.webAnnotationExtractor.parameterAnnotations(callable, index)
          paramAnnotations.isEmpty()
        }
      }
    }

    val functionDescription = callable.description()
    check(potentialBodyParameters.size <= allowedBodyParameters) {
      "cannot bind '${functionDescription} because there are ${potentialBodyParameters.count()} parameter(s) " +
        "suitable for body binding and there can only be one for POST, PUT and DELETE methods, and zero otherwise '"
    }

    val withoutDefaults = callable.valueParameters.filter { parameter ->
      parameter.isOptional && !parameter.type.isMarkedNullable &&
        braidContext.webAnnotationExtractor.parameterAnnotation(callable, parameter, DefaultValue::class) == null
    }
    check(withoutDefaults.isEmpty()) {
      "optional parameters require @DefaultValue on method ${functionDescription}: " + withoutDefaults.map { it.name }
        .joinToString(", ")
    }
  }
}

