/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.context

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.cordite.braid.core.annotation.AnnotationsCache
import io.cordite.braid.core.async.mapUnit
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.json.*
import io.cordite.braid.core.logging.BraidLoggerInit
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.openapi.annotations.WebAnnotationsExtractor
import io.cordite.braid.core.openapi.swaggermodel.FullyQualifiedTypeNameResolver
import io.cordite.braid.core.openapi.swaggermodel.JacksonSerializerModelResolver
import io.cordite.braid.core.openapi.utils.EndPointUtilities.Companion.toRefSchema
import io.cordite.braid.core.reflection.getGenericParameterType
import io.cordite.braid.core.server.BraidServer
import io.cordite.braid.core.service.AsyncResultAdapters
import io.cordite.braid.core.service.ServiceNaming
import io.github.classgraph.ClassGraph
import io.github.classgraph.ScanResult
import io.netty.buffer.ByteBuf
import io.netty.buffer.ByteBufInputStream
import io.swagger.v3.core.converter.AnnotatedType
import io.swagger.v3.core.converter.ModelConverters
import io.swagger.v3.core.util.AnnotationsUtils
import io.swagger.v3.core.util.Json
import io.swagger.v3.core.util.PrimitiveType
import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.media.ArraySchema
import io.swagger.v3.oas.models.media.BinarySchema
import io.swagger.v3.oas.models.media.ComposedSchema
import io.swagger.v3.oas.models.media.Schema
import io.vertx.core.Future
import io.vertx.core.Future.succeededFuture
import io.vertx.core.Promise
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.json.DecodeException
import io.vertx.core.json.EncodeException
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.AuthProvider
import java.io.IOException
import java.io.InputStream
import java.lang.reflect.GenericArrayType
import java.lang.reflect.Parameter
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.nio.ByteBuffer
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.jvm.javaType
import kotlin.reflect.jvm.jvmErasure

open class BraidContext protected constructor(
    mapper: ObjectMapper = createObjectMapper(),
    modelConverters: ModelConverters = ModelConverters(),
    asyncResultAdapters: AsyncResultAdapters = AsyncResultAdapters.DEFAULT_ASYNC_RESULT_ADAPTERS,
    annotationsCache: AnnotationsCache = AnnotationsCache(),
    serviceNaming: ServiceNaming = ServiceNaming.create(annotationsCache),
    webAnnotationExtractor: WebAnnotationsExtractor = WebAnnotationsExtractor(annotationsCache),
    vertx: Vertx = Vertx.vertx(),
    authConstructor: ((Vertx) -> AuthProvider)? = null
) : BraidJsonContext,
    BraidAsyncResultContext,
    BraidAnnotationsContext,
    BraidNamingContext,
    BraidVertxContext,
    BraidWebProcessingContext,
    BraidServerCreatorContext,
    BraidAuthContext {

  companion object {

    private val log = loggerFor<BraidContext>()
    protected val allServers = ConcurrentHashMap<Int, Future<BraidServer>>()
    private val staticScanResult: ScanResult by lazy {
      log.debug("scanning classpath")
      try {
        ClassGraph().enableClassInfo().enableAnnotationInfo().scan()
      } finally {
        log.debug("classpath scanning done")
      }
    }

    init {
      BraidLoggerInit.init()
    }

    fun create(
        mapper: ObjectMapper = createObjectMapper(),
        modelConverters: ModelConverters = ModelConverters(),
        asyncResultAdapters: AsyncResultAdapters = AsyncResultAdapters.DEFAULT_ASYNC_RESULT_ADAPTERS,
        annotationsCache: AnnotationsCache = AnnotationsCache(),
        serviceNaming: ServiceNaming = ServiceNaming.create(annotationsCache),
        webAnnotationExtractor: WebAnnotationsExtractor = WebAnnotationsExtractor(annotationsCache),
        vertx: Vertx = Vertx.vertx(),
        authConstructor: ((Vertx) -> AuthProvider)? = null
    ): BraidContext = BraidContext(
        mapper,
        modelConverters,
        asyncResultAdapters,
        annotationsCache,
        serviceNaming,
        webAnnotationExtractor,
        vertx,
        authConstructor
    )

    /**
     * Used by [BraidServer] to remove itself after shutting down
     */
    internal fun removeServer(port: Int) {
      allServers.remove(port)
    }
  }

  private val serverPorts = mutableSetOf<Int>()

  protected val context: MutableMap<Class<*>, Any> = mutableMapOf()

  @Suppress("UNCHECKED_CAST")
  fun <T : Any> getContextObjectOrNull(objectClass: Class<out T>): T? {
    return when {
      objectClass.isInstance(this) -> this as T
      else -> context.values.firstOrNull { objectClass.isInstance(it) } as T?
    }
  }

  @Suppress("UNCHECKED_CAST")
  fun <T : Any> getContextObject(objectClass: Class<out T>): T {
    return getContextObjectOrNull(objectClass) ?: error("cannot find object that is instance of ${objectClass.name}")
  }

  inline fun <reified T : Any> getContextObject(): T {
    return getContextObject(T::class.java)
  }

  inline fun <reified T : Any> getContextObjectOrNull(): T? {
    return getContextObjectOrNull(T::class.java)
  }

  protected inline fun <reified T : Any> putContextObject(obj: T) {
    putContextObject(T::class.java, obj)
  }

  protected fun <T : Any> putContextObject(objClass: Class<out T>, obj: T) {
    context[objClass] = obj
  }

  fun bindParameters(parameters: Array<Parameter>): Array<Any> {
    return parameters.map { getContextObject<Any>(it.type) }.toTypedArray()
  }

  override val openapiComponents: Components get() = getContextObject()
  override val mapper: ObjectMapper get() = getContextObject()
  override val modelConverters: ModelConverters get() = getContextObject()
  override val annotationsCache: AnnotationsCache get() = getContextObject()
  override val asyncResultAdapters: AsyncResultAdapters get() = getContextObject()
  override val serviceNaming: ServiceNaming get() = getContextObject()
  override val vertx: Vertx get() = getContextObject()
  override val webAnnotationExtractor: WebAnnotationsExtractor get() = getContextObject()
  override val authProvider: AuthProvider? get() = getContextObjectOrNull()
  override val classScanResult: ScanResult get() = staticScanResult

  init {
    putContextObject(mapper)
    putContextObject(modelConverters)
    putContextObject(Components())
    putContextObject(asyncResultAdapters)
    putContextObject(annotationsCache)
    putContextObject(serviceNaming)
    putContextObject(webAnnotationExtractor)
    putContextObject(vertx)
    authConstructor?.let { putContextObject(AuthProvider::class.java, authConstructor.invoke(vertx)) }

    // Non-standard JSON but we allow C style comments in our JSON
    mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true)
    //
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)

    val module = SimpleModule()
    // custom types
    module.addSerializer(JsonObject::class.java, JsonObjectSerializer())
    module.addSerializer(JsonArray::class.java, JsonArraySerializer())
    module.addSerializer(ByteArray::class.java, ByteArraySerializer())
    module.addDeserializer(ByteArray::class.java, ByteArrayDeserializer())

    listOf(
        KotlinModule(),
        JavaTimeModule(),
        module,
        BraidJacksonModule()
    ).forEach {
      mapper.registerModule(it)
      Json.mapper().registerModule(it)
    }
    modelConverters.apply {
      addConverter(JacksonSerializerModelResolver(mapper, FullyQualifiedTypeNameResolver))
    }
  }

  /**
   * Called by [BraidServer]
   */
  internal fun removeServerForPort(port: Int) {
    serverPorts.remove(port)
    allServers.remove(port)
  }

  fun close(): Future<Unit> {
    val fClose = try {
      val promise = Promise.promise<Void>()
      vertx.close(promise::handle)
      promise.future().mapUnit().recover {
        succeededFuture(Unit)
      }
    } catch (err: Throwable) {
      // vertx sometimes complains about being unloaded with "Unknown deployment"
      succeededFuture(Unit)
    }
    return fClose
        .setHandler {
          serverPorts.toList().forEach { port -> removeServerForPort(port) }
        }
  }

  override fun encode(obj: Any?): String {
    return try {
      when (obj) {
        is JsonObject -> mapper.writeValueAsString(obj.map)
        is JsonArray -> mapper.writeValueAsString(obj.list)
        else -> mapper.writeValueAsString(obj)
      }
    } catch (e: Exception) {
      throw EncodeException("Failed to encode as JSON: " + e.message)
    }
  }

  @Throws(DecodeException::class)
  override fun <T> decodeValue(buffer: Buffer, clazz: Class<T>): T {
    return fromParser(createParser(buffer), clazz)
  }

  @Throws(DecodeException::class)
  override fun <T> decodeValue(str: String, clazz: Class<T>): T {
    return fromParser(createParser(str), clazz)
  }

  @Throws(DecodeException::class)
  override fun <T> decodeValue(str: String, javaType: JavaType): T {
    return fromParser(createParser(str), javaType)
  }

  @Throws(DecodeException::class)
  override fun <T : Any> decodeValue(str: String, kClass: KClass<T>): T {
    return fromParser(createParser(str), kClass.java)
  }

  @Throws(EncodeException::class)
  override fun encodeToBuffer(obj: Any?): Buffer {
    return toBuffer(obj)
  }

  @Throws(EncodeException::class)
  override fun toBuffer(obj: Any?): Buffer {
    return try {
      Buffer.buffer(mapper.writeValueAsBytes(obj))
    } catch (e: java.lang.Exception) {
      throw EncodeException("Failed to encode as JSON: " + e.message)
    }
  }

  fun <T> isAsyncResultType(obj: T): Boolean {
    if (obj == null) return false
    return asyncResultAdapters.isAsyncResultType((obj as Any).javaClass)
  }

  override fun createServer(braidConfig: BraidConfig): Future<BraidServer> {
    return allServers.compute(braidConfig.port) { port, fServer ->
      if (fServer == null) {
        serverPorts.add(port)
        internalCreateServer(braidConfig)
      } else {
        log.warn("server for port $port already started")
        fServer
      }
    }!!
  }

  protected open fun internalCreateServer(braidConfig: BraidConfig): Future<BraidServer> {
    val braidServer = BraidServer(this, braidConfig)
    return braidServer.start()
  }

  private fun createParser(buffer: Buffer): JsonParser {
    return try {
      mapper.factory.createParser(ByteBufInputStream(buffer.byteBuf) as InputStream)
    } catch (e: IOException) {
      throw DecodeException("Failed to decode:" + e.message, e)
    }
  }

  private fun createParser(str: String): JsonParser {
    return try {
      mapper.factory.createParser(str)
    } catch (e: IOException) {
      throw DecodeException("Failed to decode:" + e.message, e)
    }
  }

  @Throws(DecodeException::class)
  private fun <T> fromParser(parser: JsonParser, type: Class<T>): T {
    var value = try {
      mapper.readValue(parser, type)
    } catch (e: java.lang.Exception) {
      throw DecodeException("Failed to decode:" + e.message, e)
    } finally {
      parser.silentClose()
    }
    @Suppress("UNCHECKED_CAST")
    if (type == Any::class.java) {
      value = adapt(value as Any) as T
    }
    return value
  }

  @Throws(DecodeException::class)
  private fun <T> fromParser(parser: JsonParser, javaType: JavaType): T {
    var value: T = try {
      mapper.readValue(parser, javaType)
    } catch (e: java.lang.Exception) {
      throw DecodeException("Failed to decode:" + e.message, e)
    } finally {
      parser.silentClose()
    }
    @Suppress("UNCHECKED_CAST")
    if (javaType.rawClass == Any::class.java) {
      value = adapt(value as Any) as T
    }
    return value
  }

  private fun JsonParser.silentClose() {
    try {
      close()
    } catch (ignore: IOException) {
    }
  }

  private fun adapt(o: Any): Any {
    return try {
      if (o is List<*>) {
        return JsonArray(o)
      } else if (o is Map<*, *>) {
        @Suppress("UNCHECKED_CAST") val map = o as Map<String, Any>
        return JsonObject(map)
      }
      o
    } catch (e: java.lang.Exception) {
      throw DecodeException("Failed to decode: " + e.message)
    }
  }

  fun getOpenAPISchema(type: KType): Schema<*>? {
    return getOpenAPISchema(type.javaType)?.apply {
      nullable = type.isMarkedNullable
    }
  }

  fun getOpenAPISchema(type: Type): Schema<*>? {
    val actualType = typeWithoutAsyncWrapper(type)

    return when {
      isBinary(actualType) -> BinarySchema()
      isArrayLike(actualType) -> {
        val componentType = getArrayLikeItemType(actualType)
        val componentSchemaOrRef = getOpenAPISchemaReference(componentType)
        ArraySchema().items(componentSchemaOrRef)
      }
      else -> {
        val primitiveSchema = PrimitiveType.fromType(actualType)
        when {
          primitiveSchema != null -> primitiveSchema.createProperty()
          else -> getComplexOpenAPISchema(actualType)
        }
      }
    }
  }

  fun getOpenAPISchemaReference(type: KType): Schema<*>? {
    val schema = getOpenAPISchema(type) ?: return null
    return when {
      schema.`$ref`.isNullOrBlank() &&
        schema.type == "object" &&
        schema.additionalProperties == null -> {
        schema.toRefSchema()
      }
      else -> {
        schema
      }
    }
  }

  private fun getComplexOpenAPISchema(complexType: Type): Schema<*>? {
    val resolvedSchema = modelConverters.readAllAsResolvedSchema(AnnotatedType(complexType).schemaProperty(true))
    resolvedSchema?.referencedSchemas?.forEach { key, schema -> addSchema(key, schema) }
    return resolvedSchema?.schema
  }

  private fun addSchema(key: String, schema: Schema<*>) {
    if (schema is ComposedSchema) {
      fixComposedSchemaBugs(schema)
    }
    openapiComponents.addSchemas(key, schema)
  }

  private fun fixComposedSchemaBugs(schema: ComposedSchema) {
    // correct swagger bug for allOf, anyOf, oneOf
    schema.anyOf?.forEach { fixSchemaRef(it) }
    schema.allOf?.forEach { fixSchemaRef(it) }
    schema.oneOf?.forEach { fixSchemaRef(it) }
  }

  private fun fixSchemaRef(schema: Schema<*>?) {
    if (schema == null || schema.`$ref` == null) return
    if (!schema.`$ref`.startsWith(AnnotationsUtils.COMPONENTS_REF)) {
      schema.`$ref` = AnnotationsUtils.COMPONENTS_REF + schema.`$ref`
    }
  }

  fun typeWithoutAsyncWrapper(type: KType): KType {
    return if (asyncResultAdapters.isAsyncResultType(type.jvmErasure.java)) {
      type.arguments.last().type!!
    } else {
      type
    }
  }

  fun typeWithoutAsyncWrapper(type: Type): Type {
    return when (type) {
      is ParameterizedType -> {
        when {
          asyncResultAdapters.isAsyncResultType(type.rawType as Class<*>) -> type.actualTypeArguments[0]
          else -> type
        }
      }
      else -> type
    }
  }

  private val binaryTypes =
      listOf(Buffer::class.java, ByteArray::class.java, ByteBuffer::class.java, ByteBuf::class.java)

  fun isBinary(type: Type): Boolean {
    return when (type) {
      is Class<*> -> {
        binaryTypes.any {
          it.isAssignableFrom(type)
        }
      }
      else -> false
    }
  }

  fun isArrayLike(type: Type): Boolean {
    return when {
      (type is ParameterizedType) && (type.rawType is Class<*>) -> Collection::class.java.isAssignableFrom(type.rawType as Class<*>)
      type is Class<*> -> type.isArray || Collection::class.java.isAssignableFrom(type)
      (type is GenericArrayType) -> true
      else -> false
    }
  }

  fun getArrayLikeItemType(type: Type): Type {
    return when {
      (type is GenericArrayType) -> type.genericComponentType
      (type is ParameterizedType) && (type.rawType is Class<*>) -> type.getGenericParameterType(0)
      type is Class<*> -> {
        if (type.isArray) {
          type.componentType
        } else {
          type
        }
      }
      else -> type
    }
  }

  fun getOpenAPISchemaReference(type: Type): Schema<*>? {
    val schema = getOpenAPISchema(type) ?: return null
    return when {
      schema.`$ref`.isNullOrBlank() && schema.type == "object" -> schema.toRefSchema()
      else -> schema
    }
  }
}

private fun createObjectMapper(): ObjectMapper {
  return ObjectMapper()
}
