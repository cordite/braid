/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.jaxrs

import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.StatusType

/**
 * This replaces [javax.ws.rs.WebApplicationException] until braid provides a full implementation of a JAXRS runtime provider
 */
class BraidWebApplicationException(
  message: String?,
  cause: Throwable?,
  val status: StatusType = Response.Status.INTERNAL_SERVER_ERROR
) :
  Throwable(message, cause) {

  constructor(message: String?, status: StatusType) : this(message, null, status)
}
