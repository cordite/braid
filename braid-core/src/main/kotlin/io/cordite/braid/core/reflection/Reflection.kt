/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.reflection

import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.type.ArrayType
import com.fasterxml.jackson.databind.type.CollectionType
import com.fasterxml.jackson.databind.type.SimpleType
import com.fasterxml.jackson.databind.type.TypeFactory
import io.cordite.braid.core.service.AsyncResultAdapters
import io.vertx.core.Future
import rx.Observable
import java.lang.reflect.GenericArrayType
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import kotlin.reflect.KCallable
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.KType
import kotlin.reflect.full.instanceParameter
import kotlin.reflect.full.valueParameters
import kotlin.reflect.jvm.javaMethod
import kotlin.reflect.jvm.javaType

/**
 * TODO: find some appropriate mechanism to avoid this global
 */
val asyncTypes = mutableSetOf(Future::class.java, Observable::class.java)

fun Method.getAsyncResultType(asyncResultAdapters: AsyncResultAdapters): Type {
  return genericReturnType.getAsyncResultType(asyncResultAdapters)
}

fun Type.isStreaming() = this.actualType() == Observable::class.java

/**
 * If this is an Observable<*> or a Future<*> this will return
 * Observable or Future (as _this_ will actually be a ParametrizedType.
 *
 * An Int will just return itself.
 */
fun Type.actualType() = (this as? ParameterizedType)?.rawType ?: this

/**
 * For ParametrizedTypes, this returns the underlying parameter.
 *
 * E.g. Observerble<Int> would return Int
 *
 * The slight subtlety here is that anything with underlying types is passed around
 * as a ParametrizedType.
 */
fun Type.getAsyncResultType(asyncResultAdapters: AsyncResultAdapters): Type {
  return when (this) {
    is ParameterizedType -> {
      when {
        this.isAsyncResponse(asyncResultAdapters) -> getGenericParameterType(0)
        else -> this
      }
    }
    else -> this
  }
}

fun Type.isAsyncResponse(asyncResultAdapters: AsyncResultAdapters): Boolean {
  return when (this) {
    is Class<*> -> asyncResultAdapters.isAsyncResultType(this)
    is ParameterizedType -> rawType.isAsyncResponse(asyncResultAdapters)
    else -> false
  }
}

fun Type.getGenericParameterType(index: Int): Type {
  return (this as ParameterizedType).actualTypeArguments[index]
}

fun <Response> KCallable<Response>.javaMethod(): Method? {
  return when {
    this is KFunction<Response> && javaMethod != null -> javaMethod
    this.instanceParameter != null -> {
      val clazz = (this.instanceParameter!!.type.javaType as Class<*>?) ?: return null
      val params = this.valueParameters.map { TypeFactory.rawClass(it.type.javaType) }.toTypedArray()
      try {
        clazz.getMethod(this.name, *params)
      } catch (err: Throwable) {
        null
      }
    }
    else -> null
  }
}

fun <Response> KCallable<Response>.description(): String {
  val javaMethod = javaMethod()
  return when {
    javaMethod != null -> {
      "${javaMethod.declaringClass.name}#${javaMethod.name}"
    }
    else -> {
      this.name
    }
  }
}

fun Type.isArrayLike(): Boolean {
  return when (this) {
    is Class<*> -> Collection::class.java.isAssignableFrom(this) || this.isArray
    is ParameterizedType -> this.rawType.isArrayLike()
    is GenericArrayType -> true
    is ArrayType -> true
    is CollectionType -> true
    else -> false
  }
}

fun Type.isJsonPrimitive(): Boolean {
  return when {
    isArrayLike() -> false
    this is Class<*> -> {
      when {
        Number::class.java.isAssignableFrom(this) ||
          String::class.java.isAssignableFrom(this) ||
          Boolean::class.java.isAssignableFrom(this) -> true
        else -> false
      }
    }
    this is SimpleType -> rawClass?.isJsonPrimitive() ?: false
    else -> false
  }
}

fun Type.getArrayItemType(): Type {
  if (!this.isArrayLike()) return this
  return when (this) {
    is Class<*> -> {
      this.componentType
    }
    is ParameterizedType -> {
      this.actualTypeArguments[0]
    }
    is GenericArrayType -> {
      this.genericComponentType
    }
    else -> error("unhandled array type $this")
  }
}

fun KType.isArrayLike(): Boolean {
  return javaType.isArrayLike()
}

fun KParameter.isArrayLike(): Boolean {
  return type.isArrayLike()
}

fun KParameter.isJsonPrimitive(): Boolean {
  return type.javaType.isJsonPrimitive()
}

fun KParameter.javaType(): Type {
  return type.javaType
}

fun KParameter.createJacksonJavaType(mapper: ObjectMapper): JavaType {
  return mapper.typeFactory.constructType(javaType())
}