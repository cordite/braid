/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi

import io.cordite.braid.core.annotation.BraidMethod
import io.cordite.braid.core.constants.BraidConstants.BRAID_AUTHORIZATION_SCHEME_NAME
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.parser.RestParametersParser.Companion.nonEmptyOrNull
import io.cordite.braid.core.openapi.parser.parsers.BodyParameterParser
import io.cordite.braid.core.openapi.swaggermodel.ParameterAnnotationConverter
import io.cordite.braid.core.openapi.swaggermodel.ParametersAnnotationConverter
import io.cordite.braid.core.openapi.utils.EndPointUtilities
import io.swagger.v3.oas.models.*
import io.swagger.v3.oas.models.parameters.*
import io.swagger.v3.oas.models.responses.ApiResponses
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpMethod.*
import java.lang.reflect.Type
import javax.ws.rs.HeaderParam
import javax.ws.rs.PathParam
import javax.ws.rs.QueryParam
import kotlin.reflect.KCallable
import kotlin.reflect.KParameter
import kotlin.reflect.KType
import kotlin.reflect.full.valueParameters
import kotlin.reflect.jvm.javaType
import io.swagger.v3.oas.annotations.Operation as OperationAnnotation
import io.swagger.v3.oas.annotations.Parameter as ParameterAnnotation
import io.swagger.v3.oas.annotations.Parameters as ParametersAnnotation
import io.swagger.v3.oas.annotations.extensions.Extension as ExtensionAnnotation
import io.swagger.v3.oas.annotations.parameters.RequestBody as RequestBodyAnnotation
import io.swagger.v3.oas.annotations.responses.ApiResponse as ApiResponseAnnotation
import io.swagger.v3.oas.annotations.security.SecurityRequirement as SecurityRequirementAnnotation
import io.swagger.v3.oas.annotations.servers.Server as ServerAnnotation
import io.swagger.v3.oas.annotations.tags.Tag as TagAnnotation

open class EndPoint(
  val braidContext: BraidContext,
  private val groupName: String,
  val protected: Boolean,
  val method: HttpMethod,
  val path: String,
  val endPointUtilities: EndPointUtilities,
  val handler: KCallable<*>
) {

  companion object {

    fun create(
      braidContext: BraidContext,
      groupName: String,
      protected: Boolean,
      method: HttpMethod,
      path: String,
      endPointUtils: EndPointUtilities,
      handler: KCallable<*>
    ): EndPoint {
      return EndPoint(braidContext, groupName, protected, method, path, endPointUtils, handler)
    }
  }

  //region public api

  /**
   * if this endpoint is not hidden, add it as an operaton to the [openAPI] object
   */
  open fun addToOpenApi(openAPI: OpenAPI): Operation? {
    val operation = createOperationOrNull(openAPI, endPointUtilities) ?: return null
    addOperationToOpenApi(openAPI, operation)
    return operation
  }

  //endregion

  //region reflection properties

  protected open val annotations: List<Annotation> = braidContext.annotationsCache.allMatchingAnnotations(handler)

  protected val returnType: KType by lazy { handler.returnType }

  protected val parameters: List<KParameter> by lazy { handler.valueParameters }

  @Suppress("MemberVisibilityCanBePrivate")
  protected val headerParameters: List<KParameter> by lazy {
    parameters.filter { parameter ->
      endPointUtilities.getParameterAnnotation(handler, parameter, HeaderParam::class) != null
    }
  }

  @Suppress("MemberVisibilityCanBePrivate")
  protected val queryParameters: List<KParameter> by lazy {
    parameters.filter { parameter ->
      endPointUtilities.getParameterAnnotation(handler, parameter, QueryParam::class) != null
    }
  }

  protected val pathParameters: List<KParameter> by lazy {
    parameters.filter { parameter ->
      endPointUtilities.getParameterAnnotation(handler, parameter, PathParam::class) != null
    }
  }

  protected val bodyParameter: KParameter? by lazy {
    parameters.filterIndexed { index, _ ->
      braidContext.annotationsCache.allMatchingAnnotations(handler, index) {
        BodyParameterParser.ignoredParamAnnotations.contains(it.annotationClass)
      }.isEmpty()
    }.lastOrNull()
  }

  protected open val parameterTypes: List<Type> by lazy {
    handler.parameters.map { it.type.javaType }
  }

  protected open val braidMethodAnnotation: BraidMethod? by lazy {
    annotations.filterIsInstance<BraidMethod>().firstOrNull()
  }

  protected open val apiOperationAnnotation by lazy {
    annotations.filterIsInstance<OperationAnnotation>().firstOrNull()
  }

  protected open val securityRequirementAnnotations by lazy {
    annotations.filterIsInstance<SecurityRequirementAnnotation>()
  }

  protected open val apiResponseAnnotations by lazy {
    annotations.filterIsInstance<ApiResponseAnnotation>()
  }

  protected open val description: String
    get() {
      return braidMethodAnnotation?.description?.nonEmptyOrNull()
        ?: apiOperationAnnotation?.description?.nonEmptyOrNull()
        ?: ""
    }

  //endregion

  /**
   * Returns an Operation, converted from the OpenAPI annotations, and if these aren't present, a simple base Operation
   * that the caller can populate with type information collected from the function being bound.
   * @return null if this end point should be skipped because [OperationAnnotation.hidden] is set
   */
  protected open fun createOperationOrNull(openAPI: OpenAPI, endPointUtilities: EndPointUtilities): Operation? {
    // just to make matters more complicated
    // swagger annotations can be placed either within a @Operation annotation (renamed here as OperationAnnotation to
    // avoid the conflict with Operation in swagger model.
    // AND/OR they can be placed as direction annotations on the method / parameters
    // Thank you, Swagger

    val openApiAnnotation = annotations.filterIsInstance<OperationAnnotation>().firstOrNull()
    if (openApiAnnotation != null && openApiAnnotation.hidden) return null

    val operation = Operation()

    // definition from reading the raw type information on the method
    applyFromTypeInformation(operation, openAPI, endPointUtilities)

    // overlay any annotations at the method level apply these to the operation
    applyFromSwaggerAnnotations(operation, openAPI, endPointUtilities)

    // if we have an @Operation annotation, populate the initial Operation model from that
    val operationFromAnnotation = openApiAnnotation?.let { endPointUtilities.toModel(openApiAnnotation, handler) }
    if (operationFromAnnotation != null)
    endPointUtilities.fill(operation, operationFromAnnotation)

    // and tag which group the operation belongs to, if it's already not been specified
    if (operation.tags == null || operation.tags.isEmpty()) {
      operation.addTagsItem(groupName)
    }

    // we're done
    return operation
  }

  private fun applyFromTypeInformation(operation: Operation, openAPI: OpenAPI, endPointUtilities: EndPointUtilities) {
    // overlay from reflection of the handler parameters
    applyParameterTypeInformation(operation, openAPI, endPointUtilities)

    // result type information
    applyResultTypeInformation(operation, openAPI, endPointUtilities)
  }

  protected open fun applyFromSwaggerAnnotations(
    operation: Operation,
    openAPI: OpenAPI,
    endPointUtilities: EndPointUtilities
  ) {
    addMethodLevelServerAnnotations(operation, endPointUtilities)
    addMethodLevelParametersAnnotations(operation, endPointUtilities)
    addParameterLevelAnnotations(operation, endPointUtilities)
    addRequestBodyAnnotations(operation, endPointUtilities)
    addMethodLevelTagAnnotations(operation, openAPI, endPointUtilities)
    addMethodLevelExtensionAnnotations(operation, endPointUtilities)
    addMethodLevelSecurityAnnotations(operation, endPointUtilities)
    operation.description = extractDescription()
    operation.summary = extractSummary()
    operation.operationId = extractOperationId()
  }


  protected open fun extractSummary(): String? {
    return braidMethodAnnotation?.value?.nonEmptyOrNull()
      ?: apiOperationAnnotation?.summary?.nonEmptyOrNull()
      ?: ""
  }

  protected open fun extractDescription(): String {
    return braidMethodAnnotation?.description?.nonEmptyOrNull()
      ?: apiOperationAnnotation?.description?.nonEmptyOrNull()
      ?: ""
  }

  protected open fun extractOperationId(): String {
    return method.name.toLowerCase() + path.split("/").filter { it.isNotBlank() }.map { it.capitalize() }
  }

  protected open fun extractRequestBodyFromTypeInfo(endPointUtilities: EndPointUtilities): RequestBody? {
    val bp = bodyParameter ?: return null
    return RequestBody().apply {
      this.content = endPointUtilities.getContent(bp.type)
      this.required = !bp.isOptional
    }
  }

  protected open fun requestBodyFromAnnotation(): RequestBody? {
    val bp = bodyParameter ?: return null
    val rbAnnotation = endPointUtilities.getParameterAnnotation(handler, bp, RequestBodyAnnotation::class) ?: return null
    return endPointUtilities.toModel(rbAnnotation, bp)
  }

  protected open fun mapHeaderParameters(endPointUtilities: EndPointUtilities): List<HeaderParameter> {
    return headerParameters.map { parameter ->
      val annotation = endPointUtilities.getParameterAnnotation(handler, parameter, HeaderParam::class)!!
      val schemaRef = braidContext.getOpenAPISchemaReference(parameter.type)

      HeaderParameter().apply {
        style = Parameter.StyleEnum.SIMPLE
        schema = schemaRef
        name = annotation.value
        required = parameter.isOptional
      }
    }
  }

  protected open fun mapParameters(endPointUtilities: EndPointUtilities): List<Parameter> {
    val pathParams = mapPathParameters(endPointUtilities)
    val queryParams = mapQueryParameters(endPointUtilities)
    val headerParams = mapHeaderParameters(endPointUtilities)
    return pathParams + queryParams + headerParams
  }

  protected open fun mapQueryParameters(endPointUtilities: EndPointUtilities): List<QueryParameter> {
    return queryParameters.map { parameter ->
      val annotation = endPointUtilities.getParameterAnnotation(handler, parameter, QueryParam::class)!!
      val schemaRef = braidContext.getOpenAPISchemaReference(parameter.type)

      QueryParameter().apply {
        schema = schemaRef
        name = annotation.value
        required = parameter.isOptional
      }
    }
  }

  protected open fun mapPathParameters(endPointUtilities: EndPointUtilities): List<PathParameter> {
    return pathParameters.map { parameter ->
      val annotation = endPointUtilities.getParameterAnnotation(handler, parameter, PathParam::class)!!
      val schemaRef = braidContext.getOpenAPISchemaReference(parameter.type)

      PathParameter().apply {
        schema = schemaRef
        name = annotation.value
      }
    }
  }

  protected open fun applyResultTypeInformation(
    operation: Operation,
    openAPI: OpenAPI,
    endPointUtilities: EndPointUtilities
  ) {
    val reflectedResponse = endPointUtilities.createResponseOrEmpty(returnType)
    val existingResponse = operation.responses()["200"]
    if (existingResponse == null) {
      operation.responses().addApiResponse("200", reflectedResponse)
    } else {
      endPointUtilities.fill(existingResponse, reflectedResponse)
    }
  }

  protected open fun applyParameterTypeInformation(
    operation: Operation,
    openAPI: OpenAPI,
    endPointUtilities: EndPointUtilities
  ) {
    val parameters = mapParameters(endPointUtilities)
    parameters.forEachIndexed { index, parameter ->
      // now we attempt to fill the missing parts that were extracted from the annotations
      val operationParameter = operation.parameterAtIndexOrNull(index)
      if (operationParameter == null) {
        operation.addParametersItem(parameter)
      } else {
        endPointUtilities.fill(operationParameter, parameter)
      }
    }
  }


  private fun addMethodLevelSecurityAnnotations(operation: Operation, endPointUtilities: EndPointUtilities) {
    val result = endPointUtilities.toModel(securityRequirementAnnotations)

    val security = if ((result == null || result.isEmpty()) && protected) {
      listOf(SecurityRequirement().apply {
        this.addList(BRAID_AUTHORIZATION_SCHEME_NAME)
      })
    } else {
      result
    }
    operation.security = security
  }

  protected open fun addMethodLevelServerAnnotations(operation: Operation, endPointUtilities: EndPointUtilities) {
    annotations
      .filterIsInstance<ServerAnnotation>()
      .mapNotNull { endPointUtilities.toModel(it) }
      .forEach { server ->
        val operationServer = operation.servers.firstOrNull { it.url == server.url }
        if (operationServer == null) {
          operation.addServersItem(server)
        } else {
          endPointUtilities.fill(operationServer, server)
        }
      }
  }

  protected open fun addMethodLevelParametersAnnotations(operation: Operation, endPointUtilities: EndPointUtilities) {
    val converter = ParametersAnnotationConverter(braidContext, endPointUtilities)
    val parametersAnnotation = annotations.filterIsInstance<ParametersAnnotation>().firstOrNull()
    if (parametersAnnotation != null) {
      val parameters = converter.toModel(parametersAnnotation)
      if (operation.parameters == null) {
        operation.parameters = parameters.toMutableList()
      } else {
        operation.parameters.addAll(parameters)
      }
    }
  }

  protected open fun addParameterLevelAnnotations(operation: Operation, endPointUtilities: EndPointUtilities) {
    val converter = ParameterAnnotationConverter(braidContext, endPointUtilities)
    parameters.mapIndexed { index, parameter ->
      val parameterAnnotation = endPointUtilities.getParameterAnnotation(handler, parameter, ParameterAnnotation::class)
      if (parameterAnnotation != null) {
        val jaxParamAnnotation = endPointUtilities.getJaxRSParameterAnnotation(handler, parameter)
        val parameterModel = converter.toModel(parameterAnnotation, parameter, jaxParamAnnotation)
        val existingParameter = operation.parameterAtIndexOrNull(index)
        if (existingParameter == null) {
          operation.addParametersItem(parameterModel)
        } else {
          endPointUtilities.fill(existingParameter, parameterModel)
        }
      }
    }
  }

  protected open fun addRequestBodyAnnotations(operation: Operation, endPointUtilities: EndPointUtilities) {
    operation.requestBody = requestBodyFromAnnotation()
    val extracted = extractRequestBodyFromTypeInfo(endPointUtilities)
    if (operation.requestBody != null && extracted != null) {
      endPointUtilities.fill(operation.requestBody, extracted)
    } else {
      operation.requestBody = extracted
    }
  }

  protected open fun addMethodLevelTagAnnotations(
    operation: Operation,
    openAPI: OpenAPI,
    endPointUtilities: EndPointUtilities
  ) {
    annotations.filterIsInstance<TagAnnotation>().map { endPointUtilities.toModel(it) }.forEach { tag ->
      operation.addTagsItem(tag.name)
      openAPI.addTagsItem(tag)
    }
  }

  protected open fun addMethodLevelExtensionAnnotations(operation: Operation, endPointUtilities: EndPointUtilities) {
    annotations.filterIsInstance<ExtensionAnnotation>().toTypedArray()
      .let { endPointUtilities.toModel(it) }
      ?.forEach { (name, extension) ->
        if (!operation.extensions.contains(name)) {
          operation.addExtension(name, extension)
        }
      }
  }

  protected open fun addOperationToOpenApi(
    openAPI: OpenAPI,
    operation: Operation
  ) {
    val pathItem = openAPI.pathItem()
    when (method) {
      PUT -> pathItem.put = operation
      POST -> pathItem.post = operation
      GET -> pathItem.get = operation
      DELETE -> pathItem.delete = operation
      OPTIONS -> pathItem.options = operation
      HEAD -> pathItem.head = operation
      PATCH -> pathItem.patch = operation
      TRACE -> pathItem.trace = operation
      CONNECT -> error("method $CONNECT is not supported")
      OTHER -> error("method $OTHER is not supported")
    }

  }

  private fun Operation.parameterAtIndexOrNull(index: Int): Parameter? {
    return when {
      parameters != null && parameters.size > index -> parameters[index]
      else -> null
    }
  }

  open fun OpenAPI.pathItem(): PathItem {
    if (paths == null) {
      paths = Paths()
    }
    return paths.computeIfAbsent(path) { PathItem() }
  }

  open fun Operation.responses(): ApiResponses {
    if (responses == null) {
      responses = ApiResponses()
    }
    return responses
  }
}