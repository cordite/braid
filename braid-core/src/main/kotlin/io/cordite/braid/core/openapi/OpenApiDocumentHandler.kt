/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi

import io.cordite.braid.core.constants.BraidConstants.BRAID_AUTHORIZATION_SCHEME_NAME
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.http.endWithException
import io.cordite.braid.core.http.endWithUtf8
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.openapi.utils.EndPointUtilities
import io.swagger.v3.core.util.Json
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.security.SecurityScheme
import io.swagger.v3.oas.models.servers.Server
import io.vertx.core.Handler
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.RoutingContext
import java.net.URL
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import kotlin.reflect.KCallable

class OpenApiDocumentHandler(
  private val braidContext: BraidContext,
  private val title: String = "",
  private val version: String = "",
  private val description: String = "",
  private val baseURL: URL = URL("http://localhost:8080"),
  private val contact: Contact = Contact().name("").email("").url(""),
  private val auth: SecurityScheme? = null,
  private val debugMode: Boolean = false
) : Handler<RoutingContext> {

  companion object {

    private val log = loggerFor<OpenApiDocumentHandler>()
  }

  private var currentGroupName: String = ""
  private val endpoints = mutableListOf<EndPoint>()
  private val endpointUtilities = EndPointUtilities(braidContext)
  private val openApi: OpenAPI by lazy { createOpenApiObject() }

  override fun handle(context: RoutingContext) {
    try {
      val requestURI = URL(context.request().absoluteURI())
      val modifiedURL = URL(requestURI.protocol, requestURI.host, requestURI.port, baseURL.path).toString()
      val openApiToBeSerialized = if (debugMode) createOpenApiObject() else openApi
      openApiToBeSerialized.servers.forEach {
        it.url = modifiedURL
      }
      val output = Json.pretty().writeValueAsString(openApiToBeSerialized)
      context.endWithUtf8(output, APPLICATION_JSON)
    } catch (err: Throwable) {
      log.error("failed while serving swagger", err)
      context.endWithException(err)
    }
  }

  private fun createOpenApiObject(): OpenAPI {
    // reset the schemas to rebuild the from scratch
    braidContext.openapiComponents.schemas = null
    return OpenAPI()
      .info(createOpenApiInfo())
      .addServersItem(Server().url(baseURL.toString()))
      .apply {
        endpoints.forEach { endpoint -> endpoint.addToOpenApi(this) }
        components = braidContext.openapiComponents
        if (auth != null) {
          components.addSecuritySchemes(BRAID_AUTHORIZATION_SCHEME_NAME, auth)
        }
      }
  }

  private fun createOpenApiInfo(): Info? {
    val info = Info()
      .version(version)
      .title(title)
    info.description = description
    info.contact = contact
    return info
  }

  fun <Response> add(
    groupName: String,
    protected: Boolean,
    method: HttpMethod,
    path: String,
    handler: KCallable<Response>
  ) {
    val endpoint = EndPoint.create(
      braidContext,
      groupName,
      protected,
      method,
      path,
      endpointUtilities,
      handler
    )
    add(endpoint)
  }

  private fun add(endpoint: EndPoint) {
    endpoints.add(endpoint)
  }

  fun group(groupId: String, fn: () -> Unit) {
    this.currentGroupName = groupId
    fn()
  }
}

