/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.jsonrpc

import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.service.ServiceExecutor
import io.vertx.core.Handler
import io.vertx.ext.auth.AuthProvider
import io.vertx.ext.web.handler.sockjs.SockJSSocket

/**
 * This class is used as handler for when [SockJSSocket] connection has been made.
 * It binds the socke to a set of named service executors.
 * Note: This will need alteration to unify the number of connections down to one.
 */
class SockJSSocketServicesHandler(
  private val braidContext: BraidContext,
  private val braidConfig: BraidConfig,
  private val services: Map<String, ServiceExecutor>,
  private val authProvider: AuthProvider?
) : Handler<SockJSSocket> {

  private val pathRegEx = Regex("${braidConfig.jsonRpcConfig.rootPath.replace("/", "\\/")}\\/([^\\/]+).*")

  override fun handle(socket: SockJSSocket) {
    val serviceName = pathRegEx.matchEntire(socket.uri())?.groupValues?.get(1) ?: ""
    val service = services[serviceName]
    if (service != null) {
      handleKnownService(socket, authProvider, service)
    } else {
      handleUnknownService(socket, serviceName)
    }
  }

  private fun handleKnownService(
    socket: SockJSSocket,
    authProvider: AuthProvider?,
    service: ServiceExecutor
  ) {
    SockJSSocketServiceBinder(braidContext, service, braidContext.vertx, authProvider, braidConfig.httpServerConfig.threadPoolSize).bind(
      socket
    )
  }

  private fun handleUnknownService(socket: SockJSSocket, serviceName: String) {
    socket.write("cannot find service $serviceName")
    socket.close()
  }
}