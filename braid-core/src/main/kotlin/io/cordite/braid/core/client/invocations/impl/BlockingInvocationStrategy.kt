/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.client.invocations.impl

import io.cordite.braid.core.context.BraidJsonContext
import io.cordite.braid.core.jsonrpc.error
import io.cordite.braid.core.jsonrpc.trace
import io.cordite.braid.core.jsonrpc.warn
import io.cordite.braid.core.logging.loggerFor
import io.vertx.core.Promise
import java.lang.reflect.Type
import java.util.concurrent.CountDownLatch

internal class BlockingInvocationStrategy(
  parent: InvocationsStrategySPI,
  method: String,
  returnType: Type,
  params: Array<out Any?>,
  braidJsonContext: BraidJsonContext
) : InvocationStrategy<Any?>(parent, method, returnType, params, braidJsonContext) {

  private val promise = Promise.promise<Any?>()
  private val future = promise.future()
  private val latch = CountDownLatch(1)
  override val isAsyncResultType: Boolean = false

  companion object {

    private val log = loggerFor<BlockingInvocationStrategy>()
  }

  object Factory : InvocationStrategyFactory<Any?> {

    override fun create(
      parent: InvocationsStrategySPI,
      method: String,
      returnType: Type,
      params: Array<out Any?>,
      braidJsonContext: BraidJsonContext
    ): InvocationStrategy<Any?>? {
      // should be the last in the chain-of-command strategies as it always attempt to handle the request
      return BlockingInvocationStrategy(parent, method, returnType, params, braidJsonContext)
    }

    override val handledClass: Class<*> = Any::class.java
  }

  override fun getResult(): Any? {
    try {
      checkIdIsNotSet()
      requestId = nextRequestId()
      log.trace(requestId) { "preparing invocation" }
      beginInvoke(requestId)
      log.trace(requestId) { "awaiting result" }
      latch.await()
      log.trace(requestId) { "processing result" }
      return when {
        !future.isComplete -> error("I should have a result or error for you but but neither condition was met!")
        future.failed() -> throw future.cause()
        else -> future.result()
      }
    } catch (err: Throwable) {
      log.error(requestId, err) { err.message!! }
      throw err
    }
  }

  override fun onNext(requestId: Long, item: Any?) {
    log.trace(requestId) { "processing item $item" }
    checkIdIsSet(requestId)
    endInvoke(requestId)
    checkComputationIsNotComplete()
    promise.tryComplete(item)
    log.trace(requestId) { "signalling to the blocked client" }
    latch.countDown()
  }

  override fun onError(requestId: Long, error: Throwable) {
    log.trace(requestId) { "processing error ${error.message}" }
    endInvoke(requestId)
    checkIdIsSet(requestId)
    checkComputationIsNotComplete()
    promise.tryFail(error)
    latch.countDown()
  }

  override fun onCompleted(requestId: Long) {
    log.warn(requestId) { "processing onCompleted message - unexpected for a blocking synchronous call" }
    checkIdIsSet(requestId)
    endInvoke(requestId)
    checkComputationIsComplete()
    // NO OP
  }

  private fun checkComputationIsNotComplete() {
    if (future.isComplete) error("I received a message for request $requestId but computation is already complete!")
  }

  private fun checkComputationIsComplete() {
    if (!future.isComplete) error("I received a message for completion for request $requestId but I haven't received a result!")
  }
}
