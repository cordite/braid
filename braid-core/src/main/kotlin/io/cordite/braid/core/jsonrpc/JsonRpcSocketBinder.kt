/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.jsonrpc

import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.corda.CordaBasicInfo
import io.cordite.braid.core.http.endWithException
import io.cordite.braid.core.http.endWithObject
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.meta.ServiceDescriptor
import io.cordite.braid.core.meta.defaultServiceEndpoint
import io.cordite.braid.core.service.ServiceExecutor
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.sockjs.SockJSHandler

/**
 * Binds a collection of services to a socket created under a given router
 */
object JsonRpcSocketBinder {

  private val log = loggerFor<JsonRpcSocketBinder>()
  fun bind(
    braidContext: BraidContext,
    braidConfig: BraidConfig,
    router: Router
  ) {
    val config = braidConfig.jsonRpcConfig

    val services =
      config.serviceExecutorFactories.map { (name, factory) -> name to factory.create(braidContext) }.toMap()

    val sockJSHandler = SockJSHandler.create(braidContext.vertx)
    val authProvider = braidContext.authProvider
    val handler = SockJSSocketServicesHandler(braidContext, braidConfig, services, authProvider)
    sockJSHandler.socketHandler(handler)

    mountServices(braidConfig, router, sockJSHandler)
    bindApiDocs(braidContext, braidConfig, router, services)
  }

  private fun bindApiDocs(
    braidContext: BraidContext,
    braidConfig: BraidConfig,
    router: Router,
    services: Map<String, ServiceExecutor>
  ) {
    val config = braidConfig.jsonRpcConfig
    router.get(config.rootPath).handler {
      val serviceNames = config.serviceNames
      it.endWithObject(braidContext, ServiceDescriptor.createServiceDescriptors(config.rootPath, serviceNames))
    }

    val descriptors = services.map { (name, executor) ->
      name to executor.getMethodDescriptors()
    }.toMap()

    router.get("${config.rootPath}/:serviceName").handler {
      try {
        val serviceName = it.pathParam("serviceName")
        val serviceDoc = descriptors[serviceName]
        if (serviceDoc != null) {
          it.endWithObject(braidContext, serviceDoc)
        } else {
          it.endWithException(RuntimeException("could not find service $serviceName"))
        }
      } catch (err: Throwable) {
        log.error("failure in serving documentation for service", err)
        it.endWithException(err)
      }
    }
    val msg = "JSON-RPC end point bound to ${braidConfig.server}${config.rootPath}"
    log.info(msg)
    CordaBasicInfo.printBasicNodeInfo(msg)
  }

  private fun mountServices(
    braidConfig: BraidConfig,
    router: Router,
    sockJSHandler: SockJSHandler
  ) {
    val config = braidConfig.jsonRpcConfig
    config.serviceNames.forEach { serviceName ->
      mountServiceName(
        braidConfig,
        serviceName,
        router,
        sockJSHandler
      )
    }
  }

  private fun mountServiceName(
    braidConfig: BraidConfig,
    serviceName: String,
    router: Router,
    sockJSHandler: SockJSHandler
  ) {
    val config = braidConfig.jsonRpcConfig
    val endpoint = defaultServiceEndpoint(config.rootPath, serviceName) + "/*"
    log.info("binding braid service '$serviceName' to ${braidConfig.server}$endpoint")
    router.route(endpoint).handler(sockJSHandler)
  }
}
