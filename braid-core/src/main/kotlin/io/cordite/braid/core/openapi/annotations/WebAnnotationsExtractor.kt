/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.annotations

import io.cordite.braid.core.annotation.AnnotationsCache
import io.cordite.braid.core.annotation.createKey
import io.cordite.braid.core.reflection.description
import javax.ws.rs.*
import javax.ws.rs.core.Context
import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.full.valueParameters

class WebAnnotationsExtractor(val annotationsCache: AnnotationsCache) {

  val methodAnnotations = listOf(
    GET::class,
    HEAD::class,
    DELETE::class,
    OPTIONS::class,
    POST::class,
    PUT::class
  )

  val parameterAnnotationClasses = listOf(
    HeaderParam::class,
    PathParam::class,
    QueryParam::class,
    Context::class
  )

  fun <R> parameterAnnotations(callable: KCallable<R>, parameterIndex: Int): List<Annotation> {
    return annotationsCache.allMatchingAnnotations(callable, parameterIndex) { annotation ->
      parameterAnnotationClasses.contains(annotation.annotationClass)
    }
  }

  fun <A : Annotation, R> parameterAnnotation(
    callable: KCallable<R>,
    parameterIndex: Int,
    annotationClass: KClass<A>
  ): A? {
    return annotationsCache.allMatchingAnnotations(callable, parameterIndex) { annotation ->
      annotation.annotationClass == annotationClass
    }.map {
      @Suppress("UNCHECKED_CAST")
      it as A
    }.firstOrNull()
  }

  fun <R, A : Annotation> parameterAnnotation(
    callable: KCallable<R>,
    parameter: KParameter,
    annotationClass: KClass<A>
  ): A? {
    val key = parameter.createKey()

    val paramIndex = callable.valueParameters
      .mapIndexed { index, paramBeingChecked -> index to paramBeingChecked }
      .filter { (_, paramBeingChecked) -> paramBeingChecked.createKey() == key }
      .map { (index, _) -> index }
      .firstOrNull()
      ?: error("could not locate parameter $parameter in ${callable.description()}")

    return parameterAnnotation(callable, paramIndex, annotationClass)
  }

  fun <R> methodAnnotations(callable: KCallable<R>): List<Annotation> =
    annotationsCache.allMatchingAnnotations(callable) { annotation ->
      methodAnnotations.any {
        it.isInstance(annotation)
      }
    }

  fun <R> hasMethodAnnotations(callable: KCallable<R>): Boolean = methodAnnotations(callable).isNotEmpty()

  fun <R> methodAnnotation(callable: KCallable<R>): Annotation {
    val boundWebMethods = methodAnnotations(callable)
    return when {
      boundWebMethods.isEmpty() -> {
        error("method ${callable.description()} doesn't have a web method annotation")
      }
      boundWebMethods.size > 1 -> {
        error("method ${callable.description()} has too many web method annotations")
      }
      else -> {
        boundWebMethods.first()
      }
    }

  }
}