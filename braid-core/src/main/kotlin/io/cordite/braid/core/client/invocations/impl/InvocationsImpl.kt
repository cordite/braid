/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.client.invocations.impl

import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.async.withPromise
import io.cordite.braid.core.client.BraidClientConfig
import io.cordite.braid.core.jsonrpc.JsonRPCRequest
import io.cordite.braid.core.jsonrpc.error
import io.cordite.braid.core.logging.loggerFor
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.http.WebSocket
import io.vertx.core.http.WebSocketFrame
import java.net.URL

internal class InvocationsImpl internal constructor(
  private val config: BraidClientConfig<*>,
  private val exceptionHandler: (Throwable) -> Unit,
  private val closeHandler: (() -> Unit),
  clientOptions: HttpClientOptions
) : InvocationsStrategySPIImpl(config.serviceURI, InvocationStrategy.Companion::invoke, config.braidContext) {

  companion object {

    private val log = loggerFor<InvocationsImpl>()
  }

  /**
   * connection to the server
   * */
  private var socket: WebSocket? = null

  private val client = config.braidContext.vertx.createHttpClient(
    clientOptions
      .setDefaultHost(config.host)
      .setDefaultPort(config.port)
      .setSsl(config.tls)
      .setVerifyHost(config.verifyHost)
      .setTrustAll(config.trustAll)
  )

  init {
    // set up the websocket with all the required handlers

    val url = URL(config.serviceURI + "/websocket")
    log.info("Connecting to $url")
    withPromise<Boolean> { promise ->
      client.webSocket(url.toString()) { asyncResult ->
        when {
          asyncResult.succeeded() -> {
            log.info("connected to websocket $url")
            val sock = asyncResult.result()
            socket = sock
            sock.handler(this::receive)
            sock.exceptionHandler(exceptionHandler)
            sock.closeHandler(closeHandler)
            promise.tryComplete(true)
          }
          else -> {
            log.error("failed to connect to websocket $url", asyncResult.cause())
            socket = null
            promise.tryFail(asyncResult.cause())
          }
        }
      }
    }.future().getOrThrow()

    if (config.authCredentials != null) {
      try {
        log.info("sending authentication credentials")
        invoke("login", String::class.java, arrayOf(config.authCredentials))
        log.info("authenticated")
      } catch(err: Throwable) {
        log.error("failed to authenticate", err)
      }
    }
  }

  /**
   * shutdown everything
   * after calling this all calls to [invoke] will fail with [IllegalStateException]
   */
  override fun close() {
    super.close()
    if (socket != null) {
      try {
        socket?.close()
      } catch (err: Throwable) {
        log.info("exception during closing of braid client socket", err)
      }
      socket = null
    }
    client.close()
  }

  /**
   * writes a [JsonRPCRequest] [request] on the socket to the server
   * @returns future to indicate if the send was succesful or not
   */
  override fun send(request: JsonRPCRequest): Future<Unit> {
    return withPromise<Unit> { promise ->
      try {
        config.braidContext.vertx.runOnContext { sendDirectOnThisContext(request, promise) }
      } catch (err: Throwable) {
        log.error(request.id, err) { "failed to schedule send operation to context" }
        promise.tryFail(err)
      }
    }.future()
  }

  private fun sendDirectOnThisContext(request: JsonRPCRequest, result: Promise<Unit>) {
    try {
      val encoded = config.braidContext.encode(request)
      if (log.isTraceEnabled) {
        log.trace("writing request to socket {}", encoded)
      }
      socket
        ?.writeFrame(WebSocketFrame.textFrame(encoded, true))
        ?: error("socket was not created or was closed")
      try {
        result.tryComplete()
      } catch (err: Throwable) {
        log.error(request.id, err) { "failed to send completion notification to handler" }
      }
    } catch (err: Throwable) {
      log.error(request.id, err) { "failed to send packet to socket" }
      result.tryFail(err)
    }
  }

  /**
   * syntactic sugar to set a kotlin function as the close handler of a [WebSocket]
   */
  private fun WebSocket.closeHandler(fn: () -> Unit) {
    this.closeHandler {
      fn()
    }
  }
}

