/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.config

import com.fasterxml.jackson.annotation.JsonIgnore
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.openapi.RestMounter
import io.cordite.braid.core.openapi.auth.AuthSchema
import io.cordite.braid.core.openapi.utils.PathsBinder
import io.swagger.v3.oas.models.info.Contact
import io.vertx.ext.auth.AuthProvider
import io.vertx.ext.web.Router

data class RestConfig(
  val title: String = DEFAULT_SERVICE_NAME,
  val version: String = "1.0.0",
  val description: String = DEFAULT_DESCRIPTION,
  val hostAndPortUri: String = DEFAULT_HOST_AND_PORT_URI,
  val restPath: String = DEFAULT_REST_PATH,
  val openApiPath: String = DEFAULT_OPEN_API_PATH,
  val contact: Contact = DEFAULT_CONTACT,
  val authSchema: AuthSchema = DEFAULT_AUTH_SCHEMA,
  val jwtSecret: String = DEFAULT_AUTH_JWT_SECRET,
  val debugMode: Boolean = false,
  @JsonIgnore
  val pathsBinder: PathsBinder = PathsBinder()
) {

  companion object {

    private val log = loggerFor<RestConfig>()
    const val DEFAULT_SERVICE_NAME = ""
    const val DEFAULT_DESCRIPTION = ""
    const val DEFAULT_HOST_AND_PORT_URI = "http://localhost:8080"
    const val DEFAULT_REST_PATH = "/api/rest"
    const val DEFAULT_OPEN_API_PATH = "/openapi"
    val DEFAULT_CONTACT: Contact = Contact().email("").name("").url("")
    val DEFAULT_AUTH_PROVIDER: AuthProvider? = null
    val DEFAULT_AUTH_SCHEMA = AuthSchema.None
    const val DEFAULT_AUTH_JWT_SECRET = "7!(*|5ZEO=%TlzK<tNWH3gT%29G0*<Bt#{nl}N9Iiurt0%Yq#ua\$|WK%UEH~P\"h"
  }

  @Suppress("unused")
  fun withTitle(value: String) = this.copy(title = value)

  @Suppress("unused")
  fun withVersion(value: String) = this.copy(version = value)

  @Suppress("unused")
  fun withDescription(value: String) = this.copy(description = value)

  fun withHostAndPortUri(value: String) = this.copy(hostAndPortUri = value)

  @Suppress("unused")
  fun withRestPath(value: String) = this.copy(restPath = value)

  @Suppress("unused")
  fun withOpenApiPath(value: String) = this.copy(openApiPath = value)

  @Suppress("unused")
  fun withContact(value: Contact) = this.copy(contact = value)

  @Suppress("unused")
  fun withPaths(value: RestMounter.(Router) -> Unit) = this.copy(pathsBinder = pathsBinder.withPaths(value))

  @Suppress("unused")
  fun withAuthSchema(authSchema: AuthSchema) = this.copy(authSchema = authSchema)

  @Suppress("unused")
  fun withDebugMode() = this.copy(debugMode = true)

  @Suppress("unused")
  fun withJwtSecret(jwtSecret: String) = this.copy(jwtSecret = jwtSecret)

  fun withService(braidContext: BraidContext, service: Any): RestConfig {
    val serviceName = braidContext.serviceNaming.getServiceName(service)
    log.info("binding service $serviceName of type ${service::class} to REST")
    return try {
      withPaths {
        group(serviceName) {
          route(service)
        }
      }
    } catch (err: Throwable) {
      log.warn(
        "failed to bind service $serviceName of type ${service::class} to REST: ${err.message}",
        err
      )
      this
    }
  }
}

