/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.context

import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.ObjectMapper
import io.vertx.core.buffer.Buffer
import io.vertx.core.json.DecodeException
import io.vertx.core.json.EncodeException
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import kotlin.reflect.KClass

interface BraidJsonContext {

  val mapper: ObjectMapper

  fun encode(obj: Any?): String {
    return try {
      when (obj) {
        is JsonObject -> mapper.writeValueAsString(obj.map)
        is JsonArray -> mapper.writeValueAsString(obj.list)
        else -> mapper.writeValueAsString(obj)
      }
    } catch (e: Exception) {
      throw EncodeException("Failed to encode as JSON: " + e.message)
    }
  }

  @Throws(DecodeException::class)
  fun <T> decodeValue(buffer: Buffer, clazz: Class<T>): T

  @Throws(DecodeException::class)
  fun <T> decodeValue(str: String, clazz: Class<T>): T

  @Throws(DecodeException::class)
  fun <T> decodeValue(str: String, javaType: JavaType): T

  @Throws(DecodeException::class)
  fun <T : Any> decodeValue(str: String, kClass: KClass<T>): T

  @Throws(EncodeException::class)
  fun encodeToBuffer(obj: Any?): Buffer

  @Throws(EncodeException::class)
  fun toBuffer(obj: Any?): Buffer
}