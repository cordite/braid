/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.utils

import com.fasterxml.jackson.annotation.JsonIgnore
import io.cordite.braid.core.openapi.RestMounter
import io.vertx.ext.web.Router

class PathsBinder(@JsonIgnore private val fn: (RestMounter.(Router) -> Unit) = {}) {

  fun bind(restMounter: RestMounter, router: Router) {
    restMounter.fn(router)
  }

  @Suppress("unused")
  fun withPaths(value: RestMounter.(Router) -> Unit) = PathsBinder(fn = {
    this.fn(it)
    value(it)
  })

  override fun equals(other: Any?): Boolean {
    return true // we can't meaningfully at this stage compare these
  }

  override fun hashCode(): Int {
    return fn.hashCode()
  }
}