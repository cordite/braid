/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi

import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.constants.BraidConstants.BRAID_AUTHORIZATION_SCHEME_NAME
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.corda.CordaBasicInfo
import io.cordite.braid.core.http.endWithObject
import io.cordite.braid.core.http.withErrorHandler
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.openapi.auth.AuthSchema
import io.cordite.braid.core.openapi.auth.JWTLogin
import io.cordite.braid.core.openapi.config.RestConfig
import io.cordite.braid.core.openapi.jaxrs.BraidWebApplicationException
import io.cordite.braid.core.openapi.parser.RestParametersParser
import io.cordite.braid.core.openapi.utils.PathsBinder
import io.cordite.braid.core.reflection.KCallableWithInstance.Companion.withInstance
import io.netty.handler.codec.http.HttpResponseStatus
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.models.security.SecurityScheme
import io.vertx.core.Vertx
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.Route
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BasicAuthHandler
import io.vertx.ext.web.handler.JWTAuthHandler
import io.vertx.ext.web.handler.SessionHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.sstore.LocalSessionStore
import java.lang.reflect.InvocationTargetException
import java.net.URL
import javax.ws.rs.*
import javax.ws.rs.core.Response
import kotlin.reflect.KCallable

/**
 * This class encapsulates a simpler way of setting up a Rest Service (
 * for those moments that we need one
 *
 * An instance of this class provides convenient methods for setting up HTTP methods
 * bound to method references. It automatically serves a swagger json and UI.
 *
 * This class works with Kotlin - work is needed to make it work well with other JVM languages.
 */
class RestMounter(
  private val braidContext: BraidContext,
  private val router: Router,
  private val vertx: Vertx,
  private val braidConfig: BraidConfig
) {

  companion object {

    private val log = loggerFor<RestMounter>()

    fun mount(braidContext: BraidContext, router: Router, vertx: Vertx, config: BraidConfig) {
      RestMounter(braidContext = braidContext, router = router, vertx = vertx, braidConfig = config)
    }

    private val annotationToWebMethodMap = mapOf(
      GET::class to HttpMethod.GET,
      PUT::class to HttpMethod.PUT,
      POST::class to HttpMethod.POST,
      DELETE::class to HttpMethod.DELETE
    )
    val reOpenApiPathElements = Regex("\\{([^\\s\\{\\}]+)\\}")

  }

  private val restConfig get() = braidConfig.restConfig
  private val parameterParser = RestParametersParser(braidContext)
  private val config: RestConfig = applyAuth(restConfig)
  private val jwtLogin by lazy {
    JWTLogin(
      braidContext, braidContext.authProvider ?: error("require AuthProvider to create JWT capability"),
      config.jwtSecret
    )
  }

  private val path =
    config.restPath.trim().dropWhile { it == '/' }.dropLastWhile { it == '/' }
  private val openApiUiPath =
    config.openApiPath.trim().dropWhile { it == '/' }.dropLastWhile { it == '/' }
  private val swaggerJsonPath = if (openApiUiPath.isBlank()) {
    "swagger.json"
  } else {
    "$openApiUiPath/swagger.json"
  }
  private val swaggerStaticPath = if (openApiUiPath.isBlank()) {
    "*"
  } else {
    "$openApiUiPath/*"
  }

  private val openApiDocumentHandler: OpenApiDocumentHandler = createDocsHandler()
  private val sessionHandler by lazy {
    SessionHandler.create(
      LocalSessionStore.create(
        vertx
      )
    )
  }
  private val basicAuthHandler by lazy { BasicAuthHandler.create(braidContext.authProvider) }
  private val jwtAuthProvider by lazy { createJWTAuthProvider() }

  private fun createJWTAuthProvider() =
    when (braidContext.authProvider) {
      null -> error("needed to create a JWTAuth AuthProvider, but no base authprovider available in config")
      else -> {
        jwtLogin.createJWTAuthProvider(vertx)
      }
    }

  private val unprotectedRouter = Router.router(vertx)
  private val protectedRouter: Router = Router.router(vertx)
  private var currentRouter = unprotectedRouter
  private var groupName: String = ""
  private val protected: Boolean
    get() {
      return currentRouter == protectedRouter
    }
  private var rootPath = ""

  init {
    if (!config.restPath.startsWith("/")) throw RuntimeException("path must begin with a /")
    mount(config.pathsBinder)
  }

  /**
   * Define a grouping of method bindings
   */
  fun group(groupName: String, fn: () -> Unit) {
    this.groupName.let { old ->
      this.groupName = groupName
      try {
        fn()
      } finally {
        this.groupName = old
      }
    }
  }

  /**
   * bind the enclosed bindings declared in [fn] to the unprotected, publicly accessible, router
   */
  fun unprotected(fn: () -> Unit) {
    this.currentRouter.let { old ->
      currentRouter = unprotectedRouter
      try {
        fn()
      } finally {
        currentRouter = old
      }
    }
  }

  /**
   * bind the enclosed bindings declared in [fn] to the protected router
   */
  fun protected(fn: () -> Unit) {
    this.currentRouter.let { old ->
      if (config.authSchema == AuthSchema.None) {
        log.warn("protected scope in REST bindings is ineffective because authSchema is ${config.authSchema}")
      } else {
        currentRouter = protectedRouter
      }
      try {
        fn()
      } finally {
        currentRouter = old
      }
    }
  }

  /**
   * Creates a protected context where methods that are bound require authorisation
   */
  fun protected(protected: Boolean, fn: () -> Unit) {
    when {
      protected == this.protected -> fn()
      protected -> protected(fn)
      else -> unprotected(fn)
    }
  }

  /**
   * Temporarily alter the root path
   */
  fun rootPath(newRootPath: String, fn: () -> Unit) {
    check(!newRootPath.endsWith("/") || newRootPath == "/") { "root path must not end with /: $newRootPath" }
    val currentRootPath = rootPath
    rootPath = newRootPath
    try {
      fn()
    } finally {
      rootPath = currentRootPath
    }
  }

  /**
   * Get access to the raw Vertx router
   * This method can be used to bind additional behaviours (e.g. serving a static website)
   */
  @Suppress("unused")
  fun router(fn: Router.() -> Unit) {
    router.fn()
  }

  /**
   * Bind a [path] and function [fn]to the [GET] verb
   */
  fun <Response> get(path: String, callable: KCallable<Response>) {
    bind(HttpMethod.GET, path, callable)
  }

  /**
   * Bind a [path] and function [fn] that receives the raw [RoutingContext] to the [GET] verb
   */
  @JvmName("getRaw")
  fun get(path: String, fn: RoutingContext.() -> Unit) {
    bind(HttpMethod.GET, path, fn)
  }

  /**
   * Bind a [path] and function [fn] to the [PUT] verb
   */
  fun <Response> put(path: String, callable: KCallable<Response>) {
    bind(HttpMethod.PUT, path, callable)
  }

  /**
   * Bind a [path] and function [fn] that receives the raw [RoutingContext] to the [PUT] verb
   */
  @JvmName("putRaw")
  fun put(path: String, fn: RoutingContext.() -> Unit) {
    bind(HttpMethod.PUT, path, fn)
  }

  /**
   * Bind a [path] and function [fn] to the [POST] verb
   */
  fun <Response> post(path: String, callable: KCallable<Response>) {
    bind(HttpMethod.POST, path, callable)
  }

  /**
   * Bind a [path] and a function [fn] that receive the raw [RoutingContext] to the [POST] verb
   */
  @JvmName("postRaw")
  fun post(path: String, fn: RoutingContext.() -> Unit) {
    bind(HttpMethod.POST, path, fn)
  }

  /**
   * Bind a [path] and function [fn] to the [DELETE] verb
   */
  fun <Response> delete(path: String, callable: KCallable<Response>) {
    bind(HttpMethod.DELETE, path, callable)
  }

  /**
   * Bind a [path] and function [fn] to the [DELETE] verb
   */
  @JvmName("deleteRaw")
  fun delete(path: String, fn: RoutingContext.() -> Unit) {
    bind(HttpMethod.DELETE, path, fn)
  }

  fun <T : Any> route(obj: T) {
    val classSecurityRequirement = braidContext.annotationsCache.findAnnotation(obj::class, SecurityRequirement::class)
    val classPathAnnotation = braidContext.annotationsCache.findAnnotation(obj::class, Path::class)
    val isProtected = classSecurityRequirement?.name?.let { it == BRAID_AUTHORIZATION_SCHEME_NAME } ?: protected
    val root = classPathAnnotation?.value ?: rootPath
    val serviceGroup = braidContext.serviceNaming.getServiceCategory(obj)
    protected(isProtected) {
      rootPath(root) {
        obj::class.members.asSequence()
          .filter { braidContext.webAnnotationExtractor.hasMethodAnnotations(it) }
          .map { it.withInstance(obj) }
          .forEach { fn ->
            try {
              route(fn, serviceGroup)
            } catch (err: Throwable) {
              log.error(err.message, err)
            }
          }
      }
    }
  }

  fun <Response> route(callable: KCallable<Response>, serviceGroup: String? = null) {
    val webMethodAnnotation = braidContext.webAnnotationExtractor.methodAnnotation(callable)
    val method = annotationToWebMethodMap[webMethodAnnotation.annotationClass]
      ?: error("can not bind unhandled web method annotation: $webMethodAnnotation")
    val path = braidContext.annotationsCache.findAnnotation(callable, Path::class)?.value
      ?: error("could not find @Path annotation on method ${callable.name}")
    val operation = braidContext.annotationsCache.findAnnotation(callable, Operation::class)
    val group = serviceGroup ?: operation?.tags?.firstOrNull { it.isNotBlank() } ?: groupName
    val requireProtection =
      operation?.security?.any { it.name == BRAID_AUTHORIZATION_SCHEME_NAME } ?: protected || protected
    // TODO examine the @SecurityScheme on the class as well
    val protection = (braidContext.authProvider != null && requireProtection)
    bind(protection, group, method, path, callable)
  }

  private fun bind(method: HttpMethod, path: String, fn: RoutingContext.() -> Unit) {
    val fullPath = "${rootPath}${path}"
    currentRouter.route(method, convertToVertxPath(fullPath)).handler { it.fn() }
    fun caller(routingContext: RoutingContext) {
      fn(routingContext)
    }
    openApiDocumentHandler.add(groupName, protected, method, fullPath, ::caller)
  }

  private fun <Response> bind(method: HttpMethod, path: String, callable: KCallable<Response>) {
    bind(protected, groupName, method, path, callable)
  }

  private fun <Response> bind(
    protected: Boolean,
    group: String,
    method: HttpMethod,
    path: String,
    callable: KCallable<Response>
  ) {
    val fullPath = "${rootPath}${path}"
    @Suppress("UNCHECKED_CAST")
    currentRouter.route(method, convertToVertxPath(fullPath)).bind(braidContext, callable, method)
    openApiDocumentHandler.add(group, protected, method, fullPath, callable)
  }

  private fun convertToVertxPath(path: String): String {
    return path.replace(reOpenApiPathElements) { result ->
      val value = result.groups[1]!!.value
      ":$value"
    }
  }

  private fun <R> Route.bind(braidContext: BraidContext, callable: KCallable<R>, method: HttpMethod) {
    parameterParser.validateIsBindable(callable, method)
    this.handler { rc ->
      rc.withErrorHandler {
        try {
          val args = parameterParser.parseArguments(braidContext, rc, callable)
          val result = callable.call(*args)
          rc.endWithObject(braidContext, result)
        } catch (err: BraidWebApplicationException) {
          throw err
        } catch (err: InvocationTargetException) {
          val ex = when (err.targetException) {
            is BraidWebApplicationException -> err.targetException
            else -> BraidWebApplicationException(
              err.targetException.message,
              err.targetException,
              Response.Status.BAD_REQUEST
            )
          }
          throw ex
        } catch (err: Throwable) {
          throw BraidWebApplicationException(err.message, err, Response.Status.BAD_REQUEST)
        }
      }
    }
  }

  private fun createDocsHandler(): OpenApiDocumentHandler {
    return OpenApiDocumentHandler(
      braidContext = braidContext,
      title = config.title,
      version = config.version,
      description = config.description,
      baseURL = URL("${config.hostAndPortUri}/$path"),
      contact = config.contact,
      auth = getSecuritySchemeDefinition(),
      debugMode = config.debugMode
    )
  }

  private fun getSecuritySchemeDefinition(): SecurityScheme? {
    return when (config.authSchema) {
      AuthSchema.Basic -> {
        SecurityScheme().type(SecurityScheme.Type.HTTP)
      }
      AuthSchema.Token -> {
        SecurityScheme()
          .name(BRAID_AUTHORIZATION_SCHEME_NAME)
          .type(SecurityScheme.Type.HTTP)
          .scheme("bearer")
          .bearerFormat("JWT")
      }
      else -> {
        null
      }
    }
  }

  private fun mount(pathsBinder: PathsBinder) {
    configureSwaggerAndStatic()
    mountUnprotectedRouter()
    mountProtectedRouter()
    // pass control to caller to setup rest bindings
    pathsBinder.bind(this, router)
    if (log.isDebugEnabled) {
      log.debug("dumping routes")
      log.debug("unprotected:")
      unprotectedRouter.routes.forEach { route ->
        log.debug("${route.methods()} /$path${route.path}")
      }
      log.debug("protected:")
      protectedRouter.routes.forEach { route ->
        log.debug("${route.methods()} /$path${route.path}")
      }
    }
    val msg = "REST end point bound to ${config.hostAndPortUri}/$path"
    log.info(msg)
    CordaBasicInfo.printBasicNodeInfo(msg)
  }

  private fun applyAuth(config: RestConfig): RestConfig {
    return if (config.authSchema == AuthSchema.Token) {
      check(braidContext.authProvider != null) {
        "token auth schema requires an AuthProvider which was not supplied"
      }
      config.withPaths {
        group("auth") {
          unprotected {
            post("/auth/login", jwtLogin::login)
          }
        }
      }
    } else {
      config
    }
  }

  private fun mountUnprotectedRouter() {
    router.mountSubRouter("/$path", unprotectedRouter)
  }

  private fun configureSwaggerAndStatic() {
    // configure the swagger json
    router.get("/$swaggerJsonPath").handler(openApiDocumentHandler)
    log.info("swagger json bound to ${config.hostAndPortUri}/$swaggerJsonPath")

    // and now for the swagger static
    val sh = StaticHandler.create("io/cordite/braid/core/swagger3")
    router.getWithRegex("/$openApiUiPath").handler {
      if (it.request().path().endsWith("/")) {
        sh.handle(it)
      } else {
        it.response().putHeader("Location", "/$openApiUiPath/")
          .setStatusCode(HttpResponseStatus.TEMPORARY_REDIRECT.code()).end()
      }
    }
    router.get("/$swaggerStaticPath").handler(sh)
    log.info("Swagger UI bound to ${config.hostAndPortUri}/$openApiUiPath")
  }

  private fun mountProtectedRouter() {
    validateAuthSchemaAndProvider()
    if (config.authSchema == AuthSchema.None) return

    router.mountSubRouter("/$path", protectedRouter)
    when (config.authSchema) {
      AuthSchema.Basic -> {
        protectedRouter.route().handler(sessionHandler)
        protectedRouter.route().handler(basicAuthHandler)
      }
      AuthSchema.Token -> {
        protectedRouter.route()
          .handler(JWTAuthHandler.create(jwtAuthProvider))
      }
      AuthSchema.None -> {
        // don't add any auth provider
      }
    }
  }

  private fun validateAuthSchemaAndProvider() {
    if (config.authSchema != AuthSchema.None && braidContext.authProvider == null) throw RuntimeException(
      "authprovider cannot be null for ${config.authSchema}"
    )
  }
}
