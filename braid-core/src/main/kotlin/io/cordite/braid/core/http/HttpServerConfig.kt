/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.http

import io.cordite.braid.core.http.HttpServerConfig.Companion.BRAID_DEFAULT_MESSAGE_LIMIT
import io.cordite.braid.core.logging.loggerFor
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.impl.cpu.CpuCoreSensor
import io.vertx.core.net.JksOptions
import java.io.ByteArrayOutputStream
import java.io.File

/**
 * Options for configuring the HTTP and Websocket server
 * [tlsEnabled] - true iff TLS is to be enabled
 * [certPath] - path to the certificate for the webserver. if left blank and tlsEnabled then uses Braid dev cert
 * [keyPath] - path to the secret key. if left blank and tlsEnabled then uses Braid dev key
 * [webSocketFrameSize] - max framesize. set to [BRAID_DEFAULT_MESSAGE_LIMIT] which is currently 10MB by default
 * [webSocketMessageSize] - max message size. set to [BRAID_DEFAULT_MESSAGE_LIMIT] which is currently 10MB by default
 */
data class HttpServerConfig(
  val tlsEnabled: Boolean = true,
  val certPath: String? = null,
  val keyPath: String? = null,
  val threadPoolSize: Int = CpuCoreSensor.availableProcessors(),
  val webSocketFrameSize: Int = BRAID_DEFAULT_MESSAGE_LIMIT,
  val webSocketMessageSize: Int = BRAID_DEFAULT_MESSAGE_LIMIT
) {

  companion object {

    private val log = loggerFor<HttpServerConfig>()
    const val BRAID_DEFAULT_MESSAGE_LIMIT = 50 * 1024 * 1024 // 10 MB

    @JvmStatic
    fun defaultServerOptions(tls: Boolean = true): HttpServerOptions {
      val jksPath =
        HttpServerConfig::class.java.`package`.name.replace(".", "/") + "/default.jks"
      val jksBuffer = getResourceAsBuffer(jksPath)
      return HttpServerOptions()
        .setSsl(tls)
        .setKeyStoreOptions(
          JksOptions()
            .setValue(jksBuffer)
            .setPassword("8a5500n")
        )
    }

    private fun getResourceAsBuffer(path: String): Buffer? {
      val jksStream = HttpServerConfig::class.java.classLoader.getResourceAsStream(path)
      val baos = ByteArrayOutputStream(jksStream.available())
      while (jksStream.available() > 0) {
        baos.write(jksStream.read())
      }
      val jksBytes = baos.toByteArray()!!
      return Buffer.buffer(jksBytes)
    }
  }

  fun build(): HttpServerOptions {
    return when (tlsEnabled) {
      false -> HttpServerOptions()
      else -> createTlsHttpServerOptions()
    }
  }

  private fun createTlsHttpServerOptions(): HttpServerOptions {
    return if (certPath == null || keyPath == null) {
      log.info("without either cert or key paths not present, defaulting to developer HttpServerOptions")
      defaultServerOptions()
    } else if (!File(certPath).exists()) {
      log.error("certificate path does not exist $certPath. falling back to using Braid's dev certificate")
      defaultServerOptions()
    } else if (!File(keyPath).exists()) {
      log.error("key path does not exists $keyPath. falling back to using Braid's dev key")
      defaultServerOptions()
    } else HttpServerOptions().apply {
      isSsl = true
      keyCertOptions = convertPemFilesToJksOptions(certPath, keyPath)
    }
  }

  private fun convertPemFilesToJksOptions(
    certificatePath: String,
    privateKeyPath: String
  ): JksOptions {
    return CertsToJksOptionsConverter(certificatePath, privateKeyPath).createJksOptions()
  }
}