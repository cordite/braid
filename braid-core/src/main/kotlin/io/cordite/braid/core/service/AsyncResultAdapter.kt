/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.service

import io.cordite.braid.core.jsonrpc.withMDC
import io.cordite.braid.core.logging.loggerFor
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import rx.Observable
import rx.Subscriber

/**
 * Interface for adapters of a result type [T] such that it is bound to a [Subscriber<T>].
 */
abstract class AsyncResultAdapter<T> {

  companion object {

    @JvmStatic
    protected val log = loggerFor<AsyncResultAdapter<*>>()
  }

  abstract val handledClass: Class<*>

  abstract val isSingleResult: Boolean

  fun adapt(requestId: Long, result: T?): Observable<Any> {
    return when (result) {
      null -> Observable.empty<Any>()
      else -> {
        adaptInternal(requestId, result)
      }
    }.doOnSubscribe {
      if (log.isTraceEnabled) {
        withMDC(requestId) {
          log.trace("starting result response for request $requestId")
        }
      }
    }.doOnNext {
      if (log.isTraceEnabled) {
        withMDC(requestId) {
          log.trace("sending response for request $requestId: $it")
        }
      }
    }.doOnCompleted {
      if (log.isTraceEnabled) {
        withMDC(requestId) {
          log.trace("response for request $requestId completed")
        }
      }
    }.doOnError {
      if (log.isTraceEnabled) {
        withMDC(requestId) {
          log.trace("request $requestId failed", it)
        }
      }
    }
  }

  protected abstract fun adaptInternal(requestId: Long, result: T): Observable<Any>
}

internal object DefaultAsyncResultAdapter : AsyncResultAdapter<Any>() {

  override val handledClass: Class<Any> = Any::class.java
  override fun adaptInternal(requestId: Long, result: Any): Observable<Any> {
    return Observable.just(result)
  }

  override val isSingleResult: Boolean = true
}

object VertxFutureAsyncResultAdapter : AsyncResultAdapter<Future<Any>>() {

  override val handledClass: Class<Future<*>> = Future::class.java

  @Suppress("UNCHECKED_CAST")
  override fun adaptInternal(requestId: Long, result: Future<Any>): Observable<Any> {
    return Observable.unsafeCreate<Any> { subscriber ->
      result.setHandler {
        handleAsyncResult(it, subscriber)
      }
    }
  }

  private fun handleAsyncResult(
    response: AsyncResult<*>,
    subscriber: Subscriber<Any>
  ) {
    when (response.succeeded()) {
      true -> {
        subscriber.onNext(response.result())
        subscriber.onCompleted()
      }
      else -> {
        subscriber.onError(response.cause())
      }
    }
  }

  override val isSingleResult: Boolean = true
}

object ObservableAsyncResultAdapter : AsyncResultAdapter<Observable<Any>>() {

  override val handledClass: Class<Observable<*>> = Observable::class.java

  override val isSingleResult: Boolean = false
  override fun adaptInternal(requestId: Long, result: Observable<Any>): Observable<Any> {
    return result
  }
}

