/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.jaxrs.runtime

//import javax.ws.rs.core.Cookie
//import javax.ws.rs.ext.RuntimeDelegate
//
//class BraidCookieProvider : RuntimeDelegate.HeaderDelegate<Cookie> {
//
//  override fun toString(cookie: Cookie?): String {
//    check(cookie != null) { "cookie is null" }
//    val b = StringBuilder()
//    b.append("\$Version=").append(cookie!!.version).append(';')
//    b.append(cookie.name).append('=')
//    b.appendQuotedIfWhitespace(cookie.value)
//    if (cookie.domain != null) {
//      b.append(";\$Domain=")
//      b.appendQuotedIfWhitespace(cookie.domain)
//    }
//
//    if (cookie.path != null) {
//      b.append(";\$Path=")
//      b.appendQuotedIfWhitespace(cookie.path)
//    }
//    return b.toString()
//  }
//
//  override fun fromString(value: String?): Cookie {
//    TODO("tbi")
//  }
//}
//
//private val reWhiteSpace = Regex("\\s")
//fun StringBuilder.appendQuotedIfWhitespace(value: String) {
//  val needsQuote = value.contains(reWhiteSpace)
//  if (needsQuote)
//    append('"')
//  append(value)
//  if (needsQuote)
//    append('"')
//}