/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.client.invocations.impl

import io.cordite.braid.core.context.BraidJsonContext
import java.lang.reflect.Type

interface InvocationStrategyFactory<R> {
  val handledClass: Class<*>

  /**
   * @return non-null if an invocation can be created by this factory for the given parameters
   * @return Null if it's not able to create the strategy
   */
  fun create(
    parent: InvocationsStrategySPI,
    method: String,
    returnType: Type,
    params: Array<out Any?>,
    braidJsonContext: BraidJsonContext
  ): InvocationStrategy<R>?

  companion object {
    @Suppress("MemberVisibilityCanBePrivate")
    val DEFAULT_FACTORIES: List<InvocationStrategyFactory<*>> = listOf(
      FutureInvocationStrategy.Factory,
      ObservableInvocationStrategy.Factory
    )
    val DEFAULT_FINAL_FACTORY: InvocationStrategyFactory<*> = BlockingInvocationStrategy.Factory

    private var factories = DEFAULT_FACTORIES

    /**
     * The final factory is the last factory that is passed a request
     * This concept is seperated becaues
     * 1. ordering of the factories matters and typically [BlockingInvocationStrategy] should be called last
     * 2. most operations to add a new factory do not need to change the final factory
     */
    private var finalFactory = DEFAULT_FINAL_FACTORY

    val allFactories: List<InvocationStrategyFactory<*>>
      get() = factories + finalFactory

    fun <T> addFactory(factory: InvocationStrategyFactory<T>) {
      factories = factories + factory
    }

    fun setFactories(factories: List<InvocationStrategyFactory<*>>) {
      Companion.factories = factories
    }

    fun setFinalFactory(factory: InvocationStrategyFactory<*>) {
      finalFactory = factory
    }
  }
}