/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.util.StdConverter
import io.cordite.braid.core.annotation.BraidMethod
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.http.HttpServerConfig
import io.cordite.braid.core.http.body
import io.cordite.braid.core.http.futureGet
import io.cordite.braid.core.http.verifyNoError
import io.cordite.braid.core.json.JsonQuery.SLASH
import io.cordite.braid.core.json.JsonQuery.query
import io.cordite.braid.core.server.BraidServer
import io.cordite.braid.core.socket.findFreePort
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.json.JsonObject
import org.junit.After
import org.junit.Test
import javax.ws.rs.GET
import javax.ws.rs.HeaderParam
import javax.ws.rs.Path
import kotlin.test.assertEquals

class BespokeBraidContextTest {

  private val braidContext = BespokeBraidContext.create()
  private val port = findFreePort()
  private val service = ComplexDataServiceApp(braidContext, port)
  private val client =
    braidContext.vertx.createHttpClient(HttpClientOptions().setDefaultPort(port).setDefaultHost("localhost"))

  @After
  fun after() {
    client.close()
    service.stop()
    braidContext.close().getOrThrow()
  }

  @Test
  fun `that swagger maps CordaX500Name to string`() {
    val swagger = getSwagger()
    val type = swagger
      .query<String>(
        braidContext,
        "/paths/${SLASH}complex-data/get/responses/200/content/application${SLASH}json/schema/type"
      )
    assertEquals("string", type)
  }

  private fun getSwagger(): JsonObject {
    return client
      .futureGet(path = "http://localhost:$port/openapi/swagger.json")
      .map { it.verifyNoError() }
      .compose { it.body() }
      .map { JsonObject(it) }
      .getOrThrow()
  }

}

class ComplexDataServiceApp(braidContext: BraidContext, port: Int) {
  companion object {

    @JvmStatic
    fun main(args: Array<String>) {
      ComplexDataServiceApp(BespokeBraidContext.create(), 8080)
    }
  }

  private var braidServer: BraidServer? = BraidConfig()
    .withPort(port)
    .withService(braidContext, ComplexDataService())
    .withHttpServerConfig(
      HttpServerConfig(tlsEnabled = false)
    )
    .amendRestConfig {
      withDebugMode()
    }.start(braidContext).getOrThrow()

  fun stop() {
    if (braidServer != null) {
      braidServer!!.stop().getOrThrow()
      braidServer = null
    }
  }
}

class ComplexDataService {

  @GET
  @Path("/complex-data")
  @BraidMethod(value = "round-trip complex data by string")
  fun complexData(@HeaderParam("name") name: ComplexData): ComplexData {
    return name
  }
}

class BespokeBraidContext : BraidContext() {
  companion object {

    fun create(): BespokeBraidContext {
      return BespokeBraidContext()
    }
  }

  init {
    val bespokeModules = BespokeJacksonModule()
    mapper.registerModule(bespokeModules)
  }
}

data class ComplexData(val name: String)

@JsonSerialize(converter = ComplexDataToStringConverter::class)
@JsonDeserialize(converter = StringToComplexTypeConverter::class)
abstract class ComplexDataMixin

class ComplexDataToStringConverter : StdConverter<ComplexData, String>() {

  override fun convert(value: ComplexData): String {
    return value.name
  }
}

class StringToComplexTypeConverter : StdConverter<String, ComplexData>() {

  override fun convert(value: String): ComplexData {
    return ComplexData(value)
  }
}

class BespokeJacksonModule() : SimpleModule() {
  init {
    setMixInAnnotation(ComplexData::class.java, ComplexDataMixin::class.java)
  }
}

