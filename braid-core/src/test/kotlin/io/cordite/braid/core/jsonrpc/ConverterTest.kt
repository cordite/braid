/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.jsonrpc

import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.context.BraidContext
import org.junit.After
import org.junit.Test
import java.time.Instant
import kotlin.reflect.full.valueParameters
import kotlin.reflect.jvm.jvmErasure

data class Person(val name: String, val dob: Instant)
class JsonRpcTestService {

  fun getNames(people: List<Person>): List<String> {
    return people.map { it.name }
  }
}

class ConverterTest {

  private val context = BraidContext.create()

  @After
  fun after() {
    context.close().getOrThrow()
  }

  @Test
  fun `that we can deserialise collection type`() {
    val method = JsonRpcTestService::getNames
    val parameter = method.valueParameters[0]
    val ktype = parameter.type
    val arguments = ktype.arguments
    val argument = arguments[0]
    val elementType = argument.type!!.jvmErasure.javaObjectType

    val people = listOf(Person("Jim", Instant.now()))
    val request = JsonRPCRequest(id = 1, method = "getNames", params = listOf(people))
    val json = context.encode(request)
    val request2 = context.decodeValue(json, JsonRPCRequest::class.java)
    val params = request2.params as List<*>
    val param1 = params[0]
    context.encode(param1)
    val collectionType = context.mapper.typeFactory.constructCollectionType(ArrayList::class.java, elementType)
    val any = context.mapper.convertValue<ArrayList<*>>(param1!!, collectionType)
    val anyList = any.toList()
    val testService = JsonRpcTestService()
    @Suppress("UNCHECKED_CAST")
    JsonRpcTestService::getNames.invoke(testService, anyList as List<Person>)
  }
}

