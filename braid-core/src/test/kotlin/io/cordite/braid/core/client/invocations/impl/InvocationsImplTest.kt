/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.client.invocations.impl

import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.async.mapUnit
import io.cordite.braid.core.client.BraidClientConfig
import io.cordite.braid.core.client.invocations.Invocations
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.jsonrpc.JsonRPCResultResponse
import io.cordite.braid.core.socket.findFreePort
import io.vertx.core.Future
import io.vertx.core.Future.succeededFuture
import io.vertx.core.Promise
import io.vertx.core.http.HttpServer
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import java.net.ConnectException
import kotlin.test.assertFailsWith

@RunWith(VertxUnitRunner::class)
class InvocationsImplTest {

  private val port = findFreePort()
  private val braidContext = BraidContext.create()

  @After
  fun after() {
    braidContext.close().getOrThrow()
  }

  fun closeServer(httpServer: HttpServer?): Future<Unit> {
    return when (httpServer) {
      null -> succeededFuture<Unit>()
      else -> {
        val promise = Promise.promise<Void>()
        httpServer.close(promise::handle)
        promise.future().mapUnit()
      }
    }
  }

  @Test
  fun `that sending a malformed request does not break the flow`(context: TestContext) {
    val async = context.async()
    val server = braidContext.vertx.createHttpServer()
      .webSocketHandler { socket ->
        socket.handler {
          socket.writeFinalTextFrame("&&&&&&&&&&")
          socket.writeFinalTextFrame(
            braidContext.encode(
              JsonRPCResultResponse(
                id = 1,
                result = "hello"
              )
            )
          )
        }
      }.listen(port) {
        async.complete()
      }
    async.await()
    val invocations = Invocations.create(
      config = BraidClientConfig(
        braidContext = braidContext,
        serviceClass = Any::class.java,
        port = port,
        tls = false
      ),
      exceptionHandler = { err ->
        context.fail(err)
      },
      closeHandler = {
      }
    )
    val result = invocations.invoke("foo", String::class.java, arrayOf()) as String
    context.assertEquals("hello", result)
    closeServer(server).getOrThrow()
  }

  @Test
  fun `that trying to connect to a non existent uri fails`(context: TestContext) {
    assertFailsWith<ConnectException> {
      Invocations.create(
        config = BraidClientConfig(
          serviceClass = Any::class.java,
          port = port,
          tls = false,
          braidContext = braidContext
        ),
        exceptionHandler = {
          context.fail(it)
        },
        closeHandler = {
        }
      )
    }
  }

  @Test(expected = IllegalStateException::class)
  fun `that submitting strategies with duplicate ids does not fail - should ra`() {
    val invocations = MockInvocations(
      braidJsonContext = braidContext,
      invocationTarget = { parent, method, returnType, params, _, braidContext ->
        parent.setStrategy(1, object : InvocationStrategy<Any>(parent, method, returnType, params, braidContext) {
          override val isAsyncResultType = false

          override fun getResult(): Any {
            error("should not be called")
          }

          override fun onNext(requestId: Long, item: Any?) {
            error("should not be called")
          }

          override fun onError(requestId: Long, error: Throwable) {
            error("should not be called")
          }

          override fun onCompleted(requestId: Long) {
            error("should not be called")
          }
        })
      })
    invocations.invoke("foo", Any::class.java, emptyArray())
    invocations.invoke("foo", Any::class.java, emptyArray())
  }

  @Test()
  fun `that removing an unregistered strategy does not fail - should write to logs`() {
    val invocations = MockInvocations(braidContext)
    invocations.removeStrategy(1)
  }

}