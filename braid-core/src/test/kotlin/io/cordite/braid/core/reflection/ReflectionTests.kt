/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.reflection

import io.cordite.braid.core.annotation.BraidService
import io.cordite.braid.core.service.AsyncResultAdapters.Companion.DEFAULT_ASYNC_RESULT_ADAPTERS
import io.vertx.core.Future
import org.junit.Test
import rx.Observable
import kotlin.reflect.jvm.javaMethod
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@BraidService(name = "namedService", description = "named-service description")
interface NamedService {
  fun getSync() : String
  fun getAsync() : Future<String>
  fun getStream() : Observable<String>
  fun getCollection(): List<String>
}

class ReflectionTests {
  @Test
  fun `that types can be correctly inferred`() {
    assertEquals(
      String::class.java,
      NamedService::getAsync.javaMethod!!.getAsyncResultType(DEFAULT_ASYNC_RESULT_ADAPTERS)
    )
    assertEquals(
      String::class.java,
      NamedService::getSync.javaMethod!!.getAsyncResultType(DEFAULT_ASYNC_RESULT_ADAPTERS)
    )
    assertEquals(
      String::class.java,
      NamedService::getStream.javaMethod!!.getAsyncResultType(DEFAULT_ASYNC_RESULT_ADAPTERS)
    )
    assertEquals(
      NamedService::getCollection.javaMethod!!.genericReturnType,
      NamedService::getCollection.javaMethod!!.getAsyncResultType(DEFAULT_ASYNC_RESULT_ADAPTERS)
    )
    assertTrue { NamedService::getStream.javaMethod!!.genericReturnType.isStreaming() }
    assertEquals(Observable::class.java, NamedService::getStream.javaMethod!!.genericReturnType.actualType())
  }
}