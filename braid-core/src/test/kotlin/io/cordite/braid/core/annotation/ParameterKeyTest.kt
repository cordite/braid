/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.annotation

import org.junit.Test
import javax.ws.rs.QueryParam
import kotlin.reflect.full.valueParameters
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class ParameterKeyTest {

  @Test
  fun `that different parameters have different keys`() {
    val callable = this::someFunction
    val p1 = callable.valueParameters[0]
    val p2 = callable.valueParameters[1]
    val k1 = p1.createKey()
    val k2 = p2.createKey()
    assertNotEquals(k1, k2)
  }

  @Test
  fun `that a key can be used to locate the right parameter`() {
    val callable = this::someFunction
    val p2 = callable.valueParameters[1]
    val key = p2.createKey()
    val paramIndex = callable.valueParameters
      .mapIndexed { index, parameter -> index to parameter }
      .filter { (_, parameter) -> parameter.createKey() == key }
      .map { (index, _) -> index }
      .firstOrNull()
    assertEquals(1, paramIndex)
  }

  @Suppress("UNUSED_PARAMETER")
  fun someFunction(@QueryParam("page-size") pageSize: Int, @QueryParam("page-number") pageNumber: Int) {}
}