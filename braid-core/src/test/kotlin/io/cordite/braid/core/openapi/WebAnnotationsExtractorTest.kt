/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi

import io.cordite.braid.core.annotation.AnnotationsCache
import io.cordite.braid.core.annotation.TestNetworkMapRestBinding
import io.cordite.braid.core.openapi.annotations.WebAnnotationsExtractor
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.media.Schema
import org.junit.Test
import javax.ws.rs.GET
import javax.ws.rs.QueryParam
import kotlin.reflect.full.valueParameters
import kotlin.test.assertEquals

class WebAnnotationsExtractorTest {

  @Test
  fun `that we can can extract webmethods`() {
    val bindings = TestNetworkMapRestBinding()
    val extractor = WebAnnotationsExtractor(AnnotationsCache())
    val annotations = extractor.methodAnnotations(bindings::nodes)
    assertEquals(1, annotations.size)
    annotations.first().apply {
      assertEquals(GET::class, this.annotationClass)
    }
  }

  @Test
  fun `that we can extract parameter annotations`() {
    val extractor = WebAnnotationsExtractor(AnnotationsCache())
    val callable = this::listAccountsPagedRest
    val queryParams = callable.valueParameters.map {
      extractor.parameterAnnotation(callable, it, QueryParam::class)!!
    }.map { it.value }
    assertEquals(listOf("page-number", "page-size"), queryParams)
  }

  fun listAccountsPagedRest(
    @Suppress("UNUSED_PARAMETER")
    @QueryParam("page-number")
    @Parameter(
      description = "page number - must be greater than zero",
      schema = Schema(defaultValue = 1.toString()),
      required = true
    )
    pageNumber: Int,
    @Suppress("UNUSED_PARAMETER") @Parameter(
      description = "max accounts per page",
      schema = Schema(defaultValue = 1.toString()),
      required = true
    )
    @QueryParam("page-size")
    pageSize: Int
  ) {
  }

}