/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.jsonschema

import io.cordite.braid.core.annotation.BraidMethod
import io.cordite.braid.core.annotation.BraidService
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.service.AsyncResultAdapters.Companion.DEFAULT_ASYNC_RESULT_ADAPTERS
import io.cordite.braid.core.service.MethodDescriptor
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import org.junit.After
import org.junit.Test
import rx.Observable
import kotlin.reflect.jvm.javaMethod
import kotlin.test.assertEquals

class JsonSchemaKtTest {

  private val braidContext = BraidContext.create()

  @After
  fun after() {
    braidContext.close().getOrThrow()
  }

  @Test
  fun `that we can get the correct descriptor for a method with description`() {
    val method = JsonSchemaTestService::echo.javaMethod!!
    val descriptor = method.toDescriptor(braidContext, DEFAULT_ASYNC_RESULT_ADAPTERS)
    val match = MethodDescriptor(
      name = "echo",
      description = "echo",
      parameters = json { obj("message" to json { obj("type" to "string") }) },
      returnType = json {
        obj("type" to "string")
      })
    assertEquals(match, descriptor)
  }

  @Test
  fun `that we can get the correct descriptor for a method without description`() {
    val method = JsonSchemaTestService::echoNoDescription.javaMethod!!
    val descriptor = method.toDescriptor(braidContext, DEFAULT_ASYNC_RESULT_ADAPTERS)
    val match = MethodDescriptor(
      name = "echoNoDescription",
      description = "",
      parameters = json {
        obj("message" to json { obj("type" to "string") })
      },
      returnType = json { obj("type" to "string") }
    )
    assertEquals(match, descriptor)
  }

  @Test
  fun `that we can get the correct descriptor for a method returning observable`() {
    val method = JsonSchemaTestService::numbers.javaMethod!!
    val descriptor = method.toDescriptor(braidContext, DEFAULT_ASYNC_RESULT_ADAPTERS)
    val match =
      MethodDescriptor(
        name = "numbers",
        description = "",
        parameters = json { obj() },
        returnType = json {
          obj(
            "type" to "integer",
            "x-json-rpc-streaming" to true
          )
        }
      )
    assertEquals(match, descriptor)
  }

  @Test
  fun `that we can get the correct descriptor for a class constructor`() {
    val constructor = EchoFlow::class.java.constructors.first()
    val descriptor = constructor.toDescriptor(braidContext)
    val match = MethodDescriptor(
      name = EchoFlow::class.qualifiedName!!,
      description = "echo",
      parameters = json { obj("message" to json { obj("type" to "string") }) },
      returnType = json {
        obj(
          "type" to "EchoFlow"
        )
      }
    )
    assertEquals(match, descriptor)
  }
}

class JsonSchemaTestService(val config: String) {

  @BraidMethod(value = "echo", returnType = String::class)
  fun echo(message: String) = message
  fun echoNoDescription(message: String) = message
  fun numbers(): Observable<Int> = Observable.just(1, 2, 3)
}

@BraidService(description = "echo", name = "EchoFlow")
class EchoFlow(val message: String) {

  fun call(): String = message
}