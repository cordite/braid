/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.http

import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.context.BraidContext
import org.junit.After
import org.junit.Test
import kotlin.test.assertEquals

class HttpServerConfigTest {

  private val braidContext = BraidContext.create()

  @After
  fun after() {
    braidContext.close().getOrThrow()
  }

  @Test
  fun loadJKSTest() {
    HttpServerConfig.defaultServerOptions()
  }

  @Test
  fun `that we can roundtrip the config`() {
    val expected = HttpServerConfig()
    val json = braidContext.encode(expected)
    val actual = braidContext.decodeValue(json, HttpServerConfig::class.java)
    assertEquals(expected, actual)
  }
}