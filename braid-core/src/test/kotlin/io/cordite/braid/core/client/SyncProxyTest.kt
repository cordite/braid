/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.client

import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.client.server.ComplexObject
import io.cordite.braid.core.client.server.MeteringModelData
import io.cordite.braid.core.client.server.MyService
import io.cordite.braid.core.client.server.MyServiceImpl
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import java.net.ServerSocket
import java.util.concurrent.atomic.AtomicInteger

@RunWith(VertxUnitRunner::class)
class SyncProxyTest {

  private val braidContext: BraidContext = BraidContext.create()
  private val port = getFreePort()
  private val braidServer = BraidConfig()
    .withService(braidContext, MyServiceImpl(braidContext))
    .withPort(port)
    .start(braidContext).getOrThrow()

  private val braidClient: BraidClient<MyService> by lazy {
    BraidClient.createClient(
      config = BraidClientConfig(
        serviceClass = MyService::class.java,
        port = port,
        trustAll = true,
        verifyHost = false,
        braidContext = braidContext
      )
    )
  }

  private val myService: MyService by lazy { braidClient.bind() }

  @After
  fun after() {
    braidClient.close()
    braidServer.stop().getOrThrow()
    braidContext.close().getOrThrow()
  }

  @Test
  fun `should be able to add two numbers together`() {
    val result = myService.add(1.0, 2.0)
    Assert.assertEquals(0, braidClient.activeRequestsCount())
    Assert.assertEquals(3.0, result, 0.0001)
  }

  @Test
  fun `should be able to get a complex object back from the proxy`() {
    val complexObject = ComplexObject("1", 2, 3.0)
    val result = myService.echoComplexObject(complexObject)
    Assert.assertEquals(0, braidClient.activeRequestsCount())
    Assert.assertEquals(complexObject, result)
  }

  @Test
  fun `should be able to call method with no arguments`() {
    val result = myService.noArgs()
    Assert.assertEquals(0, braidClient.activeRequestsCount())
    Assert.assertEquals(5, result)
  }

  @Test
  fun `should be able to get a future back from the proxy`(context: TestContext) {
    myService.longRunning().map {
      context.assertEquals(5, it)
    }.setHandler(context.asyncAssertSuccess {
      context.assertEquals(0, braidClient.activeRequestsCount())
    })
  }

  @Test
  fun `should be able to get a stream of events back from the proxy`(context: TestContext) {
    val sequence = AtomicInteger(0)
    val async = context.async()

    myService.stream().subscribe({
      context.assertEquals(sequence.getAndIncrement(), it, "incorrect message received")
    }, {
      context.fail(it.message)
    }, {
      context.assertEquals(11, sequence.get())
      context.assertEquals(0, braidClient.activeRequestsCount())
      async.complete()
    })
  }

  @Test
  fun `should blow up and report runtime exception`(context: TestContext) {
    try {
      myService.blowUp()
    } catch (e: RuntimeException) {
      context.assertEquals("expected exception", e.message)
    }
  }

  @Test
  fun `should blow up and remove handler for streaming error`(context: TestContext) {
    val async = context.async()

    myService.largelyNotStream().subscribe({
      context.fail("should never get called")
    }, {
      context.assertEquals("stream error", it.message)
      context.assertEquals(0, braidClient.activeRequestsCount())
      async.complete()
    }, {
      context.fail("should never get called")
    })
  }

  @Test(expected = IllegalArgumentException::class)
  fun `should throw exception if json object blows up`() {
    myService.stuffedJsonObject()
  }

  @Test
  fun `should be able to bounce data classes with default params`() {
    val modelData = MeteringModelData("wobble")
    val result = myService.exposeParameterListTypeIssue("wibble", modelData)
    Assert.assertEquals(0, braidClient.activeRequestsCount())
    Assert.assertEquals(modelData, result)
  }

  private fun getFreePort(): Int {
    return (ServerSocket(0)).use {
      it.localPort
    }
  }

}
