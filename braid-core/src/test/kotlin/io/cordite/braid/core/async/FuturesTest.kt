/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.async

import io.cordite.braid.core.context.BraidContext
import io.vertx.core.Future
import io.vertx.core.impl.NoStackTraceThrowable
import org.junit.Test

class FuturesTest {

  private val braidContext = BraidContext.create()

  @Test(expected = NoStackTraceThrowable::class)
  fun `that we can block on failed future`() {
    val msg = "all messed up"
    val future = Future.failedFuture<String>(msg)
    future.getOrThrow()
  }

  @Test(expected = NoStackTraceThrowable::class)
  fun `that we can fail a blocked future`() {
    val future = braidContext.vertx.executeAsync<String> {
      it.fail("all messed up")
    }
    future.getOrThrow()
  }
}