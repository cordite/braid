/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.context

import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.http.body
import io.cordite.braid.core.http.futureGet
import io.cordite.braid.core.http.verifyNoError
import io.cordite.braid.core.json.JsonQuery.query
import io.cordite.braid.core.openapi.config.RestConfig
import io.cordite.braid.core.openapi.utils.EndPointUtilities.Companion.fullyQualifiedRef
import io.cordite.braid.core.server.BraidServer
import io.cordite.braid.core.socket.findFreePort
import io.cordite.braid.core.testing.ComplexObject
import io.cordite.braid.core.testing.TestService
import io.swagger.v3.oas.models.media.ArraySchema
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.json.JsonObject
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class BraidContextTest {

  private val port = findFreePort()

  @Test
  fun `that we can shutdown and create a server on the same port`() {
    val braidContext = BraidContext.create()
    try {
      val braidServer = createServer(braidContext)
      verifyOpenApiDefinition(braidContext)
      braidServer.stop().getOrThrow()
      val braidServer2 = createServer(braidContext)
      verifyOpenApiDefinition(braidContext)
      braidServer2.stop().getOrThrow()
    } finally {
      braidContext.close().getOrThrow()
    }
  }

  @Test
  fun `that we can create a new server after the old context has been shutdown`() {
    val braidContext1 = BraidContext.create()
    try {
      createServer(braidContext1)
      verifyOpenApiDefinition(braidContext1)
    } finally {
      braidContext1.close().getOrThrow()
    }
    val braidContext2 = BraidContext.create()
    try {
      createServer(braidContext2)
      verifyOpenApiDefinition(braidContext2)
    } finally {
      braidContext1.close().getOrThrow()
    }
  }

  @Test
  fun `that future of list returns correct schema`() {
    val braidContext = BraidContext.create()
    try {
      val schema = braidContext.getOpenAPISchema(TestService::asyncListOfComplex.returnType) as ArraySchema
      val itemSchema = schema.items!!
      val complexRef = braidContext.getOpenAPISchema(ComplexObject::class.java)!!.fullyQualifiedRef
      assertEquals(complexRef, itemSchema.`$ref`)
    } finally {
      braidContext.close().getOrThrow()
    }
  }

  private fun createServer(braidContext: BraidContext): BraidServer {
    return BraidConfig()
      .withPort(port)
      .start(braidContext)
      .getOrThrow()
  }

  private fun verifyOpenApiDefinition(braidContext: BraidContext) {
    val openApiDefinition = getOpenApiDefinition(braidContext)
    val versionString = openApiDefinition.query<String>(braidContext, "/openapi")!!
    val version = SemVer.parse(versionString)
    assertTrue(SemVer(3, 0, 0) <= version)
  }

  private fun getOpenApiDefinition(braidContext: BraidContext): JsonObject {
    val client = braidContext.vertx.createHttpClient(
      HttpClientOptions()
        .setDefaultHost("localhost")
        .setDefaultPort(port)
        .setSsl(true)
        .setTrustAll(true)
        .setVerifyHost(false)
    )

    return client
      .futureGet(path = "${RestConfig.DEFAULT_OPEN_API_PATH}/swagger.json")
      .map { it.verifyNoError() }
      .compose { it.body() }
      .map { JsonObject(it) }
      .setHandler { client.close() }
      .getOrThrow()
  }

}
