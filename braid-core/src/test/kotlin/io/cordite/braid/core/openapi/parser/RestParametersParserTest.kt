/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.parser

import io.cordite.braid.core.annotation.BraidMethod
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.context.BraidContext
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpMethod
import org.junit.After
import org.junit.Test
import javax.ws.rs.GET
import javax.ws.rs.HeaderParam
import javax.ws.rs.POST

class RestParametersParserTest {

  private val braidContext = BraidContext.create()
  private val restParametersParser = RestParametersParser(braidContext)

  @After
  fun after() {
    braidContext.close().getOrThrow()
  }

  @Test
  fun `that GET method with single header parameter validates`() {
    restParametersParser.validateIsBindable(MyServiceImpl::headerListOfStrings, HttpMethod.GET)
  }

  @Test
  fun `that POST method with single body parameter validates`() {
    restParametersParser.validateIsBindable(MyServiceImpl::doubleBuffer, HttpMethod.POST)
  }
}

interface MyService {

  @GET
  @BraidMethod(value = "string lists")
  fun headerListOfStrings(@HeaderParam("value-header") value: List<String>): List<String>

  @POST
  @BraidMethod(value = "vertx buffers")
  fun doubleBuffer(bytes: Buffer): Buffer

}

class MyServiceImpl : MyService {

  override fun headerListOfStrings(@HeaderParam("value") value: List<String>): List<String> {
    return value
  }

  override fun doubleBuffer(bytes: Buffer): Buffer {
    TODO("Not yet implemented")
  }

}