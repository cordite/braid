/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi

import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.http.HttpServerConfig
import io.cordite.braid.core.server.BraidServer
import io.cordite.braid.core.testing.Apple
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.vertx.core.Future
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.PathParam

// These classes are used for quickly trying out an end point definition to debug issues. Mostly because [TestService]
// has a very large surface. Perhaps the latter should be refactored.

class TestSandboxService(private val braidContext: BraidContext) {

  @GET
  @Path("/tags/{tag}/balances")
  @Operation(
      summary = "Get available balances by tag.",
      description = "Returns a map from a token descriptor to an object with available and total balance amounts",
      tags = ["transactions and balances"]
  )
  fun balanceForAccountTag(
      @PathParam("tag")
      @Parameter(
          description = "grouping tag for account in the form <code>[tag-category-name]:[tag-value]</code>",
          example = "location:US"
      )
      tag: String
  ) {
  }
}

class TestSandboxApp(braidContext: BraidContext, port: Int) {
  companion object {

    @JvmStatic
    fun main(args: Array<String>) {
      TestSandboxApp(BraidContext.create(), 8080)
    }
  }

  private var braidServer: BraidServer? = BraidConfig()
    .withPort(port)
    .withService(braidContext, TestSandboxService(braidContext))
    .withHttpServerConfig(
      HttpServerConfig(tlsEnabled = false)
    )
    .amendRestConfig {
      withDebugMode()
    }.start(braidContext).getOrThrow()

  fun stop() {
    if (braidServer != null) {
      braidServer!!.stop().getOrThrow()
      braidServer = null
    }
  }
}