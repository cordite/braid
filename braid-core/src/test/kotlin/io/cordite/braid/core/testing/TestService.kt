/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@file:Suppress("DEPRECATION")

package io.cordite.braid.core.testing

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import io.cordite.braid.core.annotation.BraidMethod
import io.cordite.braid.core.annotation.BraidService
import io.cordite.braid.core.annotation.JsonRpcIgnore
import io.cordite.braid.core.constants.BraidConstants.BRAID_AUTHORIZATION_SCHEME_NAME
import io.netty.buffer.ByteBuf
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.Parameters
import io.swagger.v3.oas.annotations.enums.Explode
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.enums.ParameterStyle
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.RoutingContext
import rx.Observable
import java.math.BigDecimal
import java.nio.ByteBuffer
import java.time.Duration
import java.time.Instant
import java.util.*
import javax.ws.rs.GET
import javax.ws.rs.HeaderParam
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.core.Context

const val X_HEADER_LIST_STRING = "x-list-string"
const val X_HEADER_STRING = "x-string"
const val X_HEADER_OBJECT = "x-object"

@BraidService(name = TestService.SERVICE_NAME, description = TestService.SERVICE_DESCRIPTION)
interface TestService {

  companion object {

    const val SERVICE_NAME = "myService"
    const val SERVICE_DESCRIPTION = "a test service to provide exhaustive cases for remoting"
  }

  // REST API

  @GET
  @Path("/hello")
  @BraidMethod(value = "say hello")
  fun sayHello(): String

  @GET
  @Path("/hello-async")
  @BraidMethod(value = "say hello async")
  fun sayHelloAsync(): Future<String>

  @GET
  @Path("/quiet-async-void")
  @BraidMethod(value = "quiet async method with void")
  fun quietAsyncVoid(): Future<Void>

  @GET
  @Path("/quiet-async-unit")
  @BraidMethod(value = "quiet async method with unit")
  fun quietAsyncUnit(): Future<Unit>

  @GET
  @Path("/quiet-unit")
  @BraidMethod(value = "quiet sync method with unit")
  fun quietUnit()

  @GET
  @Path("/complex-list-async")
  fun asyncListOfComplex(): Future<List<ComplexObject>>

  @POST
  @Path("/echo")
  @BraidMethod(value = "... and the bunny men")
  @Operation(summary = "echo echo", security = [SecurityRequirement(name = BRAID_AUTHORIZATION_SCHEME_NAME)])
  fun echo(msg: String): String

  @GET
  @Path("/buffer")
  @BraidMethod(value = "every railway needs them")
  fun getBuffer(): Buffer

  @GET
  @Path("/bytearray")
  @BraidMethod(value = "for those moments that corda sees it fit to return binary payloads")
  fun getByteArray(): ByteArray

  @GET
  @Path("/bytebuf")
  @BraidMethod(value = "netty buffer")
  fun getByteBuf(): ByteBuf

  @GET
  @Path("/bytebuffer")
  @BraidMethod(value = "nio buffer")
  fun getByteBuffer(): ByteBuffer

  @POST
  @Path("/doublebuffer")
  @BraidMethod(value = "vertx buffers")
  fun doubleBuffer(bytes: Buffer): Buffer

  @POST
  @Path("/custom/{name}")
  @Operation(
    summary = "do something custom",
    responses = [ApiResponse(content = [Content(schema = Schema(implementation = String::class))])]
  )
  @Parameters(
    Parameter(
      name = "name",
      description = "name parameter",
      `in` = ParameterIn.PATH,
      schema = Schema(implementation = String::class, defaultValue = "Margaret"),
      required = true,
      examples = [
        ExampleObject(name = "Satoshi", value = "Satoshi"),
        ExampleObject(name = "Margaret", value = "Margaret"),
        ExampleObject(name = "Alan", value = "Alan")
      ]
    )
  )
  @JsonRpcIgnore // json-rpc doesn't understand Context binding
  fun somethingCustom(@Context rc: RoutingContext)

  @GET
  @Path("/stringlist")
  @Operation(
    summary = "return list of strings",
    responses = [ApiResponse(content = [Content(array = ArraySchema(schema = Schema(implementation = String::class)))])]
  )
  @JsonRpcIgnore
  fun returnsListOfStuff(@Context context: RoutingContext)

  @GET
  @Path("/willfail")
  @BraidMethod(value = "will definitely fail. really")
  fun willFail(): String

  @GET
  @Path("/headers/list/string")
  @BraidMethod(value = "string lists")
  fun headerListOfStrings(@HeaderParam(X_HEADER_LIST_STRING) value: List<String>): List<String>

  @GET
  @Path("/headers/object")
  @BraidMethod(value = "string lists")
  fun headerListOfComplex(@HeaderParam(X_HEADER_OBJECT) value: ComplexObject): ComplexObject

  @GET
  @Path("/headers/list/int")
  @BraidMethod(value = "int lists")
  fun headerListOfInt(@HeaderParam(X_HEADER_LIST_STRING) value: List<Int>): List<Int>

  @GET
  @Path("/headers/optional")
  @BraidMethod(value = "nullable header params")
  fun optionalHeader(@HeaderParam(X_HEADER_STRING) value: String?): String

  @GET
  @Path("/headers/non-optional")
  @BraidMethod(value = "non-nullable header params")
  fun nonOptionalHeader(@HeaderParam(X_HEADER_STRING) value: String): String

  @GET
  @Path("/headers")
  @BraidMethod(value = "context headers")
  @Parameters(
    Parameter(
      `in` = ParameterIn.HEADER,
      name = X_HEADER_LIST_STRING,
      required = true,
      style = ParameterStyle.SIMPLE,
      explode = Explode.FALSE,
      content = [
        Content(mediaType = "text/plain", array = ArraySchema(schema = Schema(implementation = String::class)))
      ]
    )
  )
  fun headers(@Context headers: javax.ws.rs.core.HttpHeaders): List<Int>

  @GET
  @Path("/stream")
  fun stream(): Observable<Int>

  @GET
  @Path("/infinite-stream")
  fun infiniteStream(): Observable<Date>

  @GET
  @Path("/thingWithBigNumber")
  fun thingWithBigNumber(): ThingWithBigDecimal

  @POST
  @Path("/complex-objects")
  fun complexObjects(complexObjects: List<ComplexObject>): List<ComplexObject>

  @GET
  @Path("/now")
  fun now(): Instant

  @GET
  @Path("/duration")
  fun duration(): ThingWithDuration

  // JSON-RPC specific API
  @BraidMethod(value = "")
  fun add(lhs: Double, rhs: Double): Double

  fun noArgs(): Int

  fun noResult()

  fun longRunning(): Future<Int>

  fun largelyNotStream(): Observable<Int>
  fun echoComplexObject(inComplexObject: ComplexObject): ComplexObject
  fun stuffedJsonObject(): JsonStuffedObject
  fun blowUp()
  fun exposeParameterListTypeIssue(str: String, md: ModelData): ModelData
  fun functionWithTheSameNameAndNumberOfParameters(amount: String, accountId: String): Int
  fun functionWithTheSameNameAndNumberOfParameters(amount: BigDecimal, accountId: String): Int
  fun functionWithTheSameNameAndNumberOfParameters(amount: BigDecimal, accountId: BigDecimal): Int
  fun functionWithTheSameNameAndNumberOfParameters(amount: Long, accountId: String): Int
  fun functionWithTheSameNameAndNumberOfParameters(amount: Int, accountId: String): Int
  fun functionWithTheSameNameAndNumberOfParameters(amount: Float, accountId: String): Int
  fun functionWithTheSameNameAndNumberOfParameters(amount: Double, accountId: String): Int
  fun functionWithTheSameNameAndNumberOfParameters(amount: ComplexObject?, accountId: String): Int
  fun functionWithTheSameNameAndNumberOfParameters(amount: List<String>, accountId: String): Int
  fun functionWithTheSameNameAndNumberOfParameters(amount: Array<String>, accountId: String): Int
  fun functionWithTheSameNameAndASingleParameter(amount: String?): Int
  fun functionWithTheSameNameAndASingleParameter(amount: Long): Int
  fun functionWithTheSameNameAndASingleParameter(amount: Int): Int
  fun functionWithTheSameNameAndASingleParameter(amount: Double): Int
  fun functionWithTheSameNameAndASingleParameter(amount: Float): Int
  fun functionWithTheSameNameAndASingleParameter(amount: ComplexObject): Int
  fun functionWithTheSameNameAndASingleParameter(amount: List<String>): Int
  fun functionWithTheSameNameAndASingleParameter(amount: Array<String>): Int
  fun functionWithBigDecimalParameters(amount: BigDecimal, anotherAmount: BigDecimal): Int
  fun functionWithComplexOrDynamicType(value: ComplexObject): Int
  fun functionWithComplexOrDynamicType(value: Map<String, Any>): Int

  @GET
  @Path("/fruit")
  fun fruits(): FruitBox

  @GET
  @Path("/returns-map")
  fun namedFruit(): Map<String, Fruit>

  @GET
  @Path("/returns-future-map")
  fun futureNamedFruit(): Future<Map<String, Fruit>>
}

data class ComplexObject(val a: String, val b: Int, val c: Double)
data class ThingWithBigDecimal(val amount: BigDecimal)
data class JsonStuffedObject(val a: String) {
  val b: String
    get() = a
}
data class ThingWithDuration(val duration: Duration)
@JsonTypeInfo(
  use = JsonTypeInfo.Id.NAME,
  include = JsonTypeInfo.As.PROPERTY,
  property = "_type",
  visible = true
)
@JsonSubTypes(JsonSubTypes.Type(value = MeteringModelData::class, name = "MeteringModelData"))
interface ModelData
data class MeteringModelData(val someString: String) : ModelData

data class FruitBox(val fruits: List<Fruit>)

@JsonTypeInfo(
  use = JsonTypeInfo.Id.NAME,
  include = JsonTypeInfo.As.PROPERTY,
  property = "_type",
  visible = true
)
@JsonSubTypes(
  JsonSubTypes.Type(value = Apple::class, name = "Apple"),
  JsonSubTypes.Type(value = Orange::class, name = "Orange")
)
//@Schema(subTypes = [Apple::class, Orange::class], discriminatorProperty = "_type", description = "Super type of all fruit")
interface Fruit {

  val name: String
}

class Orange : Fruit {

  override val name: String
    get() = "orange"
}

data class Apple(override val name: String = "Apple") : Fruit