/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.jsonrpc

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.async.getOrThrow
import org.junit.After
import org.junit.Test
import kotlin.test.assertTrue

class ParameterTests {
  data class Receiver(val params: Any)
  data class SenderArray(val params: List<Int>)
  data class SenderObject(val params: Params)
  data class Params(val name: String, val age: Int)

  private val context = BraidContext.create()

  @After
  fun after() {
    context.close().getOrThrow()
  }

  @Test
  fun deserialiseList() {
    val encoded = context.encode(SenderArray(listOf(1, 2, 3)))
    val decoded = context.decodeValue(encoded, Receiver::class.java)
    assertTrue(decoded.params is List<*>)
  }

  @Test
  fun deserialiseMap() {
    val encoded = context.encode(SenderObject(Params("Fred", 40)))
    val decoded = context.decodeValue(encoded, Receiver::class.java)
    assertTrue(decoded.params is Map<*, *>)
  }
}
