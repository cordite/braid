/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi

import io.cordite.braid.core.annotation.BraidMethod
import io.cordite.braid.core.annotation.JsonRpcIgnore
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.http.HttpServerConfig
import io.cordite.braid.core.http.body
import io.cordite.braid.core.http.futureGet
import io.cordite.braid.core.http.verifyNoError
import io.cordite.braid.core.json.JsonQuery.SLASH
import io.cordite.braid.core.json.JsonQuery.query
import io.cordite.braid.core.server.BraidServer
import io.cordite.braid.core.socket.findFreePort
import io.cordite.braid.core.testing.X_HEADER_LIST_STRING
import io.netty.handler.codec.http.HttpHeaderValues
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.Parameters
import io.swagger.v3.oas.annotations.enums.Explode
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.enums.ParameterStyle
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.http.HttpHeaders
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import org.junit.After
import org.junit.Test
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.QueryParam
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import kotlin.test.assertEquals

class CustomServiceEndpointTest {

  private val braidContext = BraidContext.create()
  private val port = findFreePort()
  private val service = CustomServiceApp(braidContext, port)
  private val client =
    braidContext.vertx.createHttpClient(HttpClientOptions().setDefaultPort(port).setDefaultHost("localhost"))

  @After
  fun after() {
    client.close()
    service.stop()
    braidContext.close().getOrThrow()
  }

  @Test
  fun `that custom service works`() {
    val swagger = getSwagger()
    val operation = swagger.query<JsonObject>(braidContext, "/paths/${SLASH}custom${SLASH}{name}/post")!!

    val expectedOperation = """
      {
          "tags": [
              "custom-service"
          ],
          "summary": "do something custom",
          "description": "",
          "operationId": "post[Custom, {name}]",
          "parameters": [
              {
                  "name": "name",
                  "in": "path",
                  "description": "name parameter",
                  "required": true,
                  "deprecated": false,
                  "allowEmptyValue": false,
                  "explode": false,
                  "allowReserved": false,
                  "schema": {
                      "type": "string",
                      "default": "Margaret"
                  },
                  "examples": {
                      "Satoshi": {
                          "value": "Satoshi"
                      },
                      "Margaret": {
                          "value": "Margaret"
                      },
                      "Alan": {
                          "value": "Alan"
                      }
                  }
              }
          ],
          "responses": {
              "200": {
                  "description": "description not given",
                  "content": {
                      "": {
                          "schema": {
                              "type": "string"
                          }
                      }
                  }
              }
          }
      }
    """.trimIndent()
    val expectedJsonObject = JsonObject(expectedOperation)
    assertEquals(expectedJsonObject, operation)
  }

  @Test
  fun `that operation defined by bespoke annotations work`() {
    val swagger = getSwagger()
    val operation = swagger.query<JsonObject>(braidContext, "/paths/${SLASH}query/get")!!
    val expectedOperation = """
      {
          "tags": [
              "custom-service"
          ],
          "summary": "query",
          "description": "dummy query",
          "operationId": "get[Query]",
          "parameters": [
              {
                  "name": "hostAndPort",
                  "in": "query",
                  "description": "host and port",
                  "required": false,
                  "deprecated": false,
                  "allowEmptyValue": true,
                  "explode": false,
                  "allowReserved": false,
                  "schema": {
                      "type": "string",
                      "nullable": true,
                      "default": "localhost:8080"
                  }
              },
              {
                  "name": "x500Name",
                  "in": "query",
                  "description": "a x500 name",
                  "required": false,
                  "deprecated": false,
                  "allowEmptyValue": true,
                  "explode": false,
                  "allowReserved": false,
                  "schema": {
                      "type": "string",
                      "nullable": true,
                      "default": "some-value"
                  }
              }
          ],
          "responses": {
              "200": {
                  "description": "description not given"
              }
          }
      }
    """.trimIndent()

    val expectedJsonObject = JsonObject(expectedOperation)
    assertEquals(expectedJsonObject, operation)
  }

  @Test
  fun `that operation with query parameters that are partly annotated are correctly combined`() {
    val swagger = getSwagger()
    val operation = swagger.query<JsonObject>(braidContext, "/paths/${SLASH}headers/get")!!
    val expectedOperation = """
{
    "tags": [
        "custom-service"
    ],
    "summary": "context headers",
    "description": "",
    "operationId": "get[Headers]",
    "parameters": [
        {
            "name": "x-list-string",
            "in": "header",
            "required": true,
            "deprecated": false,
            "allowEmptyValue": false,
            "style": "simple",
            "explode": false,
            "allowReserved": false,
            "schema": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            }
        }
    ],
    "responses": {
        "200": {
            "description": "description not given",
            "content": {
                "application/json": {
                    "schema": {
                        "type": "array",
                        "nullable": false,
                        "items": {
                            "type": "integer",
                            "format": "int32"
                        }
                    }
                }
            }
        }
    }
}
    """.trimIndent()

    assertEquals(JsonObject(expectedOperation), operation)
  }

  private fun getSwagger(): JsonObject {
    return client
      .futureGet(path = "http://localhost:$port/openapi/swagger.json")
      .map { it.verifyNoError() }
      .compose { it.body() }
      .map { JsonObject(it) }
      .getOrThrow()
  }

}

class CustomService(private val braidContext: BraidContext) {

  @POST
  @Path("/custom/{name}")
  @Operation(
    summary = "do something custom",
    responses = [ApiResponse(
      responseCode = "200",
      content = [Content(schema = Schema(implementation = String::class))]
    )]
  )
  @Parameters(
    Parameter(
      name = "name",
      description = "name parameter",
      `in` = ParameterIn.PATH,
      schema = Schema(implementation = String::class, defaultValue = "Margaret"),
      required = true,
      examples = [
        ExampleObject(name = "Satoshi", value = "Satoshi"),
        ExampleObject(name = "Margaret", value = "Margaret"),
        ExampleObject(name = "Alan", value = "Alan")
      ]
    )
  )
  @JsonRpcIgnore // json-rpc doesn't understand Context binding
  fun somethingCustom(@Context rc: RoutingContext) {
    val name = rc.request().getParam("name") ?: "Margaret"
    rc.response().putHeader(HttpHeaders.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON)
      .setChunked(true).end(braidContext.encode("Hello, $name!"))
  }

  @GET
  @Path("/headers")
  @BraidMethod(value = "context headers")
  @Parameters(
    Parameter(
      `in` = ParameterIn.HEADER,
      name = X_HEADER_LIST_STRING,
      required = true,
      style = ParameterStyle.SIMPLE,
      explode = Explode.FALSE,
      content = [
        Content(mediaType = "text/plain", array = ArraySchema(schema = Schema(implementation = String::class)))
      ]
    )
  )
  fun headers(@Context headers: javax.ws.rs.core.HttpHeaders): List<Int> {
    val acceptableLanguages = headers.acceptableLanguages
    assert(acceptableLanguages.size == 1 && acceptableLanguages.first().language == "*")
    val acceptableMediaTypes = headers.acceptableMediaTypes
    assert(acceptableMediaTypes.size == 1 && acceptableMediaTypes.first() == MediaType.WILDCARD_TYPE)
    val cookies = headers.cookies
    assert(cookies.isEmpty())
    val date = headers.date
    assert(date == null)
    val language = headers.language
    assert(language == null)
    val length = headers.length
    assert(length == -1)
    val mediaType = headers.mediaType
    assert(mediaType == null)

    return headers.getRequestHeader(X_HEADER_LIST_STRING).map { it.toInt() }
  }

  @Suppress("UNUSED_PARAMETER")
  @GET
  @Path("/query")
  @Operation(
    summary = "query",
    description = "dummy query"
  )
  @JsonRpcIgnore
  fun nodes(
    @QueryParam(value = "hostAndPort")
    @Parameter(
      name = "hostAndPort",
      description = "host and port",
      required = false,
      allowEmptyValue = true,
      `in` = ParameterIn.QUERY,
      schema = Schema(implementation = String::class, defaultValue = "localhost:8080")
    )
    hostAndPort: String? = null,
    @QueryParam(value = "x500Name")
    @Parameter(
      name = "x500Name",
      description = "a x500 name",
      required = false,
      allowEmptyValue = true,
      `in` = ParameterIn.QUERY,
      schema = Schema(implementation = String::class, defaultValue = "some-value")
    )
    x500Name: String? = null
  ) {
  }
}

class CustomServiceApp(braidContext: BraidContext, port: Int) {
  companion object {

    @JvmStatic
    fun main(args: Array<String>) {
      CustomServiceApp(BraidContext.create(), 8080)
    }
  }

  private var braidServer: BraidServer? = BraidConfig()
    .withPort(port)
    .withService(braidContext, CustomService(braidContext))
    .withHttpServerConfig(
      HttpServerConfig(tlsEnabled = false)
    )
    .amendRestConfig {
      withDebugMode()
    }.start(braidContext).getOrThrow()

  fun stop() {
    if (braidServer != null) {
      braidServer!!.stop().getOrThrow()
      braidServer = null
    }
  }
}
