/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.jsonschema

import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.async.getOrThrow
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import org.junit.After
import org.junit.Test
import kotlin.test.assertEquals

class TestJacksonSchemas {

  private val braidContext = BraidContext.create()

  @After
  fun after() {
    braidContext.close().getOrThrow()
  }

  @Test
  fun test() {
    assertEquals(
      json { obj("type" to "integer") },
      describeClass(braidContext, Int::class.java)
    )
    assertEquals(json { obj("type" to "string") }, describeClass(braidContext, String::class.java))
    assertEquals(
      json {
        obj(
          "name" to json {
            obj(
              "type" to "string",
              "required" to true
            )
          },
          "address" to json {
            obj(
              "houseNumber" to json {
                obj(
                  "type" to "string",
                  "required" to true
                )
              },
              "postcode" to json {
                obj(
                  "type" to "string",
                  "required" to true
                )
              }
            )
          }
        )
      },
      describeClass(braidContext, Customer::class.java)
    )
  }
}

class Customer(val name: String, val address: Address)
class Address(val houseNumber: String, val postcode: String)