/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi

import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.http.HttpServerConfig
import io.cordite.braid.core.http.bodyAsString
import io.cordite.braid.core.http.endWithUtf8
import io.cordite.braid.core.http.fetch
import io.cordite.braid.core.socket.findFreePort
import io.vertx.ext.web.RoutingContext
import org.junit.After
import org.junit.Test
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class RestMounterTest {
  companion object {

    private const val rootPath = "/my-root"
    private const val customOpenApiPath = "$rootPath/openapi"
    private const val customRestPath = "$rootPath/rest"
    private const val appHtml = "<html><body>The App</body></html>"
  }

  private val braidContext = BraidContext.create()
  private val client = braidContext.vertx.createHttpClient()

  private val port = findFreePort()
  val braidServer = BraidConfig()
    .withPort(port)
    .withHttpServerConfig(HttpServerConfig(tlsEnabled = false))
    .amendRestConfig {
      this
        .withOpenApiPath(customOpenApiPath)
        .withRestPath(customRestPath)
        .withPaths {
          unprotected {
            get("/say-hello", this@RestMounterTest::hello)
            router { // raw router - anything goes
              get("/*").order(Int.MAX_VALUE).handler(this@RestMounterTest::serveHtml)
            }
          }
        }
    }.start(braidContext).getOrThrow()

  @After
  fun after() {
    client.close()
    braidServer.stop().getOrThrow()
  }

  @Test
  fun `that we can provide custom bindings`() {
    val rootResponse = client.fetch("http://localhost:$port/", braidContext).compose { it.bodyAsString() }.getOrThrow()
    assertEquals(appHtml, rootResponse)
  }

  @Test
  fun `that we can get the openapi web app`() {
    val openApiWeb = client.fetch("http://localhost:$port${customOpenApiPath}/", braidContext)
      .compose { it.bodyAsString() }
      .getOrThrow()
    assertTrue(openApiWeb.contains("swagger-ui.css"))
  }

  @Test
  fun `that we can get the openapi swagger file`() {
    val openApiWeb = client.fetch("http://localhost:$port${customOpenApiPath}/swagger.json", braidContext)
      .compose { it.bodyAsString() }
      .getOrThrow()
    assertTrue(openApiWeb.contains("openapi"))
  }

  @Test
  fun `that we can call the rest method`() {
    val openApiWeb = client.fetch("http://localhost:$port${customRestPath}/say-hello", braidContext)
      .compose { it.bodyAsString() }
      .getOrThrow()
    assertEquals("hello", openApiWeb)
  }

  private fun serveHtml(@Context routingContext: RoutingContext) {
    routingContext.endWithUtf8(appHtml, MediaType.TEXT_HTML)
  }

  fun hello(): String = "hello"
}
