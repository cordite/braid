/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.parser

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.parser.parsers.QueryParameterParser
import io.vertx.core.http.HttpServerRequest
import io.vertx.ext.web.RoutingContext
import org.junit.After
import org.junit.Test
import java.net.URLEncoder
import kotlin.reflect.KCallable
import kotlin.reflect.full.valueParameters
import kotlin.test.assertEquals

class QueryParameterParserTest {

  private val braidContext = BraidContext.create()
  private val service = DummyParameterTestService()

  @After
  fun after() {
    braidContext.close().getOrThrow()
  }

  @Test
  fun `parse query for integer parameter`() {
    val headerValue = 23
    val result: Int = setQueryAndParse(service::takesIntQueryParam, headerValue)
    assertEquals(headerValue, result)
  }

  @Test
  fun `parse query for complex type parameter`() {
    val value = ComplexType(3.0, 2.0)
    val encoded = braidContext.encode(value)
    val result: ComplexType = setQueryAndParse(service::takesComplexTypeQueryParam, encoded)
    assertEquals(value, result)
  }

  @Test
  fun `parse query serializable to string type`() {
    val name = "fuzz"
    val expected = StringSerializable(name)
    val result: StringSerializable = setQueryAndParse(service::takesStringSerializableQueryParam, name)
    assertEquals(expected, result)
  }

  @Test
  fun `parse query to list of StringSerializable`() {
    val names = arrayOf("fuzz", "ben", "mark", "aime", "ramiz", "richard")
    val expected = names.map { StringSerializable(it) }
    val result: List<StringSerializable> = setQueryAndParse(service::takesListOfStringSerializableQueryParam, *names)
    assertEquals(expected, result)
  }

  @Suppress("UNCHECKED_CAST")
  private fun <R> setQueryAndParse(callable: KCallable<*>, vararg headerValues: Any): R {
    val routingContext = createRoutingContext(headerValues.toList())
    val param = callable.valueParameters.first()
    val parser = QueryParameterParser(braidContext, routingContext)
    val parseResult = parser.parse(callable, param, 0)
    return parseResult.value as R
  }

  private fun <T> createRoutingContext(queryValues: List<T>): RoutingContext {
    val query = queryValues.map { URLEncoder.encode(it.toString(), "UTF-8") }.joinToString("&") { "value=$it" }

    val request = mock<HttpServerRequest> {
      on { query() } doReturn query
    }
    return mock {
      on { request() } doReturn request
    }
  }

}

