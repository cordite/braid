/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.parser

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.parser.parsers.HeaderParameterParser
import io.vertx.core.MultiMap
import io.vertx.core.http.HttpServerRequest
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import org.junit.After
import org.junit.Test
import kotlin.reflect.KCallable
import kotlin.reflect.full.valueParameters
import kotlin.test.assertEquals
import kotlin.test.assertNull

class HeaderParameterParserTest {

  private val braidContext = BraidContext.create()
  private val service = DummyParameterTestService()

  @After
  fun after() {
    braidContext.close().getOrThrow()
  }

  @Test
  fun `parse headers for integer parameter`() {
    val headerValue = 23
    val result: Int = setHeaderAndParse(service::takesIntHeader, headerValue)
    assertEquals(headerValue, result)
  }

  @Test
  fun `parse headers for optional string parameter`() {
    val result: String? = setHeaderAndParse(service::takesOptionalHeader)
    assertNull(result)
  }

  @Test
  fun `parse headers for complex type parameter`() {
    val value = ComplexType(3.0, 2.0)
    val encoded = braidContext.encode(value)
    val headerValue = JsonObject(encoded).map.map { entry -> "${entry.key},${entry.value}" }.joinToString(",")
    val result: ComplexType = setHeaderAndParse(service::takesComplexTypeHeader, headerValue)
    assertEquals(value, result)
  }

  @Test
  fun `parse headers serializable to string type`() {
    val name = "fuzz"
    val expected = StringSerializable(name)
    val result: StringSerializable = setHeaderAndParse(service::takesStringSerializableHeader, name)
    assertEquals(expected, result)
  }

  @Test
  fun `parse headers to list of StringSerializable`() {
    val names = arrayOf("fuzz", "ben", "mark", "aime", "ramiz", "richard")
    val expected = names.map { StringSerializable(it) }
    val result: List<StringSerializable> = setHeaderAndParse(service::takesListOfStringSerializableHeader, *names)
    assertEquals(expected, result)
  }

  @Suppress("UNCHECKED_CAST")
  private fun <R> setHeaderAndParse(callable: KCallable<*>, vararg headerValues: Any): R {
    val routingContext = createRoutingContext(headerValues.toList())
    val param = callable.valueParameters.first()
    val parser = HeaderParameterParser(braidContext, routingContext)
    val parseResult = parser.parse(callable, param, 0)
    return parseResult.value as R
  }

  private fun <T> createRoutingContext(headerValue: List<T>): RoutingContext {
    val mm = MultiMap.caseInsensitiveMultiMap()
    headerValue.map { it.toString() }.forEach { mm.add("value", it) }

    val request = mock<HttpServerRequest> {
      on { headers() } doReturn mm
    }
    return mock {
      on { request() } doReturn request
    }
  }

}

