/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.testing

import io.cordite.braid.core.async.withPromise
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.jaxrs.BraidWebApplicationException
import io.netty.buffer.ByteBuf
import io.netty.handler.codec.http.HttpHeaderValues
import io.vertx.core.Future
import io.vertx.core.Future.succeededFuture
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpHeaders
import io.vertx.ext.web.RoutingContext
import rx.Observable
import rx.schedulers.Schedulers
import java.math.BigDecimal
import java.nio.ByteBuffer
import java.time.Duration
import java.time.Instant
import java.util.*
import javax.ws.rs.HeaderParam
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

class TestServiceImpl(private val braidContext: BraidContext) : TestService {

  private val vertx = braidContext.vertx

  override fun sayHello() = "hello"
  override fun sayHelloAsync(): Future<String> = succeededFuture("hello")
  override fun quietAsyncVoid(): Future<Void> = succeededFuture()
  override fun quietAsyncUnit(): Future<Unit> = succeededFuture()
  override fun quietUnit(): Unit = Unit
  override fun asyncListOfComplex(): Future<List<ComplexObject>> = succeededFuture(listOf(ComplexObject("complex", 1, 2.0)))
  override fun echo(msg: String) = "echo: $msg"
  override fun getBuffer(): Buffer = Buffer.buffer("hello")
  override fun getByteArray(): ByteArray = Buffer.buffer("hello").bytes
  override fun getByteBuf(): ByteBuf = Buffer.buffer("hello").byteBuf
  override fun getByteBuffer(): ByteBuffer = Buffer.buffer("hello").byteBuf.nioBuffer()
  override fun doubleBuffer(bytes: Buffer): Buffer =
    Buffer.buffer(bytes.length() * 2)
      .appendBytes(bytes.bytes)
      .appendBytes(bytes.bytes)

  override fun somethingCustom(rc: RoutingContext) {
    val name = rc.request().getParam("name") ?: "Margaret"
    rc.response().putHeader(HttpHeaders.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON)
      .setChunked(true).end(braidContext.encode("Hello, $name!"))
  }

  override fun returnsListOfStuff(context: RoutingContext) {
    context.response()
      .putHeader(HttpHeaders.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON)
      .setChunked(true).end(braidContext.encode(listOf("one", "two")))
  }

  override fun willFail(): String = throw BraidWebApplicationException("total fail!", Response.Status.BAD_REQUEST)
  override fun headerListOfStrings(@HeaderParam(X_HEADER_LIST_STRING) value: List<String>): List<String> = value
  override fun headerListOfComplex(value: ComplexObject): ComplexObject = value
  override fun headerListOfInt(@HeaderParam(X_HEADER_LIST_STRING) value: List<Int>): List<Int> = value
  override fun optionalHeader(@HeaderParam(X_HEADER_STRING) value: String?): String = value ?: "null"
  override fun nonOptionalHeader(@HeaderParam(X_HEADER_STRING) value: String): String = value
  override fun headers(@Context headers: javax.ws.rs.core.HttpHeaders): List<Int> {
    val acceptableLanguages = headers.acceptableLanguages
    assert(acceptableLanguages.size == 1 && acceptableLanguages.first().language == "*")
    val acceptableMediaTypes = headers.acceptableMediaTypes
    assert(acceptableMediaTypes.size == 1 && acceptableMediaTypes.first() == MediaType.WILDCARD_TYPE)
    val cookies = headers.cookies
    assert(cookies.isEmpty())
    val date = headers.date
    assert(date == null)
    val language = headers.language
    assert(language == null)
    val length = headers.length
    assert(length == -1)
    val mediaType = headers.mediaType
    assert(mediaType == null)

    return headers.getRequestHeader(X_HEADER_LIST_STRING).map { it.toInt() }
  }

  override fun add(lhs: Double, rhs: Double): Double {
    return lhs + rhs
  }

  override fun noArgs(): Int {
    return 5
  }

  override fun noResult() {}

  override fun longRunning(): Future<Int> {
    return withPromise<Int> { promise ->
      vertx.setTimer(100) {
        promise.complete(5)
      }
    }.future()
  }

  override fun stream(): Observable<Int> {
    return Observable.from(0..10)
  }

  override fun infiniteStream(): Observable<Date> {
    val o = Observable.unsafeCreate<Date> {
      while (!it.isUnsubscribed) {
        it.onNext(Date())
        Thread.sleep(1000)
      }
    }
    return o.subscribeOn(Schedulers.computation())
  }

  override fun largelyNotStream(): Observable<Int> {
    return Observable.error(RuntimeException("stream error"))
  }

  override fun echoComplexObject(inComplexObject: ComplexObject): ComplexObject {
    return inComplexObject
  }

  override fun blowUp() {
    throw RuntimeException("expected exception")
  }

  override fun stuffedJsonObject(): JsonStuffedObject {
    return JsonStuffedObject("this is hosed")
  }

  override fun exposeParameterListTypeIssue(str: String, md: ModelData): ModelData {
    return md
  }

  override fun functionWithTheSameNameAndNumberOfParameters(
    amount: String,
    accountId: String
  ): Int = 1

  override fun functionWithTheSameNameAndNumberOfParameters(
    amount: BigDecimal,
    accountId: String
  ): Int = 2

  override fun functionWithTheSameNameAndNumberOfParameters(amount: BigDecimal, accountId: BigDecimal): Int = 3
  override fun functionWithTheSameNameAndNumberOfParameters(amount: Long, accountId: String): Int = 4
  override fun functionWithTheSameNameAndNumberOfParameters(amount: Int, accountId: String): Int = 5
  override fun functionWithTheSameNameAndNumberOfParameters(amount: Float, accountId: String): Int = 6
  override fun functionWithTheSameNameAndNumberOfParameters(amount: Double, accountId: String): Int = 7
  override fun functionWithTheSameNameAndNumberOfParameters(amount: ComplexObject?, accountId: String): Int = 8
  override fun functionWithTheSameNameAndNumberOfParameters(amount: List<String>, accountId: String): Int = 9
  override fun functionWithTheSameNameAndNumberOfParameters(amount: Array<String>, accountId: String): Int = 11
  override fun functionWithTheSameNameAndASingleParameter(amount: String?): Int = 12
  override fun functionWithTheSameNameAndASingleParameter(amount: Long): Int = 13
  override fun functionWithTheSameNameAndASingleParameter(amount: Int): Int = 14
  override fun functionWithTheSameNameAndASingleParameter(amount: Double): Int = 15
  override fun functionWithTheSameNameAndASingleParameter(amount: Float): Int = 16
  override fun functionWithTheSameNameAndASingleParameter(amount: ComplexObject): Int = 17
  override fun functionWithTheSameNameAndASingleParameter(amount: List<String>): Int = 18
  override fun functionWithTheSameNameAndASingleParameter(amount: Array<String>): Int = 20
  override fun functionWithBigDecimalParameters(amount: BigDecimal, anotherAmount: BigDecimal): Int = 21
  override fun functionWithComplexOrDynamicType(value: ComplexObject) = 22
  override fun functionWithComplexOrDynamicType(value: Map<String, Any>) = 23
  override fun thingWithBigNumber(): ThingWithBigDecimal = ThingWithBigDecimal(BigDecimal("42.01"))
  override fun complexObjects(complexObjects: List<ComplexObject>): List<ComplexObject> = complexObjects
  override fun now(): Instant = Instant.now()
  override fun duration(): ThingWithDuration = ThingWithDuration(Duration.ofDays(10))
  override fun fruits(): FruitBox = FruitBox(listOf(Orange(), Apple()))
  override fun namedFruit(): Map<String, Fruit> = mapOf("apple" to Apple(), "orange" to Orange())
  override fun futureNamedFruit(): Future<Map<String, Fruit>> = succeededFuture(namedFruit())
}