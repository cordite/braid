/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.http

import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.router.Routers
import io.vertx.core.Promise
import io.vertx.core.Vertx
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.http.HttpServerOptions
import org.bouncycastle.asn1.x500.X500Name
import org.junit.After
import org.junit.Test
import java.net.ServerSocket
import java.security.cert.X509Certificate
import java.util.concurrent.CountDownLatch

class CertsToJksOptionsConverterTest {
  companion object {

    val log = loggerFor<CertsToJksOptionsConverterTest>()
  }

  private val vertx = Vertx.vertx()

  @After
  fun after() {
    vertx.close()
  }

  @Test(timeout = Long.MAX_VALUE)
  fun testWithFiles() {
    `validate that we can load the certificate and key and serve a http request`(
      "src/test/resources/tls.crt",
      "src/test/resources/tls.key"
    )
    `validate that we can load the certificate and key and serve a http request`(
      "src/test/resources/cert.pem",
      "src/test/resources/private.key"
    )
  }

  private fun `validate that we can load the certificate and key and serve a http request`(cert: String, key: String) {
    log.info("validating that we can load the certificate and key and serve a http request for\ncert: $cert\nand key: $key")
    val port = ServerSocket(0).use { it.localPort }
    val converter = CertsToJksOptionsConverter(cert, key)
    val keyStore = converter.keyStore
    val cc = keyStore.aliases().asSequence()
      .flatMap { keyStore.getCertificateChain(it).asSequence() }
      .filter { it != null }
      .map { it as X509Certificate }
      .map { X500Name(it.subjectDN.name) }.toList()
    val jksOptions = converter.createJksOptions()
    val router = Routers.create(vertx, port)
    router.get("/").handler {
      log.info("GET / called. Responding")
      it.response().setChunked(true).end("Hello")
    }
    val readyWebServer = Promise.promise<Unit>()

    val server = vertx.createHttpServer(
      HttpServerOptions()
        .setSsl(true)
        .setKeyStoreOptions(jksOptions)
    )
      .requestHandler(router)
      .listen(port) {
        if (it.failed()) {
          readyWebServer.fail(it.cause())
        } else {
          readyWebServer.complete()
        }
      }

    readyWebServer.future().getOrThrow()

    log.info("invoking http request")
    val client = vertx.createHttpClient(
      HttpClientOptions()
        .setSsl(true)
        .setTrustAll(true)
        .setTrustStoreOptions(jksOptions)
        .setVerifyHost(false)
    )

    val operationCompleted = CountDownLatch(2)
    val certificateLoadedPromise = Promise.promise<Unit>()
    val responseReceivedPromise = Promise.promise<Unit>()

    @Suppress("DEPRECATION")
    client.get(port, "127.0.0.1", "/") {
      responseReceivedPromise.complete()
    }.connectionHandler {
      log.debug("connection handler called - checking if certificate contains name")
      val found = try {
        val peerChain = it.peerCertificateChain()
        peerChain.all { cert ->
          val name = X500Name(cert.subjectDN.name)
          cc.contains(name)
        }
      } catch (err: Throwable) {
        log.error("failed to check peer chain", err)
        false
      }
      log.debug("certificate located: $found")
      if (!found) {
        certificateLoadedPromise.fail("matching server certificate names to source names")
      } else {
        certificateLoadedPromise.complete()
      }
    }.exceptionHandler {
      if (operationCompleted.count > 0) {
        log.error("exception in http request", it)
        certificateLoadedPromise.fail("exception in http request: ${it.message}")
        throw it
      }
    }.end()

    certificateLoadedPromise.future().getOrThrow()
    responseReceivedPromise.future().getOrThrow()
    client.close()
    server.close()
  }
}