/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.swaggermodel

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.util.StdConverter
import io.cordite.braid.core.context.BraidContext
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.models.media.ArraySchema
import io.swagger.v3.oas.models.media.ObjectSchema
import io.swagger.v3.oas.models.media.StringSchema
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

// Fruit use-case types
@JsonSerialize(converter = FruitToStringConverter::class)
@JsonDeserialize(converter = StringToFruitConverter::class)
interface Fruit {

  val name: String

  class Apple() : Fruit {

    override val name: String = "apple"
  }

  class Orange : Fruit {

    override val name: String = "orange"
  }

  object StrangeFruit : Fruit {

    override val name: String = "strange"
  }
}

class FruitToStringConverter : StdConverter<Fruit, String>() {

  override fun convert(value: Fruit): String {
    return value.name
  }
}

class StringToFruitConverter : StdConverter<String, Fruit>() {

  override fun convert(value: String): Fruit {
    return when (value) {
      "apple" -> Fruit.Apple()
      "orange" -> Fruit.Orange()
      else -> Fruit.StrangeFruit
    }
  }
}

data class FruitBox<F : Fruit>(val contents: List<F>)
data class FruitStore(val apples: FruitBox<Fruit.Apple>, val oranges: FruitBox<Fruit.Orange>)

// Expression use-case types
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "_type", include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes(
  JsonSubTypes.Type(AndExpression::class),
  JsonSubTypes.Type(BooleanExpression::class),
  JsonSubTypes.Type(BooleanValueExpression::class)
)
interface Expression

data class ExpressionHolder(
  @Schema(description = "the expression", oneOf = [AndExpression::class, BooleanValueExpression::class])
  val expression: Expression
)

@Schema(description = "and expression", allOf = [BooleanExpression::class])
data class AndExpression(
  @Schema(oneOf = [BooleanValueExpression::class, AndExpression::class])
  val lhs: BooleanExpression,
  @Schema(oneOf = [BooleanValueExpression::class, AndExpression::class])
  val rhs: BooleanExpression
) : BooleanExpression {

  override val value: Boolean get() = lhs.value && rhs.value
}

@Schema(allOf = [Expression::class])
interface BooleanExpression : Expression {

  val value: Boolean
}

@Schema(allOf = [BooleanExpression::class])
data class BooleanValueExpression(override val value: Boolean) : BooleanExpression

data class Request(val participants: List<String>, val metaData: Any? = null)

class JacksonSerializerModelResolverTest {

  val context = BraidContext.create()

  @After
  fun after() {
    context.close()
  }

  @Test
  fun `that we can generate correct schema for fruit store`() {
    val resolvedSchema = context.modelConverters.readAllAsResolvedSchema(FruitStore::class.java)
    assertEquals(FruitStore::class.java.canonicalName, resolvedSchema.schema.name)
    val objectMapper = context.getContextObject<ObjectMapper>()
    val fruitStoreSchema = resolvedSchema.referencedSchemas[resolvedSchema.schema.name]!!
    assertTrue(fruitStoreSchema.properties.containsKey("apples"))
    assertTrue(fruitStoreSchema.properties.containsKey("oranges"))
    val appleFruitBoxRef = fruitStoreSchema.properties["apples"]!!.`$ref`
    val appleFruitBoxId = appleFruitBoxRef.drop("#/components/schemas/".count())
    val appleFruitBoxSchema = resolvedSchema.referencedSchemas[appleFruitBoxId]!!
    assertTrue(appleFruitBoxSchema.properties.containsKey("contents"))
    val contentsProperty = appleFruitBoxSchema.properties["contents"]!! as ArraySchema
    assertEquals("contents", contentsProperty.name)
    assertEquals("array", contentsProperty.type)
    assertTrue(contentsProperty.items is StringSchema)
    assertEquals("string", contentsProperty.items.type)
  }

  @Test
  fun `that we can get the schema for expression holder`() {
    val resolvedSchema = context.modelConverters.readAllAsResolvedSchema(ExpressionHolder::class.java)
  }

  @Test
  fun `that we can resolve the request with participants`() {
    val schema = context.getOpenAPISchema(Request::class.java)!!
    schema.properties["participants"].let { property ->
      assertTrue(property is ArraySchema)
      (property as ArraySchema).apply {
        assertEquals("array", type)
        assertTrue(items is StringSchema)
      }
    }
    assertTrue(schema.properties["metaData"] is ObjectSchema)
  }
}
