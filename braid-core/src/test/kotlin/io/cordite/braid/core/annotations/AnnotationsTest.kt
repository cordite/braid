/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.annotations

import io.cordite.braid.core.annotation.AnnotationsCache
import io.cordite.braid.core.annotation.BraidService
import io.cordite.braid.core.annotation.JsonRpcIgnore
import io.swagger.v3.oas.annotations.Operation
import org.junit.Test
import kotlin.reflect.jvm.javaMethod
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@BraidService(name = "DaService", description = "a simple service")
interface AnInterface {

  @JsonRpcIgnore
  @Operation(summary = "a method that should be ignored")
  fun ignoreMe()
}

abstract class AnAbstractClass : AnInterface

class ConcreteClass : AnAbstractClass() {

  override fun ignoreMe() {
    TODO("Not yet implemented")
  }
}

class AnnotationsTest {

  private val annotationsCache = AnnotationsCache()
  @Test
  fun `check that methods with BraidIgnore are ignored`() {
    val annotation = annotationsCache.findAnnotation(ConcreteClass::ignoreMe.javaMethod!!, JsonRpcIgnore::class.java)
    assertNotNull(annotation)
  }

  @Test
  fun `that we can retrieve a BraidService from the hierarchy`() {
    val annotation = annotationsCache.findAnnotation(ConcreteClass::class.java, BraidService::class.java)
    assertNotNull(annotation)
  }

  @Test
  fun `that we can retrieve a BraidService from the hierarchy using Kotlin types`() {
    val annotation = annotationsCache.findAnnotation(ConcreteClass::class, BraidService::class)
    assertNotNull(annotation)
  }

  @Test
  fun `that we can retrieve all annotations on a KCallable method reference`() {
    val annotations = annotationsCache.allMatchingAnnotations(ConcreteClass::ignoreMe)
    assertEquals(1, annotations.filterIsInstance<Operation>().count())
    assertEquals(1, annotations.filterIsInstance<JsonRpcIgnore>().count())
  }
}