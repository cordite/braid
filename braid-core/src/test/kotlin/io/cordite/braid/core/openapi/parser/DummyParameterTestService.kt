/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@file:Suppress("UnresolvedRestParam")

package io.cordite.braid.core.openapi.parser

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.util.StdConverter
import javax.ws.rs.HeaderParam
import javax.ws.rs.PathParam
import javax.ws.rs.QueryParam

@Suppress("UNUSED_PARAMETER")
class DummyParameterTestService {

  fun takesIntHeader(@HeaderParam("value") value: Int) {}
  fun takesComplexTypeHeader(@HeaderParam("value") value: ComplexType) {}
  fun takesStringSerializableHeader(@HeaderParam("value") value: StringSerializable) {}
  fun takesListOfStringSerializableHeader(@HeaderParam("value") value: List<StringSerializable>) {}
  fun takesOptionalHeader(@HeaderParam("value") value: String?) {}

  fun takesIntPathParam(@PathParam("value") value: Int) {}
  fun takesComplexTypePathParam(@PathParam("value") value: ComplexType) {}
  fun takesStringSerializablePathParam(@PathParam("value") value: StringSerializable) {}
  fun takesListOfStringSerializablePathParam(@PathParam("value") value: List<StringSerializable>) {}

  fun takesIntQueryParam(@QueryParam("value") value: Int) {}
  fun takesComplexTypeQueryParam(@QueryParam("value") value: ComplexType) {}
  fun takesStringSerializableQueryParam(@QueryParam("value") value: StringSerializable) {}
  fun takesListOfStringSerializableQueryParam(@QueryParam("value") value: List<StringSerializable>) {}
}

data class ComplexType(val i: Double, val j: Double)

@JsonSerialize(converter = StringSerializableToStringConverter::class)
@JsonDeserialize(converter = StringToStringSerializableConverter::class)
data class StringSerializable(val name: String)

class StringSerializableToStringConverter : StdConverter<StringSerializable, String>() {

  override fun convert(value: StringSerializable): String {
    return value.name
  }
}

class StringToStringSerializableConverter : StdConverter<String, StringSerializable>() {

  override fun convert(value: String): StringSerializable {
    return StringSerializable(value)
  }

}