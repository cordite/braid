/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi.parser

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.parser.parsers.PathParameterParser
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpServerRequest
import io.vertx.ext.web.RoutingContext
import org.junit.After
import org.junit.Test
import kotlin.reflect.KCallable
import kotlin.reflect.full.valueParameters
import kotlin.test.assertEquals

class PathParameterParserTest {

  val braidContext = BraidContext.create()
  private val service = DummyParameterTestService()

  @After
  fun after() {
    braidContext.close().getOrThrow()
  }

  @Test
  fun `parse headers for integer parameter`() {
    val value = 23
    val result: Int = createRoutingContextAndParse(service::takesIntPathParam, value)
    assertEquals(value, result)
  }

  @Test
  fun `parse headers for complex type parameter`() {
    val value = ComplexType(3.0, 2.0)
    val encoded = braidContext.encode(value)
    val result: ComplexType = createRoutingContextAndParse(service::takesComplexTypePathParam, encoded)
    assertEquals(value, result)
  }

  @Test
  fun `parse headers serializable to string type`() {
    val name = "fuzz"
    val result: StringSerializable = createRoutingContextAndParse(service::takesStringSerializablePathParam, name)
    assertEquals(StringSerializable(name), result)
  }

  @Test
  fun `parse headers to list of StringSerializable`() {
    val names = listOf("fuzz", "ben", "mark", "aime", "ramiz", "richard")
    val expected = names.map { StringSerializable(it) }
    val encoded = braidContext.encode(names)
    val result: List<StringSerializable> =
      createRoutingContextAndParse(service::takesListOfStringSerializablePathParam, encoded)
    assertEquals(expected, result)
  }

  @Suppress("UNCHECKED_CAST")
  private fun <R> createRoutingContextAndParse(callable: KCallable<*>, paramValue: Any): R {
    val rc = createRoutingContext(paramValue)
    val parser = PathParameterParser(braidContext, rc)
    val param = callable.valueParameters.first()
    val result = parser.parse(callable, param, 0)
    return result.value as R
  }

  private fun createRoutingContext(paramValue: Any): RoutingContext {
    val routingContextMock = mock<RoutingContext>()
    val requestMock = mock<HttpServerRequest>()
    doReturn(HttpMethod.GET).whenever(requestMock).method()
    doReturn("/path").whenever(requestMock).path()
    doReturn(paramValue.toString()).whenever(routingContextMock).pathParam("value")
    doReturn(requestMock).whenever(routingContextMock).request()
    return routingContextMock
  }
}