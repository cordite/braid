/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.openapi

import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.http.*
import io.cordite.braid.core.json.JsonQuery.escape
import io.cordite.braid.core.json.JsonQuery.query
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.openapi.auth.LoginRequest
import io.cordite.braid.core.openapi.utils.EndPointUtilities.Companion.fullyQualifiedRef
import io.cordite.braid.core.socket.findFreePort
import io.cordite.braid.core.testing.*
import io.vertx.core.Future
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.http.HttpClientResponse
import io.vertx.core.json.JsonObject
import org.junit.After
import org.junit.Before
import org.junit.Test
import javax.ws.rs.core.HttpHeaders
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue

class OpenApiDocumentHandlerTest {
  companion object {

    private val log = loggerFor<OpenApiDocumentHandlerTest>()
  }

  private var swagger: JsonObject? = null
  private val port = findFreePort()
  private val username = "xxxx"
  private val password = "yyyy"
  private val service = TestServiceApp(port, username, password)
  private val braidContext = BraidContext.create()
  private val client =
    braidContext.vertx.createHttpClient(HttpClientOptions().setDefaultHost("localhost").setDefaultPort(port))
  private var authToken: String = ""

  @Before
  fun before() {
    service.whenReady().getOrThrow()
    swagger = retrieveSwagger()
    authToken = authenticate()
  }

  @After
  fun after() {
    client.close()
    braidContext.close().getOrThrow()
  }

  @Test
  fun `that we can pass a list of ints as multiple headers`() {
    val paramDef = swagger!!.query<JsonObject>(braidContext, "/paths/${escape("/headers/list/int")}/get/parameters/0")
      ?: error("failed to extract the parameter definition")

    paramDef.apply {
      assertEquals(X_HEADER_LIST_STRING, getString("name"))
      assertEquals("header", getString("in"))
      assertEquals("simple", getString("style"))
      assertNull(getBoolean("explode"))
    }

    val schema = paramDef.query<JsonObject>(braidContext, "/schema") ?: error("failed to find schema for the parameter")
    schema.apply {
      assertEquals("array", getString("type"))
    }

    schema.getJsonObject("items").apply {
      assertEquals("integer", getString("type"))
      assertEquals("int32", getString("format"))
    }

    val intList = listOf(1, 2, 3)
    val result = get("/headers/list/int", mapOf("x-list-string" to intList))
      .map { it.verifyNoError() }
      .compose { it.bodyAsJsonList<Int>(braidContext) }.getOrThrow()
    assertEquals(intList, result)
  }

  @Test
  fun `that we can pass a complex object as header`() {
    val paramDef = swagger!!.query<JsonObject>(braidContext, "/paths/${escape("/headers/object")}/get/parameters/0")
      ?: error("failed to extract the parameter definition")
    paramDef.run {
      assertEquals(X_HEADER_OBJECT, getString("name"))
      assertEquals("header", getString("in"))
      assertEquals("simple", getString("style"))
      assertNull(getBoolean("explode"))
    }

    val schema = paramDef.query<JsonObject>(braidContext, "/schema") ?: error("failed to find schema for the parameter")
    assertTrue(schema.getString("\$ref").contains(ComplexObject::class.java.name))

    val input = ComplexObject("foo", 5, 0.77)
    val headerValue = convertFlatObjectToHeaderValue(input)
    log.info("about to send headers")
    val result = get("/headers/object", mapOf(X_HEADER_OBJECT to headerValue))
      .map { it.verifyNoError() }
      .compose { it.bodyAsJsonObject<ComplexObject>(braidContext) }.getOrThrow()

    assertEquals(input, result)
  }

  @Test
  fun `that durations are represented as strings`() {
    val type = swagger!!.query<String>(braidContext, "/components/schemas/io.cordite.braid.core.testing.ThingWithDuration/properties/duration/type")
    assertEquals("string", type)
  }

  @Test
  fun `that inheritance of types is denoted correctly`() {
    val fruitReference = braidContext.getOpenAPISchema(Fruit::class.java)?.fullyQualifiedRef!!
    val appleFruitReference = swagger!!.query<String>(braidContext, "/components/schemas/io.cordite.braid.core.testing.Apple/allOf/0/\$ref")
    val orangeFruitReference = swagger!!.query<String>(braidContext, "/components/schemas/io.cordite.braid.core.testing.Orange/allOf/0/\$ref")
    assertEquals(fruitReference, appleFruitReference)
    assertEquals(fruitReference, orangeFruitReference)
  }

  private fun authenticate(): String {
    return client
      .futurePost(
        braidContext = braidContext,
        path = "${TestServiceApp.REST_API_ROOT}/auth/login",
        body = LoginRequest(username, password)
      )
      .getBodyBuffer()
      .map { it.toString() }
      .getOrThrow()
  }

  @Test
  fun `that functions that return Maps correctly reference the value schema`() {
    val returnSchema = swagger!!.query<JsonObject>(
      braidContext,
      "/paths/${escape("/returns-map")}/get/responses/200/content/${escape("application/json")}/schema"
    )!!
    val type = returnSchema.query<String>(braidContext, "/type")
    val schemaReference = returnSchema.query<String>(braidContext, "/additionalProperties/\$ref")
    assertEquals("object", type)
    val fruitReference = braidContext.getOpenAPISchema(Fruit::class.java)?.fullyQualifiedRef!!
    assertEquals(fruitReference, schemaReference)
  }

  @Test
  fun `that functions that return future of Maps correctly reference the value schema`() {
    val returnSchema = swagger!!.query<JsonObject>(
      braidContext,
      "/paths/${escape("/returns-future-map")}/get/responses/200/content/${escape("application/json")}/schema"
    )!!
    val type = returnSchema.query<String>(braidContext, "/type")
    val schemaReference = returnSchema.query<String>(braidContext, "/additionalProperties/\$ref")
    assertEquals("object", type)
    val fruitReference = braidContext.getOpenAPISchema(Fruit::class.java)?.fullyQualifiedRef!!
    assertEquals(fruitReference, schemaReference)
  }

  private fun retrieveSwagger(): JsonObject {
    val map = client
      .futureGet("${TestServiceApp.OPEN_API_ROOT}/swagger.json")
      .compose { it.bodyAsJsonObject<Map<String, Any>>(braidContext) }
      .getOrThrow()
    return JsonObject(map)
  }

  fun get(
    path: String,
    headers: Map<String, Any> = emptyMap(),
    queryParams: Map<String, String> = emptyMap()
  ): Future<HttpClientResponse> {
    return client.futureGet(
      headers = addAuthHeader(headers),
      queryParams = queryParams,
      path = "${TestServiceApp.REST_API_ROOT}$path"
    )
  }

  fun post(
    path: String,
    headers: Map<String, Any> = emptyMap(),
    queryParams: Map<String, String> = emptyMap(),
    body: Any? = null
  ): Future<HttpClientResponse> {
    return client.futurePost(
      braidContext = braidContext,
      headers = addAuthHeader(headers),
      body = body,
      queryParams = queryParams,
      path = "${TestServiceApp.REST_API_ROOT}$path"
    )
  }

  private fun addAuthHeader(headers: Map<String, Any>): Map<String, Any> {
    return headers.toMutableMap().apply {
      put(HttpHeaders.AUTHORIZATION, "Bearer $authToken")
    }.toMap()
  }

  private fun <T : Any> convertFlatObjectToHeaderValue(value: T): String {
    return JsonObject(braidContext.encode(value)).joinToString(",") { entry -> "${entry.key},${entry.value}" }
  }
}
