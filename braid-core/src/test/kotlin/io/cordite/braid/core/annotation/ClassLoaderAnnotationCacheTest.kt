/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.annotation

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import org.junit.Assert
import org.junit.Test
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.QueryParam
import kotlin.test.assertTrue

class ClassLoaderAnnotationCacheTest {

  @Test
  fun `that we can lookup annotations`() {
    val cache = ClassLoaderAnnotationCache()
    val bindings = TestNetworkMapRestBinding()
    Assert.assertEquals(3, cache.allMatchingAnnotations(bindings::nodes).size)
    // check with the cache populated
    Assert.assertEquals(3, cache.allMatchingAnnotations(bindings::nodes).size)
    val parameterAnnotations = cache.allMatchingAnnotations(bindings::nodes, 0)
    assertTrue(parameterAnnotations.all {
      when (it.annotationClass) {
        Parameter::class -> (it as Parameter).example == HOST_AND_PORT_EXAMPLE
        QueryParam::class -> (it as QueryParam).value == HOST_AND_PORT_NAME
        else -> error("unrecognised annotation")
      }
    })
  }
}

private const val HOST_AND_PORT_NAME = "host-and-port"
private const val HOST_AND_PORT_EXAMPLE = "localhost:10000"

@Suppress("unused")
class TestNetworkMapRestBinding {

  @GET
  @Path("/network/my-node-info")
  @Operation(summary = "Get my NodeInfo")
  fun myNodeInfo() = 3

  @GET
  @Path("/network/nodes")
  @Operation(
    summary = "Retrieves all nodes if neither query parameter is supplied. " +
      "Otherwise returns a list of one node matching the supplied query parameter.",
    tags = ["network"]
  )
  fun nodes(
    @Parameter(description = "[host]:[port] for the Corda P2P of the node", example = HOST_AND_PORT_EXAMPLE)
    @QueryParam(value = HOST_AND_PORT_NAME)
    hostAndPort: String? = null,
    @Parameter(description = "the X500 name for the node", example = "O=PartyB, L=New York, C=US")
    @QueryParam(value = "x500-name")
    x500Name: String? = null
  ): List<String> = listOf(hostAndPort ?: "localhost:8080", x500Name ?: "dummy")

  @GET
  @Path("/network/notaries")
  @Operation(summary = "Retrieves all notaries in the network", tags = ["network"])
  fun notaries(
    @Parameter(
      description = "the X500 name for the node",
      example = "O=PartyB, L=New York, C=US"
    )
    @QueryParam(value = "x500-name")
    x500Name: String? = null
  ): List<String> = listOf(x500Name ?: "notary")
}