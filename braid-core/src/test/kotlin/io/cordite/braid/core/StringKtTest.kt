/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core

import io.cordite.braid.core.string.toKebabCase
import org.junit.Test
import kotlin.test.assertEquals

class StringKtTest {

  @Test
  fun `that we can make a string into kebab case`() {
    val kebabCase = "TeslaMotors-Are---Ultra-Fun".toKebabCase()
    assertEquals("tesla-motors-are-ultra-fun", kebabCase)
  }
}