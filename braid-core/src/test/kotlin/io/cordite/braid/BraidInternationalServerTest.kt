/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid
import io.cordite.braid.core.annotation.BraidService
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.http.body
import io.cordite.braid.core.http.bodyAsString
import io.cordite.braid.core.http.futurePost
import io.cordite.braid.core.logging.loggerFor
import io.vertx.core.Vertx
import io.vertx.core.VertxOptions
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.json.JsonObject
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import java.net.ServerSocket
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.QueryParam
import kotlin.test.assertEquals

class ComplexContainer {
  private val items = mutableMapOf<String,Any>()
  fun addItem(key: String, value: Any){
    items[key]=value
  }
  fun getItems() = items
}

@RunWith(VertxUnitRunner::class)
class BraidInternationalServerTest {

  private val log = loggerFor<BraidInternationalServerTest>()

  private val port = getFreePort()
  private val vertx: Vertx =
          Vertx.vertx(VertxOptions().setBlockedThreadCheckInterval(30_000))
  private val braidContext = BraidContext.create()
  private val braidConfig = BraidConfig()
          .withPort(port)
          .withService(braidContext, InternationalService())
  private val server = braidConfig.start(braidContext).getOrThrow()
  private val client = vertx.createHttpClient(
          HttpClientOptions()
                  .setDefaultPort(port)
                  .setDefaultHost("localhost")
                  .setSsl(true)
                  .setTrustAll(true)
                  .setVerifyHost(false)
  )!!

  private val normalString = "I am a normal payload"
  private val longInternationalByteArray = byteArrayOf(-29, -120, -91, -15, -87, -66, -126, -47, -98, -24, -70, -108, -17, -125, -87, 104, 70, 81, 104, 114, -52, -100, -13, -128, -70, -81, -33, -97, -14, -87, -91, -116, -24, -90, -122, -43, -127, -37, -98, 106, -57, -126, 75, -32, -66, -97, -14, -100, -107, -70, -39, -128, 63, -51, -121, -57, -118, 38, -28, -94, -118, -28, -77, -97, -28, -93, -73, -14, -90, -125, -105, 53, -28, -108, -90, 92, -39, -82, 116, 85, 115, 120, -16, -73, -77, -100, -24, -97, -98, 67, -33, -84, 61, -19, -110, -77, 96, 55, -15, -100, -122, -122, -48, -84, -25, -83, -75, -20, -65, -71, 96, 76, 87, -23, -85, -92, -29, -115, -82, 121, -18, -128, -112, 37, -40, -95, 58, -32, -71, -116, -22, -92, -86, -15, -110, -82, -101, -59, -73, -36, -87, -56, -92, -14, -126, -128, -89, -16, -106, -124, -84, -15, -117, -79, -73, 62, 58, -58, -111, 124, 87, 41, -14, -127, -96, -125, -37, -117, -13, -123, -107, -77, -51, -127, 72, 41, -62, -110, -15, -111, -97, -104, -40, -65, -15, -116, -122, -70, -26, -123, -72, -14, -92, -121, -121, -59, -80, 91, -43, -96, -39, -70, -14, -99, -91, -123, -28, -113, -122, 33, -25, -93, -77, -17, -65, -67, -45, -115, -16, -91, -82, -118, 94, -40, -75, 71, -28, -100, -72, -18, -100, -95, -18, -112, -89, -22, -110, -115, 33, -14, -82, -122, -69, 59, 104, -32, -70, -85, -27, -119, -91, -62, -87, -62, -70, -28, -97, -79, -16, -89, -89, -102, -19, -109, -66, -28, -105, -69, 86, 95, -41, -116, -13, -128, -74, -94, -15, -99, -106, -80, 77, 50, 123, -15, -81, -83, -69, -47, -101, 10, -60, -78, -52, -118, -16, -111, -111, -82, -16, -106, -104, -74, -22, -121, -68, 94, 82, -56, -66, -17, -108, -99, 72, -56, -82, -28, -85, -115, 61, -43, -72, 90, 57, -56, -78, 78, -15, -113, -115, -89, -13, -98, -105, -75, -23, -115, -69, -31, -127, -65, 39, -35, -103, 100, -62, -109, -14, -126, -83, -122, -13, -89, -95, -125, -36, -92, 48, -16, -104, -102, -117, -16, -69, -87, -107, -22, -114, -125, -22, -115, -111, -52, -66, 69, -13, -97, -109, -71, -17, -128, -87, 116, 48, -49, -74, -37, -74, 126, -38, -87, -14, -92, -107, -81, 118, -42, -88, -15, -116, -81, -126, -32, -77, -103, -16, -68, -103, -95, -35, -107, -15, -109, -103, -127, -16, -104, -113, -120, -14, -67, -87, -88, -50, -95, -23, -76, -68, -17, -65, -67, 42, -33, -74, -36, -81, -15, -99, -115, -72, 51, -35, -105, -45, -65, 87, 39, -15, -86, -123, -90, 48, -13, -92, -89, -111, -15, -120, -123, -118, -42, -80, 33, -13, -116, -92, -87, -43, -83, -37, -79, -13, -112, -99, -65, -34, -127, -46, -113, -24, -126, -121, -16, -67, -122, -88, -17, -87, -117, 78, -14, -118, -96, -118, -17, -97, -119, -17, -82, -99, -12, -114, -102, -81, -13, -99, -73, -124, -21, -94, -73, -14, -107, -105, -74, 40, -26, -97, -66, -16, -71, -115, -97, 108, -43, -107, -26, -92, -91, -15, -118, -83, -75, 88, -60, -117, -13, -93, -71, -69, 43, -12, -125, -65, -121, -14, -105, -89, -126, 43, -16, -91, -105, -117, -15, -68, -96, -78, -31, -98, -67, -24, -97, -79, -39, -73, -38, -98, -27, -89, -66, -37, -91, 97, -42, -93, -15, -102, -114, -88, -47, -74, -26, -110, -65, -37, -91, -49, -87, -15, -82, -98, -124, 46, 117, -38, -112, -16, -102, -121, -75, 89, -27, -87, -107, -17, -106, -93, -13, -70, -99, -68, 81, 10, -25, -114, -83, -16, -74, -103, -101, 59, 33, -49, -128, -50, -110, 92, -15, -84, -90, -86, -15, -68, -83, -119, -22, -126, -82, -22, -113, -125, -13, -88, -97, -116, -15, -73, -68, -115, 108, 35, -25, -100, -67, -37, -126, -13, -86, -114, -80, -47, -65, -15, -68, -89, -86, -57, -82, -21, -87, -72, -14, -96, -118, -106, -57, -127, -20, -114, -90, -62, -92, -14, -71, -68, -91, -22, -128, -109, 58, -58, -85, 95, -14, -85, -113, -90, -13, -80, -79, -70, -16, -93, -72, -77, 120, -26, -72, -66, -14, -127, -82, -87, -31, -98, -99, -42, -113, -16, -96, -69, -76, -16, -79, -65, -107, -31, -111, -87, -13, -65, -76, -122, -23, -74, -71, 99, -51, -111, -39, -74, -15, -114, -114, -94, 96, 117, -22, -111, -93, -23, -75, -119, -12, -119, -114, -115, 98, 51, -26, -69, -123, -20, -67, -111, -13, -95, -66, -109, -40, -98, 62, 120, 78, 70, -62, -85, -42, -87, -24, -94, -65, -14, -98, -83, -70, -14, -109, -95, -101, -22, -91, -121, -14, -117, -119, -66, -55, -102, -16, -112, -125, -110, -50, -101, -13, -114, -124, -112, 69, -21, -78, -84, -16, -99, -110, -96, 111, 92, -14, -79, -71, -82, -13, -95, -73, -102, 64, -16, -101, -96, -124, -15, -91, -115, -118, -42, -124, -13, -109, -95, -71, -16, -81, -101, -75, 90, -15, -113, -105, -92, -13, -104, -70, -88, -14, -116, -66, -72, -24, -105, -80, -43, -120, -26, -116, -126, -14, -100, -82, -99, 96, -16, -69, -85, -105, -13, -118, -109, -81, -14, -117, -80, -105, -15, -108, -82, -73, -15, -115, -103, -68, 36, 100, -16, -110, -91, -76, 78, -14, -66, -87, -96, 99, 60, -27, -71, -90, 69, 109, -32, -82, -88, -14, -101, -94, -113, -17, -108, -82, -47, -121, 51, -18, -94, -77, -12, -118, -98, -66, -14, -103, -103, -113, -14, -105, -121, -103, -26, -122, -97, -15, -106, -82, -67, -23, -95, -117, -15, -121, -97, -77, 35, -15, -65, -77, -100, -29, -71, -99, -17, -91, -72, 10, -13, -107, -113, -100, -53, -74, -16, -91, -97, -125, -51, -75, -16, -86, -106, -119, 72, -30, -113, -97, -42, -85, -47, -94, -44, -97, -16, -92, -111, -67, -13, -79, -77, -105, -28, -66, -127, 38, -21, -65, -107, -32, -92, -79, -25, -121, -86, -13, -66, -78, -65, -30, -125, -106, -33, -112, 81, -28, -109, -102, -37, -111, 120, 75, -40, -69, -14, -105, -128, -104, -17, -100, -77, -15, -90, -127, -74, -28, -102, -110, -31, -91, -99, 35, 94, -15, -95, -123, -84, -15, -120, -76, -128, -18, -103, -109, 123, -41, -93, -17, -122, -74, 102, 47, -52, -109, 126, -13, -121, -68, -102, -15, -69, -99, -128, -13, -88, -121, -102, 122, 38, 73, -24, -68, -128, -18, -116, -122, -53, -66, 94, 123, -51, -69, 116, 55, -13, -122, -86, -113, 106, -44, -102, -14, -112, -92, -116, 72, -27, -112, -123, -29, -102, -103, -14, -82, -118, -118, -14, -88, -69, -96, -15, -113, -119, -128, -13, -65, -89, -124, 115, -59, -65, -14, -95, -73, -75, -54, -81, -56, -102, -24, -68, -122, -47, -121, -20, -65, -128, 86, -35, -92, 95, 126, -17, -120, -78, 33, -45, -75, -15, -92, -77, -78, 33, -26, -109, -108, 69, 89, -32, -67, -68, 98, -48, -114, -31, -105, -97, -20, -99, -103, -27, -65, -70, -59, -78, -14, -83, -102, -89, -24, -118, -81, -13, -94, -88, -94, -38, -93, -17, -108, -119, -15, -115, -86, -72, -14, -79, -87, -78, -30, -114, -90, 58, -13, -117, -65, -69, -34, -88, 100, -13, -103, -82, -80, 84, -17, -74, -116, -13, -76, -116, -67, -22, -116, -119, -48, -114, -22, -108, -87, -16, -78, -90, -101, -31, -97, -89, -13, -96, -89, -93, 51, -14, -116, -118, -78, -26, -95, -100, -21, -126, -109, -27, -118, -84, -22, -80, -111, -12, -127, -122, -121, -34, -112, -60, -75, 54, 73, 10, 59, 94, -15, -99, -72, -97, -30, -121, -84, -42, -90, -27, -95, -87, 77, -35, -98, 55, -13, -122, -85, -85, 55, -57, -104, -48, -113, -34, -66, -12, -116, -96, -74, 45, -15, -93, -106, -74, -13, -91, -120, -114, -15, -118, -92, -100, -15, -80, -127, -125, -26, -78, -91, -15, -89, -96, -110, 125, -52, -95, -14, -75, -85, -72, -31, -117, -111, -40, -113, -43, -116, -41, -88, 54, 40, -21, -114, -85, -52, -114, -15, -107, -97, -124, -49, -83, -18, -115, -80, -36, -78, -30, -88, -124, -30, -81, -94, -26, -118, -107, -13, -107, -78, -82, -25, -71, -90, -13, -109, -119, -124, -57, -101, 42, -42, -74, -21, -80, -120, 50, -24, -76, -95, -13, -84, -88, -125, -52, -104, -16, -75, -114, -75, 46, -17, -65, -67, -30, -66, -111, 86, -59, -69, -13, -107, -80, -66, -61, -116, -16, -107, -118, -70, -15, -112, -100, -113, -60, -66, -16, -71, -113, -121, -13, -112, -73, -124, -62, -111, 51, -19, -102, -68, -14, -121, -110, -71, -33, -110, -16, -84, -97, -91, -25, -121, -65, -15, -75, -127, -104, 80, -52, -80, -15, -107, -92, -87, -15, -116, -73, -78, -52, -80, -26, -75, -109, -62, -83, -13, -77, -66, -122, 41, -14, -119, -75, -119, 88, -31, -74, -98, 84, -57, -83, -12, -122, -113, -90, -42, -95, -21, -113, -94, -32, -80, -99, 72, 64, -31, -127, -110, -20, -79, -119, -16, -95, -81, -111, -26, -90, -71, -36, -84, -25, -109, -67, -27, -101, -87, -50, -126, -59, -65, -12, -114, -66, -80, -14, -94, -81, -125, -16, -95, -80, -96, 68, -14, -96, -122, -100, -15, -127, -78, -96, -62, -128, -30, -78, -108, -26, -96, -92, -14, -122, -81, -81, -23, -115, -73, -13, -118, -102, -119, -46, -100, -27, -107, -86, -51, -99, -46, -91, 117, -13, -92, -126, -75, -20, -89, -65, 100, 79, -49, -102, 106, -27, -95, -96, -12, -126, -74, -96, -38, -123, -13, -75, -111, -125)
  private val shortInternationByteArray = byteArrayOf(87, 104, 70, 81, 104, 114, -52, -100, -13, -128, -70, -81, -33, -97, -14, -87, -91, -116, -24, -90, -122, -43, -127, -37, -98, 106, -57, -126, 75)
  private val longInternationalString = longInternationalByteArray.toString(Charsets.UTF_8)
  private val shortInternationString = shortInternationByteArray.toString(Charsets.UTF_8)
  private val mediumInternationByteArray = byteArrayOf(-47, -78, -16, -67, -78, -113, 32, -59, -65, -15, -119, -124, -100, -15, -72, -116, -92, 100, 55, 44, -15, -113, -103, -112, 38, -55, -95, -26, -95, -99, -13, -83, -68, -95, -17, -106, -77, -56, -101, -15, -124, -70, -89, 74, -18, -119, -80, 91, -13, -122, -100, -116, -55, -118, 107, 97, -14, -85, -87, -118, -23, -70, -111, -52, -118, -52, -122, 105, -13, -70, -113, -109, -15, -68, -98, -122, -21, -101, -115, 10, -47, -113, -17, -65, -67, -13, -85, -109, -87, 97, -41, -79, -13, -84, -87, -95, 97, -14, -125, -128, -120, -28, -68, -126, -14, -93, -121, -126, -13, -118, -98, -89, -57, -88, -56, -73, -17, -109, -122, -27, -125, -74, -54, -106, -47, -69, 106, 52, -18, -73, -78, 105, -15, -81, -116, -103, -41, -121, 59, 59, -23, -124, -90, 40, -46, -77, -49, -77, -56, -91, -18, -119, -125, -16, -85, -121, -74, 10, -40, -106, -30, -79, -123, 120, -40, -128, 58, -26, -118, -74, -52, -118, -14, -88, -94, -95, 59, 106, -15, -126, -84, -106, -41, -91, 62, -50, -84, -26, -72, -81, -26, -104, -125, -16, -101, -108, -103, 103, -52, -72, 59, -23, -124, -117, 87, -27, -107, -81, -53, -99, 58, 36, -55, -67, -43, -65, -16, -87, -126, -83, -35, -112, -35, -99, -15, -115, -73, -117, 10, -22, -69, -79, -17, -87, -92, -12, -123, -127, -108, -37, -112, -53, -109, -37, -89, -56, -80, -15, -78, -105, -75, -31, -127, -74, -17, -84, -104, -23, -118, -80, -51, -91, 105, 108, 73, -26, -104, -97, -62, -80, -50, -118, -29, -112, -119, -29, -68, -105, -14, -109, -81, -78, -31, -115, -74, 102, -15, -66, -66, -107, -43, -103, -60, -67, 86, -30, -90, -109, -58, -121, -23, -86, -102, -15, -127, -119, -125, -16, -87, -124, -65, 10, 113, -48, -122, -20, -68, -71, -13, -102, -84, -98, -61, -95, -17, -73, -125, -14, -110, -69, -107, -28, -85, -104, 33, -14, -125, -99, -118, -14, -100, -97, -100, 92, -51, -123, -13, -94, -113, -74, -18, -110, -94, -25, -119, -102, -17, -92, -98, -29, -98, -113, -55, -76, -52, -123, -62, -71, -23, -117, -84, -61, -75, -22, -67, -66, 115, -36, -83, -12, -117, -70, -65, -43, -99, -15, -108, -120, -126, 106, 115, -24, -72, -107)
  private val mediumInternationString = mediumInternationByteArray.toString(Charsets.UTF_8)
  private val chineseGodelEscherBach = "哥德爾·埃舍爾·巴赫".toByteArray().toString(Charsets.UTF_8)
  private val armenianGodelEscherBach = "Գոդել Էշեր Բախ".toByteArray().toString(Charsets.UTF_8)

  @After
  fun after() {
    server.stop().getOrThrow()
    braidContext.close().getOrThrow()
  }

  @Test
  fun `we can round trip an ascii string as body`() {
      val result = roundTripInternationalStringAsBody(normalString)
      assertEquals(normalString,result)
  }
  @Test
  fun `we can round trip an ascii string as query param`() {
      val result = roundTripInternationalStringAsQueryParam(normalString)
      assertEquals(normalString,result)
  }

  @Test
  fun `we can round trip chinese geb string as body`() {
    val result = roundTripInternationalStringAsBody(chineseGodelEscherBach)
    assertEquals(chineseGodelEscherBach,result)
  }
  @Test
  fun `we can round trip chinese geb string as query param`() {
    val result = roundTripInternationalStringAsQueryParam(chineseGodelEscherBach)
    assertEquals(chineseGodelEscherBach,result)
  }

  @Test
  fun `we can round trip an armenian geb string as body`() {
    val result = roundTripInternationalStringAsBody(armenianGodelEscherBach)
    assertEquals(armenianGodelEscherBach,result)
  }
  @Test
  fun `we can round trip a armenian string as query param`() {
    val result = roundTripInternationalStringAsQueryParam(armenianGodelEscherBach)
    assertEquals(armenianGodelEscherBach,result)
  }

  @Test
  fun `we can round trip a short international string as body`() {
    val result = roundTripInternationalStringAsBody(shortInternationString)
    assertEquals(shortInternationString,result)
  }
  @Test
  fun `we can round trip a short international string as query param`() {
    val result = roundTripInternationalStringAsQueryParam(shortInternationString)
    assertEquals(shortInternationString,result)
  }

  @Test
  fun `we can round trip a medium international string as body`() {
    val result = roundTripInternationalStringAsBody(mediumInternationString)
    assertEquals(mediumInternationString,result)
  }

  @Test
  fun `we can round trip a long international string as body`() {
    val result = roundTripInternationalStringAsBody(longInternationalString)
    assertEquals(longInternationalString,result)
  }

  @Test
  fun `we can round trip a complex container with no international values`(){
    val complexContainer = ComplexContainer()
    val key = "normal"
    complexContainer.addItem(key,normalString)

    val returnedComplexContainer = roundTripComplexContainerAsBody(complexContainer)
    assertEquals(returnedComplexContainer.getItems()[key],normalString)
  }

  @Test
  fun `we can round trip a complex container with embedded list`(){
    val complexContainer = ComplexContainer()
    val key = "normal"
    complexContainer.addItem(key,normalString)

    val arbitraryList = listOf("one","two","boogey")
    val listKey ="list"
    complexContainer.addItem(listKey,arbitraryList)

    val returnedComplexContainer = roundTripComplexContainerAsBody(complexContainer)
    assertEquals(returnedComplexContainer.getItems()[key],normalString)

    for((index,value) in arbitraryList.listIterator().withIndex()){
      assertEquals(value,(returnedComplexContainer.getItems()[listKey] as List<String>)[index])
    }
  }

  @Test
  fun `we can round trip a complex container with embedded list and international items`(){
    val complexContainer = ComplexContainer()
    val key = "international"
    complexContainer.addItem(key,shortInternationString)

    val arbitraryList = listOf(mediumInternationString,chineseGodelEscherBach,armenianGodelEscherBach)
    val listKey ="list"
    complexContainer.addItem(listKey,arbitraryList)

    val returnedComplexContainer = roundTripComplexContainerAsBody(complexContainer)
    assertEquals(returnedComplexContainer.getItems()[key],shortInternationString)

    for((index,value) in arbitraryList.listIterator().withIndex()){
      assertEquals(value,(returnedComplexContainer.getItems()[listKey] as List<String>)[index])
    }
  }

  private fun roundTripInternationalStringAsBody(payload : String) : String{
    val basePath = braidConfig.restConfig.restPath
    log.info("Posting body payload size = ${payload.toByteArray().size}")

    return client.futurePost(
            path = "$basePath/international-service/internationaliseBody",
            braidContext = braidContext,
            body = payload
            )
            .compose { response ->
              assertEquals(200, response.statusCode())
              response.bodyAsString()
            }
            .getOrThrow()
  }

  private fun roundTripComplexContainerAsBody(payload : ComplexContainer) : ComplexContainer {
    val basePath = braidConfig.restConfig.restPath
    val resultAsBuffer=  client.futurePost(
            path = "$basePath/international-service/internationaliseContainer",
            braidContext = braidContext,
            body = payload
    )
            .compose { response ->
              assertEquals(200, response.statusCode())
              response.body()
            }
            .getOrThrow()

    val resultAsJson = JsonObject(resultAsBuffer)
    return resultAsJson.mapTo(ComplexContainer::class.java)
  }

  private fun roundTripInternationalStringAsQueryParam(payload : String) : String{
    log.info("Posting Query Param size = ${payload.toByteArray().size}")
    val basePath = braidConfig.restConfig.restPath
    return  client.futurePost(
            path = "$basePath/international-service/internationaliseQuery",
            braidContext = braidContext,
            queryParams = mapOf("payload" to payload)
    )
            .compose { response ->
              assertEquals(200, response.statusCode())
              response.bodyAsString()
            }
            .getOrThrow()
  }

  private fun getFreePort(): Int {
    return (ServerSocket(0)).use {
      it.localPort
    }
  }
}

@BraidService(name = "International", description = "Returns all types of characters from across the world")
@Path("/international-service")
class InternationalService {
  val log = loggerFor<InternationalService>()

  @POST
  @Path("/internationaliseQuery")
  fun internationaliseQuery(@QueryParam("payload") queryString: String) : String {
    log.info("Received query string size is ${queryString.toByteArray().size} ")
    return queryString
  }

  @POST
  @Path("/internationaliseBody")
  fun internationaliseBody(payload: String) : String {
    log.info("Received payload size is ${payload.toByteArray().size} ")
    return payload
  }

  @POST
  @Path("/internationaliseContainer")
  fun internationaliseBody(payload: ComplexContainer) : ComplexContainer {
    //log.info("Received payload size is ${payload.toByteArray().size} ")
    return payload
  }
}



