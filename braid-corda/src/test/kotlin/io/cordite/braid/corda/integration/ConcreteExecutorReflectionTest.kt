/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.integration

import io.cordite.braid.corda.BraidCordaContext
import io.cordite.braid.corda.server.cordapp.CustomServiceImpl
import io.cordite.braid.core.service.ConfigurableBraidService
import io.cordite.braid.corda.testing.TestAppServiceHub
import io.cordite.braid.core.service.ConcreteServiceExecutor
import org.junit.Test
import kotlin.reflect.jvm.javaMethod
import kotlin.test.assertEquals

class ConcreteExecutorReflectionTest {

  private val context = BraidCordaContext.create(TestAppServiceHub.DEFAULT_SERVICE_HUB)
  private val service = CustomServiceImpl(context)

  @Test
  fun `that we BraidCordaService configureWith is properly ignored`() {
    val executor = ConcreteServiceExecutor.Factory(service).create(context)
    val methodDescriptors = executor.getMethodDescriptors()
    assertEquals(0, methodDescriptors.count { it.name == ConfigurableBraidService::configureWith.javaMethod!!.name })
  }
}