/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.utils

import io.cordite.braid.corda.BraidCordaContext
import io.cordite.braid.corda.testing.TestAppServiceHub
import io.cordite.braid.corda.utils.CordaConfigJacksonCodec.decodeCordappConfig
import io.cordite.braid.corda.utils.CordaConfigJacksonCodec.encodeCordappConfig
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.jsonrpc.JsonRpcConfig
import io.cordite.braid.core.openapi.config.RestConfig
import org.junit.After
import org.junit.Test
import kotlin.test.assertEquals

class CordaConfigJacksonCodecTest {

  private val braidContext = BraidCordaContext.create(TestAppServiceHub.DEFAULT_SERVICE_HUB)

  @After
  fun after() {
    braidContext.close().getOrThrow()
  }

  @Test
  fun `that we can deserialize a CordappConfig`() {
    val expected = BraidConfig()
    val cordappConfig = braidContext.encodeCordappConfig(expected)
    val actual = braidContext.decodeCordappConfig<BraidConfig>(cordappConfig)
    assertEquals(expected, actual)
  }

  @Test
  fun `that we can deserialize a CordappConfig for RestConfig`() {
    val expected = RestConfig()
    val cordappConfig = braidContext.encodeCordappConfig(expected)
    val actual = braidContext.decodeCordappConfig<RestConfig>(cordappConfig)
    assertEquals(expected, actual)
  }

  @Test
  fun `that we can deserialize a CordappConfig for JsonRpcConfig`() {
    val expected = JsonRpcConfig()
    val cordappConfig = braidContext.encodeCordappConfig(expected)
    val actual = braidContext.decodeCordappConfig<JsonRpcConfig>(cordappConfig)
    assertEquals(expected, actual)
  }
}

