/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.rest.docs

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import io.cordite.braid.core.annotation.BraidMethod
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.EndPoint
import io.cordite.braid.core.openapi.swaggermodel.FullyQualifiedTypeNameResolver
import io.cordite.braid.core.openapi.utils.EndPointUtilities
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.parameters.PathParameter
import io.swagger.v3.oas.models.parameters.QueryParameter
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpMethod.GET
import io.vertx.core.http.HttpMethod.POST
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.DEFAULT_PAGE_SIZE
import net.corda.finance.contracts.asset.Cash
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Test
import javax.ws.rs.PathParam
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import kotlin.reflect.KFunction
import kotlin.test.assertTrue
import io.swagger.v3.oas.models.Operation as OperationModel

class KEndPointTest {

  private val braidContext = BraidContext.create()
  private val endPointUtils = EndPointUtilities(braidContext)

  @After
  fun after() {
    braidContext.close().getOrThrow()
  }

  @Test
  fun `that names can be applied with @QueryParam`() {
    val method = this::listAccountsPagedRest
    val operation = createOperation(handler = method)
    assertEquals(2, operation.parameters.size)
    operation.parameters.forEach { parameter ->
      // we can't have both schema and content set
      assertFalse(parameter.schema != null && parameter.content != null)
    }
    val pnq =
      operation.parameters.single { it.name == "page-number" && it is QueryParameter } as QueryParameter
    val psq =
      operation.parameters.single { it.name == "page-size" && it is QueryParameter } as QueryParameter

    // the type for the schema is a string (i.e. not an object)
    // by default we interpret this schema as being applied to the `schema` field and not the `content`
    assertEquals(1, pnq.schema.default)
    assertEquals(DEFAULT_PAGE_SIZE, psq.schema.default)
  }

  @Test
  fun `that multiple description annotations prefers method description`() {
    val method = this::manyDescription
    val operation = createOperation(handler = method)
    assertEquals("method-description", operation.summary)
  }

  @Test
  fun `that @BraidMethod description is used`() {
    val method = this::methodDescription
    val operation = createOperation(handler = method)
    assertEquals("method-description", operation.summary)
  }

  @Test
  fun `that @ApiParameter description is used`() {
    val method = this::apiParamDescription
    val operation = createOperation(handler = method)
    assertEquals("api-operation", operation.summary)
  }

  @Test
  fun `that @PathParms are represented correctly`() {
    val method = this::postWithPathParam
    val operation = createOperation(method = POST, path = "/foo/:name", handler = method)
    assertEquals(1, operation.parameters.size)
    val p = operation.parameters.filterIsInstance<PathParameter>().single()
    assertEquals("name", p.name)
    // json primitive schemas are placed on the `schema` field and not `content`
    assertEquals("default-path-param", p.schema.default)
  }

  @Test
  fun `that all parameter types can be mixed for post`() {
    val method = this::postWithPathQueryAndBodyParam
    val operation = createOperation(method = POST, path = "/foo/:name", handler = method)
    assertEquals(3, operation.parameters.size)
    val p = operation.parameters.filterIsInstance<PathParameter>().single()
    assertEquals("name", p.name)
    // json primitive schemas are placed on the `schema` field, and not `content`
    assertEquals("default-path-param", p.schema.default)
  }

  @Test
  fun `that we can correctly represent VaultUpdates in openapi`() {
    val method = this::dummyVaultUpdate
    createOperation(method = GET, path = "/path", handler = method)
    val typeref = object : TypeReference<Vault.Update<Cash.State>>() {}
    val mapper = braidContext.getContextObject<ObjectMapper>()
    val vaultUpdateJavaType = mapper.constructType(typeref.type)
    val name = FullyQualifiedTypeNameResolver.nameForType(vaultUpdateJavaType)
    assertTrue(braidContext.openapiComponents.schemas.keys.contains(name))
  }

  fun listAccountsPagedRest(
    @Suppress("UNUSED_PARAMETER")
    @QueryParam("page-number")
    @Parameter(
      description = "page number - must be greater than zero",
      required = true,
      content = [Content(
        mediaType = APPLICATION_JSON,
        schema = Schema(implementation = Int::class, defaultValue = 1.toString(), required = true)
      )]
    )
    pageNumber: Int,
    @Suppress("UNUSED_PARAMETER") @Parameter(
      description = "max accounts per page",
      content = [Content(
        mediaType = APPLICATION_JSON,
        schema = Schema(implementation = Int::class, defaultValue = DEFAULT_PAGE_SIZE.toString(), required = true)
      )]
    )
    @QueryParam("page-size")
    pageSize: Int
  ) {
  }

  @BraidMethod(value = "method-description")
  @Operation(summary = "api-operation")
  fun manyDescription() {
  }

  @BraidMethod(value = "method-description")
  fun methodDescription() {
  }

  @Operation(summary = "api-operation")
  fun apiParamDescription() {
  }

  @Operation(summary = "post with path param")
  fun postWithPathParam(
    @Suppress("UNUSED_PARAMETER", "UnresolvedRestParam")
    @Parameter(schema = Schema(implementation = String::class, defaultValue = "default-path-param"))
    @PathParam("name")
    name: String
  ) {
  }

  fun dummyVaultUpdate(): Vault.Update<Cash.State> = Vault.Update(consumed = emptySet(), produced = emptySet())

  @Operation(summary = "post with path param query param and body")
  fun postWithPathQueryAndBodyParam(
    @Suppress("UNUSED_PARAMETER", "UnresolvedRestParam")
    @Parameter(schema = Schema(implementation = String::class, defaultValue = "default-path-param"))
    @PathParam("name")
    name: String,
    @Suppress("UNUSED_PARAMETER") @Parameter(
      schema = Schema(
        implementation = Int::class,
        defaultValue = "default-path-param"
      )
    )
    @QueryParam("age")
    age: Int,
    @Suppress("UNUSED_PARAMETER") @Parameter(schema = Schema(implementation = String::class, defaultValue = "details"))
    details: String
  ) {}

  private fun <R> createOperation(
    method: HttpMethod = GET,
    path: String = "/foo",
    group: String = "",
    protected: Boolean = false,
    handler: KFunction<R>
  ): OperationModel {
    val openApi = OpenAPI().apply { components = this@KEndPointTest.braidContext.openapiComponents }
    return EndPoint.create(braidContext, group, protected, method, path, endPointUtils, handler).addToOpenApi(openApi)!!
  }
}