/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.server.simple

import io.cordite.braid.corda.serialisation.BraidCordaJacksonModule
import io.cordite.braid.corda.testing.TestAppServiceHub
import io.cordite.braid.core.json.BraidJacksonModule
import io.cordite.braid.core.openapi.swaggermodel.FullyQualifiedTypeNameResolver
import io.cordite.braid.core.openapi.swaggermodel.JacksonSerializerModelResolver
import io.swagger.v3.core.converter.ModelConverters
import io.swagger.v3.core.util.Json
import net.corda.core.identity.CordaX500Name
import org.junit.Test

class ModelConvertersTest {
  @Test
  fun test() {
    val testServiceHub = TestAppServiceHub()
    // Swagger uses a different ObjectMapper for it's own work - here we experiment if can introduce our jackson modules
    // and see if swagger uses them correctly
    Json.mapper().registerModule(BraidCordaJacksonModule(testServiceHub))
    Json.mapper().registerModule(BraidJacksonModule())
    // create swagger model converters
    val converters = ModelConverters()
    converters.addConverter(JacksonSerializerModelResolver(Json.mapper(), FullyQualifiedTypeNameResolver))
    val appleSchema = converters.readAllAsResolvedSchema(AppleStore::class.java)
  }
}

data class Apple(val name: String)
data class FruitBox<T>(val fruit: T)
data class AppleStore(val appleBox: FruitBox<Apple>, val x500: CordaX500Name)