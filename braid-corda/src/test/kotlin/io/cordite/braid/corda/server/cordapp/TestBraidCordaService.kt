/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@file:Suppress("DEPRECATION")

package io.cordite.braid.corda.server.cordapp

import io.cordite.braid.corda.BraidCordaContext
import io.cordite.braid.corda.services.withFlow
import io.cordite.braid.corda.testing.PARTY_A_IDENTITY
import io.cordite.braid.corda.testing.PARTY_B_IDENTITY
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.server.BraidServer
import io.cordite.braid.core.server.findAndStartAllBraidServices
import io.vertx.core.Vertx
import io.vertx.ext.auth.AuthProvider
import io.vertx.ext.auth.shiro.ShiroAuth
import io.vertx.ext.auth.shiro.ShiroAuthOptions
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.CordaService
import net.corda.core.serialization.SingletonSerializeAsToken
import java.util.concurrent.ConcurrentHashMap

@Suppress("unused")
@CordaService
class TestBraidCordaService(private val appServiceHub: AppServiceHub) : SingletonSerializeAsToken() {

  companion object {

    private val log = loggerFor<TestBraidCordaService>()
    private val cache =
      ConcurrentHashMap<Pair<String, Int>, BraidServer>() // to work around a problem with Corda 3.1. See https://github.com/corda/corda/issues/2898
  }

  private val org = appServiceHub.myInfo.legalIdentities.first().name.organisation

  init {
    val port = getBraidPort()
    if (port > 0) {
      val key = org to port
      cache.computeIfAbsent(key) {
        log.info("Starting Braid service for $org on port $port")

        // DOCSTART 1
        val braidCordaContext = BraidCordaContext.create(
          appServiceHub = appServiceHub,
          authConstructor = this::shiroFactory
        )

        BraidConfig()
          .withThreadPoolSize(1)
          .withFlow("echo", EchoFlow::class)
          .withPort(port)
          .findAndStartAllBraidServices(braidCordaContext)
          .start(braidCordaContext)
          .getOrThrow()
        // DOCEND 1
      }
    } else {
      log.info("No port defined for $org")
    }
  }

  private fun getBraidPort(): Int {
    val property = "braid.$org.port"
    return System.getProperty(property)?.toInt() ?: when (org) {
      PARTY_A_IDENTITY.party.name.organisation -> 8080
      PARTY_B_IDENTITY.party.name.organisation -> 8081
      else -> 0
    }
  }

  private fun shiroFactory(it: Vertx): AuthProvider {
    val shiroConfig = json {
      obj {
        put("properties_path", "classpath:auth/shiro.properties")
      }
    }
    return ShiroAuth.create(it, ShiroAuthOptions().setConfig(shiroConfig))
  }
}