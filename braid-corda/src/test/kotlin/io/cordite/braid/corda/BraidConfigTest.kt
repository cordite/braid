/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda

import io.cordite.braid.corda.server.cordapp.EchoFlow
import io.cordite.braid.corda.services.SimpleNetworkMapServiceImpl
import io.cordite.braid.corda.services.withFlow
import io.cordite.braid.corda.testing.TestAppServiceHub
import io.cordite.braid.corda.testing.TestAuthProvider
import io.cordite.braid.core.config.BraidConfig
import io.vertx.core.json.JsonObject
import net.corda.core.utilities.contextLogger
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class BraidConfigTest {
  companion object {

    private val log = contextLogger()
  }

  private val braidContext = BraidCordaContext.create(
    appServiceHub = TestAppServiceHub.DEFAULT_SERVICE_HUB,
    authConstructor = { TestAuthProvider() }
  )

  @Test
  fun `that we can deserialize with tls turned off`() {
    val port = 9999
    val tlsEnabled = false

    val json = JsonObject().apply {
      put("port", port)
      put("httpServerConfig", JsonObject().apply {
        put("tlsEnabled", tlsEnabled)
      })
    }.toString()
    val config = braidContext.decodeValue(json, BraidConfig::class.java)
    assertEquals(tlsEnabled, config.httpServerConfig.tlsEnabled)
    assertEquals(port, config.port)
  }

  @Test
  fun `that we can serialize the config`() {
    val networkRest = SimpleNetworkMapServiceImpl(braidContext.getContextObject())
    val config = BraidConfig()
      .withFlow(EchoFlow::class.java)
      .amendRestConfig {
        withPaths {
          route(networkRest)
        }
      }
    val str = config.toString()
    log.info("serialised braidconfig: $str")
    assertTrue(str.isNotBlank())
  }
}