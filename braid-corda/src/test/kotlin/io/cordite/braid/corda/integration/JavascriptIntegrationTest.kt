/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.integration

import io.cordite.braid.corda.server.TestSharedNetwork
import io.cordite.braid.corda.testing.AbstractCordaNetworkTest
import io.cordite.braid.corda.testing.PARTY_A_IDENTITY
import io.cordite.braid.core.jsonrpc.JsonRpcConfig.Companion.DEFAULT_JSON_RPC_ROOT_PATH
import io.cordite.braid.core.logging.loggerFor
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import org.junit.Test
import org.slf4j.LoggerFactory
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class JavascriptIntegrationTest : AbstractCordaNetworkTest(TestSharedNetwork) {
  companion object {

    private val log = loggerFor<JavascriptIntegrationTest>()
    private val javascriptLogger = LoggerFactory.getLogger("javascript-logger")
  }

  private val credentials = json {
    obj(
      "username" to "admin",
      "password" to "admin"
    )
  }
  private val node = getNode(PARTY_A_IDENTITY.name, credentials)

  @Test
  fun runNPMTests() {
    log.info("project directory is ${getProjectDirectory()}")
    val testDir = getProjectDirectory().resolve("../braid-client-js")
    assertTrue { testDir.exists() }
    val pb = ProcessBuilder("npm", "run", "test:integration")
    val url = "https://localhost:${node.port}$DEFAULT_JSON_RPC_ROOT_PATH"
    pb.environment()["BRAID_SERVICE"] = url
    pb.directory(testDir)
    val process = pb.start()
    Thread {
      try {
        BufferedReader(InputStreamReader(process.errorStream)).use { reader ->
          reader.lines().forEach {
            javascriptLogger.error(it)
          }
        }
      } catch (e: Exception) {
      }
    }.start()
    Thread {
      try {
        BufferedReader(InputStreamReader(process.inputStream)).use { reader ->
          reader.lines().forEach {
            javascriptLogger.info(it)
          }
        }
      } catch (e: Exception) {
      }
    }.start()
    process.waitFor(5, TimeUnit.MINUTES)
    assertEquals(0, process.exitValue(), "tests should succeed")
  }

  private fun getProjectDirectory() = File(System.getProperty("user.dir"))
}