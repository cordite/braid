/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.serialisation

import io.cordite.braid.corda.BraidCordaContext
import io.cordite.braid.corda.testing.TestAppServiceHub
import net.corda.core.identity.CordaX500Name
import org.junit.Test
import kotlin.test.assertEquals

class CordaX500NameMixinTest {
  private val braidContext = BraidCordaContext.create(TestAppServiceHub.DEFAULT_SERVICE_HUB)

  @Test
  fun `that we can serialize x500 names to and from strings`() {
    val name = CordaX500Name("foo", "bar", "London", "GB")
    val nameAsString = braidContext.encode(name)
    assertEquals("\"CN=foo, O=bar, L=London, C=GB\"", nameAsString)
    val name2 = braidContext.decodeValue(nameAsString, CordaX500Name::class.java)
    assertEquals(name, name2)
  }
}