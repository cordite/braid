/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.server.cordapp

import io.cordite.braid.corda.BraidCordaContext
import io.cordite.braid.corda.server.cordapp.CustomService.Companion.SERVICE_DESCRIPTION
import io.cordite.braid.corda.server.cordapp.CustomService.Companion.SERVICE_NAME
import io.cordite.braid.core.annotation.BraidService
import io.cordite.braid.core.async.withPromise
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.service.ConfigurableBraidService
import io.vertx.core.Future
import net.corda.core.concurrent.CordaFuture
import net.corda.core.identity.CordaX500Name
import net.corda.core.node.AppServiceHub
import net.corda.core.toFuture
import rx.Observable
import rx.schedulers.Schedulers
import java.time.Instant
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.QueryParam

@BraidService(name = SERVICE_NAME, description = SERVICE_DESCRIPTION)
interface CustomService {

  companion object {

    const val SERVICE_DESCRIPTION = "A simple service for testing braid"
    const val SERVICE_NAME = "customService" // REQUIRED bythe Javascript tests in braid-client-js
  }

  @Suppress("MemberVisibilityCanBePrivate")
  @GET
  @Path("/echo")
  fun doEcho(@QueryParam("payload") payload: String): CordaFuture<String>

  @GET
  @Path("/add")
  fun add(@QueryParam("lhs") lhs: Int, @QueryParam("rhs") rhs: Int): Int
  fun badjuju(): Int
  fun asyncResult(): Future<String>
  fun asyncCordaResult(): CordaFuture<String>
  fun streamedResult(): Observable<Int>
  fun infiniteStream(): Observable<Long>
  fun streamedResultThatFails(): Observable<Int>

  // function to test https://gitlab.com/bluebank/braid/merge_requests/76
  fun slowData(): List<SlowDataItem>
  fun createDao(
    daoName: String,
    minimumMemberCount: Int,
    strictMode: Boolean,
    notaryName: CordaX500Name
  ): Future<DaoState>

  fun useInstant(instant: Instant): String
  fun useDate(date: Date): String
}

class CustomServiceImpl(private val braidContext: BraidCordaContext) : CustomService, ConfigurableBraidService {

  private val scheduler = Schedulers.from(Executors.newFixedThreadPool(1))
  private val serviceHub: AppServiceHub get() = braidContext.serviceHub

  override fun configureWith(config: BraidConfig): BraidConfig {
    return config
      .amendRestConfig {
        withPaths {
          group(SERVICE_NAME) {
            route(this@CustomServiceImpl::add)
            route(this@CustomServiceImpl::doEcho)
          }
        }
      }
  }

  @Suppress("MemberVisibilityCanBePrivate")
  override fun doEcho(payload: String): CordaFuture<String> {
    return serviceHub.startFlow(EchoFlow(payload)).returnValue
  }

  override fun add(lhs: Int, rhs: Int) = lhs + rhs

  override fun badjuju(): Int {
    throw RuntimeException("I threw an exception")
  }

  override fun asyncResult(): Future<String> {
    return withPromise<String> { promise ->
      streamedResult().first().single().subscribe { promise.complete(it.toString()) }
    }.future()
  }

  override fun asyncCordaResult(): CordaFuture<String> {
    return streamedResult().first().map { it.toString() }.toFuture()
  }

  override fun streamedResult(): Observable<Int> {
    return Observable.range(0, 10, scheduler).delay(1, TimeUnit.MILLISECONDS)
  }

  override fun infiniteStream(): Observable<Long> {
    return Observable.interval(1, TimeUnit.SECONDS)
  }

  override fun streamedResultThatFails(): Observable<Int> {
    return Observable.range(0, 10, scheduler)
      .doOnNext { if (it == 5) throw RuntimeException("boom") }
  }

  // function to test https://gitlab.com/bluebank/braid/merge_requests/76
  override fun slowData(): List<SlowDataItem> {
    Thread.sleep((Math.random() * 1000).toLong())
    return (1..1000).map { SlowDataItem(Date()) }
  }

  override fun createDao(
    daoName: String,
    minimumMemberCount: Int,
    strictMode: Boolean,
    notaryName: CordaX500Name
  ): Future<DaoState> {
    return Future.succeededFuture(
      DaoState(
        daoName,
        minimumMemberCount,
        strictMode,
        notaryName
      )
    )
  }

  override fun useInstant(instant: Instant): String {
    return instant.toString()
  }

  override fun useDate(date: Date): String {
    return date.toString()
  }
}

data class SlowDataItem(val date: Date)

data class DaoState(
  val name: String,
  val minimumMemberCount: Int,
  val strictMode: Boolean,
  val notaryName: CordaX500Name
)