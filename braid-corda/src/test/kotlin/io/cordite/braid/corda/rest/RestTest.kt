/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.rest

import io.cordite.braid.corda.BraidCordaContext
import io.cordite.braid.corda.server.*
import io.cordite.braid.corda.testing.PARTY_A_IDENTITY
import io.cordite.braid.corda.testing.TestAppServiceHub.Companion.DEFAULT_SERVICE_HUB
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.client.BraidClient
import io.cordite.braid.core.client.BraidClientConfig
import io.cordite.braid.core.http.*
import io.cordite.braid.core.json.JsonQuery.SLASH
import io.cordite.braid.core.json.JsonQuery.query
import io.cordite.braid.core.openapi.auth.LoginRequest
import io.cordite.braid.core.socket.findFreePort
import io.netty.handler.codec.http.HttpHeaderValues.APPLICATION_OCTET_STREAM
import io.netty.handler.codec.http.HttpHeaderValues.TEXT_PLAIN
import io.vertx.core.Promise
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.http.HttpHeaders.AUTHORIZATION
import io.vertx.core.http.HttpHeaders.CONTENT_TYPE
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.PubSecKeyOptions
import io.vertx.ext.auth.User
import io.vertx.ext.auth.jwt.JWTAuth
import io.vertx.ext.auth.jwt.JWTAuthOptions
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import net.corda.core.identity.CordaX500Name
import net.corda.core.utilities.contextLogger
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@Suppress("DEPRECATION")
@RunWith(VertxUnitRunner::class)
class RestTest {

  companion object {
    private val log = contextLogger()
    private const val username = "fuzz"
    private const val password = "ball"
  }

  private val headerValues = listOf(1, 2, 3)
  private val braidContext = BraidCordaContext.create(DEFAULT_SERVICE_HUB)

  private val port = findFreePort()
  private val service = TestServiceApp(port, username, password)
  private val client = service.braidContext.vertx.createHttpClient(
      HttpClientOptions()
          .setDefaultHost("localhost")
          .setDefaultPort(port)
  )

  @Before
  fun before() {
    service.whenReady().getOrThrow()
  }

  @After
  fun after() {
    try {
      service.shutdown()
      braidContext.close().getOrThrow()
      client.close()
    } catch(err: Throwable) {
      log.error("failed to shutdown test", err)
    }
  }

  @Test
  fun `that we can retrieve the swagger json`() {
    @Suppress("DEPRECATION")
    val swaggerJson = getSwagger()
    println(swaggerJson)
  }

  @Test
  fun `that we can retrieve the swagger website`() {
    client.futureGet("${TestServiceApp.OPEN_API_ROOT}/")
      .map { it.verifyNoError() }
      .compose { it.bodyAsString() }
      .map { body -> assertTrue(body.contains("swagger-ui", true)) }
      .getOrThrow()
  }

  @Test
  fun `that we call the hello method`() {
    client.futureGet("${TestServiceApp.REST_API_ROOT}/hello")
      .map { it.verifyNoError() }
      .compose { it.bodyAsString() }
      .map { body -> assertEquals("hello", body) }
      .getOrThrow()
  }

  @Test
  fun `that we can retrieve a binary buffer`() {
    client.futureGet("${TestServiceApp.REST_API_ROOT}/buffer")
      .map { it.verifyNoError() }
      .compose { response ->
        assertEquals(APPLICATION_OCTET_STREAM.toString(), response.getHeader(CONTENT_TYPE))
        response.bodyAsString()
      }
      .map { body -> assertEquals("hello", body.toString()) }
      .getOrThrow()
  }

  @Test
  fun `that we can return a byte array`() {
    client.futureGet("${TestServiceApp.REST_API_ROOT}/bytearray")
      .map { it.verifyNoError() }
      .compose { response ->
        assertEquals(APPLICATION_OCTET_STREAM.toString(), response.getHeader(CONTENT_TYPE))
        response.bodyAsString()
      }
      .map { body -> assertEquals("hello", body.toString()) }
      .getOrThrow()
  }

  @Test
  fun `that we can return a bytebuf`() {
    client.futureGet("${TestServiceApp.REST_API_ROOT}/bytebuf")
      .map { it.verifyNoError() }
      .compose { response ->
        assertEquals(
          APPLICATION_OCTET_STREAM.toString(),
          response.getHeader(CONTENT_TYPE)
        )
        response.bodyAsString()
      }
      .map { body -> assertEquals("hello", body) }
      .getOrThrow()
  }

  @Test
  fun `that we can return a bytebuffer`() {
    client.futureGet("${TestServiceApp.REST_API_ROOT}/bytebuffer")
      .map { it.verifyNoError() }
      .compose { response ->
        assertEquals(
          APPLICATION_OCTET_STREAM.toString(),
          response.getHeader(CONTENT_TYPE)
        )
        response.bodyAsString()
      }
      .map { body -> assertEquals("hello", body) }
      .getOrThrow()
  }

  @Test
  fun `that we can send and receive a buffer`() {
    val bytes = Buffer.buffer("hello")
    client.futurePost(braidContext = braidContext, body = bytes, path = "${TestServiceApp.REST_API_ROOT}/doublebuffer")
      .map { it.verifyNoError() }
      .compose { response ->
        assertEquals(APPLICATION_OCTET_STREAM.toString(), response.getHeader(CONTENT_TYPE))
        response.bodyAsString()
      }
      .map { body ->
        assertEquals("hellohello", body.toString())
      }
      .getOrThrow()
  }

  @Test
  fun `that we can login and call a protected method`() {
    val accessToken = client
      .futurePost(
        braidContext = braidContext,
        body = LoginRequest(
          username,
          password
        ),
        path = "${TestServiceApp.REST_API_ROOT}/auth/login"
      )
      .map { it.verifyNoError() }
      .compose { it.bodyAsString() }
      .getOrThrow()

    // verify that the JWT has been correctly encrypted with the jwt secret provided in RestConfig
    val jwtAuth = JWTAuth.create(
      service.braidContext.vertx, JWTAuthOptions()
        .addPubSecKey(
          PubSecKeyOptions()
            .setAlgorithm("HS256")
            .setPublicKey(TestServiceApp.TEST_JWT_SECRET)
            .setSymmetric(true)
        )
    )

    val decodedUserFuture = Promise.promise<User>()
    jwtAuth.authenticate(json { obj("jwt" to accessToken) }, decodedUserFuture::handle)
    val decodedUser = decodedUserFuture.future().getOrThrow()

    log.info("decoded user is ${decodedUser.principal()}")
    assertEquals(username, decodedUser.principal().getString("user"))
    client.futurePost(
      braidContext = braidContext,
      headers = mapOf(AUTHORIZATION.toString() to "Bearer $accessToken"),
      body = "hello",
      path = "${TestServiceApp.REST_API_ROOT}/echo"
    )
      .map { it.verifyNoError() }
      .compose { request -> request.bodyAsString() }
      .map { body ->
        assertEquals("echo: hello", body)
      }
      .getOrThrow()
  }

  @Test
  fun `that we can send a list of items as a header with a single name`() {
    client.futureGet(
      path = "${TestServiceApp.REST_API_ROOT}/headers/list/string",
      headers = mapOf(X_HEADER_LIST_STRING to headerValues.map { it.toString() })
    )
      .map { it.verifyNoError() }
      .compose { request -> request.bodyAsJsonList<String>(braidContext) }
      .map { body -> assertEquals(headerValues.map { it.toString() }, body) }
      .getOrThrow()

  }

  @Test
  fun `that we can send a list as headers to a function that receives ints`() {
    client.futureGet(
      path = "${TestServiceApp.REST_API_ROOT}/headers/list/int",
      headers = mapOf(X_HEADER_LIST_INT to headerValues)
    )
      .map { it.verifyNoError() }
      .compose { request -> request.bodyAsJsonList<String>(braidContext) }
      .map { body ->
        assertEquals(headerValues.map { it.toString() }, body)
      }
      .getOrThrow()
  }

  @Test
  fun `that we can send a bunch of headers and return them in the body payload`() {
    client.futureGet(
      path = "${TestServiceApp.REST_API_ROOT}/headers",
      headers = mapOf(X_HEADER_LIST_STRING to headerValues)
    )
      .map { it.verifyNoError() }
      .compose { request -> request.bodyAsJsonList<String>(braidContext) }
      .map { body ->
        assertEquals(headerValues.map { it.toString() }, body)
      }
      .getOrThrow()
  }

  @Test
  fun `that we can send headers to method that receives them optionally`() {
    val testString = "this is a test"
    client.futureGet(
      path = "${TestServiceApp.REST_API_ROOT}/headers/optional",
      headers = mapOf(X_HEADER_STRING to testString)
    )
      .map { it.verifyNoError() }
      .compose { request -> request.bodyAsString() }
      .map { body -> assertEquals(testString, body) }
      .getOrThrow()
  }

  @Test
  fun `that we can call a method that receives headers optionally with no headers`() {
    // N.B. no header set
    client.futureGet(path = "${TestServiceApp.REST_API_ROOT}/headers/optional")
      .map { it.verifyNoError() }
      .compose { request -> request.bodyAsString() }
      .map { body -> assertEquals("null", body) }
      .getOrThrow()
  }

  @Test
  fun `that calling a method that receives header non-optionally without headers causes an exception`() {
    // N.B. no header set
    client.futureGet(path = "${TestServiceApp.REST_API_ROOT}/headers/non-optional")
      .map { it.verifyError(400) }
      .compose { request -> request.bodyAsString() }
      .map { body ->
        assertEquals(
          "method GET ${TestServiceApp.REST_API_ROOT}/headers/non-optional requires header '$X_HEADER_STRING'",
          body
        )
      }
      .getOrThrow()
  }

  @Test
  fun `test that method that fails returns all headers`(context: TestContext) {
    val client = service.braidContext.vertx.createHttpClient()
    val async1 = context.async()
    client.get(port, "localhost", "${TestServiceApp.REST_API_ROOT}/willfail")
      .exceptionHandler(context::fail)
      .handler {
        context.assertEquals(400, it.statusCode())
        context.assertEquals("total fail!", it.statusMessage())
        val expectedContentType = "${TEXT_PLAIN};charset=UTF-8"
        context.assertEquals(expectedContentType, it.getHeader(CONTENT_TYPE))

        it.bodyHandler { buffer ->
          val body = buffer.toString("utf8")
          context.assertEquals("total fail!", body)
          async1.complete()
        }
      }
      .end()
  }

  @Test
  fun `that we can round trip CordaX500Name`() {
    val name = PARTY_A_IDENTITY.name

    client.futureGet(
      headers = mapOf("name" to name.toString()),
      path = "${TestServiceApp.REST_API_ROOT}/corda-x500-name"
    )
      .map { it.verifyNoError() }
      .compose { request -> request.bodyAsString() }
      .map { body ->
        braidContext.decodeValue(body, CordaX500Name::class)
      }
      .map { returnedName -> assertEquals(name, returnedName) }
      .getOrThrow()
  }

  @Test
  fun `that we can invoke a protected json-rpc method`() {
    val client = BraidClient.createClient(
      config = BraidClientConfig(
        braidContext = braidContext,
        serviceClass = TestService::class.java,
        port = port,
        tls = false,
        authCredentials = LoginRequest(username, password)
      )
    )
    val proxy = client.bind()
    val message = "hello"
    val result = proxy.echo(message)
    assertEquals("echo: $message", result)
    client.close()
  }

  @Test
  fun `that swagger maps CordaX500Name to string`() {
    val swagger = getSwagger()
    val type = swagger.query<String>(
      braidContext,
      "/paths/${SLASH}corda-x500-name/get/responses/200/content/application${SLASH}json/schema/type"
    )
    assertEquals("string", type)
  }

  private fun getSwagger(): JsonObject {
    return client
      .futureGet(path = "${TestServiceApp.OPEN_API_ROOT}/swagger.json")
      .map { it.verifyNoError() }
      .compose { it.body() }
      .map { JsonObject(it) }
      .getOrThrow()
  }

  @Test
  fun `that swagger maps lists of CordaX500Name to list of strings`() {
    val swagger = getSwagger()
    val schema = swagger.query<String>(
      braidContext,
      "/paths/${SLASH}corda-x500-name/post/responses/200/content/application${SLASH}json/schema"
    )!!
    val type = schema.query<String>(braidContext, "/type")!!
    val itemsType = schema.query<String>(braidContext, "/items/type")!!

    assertEquals("array", type)
    assertEquals("string", itemsType)
  }

  @Test
  fun `that swagger maps lists of ComplexObject to list of references`() {
    val swagger = getSwagger()
    val schemaReference = swagger.query<JsonObject>(
      braidContext,
      "/paths/${SLASH}complex-objects/post/responses/200/content/application${SLASH}json/schema"
    )!!
    val type = schemaReference.query<String>(braidContext, "/type")!!
    val itemsType = schemaReference.query<String>(braidContext, "/items/\$ref")!!
    val schema = braidContext.getOpenAPISchemaReference(ComplexObject::class.java)!!
    assertEquals("array", type)
    assertEquals(schema.`$ref`, itemsType)
  }
}

