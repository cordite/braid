/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.server

import io.cordite.braid.corda.testing.PARTY_A_IDENTITY
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.http.HttpServerConfig
import io.cordite.braid.core.server.BraidServer
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.AnonymousParty
import net.corda.core.identity.Party
import javax.ws.rs.GET
import javax.ws.rs.Path

// These classes are used for quickly trying out an end point definition to debug issues. Mostly because [TestService]
// has a very large surface. Perhaps the latter should be refactored.

class TestSandboxService {

  @GET
  @Path("/party")
  fun party(): Party {
    return PARTY_A_IDENTITY.party
  }

  @GET
  @Path("/anonymous")
  fun anonymous(): AnonymousParty {
    return PARTY_A_IDENTITY.party.anonymise()
  }

}

class TestSandboxApp(braidContext: BraidContext, port: Int) {
  companion object {

    @JvmStatic
    fun main(args: Array<String>) {
      TestSandboxApp(BraidContext.create(), 8080)
    }
  }

  private var braidServer: BraidServer? = BraidConfig()
      .withPort(port)
      .withService(braidContext, TestSandboxService())
      .withHttpServerConfig(
          HttpServerConfig(tlsEnabled = false)
      )
      .amendRestConfig {
        withDebugMode()
      }.start(braidContext).getOrThrow()

}