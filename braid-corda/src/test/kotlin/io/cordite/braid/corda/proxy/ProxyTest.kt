/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.proxy

import io.cordite.braid.corda.BraidCordaContext
import io.cordite.braid.corda.server.ComplexObject
import io.cordite.braid.corda.server.TestService
import io.cordite.braid.corda.server.TestServiceImpl
import io.cordite.braid.corda.testing.TestAppServiceHub.Companion.DEFAULT_SERVICE_HUB
import io.cordite.braid.corda.testing.TestAuthProvider
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.client.BraidClient
import io.cordite.braid.core.client.BraidClientConfig
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.openapi.auth.LoginRequest
import io.vertx.ext.unit.junit.VertxUnitRunner
import net.corda.core.utilities.getOrThrow
import org.junit.AfterClass
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import java.math.BigDecimal
import java.net.ServerSocket

@RunWith(VertxUnitRunner::class)
class ProxyTest {

  companion object {

    private val port = getFreePort()
    private val braidContext =
      BraidCordaContext.create(appServiceHub = DEFAULT_SERVICE_HUB, authConstructor = { TestAuthProvider() })
    private val braidServer = BraidConfig()
      .withService(braidContext, TestServiceImpl(braidContext))
      .withPort(port)
      .start(braidContext).getOrThrow()
    private val braidClient = BraidClient.createClient(
      config = BraidClientConfig(
        braidContext = braidContext,
        serviceClass = TestService::class.java,
        port = port,
        trustAll = true,
        authCredentials = LoginRequest("admin", "admin")
      )
    )
    private val testService = braidClient.bind()

    @AfterClass
    @JvmStatic
    fun afterClass() {
      braidClient.close()
      braidServer.stop().getOrThrow()
      braidContext.close().getOrThrow()
    }

    private fun getFreePort(): Int {
      return (ServerSocket(0)).use {
        it.localPort
      }
    }
  }

  @Test
  fun `should be able to add two numbers together`() {
    val result = testService.add(1.0, 2.0)
    assertEquals(0, braidClient.activeRequestsCount())
    assertEquals(3.0, result, 0.0001)
  }

  @Test
  fun `sending a request to a no args function finds the function`() {
    val result = testService.noArgs()
    assertEquals(5, result)
  }

  @Test
  fun `sending a request to a client with multiple functions with the same name and number of parameters finds the correct function - long input`() {
    val result = testService.functionWithTheSameNameAndNumberOfParameters(200L, "100.123")
    assertEquals(
      5,
      result
    ) // will always be a perfect match with the int overload. sorry, that's javascript
  }

  @Test
  fun `sending a request to a client with multiple functions with the same name and number of parameters finds the correct function - int input`() {
    // long version always chosen over int
    val result = testService.functionWithTheSameNameAndNumberOfParameters(200, "account id")
    assertEquals(5, result)
  }

  @Test
  fun `sending a request to a client with multiple functions with the same name and number of parameters finds the correct function (float version) - float input`() {
    // double always chosen over float
    val result =
      testService.functionWithTheSameNameAndNumberOfParameters(200.1234F, "100.123")
    assertEquals(7, result) // float binding is nearer
  }

  @Test
  fun `sending a request to a client with multiple functions with the same name and number of parameters finds the correct function - double input`() {
    val result =
      testService.functionWithTheSameNameAndNumberOfParameters(200.1234, "100.123")
    assertEquals(7, result)
  }

  @Test
  fun `sending a request to a client with multiple functions with the same name and number of parameters finds the correct function - complex object input`() {
    val result = testService.functionWithTheSameNameAndNumberOfParameters(
      ComplexObject("1", 2, 3.0),
      "100.123"
    )
    assertEquals(8, result)
  }

  @Test
  fun `sending a request to a client with multiple functions with the same name and number of parameters finds the correct function - null complex object input`() {
    val result =
      testService.functionWithTheSameNameAndNumberOfParameters(null, "account id")
    assertEquals(8, result)
  }

  @Test
  fun `sending a request to a client with multiple functions with the same name and number of parameters finds the correct function - list input`() {
    val result = testService.functionWithTheSameNameAndNumberOfParameters(
      listOf("a", "b", "c"),
      "100.123"
    )
    assertEquals(9, result)
  }

  @Test
  fun `sending a request to a client with multiple functions with the same name and number of parameters finds the correct function (list version) - array input`() {
    // list always chosen over array
    val result = testService.functionWithTheSameNameAndNumberOfParameters(
      arrayOf("a", "b", "c"),
      "100.123"
    )
    assertEquals(9, result)
  }

  @Test
  fun `sending a request to a client with multiple functions with the same name and number of parameters finds the correct function - string input`() {
    val result = testService.functionWithTheSameNameAndNumberOfParameters(
      "not a number",
      "My Netflix account"
    )
    assertEquals(1, result)
  }

  @Test
  fun `eager lazy seq`() {
    (0..10).toList()
      .asSequence()
      .map {
        println(it)
        it
      }.firstOrNull()

  }

  @Test
  fun `sending a request to a client with multiple functions with the same name and number of parameters finds the correct function (BigDecimal version) - big decimal input`() {
    // string always chosen over big decimal
    val functionWithABigDecimalParameterResult =
      testService.functionWithTheSameNameAndNumberOfParameters(
        BigDecimal("200.12345"),
        "My Netflix account"
      )
    assertEquals(2, functionWithABigDecimalParameterResult)
    val functionWithTwoBigDecimalParametersResult =
      testService.functionWithTheSameNameAndNumberOfParameters(
        BigDecimal("200.12345"),
        BigDecimal("200.12345")
      )
    assertEquals(3, functionWithTwoBigDecimalParametersResult)
    val functionWithBigDecimalAndStringNumberParametersResult =
      testService.functionWithTheSameNameAndNumberOfParameters(
        BigDecimal("200.12345"),
        "200.12345"
      )
    assertEquals(3, functionWithBigDecimalAndStringNumberParametersResult)
  }

  @Test
  fun `sending a request with a single parameter finds the function - string input`() {
    val result = testService.functionWithTheSameNameAndASingleParameter("A string")
    assertEquals(12, result)
  }

  @Test
  fun `sending a request with a single parameter finds the function - null string input`() {
    val result = testService.functionWithTheSameNameAndASingleParameter(null)
    assertEquals(12, result)
  }

  @Test
  fun `sending a request with a single parameter finds the function - long input`() {
    val result = testService.functionWithTheSameNameAndASingleParameter(11L)
    assertEquals(14, result) // perfect match with the int overload - sorry, javascript
  }

  @Test
  fun `sending a request with a single parameter finds the function (int version) - int input`() {
    // long version always chosen over int
    val result = testService.functionWithTheSameNameAndASingleParameter(12)
    assertEquals(14, result) // perfect match with the int overload
  }

  @Test
  fun `sending a request with a single parameter finds the function - double input`() {
    val result = testService.functionWithTheSameNameAndASingleParameter(12.34)
    assertEquals(15, result)
  }

  @Test
  fun `sending a request with a single parameter finds the function (double version) - float input`() {
    // double always chosen over float
    val result = testService.functionWithTheSameNameAndASingleParameter(12.34F)
    assertEquals(15, result)
  }

  @Test
  fun `sending a request with a single parameter finds the function - complex object input`() {
    val result =
      testService.functionWithTheSameNameAndASingleParameter(ComplexObject("hi", 1, 2.0))
    assertEquals(17, result)
  }

  @Test
  fun `sending a request with a single parameter finds the function - list input`() {
    val result = testService.functionWithTheSameNameAndASingleParameter(
      listOf(
        "i",
        "am",
        "a",
        "string"
      )
    )
    assertEquals(18, result)
  }

  @Test
  fun `sending a request with a single parameter finds the function - array input`() {
    val result = testService.functionWithTheSameNameAndASingleParameter(
      arrayOf(
        "i",
        "am",
        "a",
        "string"
      )
    )
    assertEquals(18, result)
  }

  @Test
  fun `sending a request to a big decimal function finds the function`() {
    val result = testService.functionWithBigDecimalParameters(
      BigDecimal("200.12345"),
      BigDecimal("200.12345")
    )
    assertEquals(21, result)
  }

  @Test
  fun `sending a complex type to an overloaded method that includes Map should always choose the map`() {
    val result = testService.functionWithComplexOrDynamicType(
      ComplexObject(
        "foo",
        Int.MAX_VALUE,
        Double.MAX_VALUE
      )
    )
    assertEquals(23, result)
  }

  @Test
  fun `sending a map type to an overloaded method that includes Map should always choose the map`() {
    val result = testService.functionWithComplexOrDynamicType(
      mapOf(
        "name" to "foo",
        "amount" to Int.MAX_VALUE
      )
    )
    assertEquals(23, result)
  }

  @Test
  fun `that we can call a method that returns a CordaFuture`() {
    val result = testService.sayHelloAsync().getOrThrow()
    assertEquals("hello, async!", result)
  }
}
