/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.rest

import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.constants.BraidConstants.BRAID_AUTHORIZATION_SCHEME_NAME
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.openapi.utils.EndPointUtilities
import io.swagger.v3.core.util.Yaml
import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.Operation
import io.swagger.v3.oas.models.PathItem
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.media.Schema
import io.swagger.v3.oas.models.parameters.RequestBody
import io.swagger.v3.oas.models.responses.ApiResponse
import io.swagger.v3.oas.models.responses.ApiResponses
import io.swagger.v3.oas.models.security.SecurityScheme
import io.swagger.v3.oas.models.servers.Server
import org.junit.After
import org.junit.Test
import java.lang.reflect.Type
import java.math.BigDecimal
import java.util.*
import kotlin.reflect.KClass
import io.swagger.v3.oas.annotations.media.Schema as SchemaAnnotation

data class Tag(val category: String, val value: String)

data class CreateAccountRequest(
  @SchemaAnnotation(example = "my-account-1")
  val accountId: String,
  @SchemaAnnotation(example = "GBP")
  val currency: Currency,
  val functionalUnitId: UUID,
  val aliases: Set<Tag> = emptySet(),
  @SchemaAnnotation(example = "0")
  val minimumBalance: BigDecimal = BigDecimal.ZERO
)

data class AccountAddress(
  val accountId: String,
  val functionalUnit: UUID,
  val organisation: String
) {

  companion object {

    private val RE = Regex("^([^:]+):([^:]+):([^:]+)$")
    private val ACCOUNT_ID_RE = Regex("^[^:\\s]+$")
    fun parse(address: String): AccountAddress {
      val mr = RE.matchEntire(address) ?: throw ParseException("failed to parse $address")
      val (_, accountId, functionalUnitStr, party) = mr.groupValues
      val functionalUnitId = UUID.fromString(functionalUnitStr)
      return AccountAddress(accountId, functionalUnitId, party)
    }

    fun create(
      accountId: String,
      organisation: String,
      functionalUnitId: UUID
    ): AccountAddress {
      validateAccountId(accountId)
      return AccountAddress(accountId, functionalUnitId, organisation)
    }

    private fun validateAccountId(accountId: String) {
      if (!ACCOUNT_ID_RE.matches(accountId)) {
        throw ParseException("accountId must match ${ACCOUNT_ID_RE.pattern}")
      }
    }
  }

  override fun toString(): String {
    return "$accountId:$functionalUnit:$organisation"
  }

  class ParseException(msg: String) : RuntimeException(msg)
}

interface Account {

  val address: AccountAddress
  val currency: Currency
  val minimumBalance: BigDecimal
  val aliases: Set<Tag>
  fun getAllAliases(): Set<Tag>
}

class SwaggerTest {

  private val braidContext = BraidContext.create()
  @After
  fun after() {
    braidContext.close().getOrThrow()
  }

  @Test
  fun swaggerBuildTest() {
    braidContext.openapiComponents.apply {
      addType(CreateAccountRequest::class)
      addType(Account::class)
    }

    val endpointUtils = EndPointUtilities(braidContext)

    braidContext.openapiComponents.addSecuritySchemes(BRAID_AUTHORIZATION_SCHEME_NAME, SecurityScheme().apply {
      type = SecurityScheme.Type.HTTP
      `in` = SecurityScheme.In.HEADER
      scheme = "bearer"
    })

    val openApi = OpenAPI().apply {
      info = Info().apply {
        version = "1.0.0"
        title = "Swagger Petstore"
        contact = Contact().apply {
          name = "Cordite"
          email = "community@cordite.io"
          url = "https://cordite.foundation"
        }
      }
      addServersItem(Server().url("http://localhost:8080"))
      components = braidContext.openapiComponents
    }

    @Suppress("DEPRECATION")
    openApi.path(
      "/api/accounts", PathItem().post(
        Operation().apply {
          this.requestBody = RequestBody().apply {
            content = endpointUtils.getContent(CreateAccountRequest::class)
            required = true
          }
          responses = ApiResponses().apply {
            this["200"] = ApiResponse().apply {
              content = endpointUtils.getContent(Account::class)
            }
          }
        }
      )
    )

    Yaml.pretty().writeValueAsString(openApi)
  }

  private fun Components.addType(type: KClass<*>): Schema<*> {
    return addType(type.java)
  }

  private fun Components.addType(type: Type): Schema<*> {
    val resolvedSchema = braidContext.modelConverters.readAllAsResolvedSchema(type)
    resolvedSchema.referencedSchemas.forEach { (key, value) -> addSchemas(key, value) }
    return resolvedSchema.schema
  }

  @Test
  fun deser() {
    val yml = """---
swagger: "2.0"
info:
  version: "1.0.0"
  title: "Swagger Petstore"
  contact:
    name: "Em Tech"
    url: "https://cordite.foundation"
    email: "community@cordite.io"
host: "localhost:8080"
schemes:
- "http"
consumes:
- "application/json"
produces:
- "application/json"
paths:
  /api/accounts:
    post:
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "CreateAccountRequest"
        required: true
        schema:
          type: string
      responses:
        default:
          description: "response"
          schema:
            ${'$'}ref: "#/definitions/Account"
securityDefinitions:
  api-key:
    type: "apiKey"
    name: "key"
    in: "header"
definitions:
  CreateAccountRequest:
    type: "object"
    properties:
      accountId:
        type: "string"
      currency:
        type: "string"
      id:
        type: "string"
  Account:
    type: "object"
    properties:
      address:
        ${'$'}ref: "#/definitions/AccountAddress"
      currency:
        type: "string"
      tags:
        type: "object"
        additionalProperties:
          type: "string"
      canonicalId:
        type: "string"
        readOnly: true
  AccountAddress:
    type: "object"
    properties:
      accountId:
        type: "string"
      functionalUnit:
        type: "string"
      organisation:
        type: "string"
      """

    Yaml.mapper().readValue(yml, OpenAPI::class.java)
  }
}