/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.server

import io.cordite.braid.corda.BraidCordaContext
import io.cordite.braid.corda.testing.TestAppServiceHub.Companion.DEFAULT_SERVICE_HUB
import io.cordite.braid.corda.testing.TestAuthProvider
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.http.HttpServerConfig
import io.cordite.braid.core.http.endWithUtf8
import io.cordite.braid.core.openapi.auth.AuthSchema
import io.cordite.braid.core.router.Routers
import io.cordite.braid.core.server.findAndStartAllBraidServices
import io.swagger.v3.oas.models.info.Contact
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.ext.auth.AuthProvider
import io.vertx.ext.auth.jwt.JWTAuth
import net.corda.core.utilities.contextLogger

class TestServiceApp(
  port: Int,
  private val username: String,
  private val password: String
) {

  companion object {

    private val log = contextLogger()
    const val OPEN_API_ROOT = "/openapi"
    const val REST_API_ROOT = "/rest-api"
    const val TEST_JWT_SECRET = "kul@sh@k3r"

    @JvmStatic
    fun main(args: Array<String>) {
      TestServiceApp(8080, "sa", "admin")
    }
  }

  // use the following when enabling SSL
  //  private val tempJKS = File.createTempFile("temp-", ".jceks")
  private lateinit var jwtAuth: JWTAuth
  internal val braidContext = BraidCordaContext.create(
    appServiceHub = DEFAULT_SERVICE_HUB,
    authConstructor = this::createAuthProvider
  ).also { bc ->
    val router = Routers.create(bc.vertx, port)
    router.get().order(Int.MAX_VALUE).handler { it ->
      it.endWithUtf8("My simple UI")
    }
  }
  private val braidConfig = BraidConfig()
    .withPort(port)
    .findAndStartAllBraidServices(braidContext)
    .withHttpServerConfig(
      HttpServerConfig(tlsEnabled = false)
    )
    .amendRestConfig {
      withContact(Contact().email("foo@bar.io"))
        .withVersion("1.0.0")
        .withAuthSchema(AuthSchema.Token)
        .withOpenApiPath(OPEN_API_ROOT)
        .withRestPath(REST_API_ROOT)
        .withDebugMode()
        .withJwtSecret(TEST_JWT_SECRET)
    }
  private val server = braidConfig.start(braidContext).getOrThrow()

  fun whenReady(): Future<String> = server.whenReady()
  fun shutdown() = server.stop().getOrThrow()

  private fun createAuthProvider(@Suppress("UNUSED_PARAMETER") vertx: Vertx): AuthProvider {
    return TestAuthProvider(username, password)
  }
}
