/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.integration

import io.cordite.braid.corda.server.TestSharedNetwork
import io.cordite.braid.corda.server.cordapp.CustomService
import io.cordite.braid.corda.testing.AbstractCordaNetworkTest
import io.cordite.braid.corda.testing.PARTY_A_IDENTITY
import io.cordite.braid.corda.testing.TestNode
import io.cordite.braid.core.async.getOrThrow
import io.vertx.ext.unit.junit.VertxUnitRunner
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import net.corda.core.utilities.getOrThrow
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertEquals

@RunWith(VertxUnitRunner::class)
class BraidClientIntegrationTest : AbstractCordaNetworkTest(TestSharedNetwork) {

  private val credentials = json { obj("username" to "admin", "password" to "admin") }
  private val testNodeA: TestNode get() = getNode(PARTY_A_IDENTITY.name, credentials)
  private val customService = testNodeA.bindService<CustomService>()

  @Test
  fun `that we can use the braid api to call the custom service`() {
    assertEquals(3, customService.add(1, 2))
    assertEquals("0", customService.asyncResult().getOrThrow())
    assertEquals("0", customService.asyncCordaResult().getOrThrow())
  }
}
