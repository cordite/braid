/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.testing

import io.cordite.braid.corda.BraidCordaContext
import io.vertx.core.Vertx
import net.corda.core.identity.CordaX500Name
import net.corda.core.utilities.loggerFor
import net.corda.testing.common.internal.testNetworkParameters
import net.corda.testing.core.DUMMY_NOTARY_NAME
import net.corda.testing.node.*
import java.util.concurrent.atomic.AtomicInteger

/**
 * A globally available singleton network for reuse across tests
 */
open class SharedNetwork(cordappsForAllNodes: Set<TestCordapp> = emptySet(), validatingNotary: Boolean = true) {

  companion object {

    private val log = loggerFor<SharedNetwork>()
  }

  /**
   * mutable, concurrent, lazily populate map of nodes
   */
  private val nodes = mutableMapOf<CordaX500Name, TestNode>()

  /**
   * a fountain of [Int] joy
   */
  private val intFountain = IntFountain()

  /**
   * shared corda network across all derivations of this class
   */
  val network: MockNetwork =
    MockNetwork(
      MockNetworkParameters(
        cordappsForAllNodes = cordappsForAllNodes,
        networkParameters = testNetworkParameters(
          minimumPlatformVersion = 4
        ),
        notarySpecs = listOf(MockNetworkNotarySpec(name = DUMMY_NOTARY_NAME, validating = validatingNotary))
      )
    )

  val appServiceHub by lazy {
    MockNodeAppServiceHubAdapter(network.defaultNotaryNode)
  }

  val braidContext by lazy {
    BraidCordaContext.create(appServiceHub)
  }

  val vertx: Vertx get() = braidContext.vertx

  /**
   * Network's default notary
   */
  val DEFAULT_NOTARY get() = network.defaultNotaryIdentity
  val DEFAULT_NOTARY_X500 get() = DEFAULT_NOTARY.name
  val DEFAULT_NOTARY_NAME get() = DEFAULT_NOTARY_X500.toString()

  /**
   * allocates and setsup config for braid ports
   */
  @Suppress("MemberVisibilityCanBePrivate")
  val braidPortHelper = BraidPortHelper()

  init {
    Runtime.getRuntime().addShutdownHook(object : Thread() {
      override fun run() {
        shutdown()
      }
    })
  }

  /**
   * called before shutting down the JVM for the tests, to clean up resources
   */
  open fun shutdown() {
    log.info("*** SHUTTING DOWN ***")
    nodes.values.forEach { it.shutdown() }
    nodes.clear()
    vertx.close()
    network.stopNodes()
    log.info("*** SHUTDOWN COMPLETED ***")
  }

  /**
   * retrieve a node by name, creating it if it does not exist
   */
  open fun getNode(name: CordaX500Name, authCredentials: Any? = null): TestNode {
    return getNode(MockNodeParameters(legalName = name), authCredentials)
  }

  /**
   * retrieve by node parameters,creating it if it does not exist
   */
  open fun getNode(mockNodeParameters: MockNodeParameters, authCredentials: Any? = null): TestNode {
    val name = mockNodeParameters.legalName ?: error("legal name must be specified")
    return nodes.computeIfAbsent(name) {
      log.info("*** STARTING NODE $name ***")
      braidPortHelper.setSystemPropertiesFor(name)
      val node = network.createNode(mockNodeParameters)
      network.runNetwork()
      TestNode(
        braidContext,
        node,
        braidPortHelper,
        authCredentials
      )
    }
  }

  /**
   * creates a [H2Server] for when we need to bind to the databases and debug them
   */
  open fun h2Server(): H2Server {
    return H2Server(network, nodes.values.map { it.node })
  }

  /**
   * @return next [Int] from the number fountain - used for isolating data between tests
   * that are sharing this cluster
   */
  open fun nextInt(): Int = intFountain.next()

}

class IntFountain {

  private val atomicInt = AtomicInteger(1)
  fun next() = atomicInt.getAndIncrement()
}
