/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.async

import io.cordite.braid.core.service.AsyncResultAdapter
import io.cordite.braid.core.service.AsyncResultAdapters
import net.corda.core.concurrent.CordaFuture
import net.corda.core.internal.concurrent.doOnComplete
import net.corda.core.internal.concurrent.doOnError
import rx.Observable

object CordaFutureAsyncResultAdapter : AsyncResultAdapter<CordaFuture<*>>() {
  override val handledClass: Class<CordaFuture<*>> = CordaFuture::class.java
  override val isSingleResult: Boolean = true
  override fun adaptInternal(requestId: Long, result: CordaFuture<*>): Observable<Any> {
    return Observable.unsafeCreate { subscriber ->
      result
        .doOnComplete {
          subscriber.onNext(it)
          subscriber.onCompleted()
        }
        .doOnError {
          subscriber.onError(it)
        }
    }
  }

}

@JvmField
val DEFAULT_CORDA_ASYNC_RESULT_ADAPTERS = AsyncResultAdapters.DEFAULT_ASYNC_RESULT_ADAPTERS + CordaFutureAsyncResultAdapter