/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.testing

import io.cordite.braid.corda.BraidCordaContext
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.client.BraidClient
import io.cordite.braid.core.client.BraidClientConfig
import io.cordite.braid.core.http.futureGet
import io.cordite.braid.core.http.getBodyBuffer
import io.cordite.braid.core.jsonrpc.JsonRpcConfig
import io.cordite.braid.core.openapi.config.RestConfig
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.Vertx
import io.vertx.core.http.HttpClient
import io.vertx.core.http.HttpClientOptions
import net.corda.core.identity.Party
import net.corda.core.utilities.loggerFor
import net.corda.testing.node.StartedMockNode

class TestNode(
  val braidContext: BraidCordaContext,
  val node: StartedMockNode,
  braidPortHelper: BraidPortHelper,
  private val authCredentials: Any? = null
) {

  companion object {
    private val log = loggerFor<TestNode>()
  }

  val party: Party = node.info.legalIdentities.first()
  val port = braidPortHelper.portForParty(party)
  val httpClient: HttpClient
  private val braidClients = mutableMapOf<Class<*>, BraidClient<*>>()

  init {
    val succeeded = Promise.promise<Unit>()
    WaitForHttpEndPoint.waitForHttpEndPoint(
      vertx = braidContext.vertx,
      port = port,
      handler = succeeded,
      path = JsonRpcConfig.DEFAULT_JSON_RPC_ROOT_PATH
    )
    succeeded.future().getOrThrow()
    log.info("attempting to bind to test service on $port")
    httpClient = createHttpClient()
    log.info("bound to test service on $port")
  }

  inline fun <reified ServiceType> bindService() = bindService(ServiceType::class.java)

  fun <ServiceType> bindService(serviceClass: Class<ServiceType>): ServiceType {
    @Suppress("UNCHECKED_CAST")
    val braidClient = braidClients.computeIfAbsent(serviceClass) {
      BraidClient.createClient(config = getBraidConfig(serviceClass))
    } as BraidClient<ServiceType>
    return braidClient.bind()
  }

  inline fun <reified ServiceType> getBraidConfig() = getBraidConfig(ServiceType::class.java)
  fun <ServiceType> getBraidConfig(serviceClass: Class<ServiceType>) =
    BraidClientConfig.createBraidConfig(braidContext, serviceClass, port, "localhost", authCredentials)

  private fun createHttpClient(): HttpClient {
    return createHttpClient(braidContext.vertx, port)
  }

  fun swaggerDoc(): Future<String> {
    return httpClient
      .futureGet("${RestConfig.DEFAULT_OPEN_API_PATH}/swagger.json")
      .getBodyBuffer()
      .map { it.toString() }
  }

  fun shutdown() {
    log.info("*** SHUTTING DOWN all Braid clients for ${node.info.legalIdentities.first().name}")
    braidClients.values.forEach { it.close() }
  }
}

private fun createHttpClient(vertx: Vertx, port: Int): HttpClient {
  val httpClientOptions = HttpClientOptions()
    .setDefaultHost("localhost")
    .setDefaultPort(port)
    .setSsl(true)
    .setVerifyHost(false)
    .setTrustAll(true)

  return vertx.createHttpClient(httpClientOptions)
}
