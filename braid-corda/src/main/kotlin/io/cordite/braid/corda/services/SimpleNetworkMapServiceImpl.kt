/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.services

import io.cordite.braid.corda.utils.CordaUtilities.transaction
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.node.NodeInfo
import net.corda.core.utilities.NetworkHostAndPort
import rx.Observable
import rx.Subscription

class SimpleNetworkMapServiceImpl(private val serviceHub: AppServiceHub) : SimpleNetworkMapService {

  init {

  }

  override fun myNodeInfo(): NodeInfo {
    return serviceHub.myInfo
  }

  override fun allNodes(): List<NodeInfo> {
    return serviceHub.networkMapCache.allNodes
  }

  override fun updates(): Observable<List<MapChange>> {
    return Observable.unsafeCreate { subscriber ->
      val dataFeed = serviceHub.networkMapCache.track()
      serviceHub.transaction {
        val snapshot =
          dataFeed.snapshot.map { MapChange(SimpleNetworkMapService.MapChangeType.SNAPSHOT, it) }
        subscriber.onNext(snapshot)
        var subscription: Subscription? = null

        subscription = dataFeed.updates.subscribe { change ->
          if (subscriber.isUnsubscribed) {
            subscription?.unsubscribe()
            subscription = null
          } else {
            subscriber.onNext(listOf(change.asSimple()))
          }
        }
      }
    }
  }

  override fun notaries(x500Name: String?): List<Party> {
    return when {
      x500Name?.isNotEmpty() ?: false -> listOfNotNull(getNotary(CordaX500Name.parse(x500Name!!)))
      else -> notaryIdentities()
    }
  }

  override fun notaryIdentities(): List<Party> {
    return serviceHub.transaction {
      serviceHub.networkMapCache.notaryIdentities
    }
  }

  override fun getNotary(cordaX500Name: CordaX500Name): Party? {
    return serviceHub.transaction {
      serviceHub.networkMapCache.getNotary(cordaX500Name)
    }
  }

  override fun getNodeByAddress(hostAndPort: String): NodeInfo? {
    return serviceHub.transaction {
      serviceHub.networkMapCache.getNodeByAddress(NetworkHostAndPort.parse(hostAndPort))
    }
  }

  override fun getNodeByLegalName(name: CordaX500Name): NodeInfo? {
    return serviceHub.transaction {
      serviceHub.networkMapCache.getNodeByLegalName(name)
    }
  }

  override fun nodes(hostAndPort: String?, x500Name: String?): List<NodeInfo> {
    return when {
      hostAndPort?.isNotEmpty() ?: false -> listOfNotNull(getNodeByAddress(hostAndPort!!))
      x500Name?.isNotEmpty()
        ?: false -> listOfNotNull(getNodeByLegalName(CordaX500Name.parse(x500Name!!)))
      else -> allNodes()
    }
  }

}

