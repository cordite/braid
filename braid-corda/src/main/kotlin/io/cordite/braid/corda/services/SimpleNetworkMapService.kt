/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.services

import io.cordite.braid.core.annotation.BraidService
import io.cordite.braid.core.annotation.JsonRpcIgnore
import io.cordite.braid.core.constants.BraidConstants.BRAID_AUTHORIZATION_SCHEME_NAME
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.security.SecurityScheme
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.NodeInfo
import net.corda.core.node.services.NetworkMapCache
import rx.Observable
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.QueryParam

@BraidService(description = "network information", name = "network")
@SecurityScheme(
  type = SecuritySchemeType.APIKEY,
  `in` = SecuritySchemeIn.HEADER,
  paramName = BRAID_AUTHORIZATION_SCHEME_NAME
)
@Path("/network")
interface SimpleNetworkMapService {

  enum class MapChangeType {
    ADDED,
    REMOVED,
    MODIFIED,
    SNAPSHOT
  }

  fun allNodes(): List<NodeInfo>

  @GET
  @Path("/updates")
  @Operation(summary = "retrieve a stream of network map changes")
  fun updates(): Observable<List<MapChange>>
  fun notaryIdentities(): List<Party>
  fun getNotary(cordaX500Name: CordaX500Name): Party?
  fun getNodeByAddress(hostAndPort: String): NodeInfo?
  fun getNodeByLegalName(name: CordaX500Name): NodeInfo?

  @GET
  @Path("/my-node-info")
  @Operation(summary = "Get my NodeInfo")
  fun myNodeInfo(): NodeInfo

  @GET
  @Path("/notaries")
  @Operation(summary = "Retrieves all notaries in the network")
  @JsonRpcIgnore
  fun notaries(
    @Parameter(
      description = "the X500 name for the node",
      example = "O=PartyB, L=New York, C=US"
    )
    @QueryParam(value = "x500-name") x500Name: String? = null
  ): List<Party>

  @GET
  @Path("/nodes")
  @Operation(
    summary = "Retrieve nodes",
    description = "Retrieves all nodes if neither query parameter is supplied. " +
      "Otherwise returns a list of one node matching the supplied query parameter."
  )
  @JsonRpcIgnore
  fun nodes(
    @QueryParam(value = "hostAndPort")
    @Parameter(
      name = "hostAndPort",
      required = false,
      allowEmptyValue = true,
      `in` = ParameterIn.QUERY,
      schema = Schema(implementation = String::class)
    )
    hostAndPort: String? = null,
//    @Parameter(description = "the X500 name for the node", example = "O=PartyB, L=New York, C=US")
    @QueryParam(value = "x500Name")
    @Parameter(
      name = "x500Name",
      required = false,
      allowEmptyValue = true,
      `in` = ParameterIn.QUERY,
      schema = Schema(implementation = String::class)
    )
    x500Name: String? = null
  ): List<NodeInfo>
}

internal fun NetworkMapCache.MapChange.asSimple(): MapChange {
  return MapChange(this)
}
