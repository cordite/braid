/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.testing

import net.corda.core.concurrent.CordaFuture
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.identity.PartyAndCertificate
import net.corda.core.internal.concurrent.doneFuture
import net.corda.core.messaging.DataFeed
import net.corda.core.node.NodeInfo
import net.corda.core.node.services.NetworkMapCache
import net.corda.core.node.services.PartyInfo
import net.corda.core.utilities.NetworkHostAndPort
import net.corda.testing.core.TestIdentity
import rx.Observable
import java.security.PublicKey

class TestNetworkMapCache : NetworkMapCache {

  override val allNodes: List<NodeInfo> = listOf(
    makeNodeInfo(PARTY_A_IDENTITY, 10_001),
    makeNodeInfo(PARTY_B_IDENTITY, 10_002),
    makeNodeInfo(DUMMY_NOTARY, 10_000)
  )
  override val changed: Observable<NetworkMapCache.MapChange>
    get() = TODO("Not yet implemented")
  override val nodeReady: CordaFuture<Void?> = doneFuture(null)
  override val notaryIdentities: List<Party> = listOf(DUMMY_NOTARY.party)

  override fun clearNetworkMapCache() {
    TODO("Not yet implemented")
  }

  override fun getNodeByAddress(address: NetworkHostAndPort): NodeInfo? {
    return allNodes.firstOrNull { it.addresses.contains(address) }
  }

  override fun getNodeByLegalIdentity(party: AbstractParty): NodeInfo? {
    return allNodes.firstOrNull { nodeInfo -> nodeInfo.legalIdentities.firstOrNull { nodeParty -> nodeParty == party } != null }
  }

  override fun getNodeByLegalName(name: CordaX500Name): NodeInfo? {
    return allNodes.firstOrNull { nodeInfo -> nodeInfo.legalIdentities.firstOrNull { nodeParty -> nodeParty.name == name } != null }
  }

  override fun getNodesByLegalIdentityKey(identityKey: PublicKey): List<NodeInfo> {
    return allNodes.filter { nodeInfo -> nodeInfo.legalIdentities.firstOrNull { nodeParty -> nodeParty.owningKey == identityKey } != null }
  }

  override fun getNodesByLegalName(name: CordaX500Name): List<NodeInfo> {
    return allNodes.filter { nodeInfo -> nodeInfo.legalIdentities.firstOrNull { nodeParty -> nodeParty.name == name } != null }
  }

  override fun getPartyInfo(party: Party): PartyInfo? {
    return getNodeByLegalIdentity(party)?.let {
      PartyInfo.SingleNode(party, it.addresses)
    }
  }

  override fun getPeerCertificateByLegalName(name: CordaX500Name): PartyAndCertificate? {
    TODO("Not yet implemented")
  }

  override fun isValidatingNotary(party: Party): Boolean {
    return false
  }

  override fun track(): DataFeed<List<NodeInfo>, NetworkMapCache.MapChange> {
    TODO("Not yet implemented")
  }

}

private fun makeNodeInfo(testIdentity: TestIdentity, port: Int): NodeInfo {
  return NodeInfo(
    addresses = listOf(NetworkHostAndPort("localhost", port)),
    legalIdentitiesAndCerts = listOf(testIdentity.identity),
    platformVersion = 4,
    serial = 1
  )
}