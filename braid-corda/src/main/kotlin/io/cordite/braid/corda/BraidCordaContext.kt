/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda

import io.cordite.braid.corda.async.DEFAULT_CORDA_ASYNC_RESULT_ADAPTERS
import io.cordite.braid.corda.client.CordaFutureInvocationStrategy
import io.cordite.braid.corda.serialisation.BraidCordaJacksonModule
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.client.invocations.impl.InvocationStrategyFactory
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.server.BraidServer
import io.cordite.braid.core.service.AsyncResultAdapters
import io.swagger.v3.core.util.Json
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.ext.auth.AuthProvider
import net.corda.core.node.AppServiceHub
import net.corda.core.utilities.contextLogger

open class BraidCordaContext private constructor(
  serviceHub: AppServiceHub,
  asyncResultAdapters: AsyncResultAdapters = DEFAULT_CORDA_ASYNC_RESULT_ADAPTERS,
  authConstructor: ((Vertx) -> AuthProvider)? = null
) : BraidContext(asyncResultAdapters = asyncResultAdapters, authConstructor = authConstructor) {

  companion object {

    private val log = contextLogger()

    init {
      InvocationStrategyFactory.addFactory(CordaFutureInvocationStrategy.Factory)
    }

    fun create(appServiceHub: AppServiceHub, authConstructor: ((Vertx) -> AuthProvider)? = null): BraidCordaContext {
      return BraidCordaContext(serviceHub = appServiceHub, authConstructor = authConstructor)
    }
  }

  val serviceHub: AppServiceHub get() = super.getContextObject()

  init {
    super.putContextObject(AppServiceHub::class.java, serviceHub)
    val braidCordaModule = BraidCordaJacksonModule(serviceHub)
    mapper.registerModule(braidCordaModule)
    Json.mapper().registerModule(braidCordaModule)
  }

  override fun internalCreateServer(braidConfig: BraidConfig): Future<BraidServer> {
    serviceHub.let {
      log.info("starting up braid server for ${serviceHub.myInfo.legalIdentities.first().name.organisation} on port ${braidConfig.port}")
    }
    return super.internalCreateServer(braidConfig)
      .onSuccess { braidServer ->
        serviceHub.registerUnloadHandler {
          braidServer.stop().getOrThrow()
        }
      }
  }
}