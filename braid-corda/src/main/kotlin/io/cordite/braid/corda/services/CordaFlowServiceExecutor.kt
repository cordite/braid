/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.services

import io.cordite.braid.corda.services.CordaFlowServiceExecutor.FlowExecutorFactory.Companion.SERVICE_GROUP
import io.cordite.braid.corda.services.CordaFlowServiceExecutor.FlowExecutorFactory.Companion.SERVICE_NAME
import io.cordite.braid.core.annotation.BraidDoNotAutoBind
import io.cordite.braid.core.annotation.BraidService
import io.cordite.braid.core.config.BraidConfig
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.jsonrpc.JsonRPCRequest
import io.cordite.braid.core.jsonrpc.Params
import io.cordite.braid.core.jsonschema.toDescriptor
import io.cordite.braid.core.jsonschema.toJavascriptType
import io.cordite.braid.core.service.MethodDescriptor
import io.cordite.braid.core.service.MethodDoesNotExist
import io.cordite.braid.core.service.ServiceExecutor
import io.cordite.braid.core.service.ServiceExecutorFactory
import net.corda.core.flows.FlowLogic
import net.corda.core.node.AppServiceHub
import net.corda.core.toObservable
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.contextLogger
import rx.Observable
import java.lang.reflect.Constructor
import kotlin.reflect.KClass

data class CordaFlowServiceExecutor(
  private val braidContext: BraidContext,
  val registeredFlows: Map<String, Class<out FlowLogic<*>>> = emptyMap()
) : ServiceExecutor {

  companion object {

    private val log = contextLogger()
  }

  @BraidDoNotAutoBind
  @BraidService(name = SERVICE_NAME, category = SERVICE_GROUP, description = "execute corda flows")
  data class FlowExecutorFactory(
    private val registeredFlowExecutorFactories: Map<String, Class<out FlowLogic<*>>> = emptyMap()
  ) : ServiceExecutorFactory<CordaFlowServiceExecutor> {

    companion object {
      const val SERVICE_NAME = "flows"
      const val SERVICE_GROUP = "corda"
    }
    override fun create(braidContext: BraidContext): CordaFlowServiceExecutor {
      return CordaFlowServiceExecutor(braidContext, registeredFlowExecutorFactories)
    }

    fun withFlow(name: String, flowClass: Class<out FlowLogic<*>>): FlowExecutorFactory {
      val mutableFlows = registeredFlowExecutorFactories.toMutableMap()
      mutableFlows[name] = flowClass
      return copy(registeredFlowExecutorFactories = mutableFlows)
    }
  }

  override fun invoke(request: JsonRPCRequest): Observable<Any> {
    val flow = registeredFlows[request.method]
    return if (flow != null) {
      invoke(request, flow)
    } else {
      Observable.error(MethodDoesNotExist(request.method))
    }
  }

  override fun getMethodDescriptors(): List<MethodDescriptor> {
    return registeredFlows.flatMap { flow ->
      // we filter constructors with progress trackers for now
      // there may be a correct way of handling these but for now, these are not included
      // constructor parameters are denoted as discrete methods appended with the number of parameters that the
      // constructor takes,
      flow.value.constructors.filter {
        !it.parameterTypes.contains(ProgressTracker::class.java)
      }.map {
        val returnType = flow.value.getMethod("call").returnType
        it.toDescriptor(braidContext)
          .withName(flow.key)
          .withReturnType(returnType.toJavascriptType(braidContext))
      }
    }
  }

  private fun invoke(
    request: JsonRPCRequest,
    clazz: Class<out FlowLogic<*>>
  ): Observable<Any> {
    val parameters = request.parameters(braidContext)
    val constructor = clazz.constructors.firstOrNull { it.matches(parameters) }
    return if (constructor == null) {
      Observable.error(MethodDoesNotExist(request.method))
    } else {
      return Observable.unsafeCreate { subscriber ->
        try {
          val params = request.parameters(braidContext).mapParams(constructor)

          @Suppress("UNCHECKED_CAST")
          val flow = constructor.newInstance(*params) as FlowLogic<Any>


          braidContext.getContextObject<AppServiceHub>().startFlow(flow).returnValue
            .toObservable().subscribe({ item ->
              subscriber.onNext(item)
            }, { err ->
              subscriber.onError(err)
            }, {
              subscriber.onCompleted()
            })
        } catch (err: Throwable) {
          subscriber.onError(err)
        }
      }
    }
  }
}

private fun Constructor<*>.matches(params: Params) =
  this.parameterCount == params.count

fun BraidConfig.withFlow(flowClass: KClass<out FlowLogic<*>>): BraidConfig = withFlow(flowClass.java)
fun BraidConfig.withFlow(name: String, flowClass: KClass<out FlowLogic<*>>): BraidConfig = withFlow(name, flowClass.java)
fun BraidConfig.withFlow(flowClass: Class<out FlowLogic<*>>): BraidConfig = withFlow(flowClass.name, flowClass)
fun BraidConfig.withFlow(name: String, flowClass: Class<out FlowLogic<*>>): BraidConfig {
  return amendJsonRpcConfig {
    val newServiceFactoryMap = serviceExecutorFactories.toMutableMap()
    val factory = (newServiceFactoryMap[SERVICE_NAME]
      ?: CordaFlowServiceExecutor.FlowExecutorFactory()) as CordaFlowServiceExecutor.FlowExecutorFactory
    val newFactory = factory.withFlow(name, flowClass)
    newServiceFactoryMap[SERVICE_NAME] = newFactory
    copy(serviceExecutorFactories = newServiceFactoryMap)
  }
}