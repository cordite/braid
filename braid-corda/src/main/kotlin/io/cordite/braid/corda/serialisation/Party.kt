/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.serialisation

//@JsonSerialize(converter = PartyToStringConverter::class)
//abstract class PartyMixin

//class PartyDeserializer(private val identityService: IdentityService?) : StdDeserializer<Party>(Party::class.java) {
//
//  override fun deserialize(parser: JsonParser, ctxt: DeserializationContext): Party {
//    if (identityService == null) error("identityService has not been initialised for jackson. Did you create a ${BraidCordaContext::class.simpleName} without a ${AppServiceHub::class.simpleName}?")
//
//    val name = parser.codec.readValue(parser, CordaX500Name::class.java)
//    return identityService.wellKnownPartyFromX500Name(name) ?: error("failed to find party from x500 name $name")
//  }
//}

//class PartyToStringConverter : StdConverter<Party, String>() {
//
//  override fun convert(value: Party): String {
//    return value.name.toString()
//  }
//}
