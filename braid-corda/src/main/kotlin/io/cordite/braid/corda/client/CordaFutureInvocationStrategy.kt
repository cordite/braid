/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.client

import io.cordite.braid.core.client.invocations.impl.InvocationStrategy
import io.cordite.braid.core.client.invocations.impl.InvocationStrategyFactory
import io.cordite.braid.core.client.invocations.impl.InvocationsStrategySPI
import io.cordite.braid.core.context.BraidJsonContext
import io.cordite.braid.core.jsonrpc.error
import io.cordite.braid.core.jsonrpc.trace
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.reflection.actualType
import net.corda.core.concurrent.CordaFuture
import net.corda.core.internal.concurrent.openFuture
import java.lang.reflect.Type

class CordaFutureInvocationStrategy(
  braidJsonContext: BraidJsonContext,
  method: String,
  returnType: Type,
  params: Array<out Any?>,
  parent: InvocationsStrategySPI
) : InvocationStrategy<CordaFuture<Any?>>(parent, method, returnType, params, braidJsonContext) {

  companion object {

    private val log = loggerFor<CordaFutureInvocationStrategy>()
  }

  object Factory : InvocationStrategyFactory<CordaFuture<Any?>> {

    override fun create(
      parent: InvocationsStrategySPI,
      method: String,
      returnType: Type,
      params: Array<out Any?>,
      braidJsonContext: BraidJsonContext
    ): InvocationStrategy<CordaFuture<Any?>>? {
      return when (CordaFuture::class.java) {
        returnType.actualType() -> CordaFutureInvocationStrategy(braidJsonContext, method, returnType, params, parent)
        else -> null
      }
    }

    override val handledClass: Class<*> = CordaFuture::class.java
  }

  private val future = openFuture<Any?>()
  private var receivedCompletion = false

  override fun getResult(): CordaFuture<Any?> {
    try {
      requestId = nextRequestId()
      log.trace(requestId) { "invocation of $method initiated" }
      beginInvoke(requestId)
    } catch (err: Throwable) {
      log.error(requestId, err) { "failure in issuing invocation request" }
      endInvoke(requestId)
      future.setException(err)
    }
    return future
  }

  override fun onNext(requestId: Long, item: Any?) {
    log.trace(requestId) { "process onNext $item" }
    checkIdIsSet(requestId)
    endInvoke(requestId)
    check(!future.isDone) { "future should not be completed" }
    future.set(item)
  }

  override fun onError(requestId: Long, error: Throwable) {
    checkIdIsSet(requestId)
    endInvoke(requestId)
    check(!future.isDone) { "future should not be completed" }
    future.setException(error)
  }

  override fun onCompleted(requestId: Long) {
    checkIdIsSet(requestId)
    endInvoke(requestId)
    check(future.isDone) { "result should have been completed but didn't complete " }
    check(!receivedCompletion) { "completion message received before" }
    receivedCompletion = true
  }
}