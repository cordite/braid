/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.serialisation

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.util.StdConverter
import net.corda.core.crypto.SecureHash
import net.corda.core.node.services.TransactionStorage
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.toBase58

@JsonSerialize(converter = SignedTransactionToStringConverter::class)
abstract class SignedTransactionMixin

class SignedTransactionToStringConverter: StdConverter<SignedTransaction, String>() {
  override fun convert(value: SignedTransaction): String {
    return value.id.bytes.toBase58()
  }
}

class SignedTransactionDeserializer(private val validatedTransactions: TransactionStorage): StdDeserializer<SignedTransaction>(SignedTransaction::class.java) {

  override fun deserialize(parser: JsonParser, ctxt: DeserializationContext): SignedTransaction {
    val secureHash = parser.codec.readValue<SecureHash>(parser, SecureHash::class.java)
    return validatedTransactions.getTransaction(secureHash) ?: error("failed to find transaction ${secureHash.bytes.toBase58()}")
  }
}

