/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.serialisation

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.util.StdConverter
import net.corda.core.crypto.Base58
import net.corda.core.crypto.SecureHash
import net.corda.core.utilities.toBase58

@JsonSerialize(converter = SecureHashToStringConverter::class)
@JsonDeserialize(converter = StringToSecureHashConverter::class)
abstract class SecureHashMixin

class SecureHashToStringConverter(): StdConverter<SecureHash,String>() {
  override fun convert(value: SecureHash): String {
    return value.bytes.toBase58()
  }
}

class StringToSecureHashConverter(): StdConverter<String, SecureHash>() {
  override fun convert(value: String): SecureHash {
    return Base58.decode(value).let {
      when (it.size) {
        32 -> SecureHash.SHA256(it)
        else -> throw IllegalArgumentException("Provided string is ${it.size} bytes not 32 bytes in base58: $value")
      }
    }
  }
}

