/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.testing

import net.corda.core.flows.FlowLogic
import net.corda.core.messaging.FlowHandle
import net.corda.core.messaging.FlowProgressHandle
import net.corda.core.node.AppServiceHub
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.ServiceLifecycleObserver
import net.corda.core.node.services.vault.CordaTransactionSupport
import net.corda.core.node.services.vault.SessionScope
import net.corda.testing.node.StartedMockNode
import org.hibernate.Session

/**
 * Adapts a MockNode to an AppServiceHub used by BraidCordaContext
 */
class MockNodeAppServiceHubAdapter(val mockNode: StartedMockNode) : ServiceHub by mockNode.services, AppServiceHub {

  override val database: CordaTransactionSupport by lazy {
    object : CordaTransactionSupport {
      override fun <T> transaction(statement: SessionScope.() -> T): T {
        return object : SessionScope {
          override val session: Session
            get() = TODO("not implemented")
        }.statement()
      }
    }
  }

  override fun register(priority: Int, observer: ServiceLifecycleObserver) {
    TODO("Not relevant")
  }

  override fun <T> startFlow(flow: FlowLogic<T>): FlowHandle<T> {
    TODO("Not yet implemented until MockNetwork uniformly returns FlowHandles")
  }

  override fun <T> startTrackedFlow(flow: FlowLogic<T>): FlowProgressHandle<T> {
    TODO("Not yet implemented until MockNetwork uniformly returns FlowHandles")
  }
}