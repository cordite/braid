/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.testing

import io.vertx.core.Future
import net.corda.core.concurrent.CordaFuture
import net.corda.core.identity.CordaX500Name
import net.corda.core.utilities.getOrThrow
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNodeParameters
import java.math.BigDecimal
import kotlin.math.floor

/**
 * This should be the base class for *all* [MockNetwork] tests. It provides an API to the [SharedNetwork]
 * to minimise resource costs. Methods such [nextInt] are used to isolate / salt
 * resources such as accounts, token types etc between tests
 */
abstract class AbstractCordaNetworkTest(val sharedNetwork: SharedNetwork) {

  /**
   * Network's default notary
   */
  val DEFAULT_NOTARY get() = sharedNetwork.DEFAULT_NOTARY
  val DEFAULT_NOTARY_X500 get() = DEFAULT_NOTARY.name
  val DEFAULT_NOTARY_NAME get() = DEFAULT_NOTARY_X500.toString()

  val braidContext get() = sharedNetwork.braidContext

  /**
   * retrieve a node by name, creating it if it does not exist
   */
  open fun getNode(name: CordaX500Name, authCredentials: Any? = null): TestNode =
    sharedNetwork.getNode(name, authCredentials)

  open fun getNode(nodeParameters: MockNodeParameters, authCredentials: Any? = null): TestNode =
    sharedNetwork.getNode(nodeParameters, authCredentials)

  /**
   * the vertx instance used across all nodes and tests
   */
  open fun getVertx() = sharedNetwork.vertx

  /**
   * creates a [H2Server] for when we need to bind to the databases and debug them
   */
  open fun h2Server(): H2Server = sharedNetwork.h2Server()

  /**
   * @return next [Int] from the number fountain - used for isolating data between tests
   * that are sharing this cluster
   */
  open fun nextInt(): Int = sharedNetwork.nextInt()

  /**
   * Execute the [call] function and run the network message pump to process all
   * subsequent events
   */
  inline fun <reified T : Any> execute(call: () -> Future<T>): T {
    val future = call()
    while (!future.isComplete) {
      sharedNetwork.network.runNetwork()
    }
    if (future.failed()) {
      throw RuntimeException(future.cause())
    }
    return future.result()
  }

  /**
   * Execute the [call] function and run the network message pump to process all
   * subsequent events
   */
  @JvmName("executeCordaFuture")
  inline fun <reified T> executeCordaFuture(call: () -> CordaFuture<T>): T {
    val future = call()
    while (!future.isDone && !future.isCancelled) {
      sharedNetwork.network.runNetwork()
    }
    return future.getOrThrow()
  }

}

fun createRandomMetaData(): Any {
  return when (floor(Math.random() * 3.0).toInt()) {
    0 -> createRandomString()
    1 -> createRandomList()
    else -> createRandomObject()
  }
}

private val charPool: CharArray = (('a'..'z') + ('A'..'Z') + ('0'..'9')).toCharArray()

fun createRandomString(): String {
  return (1..5).map {
    val index = floor(Math.random() * charPool.size).toInt()
    charPool[index]
  }.joinToString("")
}

fun createRandomObject(): Map<String, String> {
  return mapOf("foo" to createRandomString(), "bar" to createRandomString())
}

fun createRandomList(): List<String> {
  return listOf(
    createRandomString(),
    createRandomString()
  )
}

fun Int.toBigDecimal(): BigDecimal {
  return BigDecimal(this.toLong())
}

fun Double.toBigDecimal(): BigDecimal {
  return BigDecimal(this)
}

fun BigDecimal.multiply(value: Int): BigDecimal {
  return this.multiply(value.toBigDecimal())
}

fun BigDecimal.minus(value: Int): BigDecimal {
  return this.minus(value.toBigDecimal())
}

fun BigDecimal.minus(value: Double): BigDecimal {
  return this.minus(value.toBigDecimal())
}

fun BigDecimal.divide(value: Int): BigDecimal {
  return this.divide(value.toBigDecimal())
}

infix fun String.at(node: TestNode): String {
  return "$this@${node.node.info.legalIdentities.first().name}"
}
