/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.serialisation

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.AnonymousParty
import net.corda.core.identity.Party

abstract class AbstractPartyMixin

object AbstractPartyDeserializer : StdDeserializer<AbstractParty>(AbstractParty::class.java) {
  override fun deserialize(parser: JsonParser, ctxt: DeserializationContext): AbstractParty {
    val str = parser.readValueAsTree<TreeNode>().toString();
    return try {
      val partyParser = parser.codec.factory.createParser(str)
      // attempt to parser as a well-known party
      partyParser.codec.readValue<Party>(partyParser, Party::class.java)
    } catch (err: Throwable) {
      val anonymousPartyParser = parser.codec.factory.createParser(str)
      anonymousPartyParser.codec.readValue<AnonymousParty>(anonymousPartyParser, AnonymousParty::class.java)
    }
  }
}
