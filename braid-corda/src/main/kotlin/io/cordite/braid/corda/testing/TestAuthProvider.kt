/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@file:Suppress("DEPRECATION")

package io.cordite.braid.corda.testing

import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.AbstractUser
import io.vertx.ext.auth.AuthProvider
import io.vertx.ext.auth.User

class TestAuthProvider(private val username: String = "admin", private val password: String = "admin") : AuthProvider {
  override fun authenticate(
    authInfo: JsonObject,
    resultHandler: Handler<AsyncResult<User>>
  ) {
    val username = authInfo.getString("username", "")
    val password = authInfo.getString("password", "")
    if (username == this.username && password == this.password) {
      resultHandler.handle(Future.succeededFuture(TestAuthUser(username)))
    } else {
      resultHandler.handle(Future.failedFuture("authentication failed"))
    }
  }

}

class TestAuthUser(username: String) : AbstractUser() {
  private val principal = JsonObject().put("username", username)

  override fun doIsPermitted(
    permission: String,
    resultHandler: Handler<AsyncResult<Boolean>>
  ) {
    // all is permitted
    resultHandler.handle(Future.succeededFuture(true))
  }

  override fun setAuthProvider(authProvider: AuthProvider?) {
  }

  override fun principal(): JsonObject {
    return principal
  }
}
