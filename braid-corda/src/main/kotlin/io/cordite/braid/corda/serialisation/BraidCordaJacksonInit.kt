/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.serialisation

import com.fasterxml.jackson.databind.module.SimpleModule
import net.corda.core.contracts.Amount
import net.corda.core.contracts.Issued
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.*
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.WireTransaction
import java.math.BigDecimal
import java.security.PublicKey

class BraidCordaJacksonModule(serviceHub: ServiceHub) : SimpleModule() {
  init {
    addDeserializer(AbstractParty::class.java, AbstractPartyDeserializer)
    addDeserializer(SignedTransaction::class.java, SignedTransactionDeserializer(serviceHub.validatedTransactions))
    addSerializer(Amount::class.java, AmountSerializer())
    addDeserializer(Amount::class.java, AmountDeserializer())
    addSerializer(Issued::class.java, IssuedSerializer())
    addDeserializer(Issued::class.java, IssuedDeserializer())
    addSerializer(PageSpecification::class.java, PageSpecificationSerializer())
    addDeserializer(PageSpecification::class.java, PageSpecificationDeserializer())

    // Mixins for transaction types to prevent some properties from being serialized
    setMixInAnnotation(SignedTransaction::class.java, SignedTransactionMixin::class.java)
    setMixInAnnotation(WireTransaction::class.java, WireTransactionMixin::class.java)
    setMixInAnnotation(AbstractParty::class.java, AbstractPartyMixin::class.java)
    setMixInAnnotation(CordaX500Name::class.java, CordaX500NameMixin::class.java)
    setMixInAnnotation(BigDecimal::class.java, BigDecimalMixin::class.java)
    setMixInAnnotation(PublicKey::class.java, PublicKeyMixin::class.java)
    setMixInAnnotation(SecureHash::class.java, SecureHashMixin::class.java)
    setMixInAnnotation(PartyAndCertificate::class.java, PartyAndCertificateMixin::class.java)
  }

  override fun getModuleName(): String {
    return "braid-corda"
  }
}

