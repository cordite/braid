/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.utils

import com.typesafe.config.ConfigFactory
import io.cordite.braid.core.context.BraidContext
import io.cordite.braid.core.logging.loggerFor
import io.vertx.core.json.JsonObject
import net.corda.core.cordapp.CordappConfig
import net.corda.core.internal.uncheckedCast
import net.corda.node.internal.cordapp.TypesafeCordappConfig

@Suppress("MemberVisibilityCanBePrivate")
object CordaConfigJacksonCodec {

  private val log = loggerFor<CordaConfigJacksonCodec>()
  const val DEFAULT_ROOT: String = "config"
  fun <T> BraidContext.encodeCordappConfig(config: T, root: String = DEFAULT_ROOT): CordappConfig {
    val encoded = encode(config)
    val map: Map<String, Any> = uncheckedCast(decodeValue(encoded, Map::class.java))
    val parsed = ConfigFactory.parseMap(mapOf(root to map))
    return TypesafeCordappConfig(parsed)
  }

  @Suppress("unused")
  fun CordappConfig.toMap(root: String = DEFAULT_ROOT): Map<String, Any> {
    // part of the mess that is Corda's Cordapp config!
    return mapOf(root to uncheckedCast(get(root))!!)
  }

  inline fun <reified T : Any> BraidContext.decodeCordappConfig(
    cordappConfig: CordappConfig,
    root: String = "config"
  ): T? {
    return decodeCordappConfig(T::class.java, cordappConfig, root)
  }

  fun <T : Any> BraidContext.decodeCordappConfig(
    targetClass: Class<T>,
    cordappConfig: CordappConfig,
    root: String = "config"
  ): T? {
    val rootConfig = getRootOfConfig(cordappConfig, root) ?: return null
    return try {
      val encoded = encode(JsonObject(rootConfig))
      decodeValue(encoded, targetClass)
    } catch (err: Throwable) {
      log.error("failed to deserialize config", err)
      null
    }
  }

  private fun getRootOfConfig(cordappConfig: CordappConfig, root: String): Map<String, Any>? {
    return try {
      uncheckedCast(cordappConfig.get(root))
    } catch (err: Throwable) {
      log.info("did not find root field '${root}' in cordapp config")
      null
    }
  }
}