/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.testing

import net.corda.core.contracts.*
import net.corda.core.cordapp.CordappProvider
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.PartyAndCertificate
import net.corda.core.messaging.FlowHandle
import net.corda.core.messaging.FlowProgressHandle
import net.corda.core.node.AppServiceHub
import net.corda.core.node.NodeInfo
import net.corda.core.node.NotaryInfo
import net.corda.core.node.StatesToRecord
import net.corda.core.node.services.*
import net.corda.core.node.services.diagnostics.DiagnosticsService
import net.corda.core.node.services.vault.CordaTransactionSupport
import net.corda.core.node.services.vault.SessionScope
import net.corda.core.serialization.SerializeAsToken
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.NetworkHostAndPort
import net.corda.coretesting.internal.DEV_ROOT_CA
import net.corda.node.services.identity.InMemoryIdentityService
import net.corda.testing.common.internal.testNetworkParameters
import net.corda.testing.core.DUMMY_BANK_A_NAME
import net.corda.testing.core.DUMMY_NOTARY_NAME
import net.corda.testing.core.TestIdentity
import net.corda.testing.node.MockServices.Companion.makeTestDatabaseAndMockServices
import net.corda.testing.node.createMockCordaService
import net.corda.testing.node.internal.MockKeyManagementService
import org.hibernate.Session
import java.security.cert.X509Certificate
import java.time.Clock
import java.util.function.Consumer
import javax.persistence.EntityManager

val PARTY_A_IDENTITY = TestIdentity.fresh("PartyA")
val PARTY_B_IDENTITY = TestIdentity.fresh("PartyB")
val DUMMY_NOTARY = TestIdentity(DUMMY_NOTARY_NAME, 20)

/**
 * A stub class to simulate the service hub - we need this for Braid Corda
 * Technically braid-corda's core could be moved to core
 */
class TestAppServiceHub(
  identities: List<PartyAndCertificate> = listOf(PARTY_A_IDENTITY.identity, PARTY_B_IDENTITY.identity),
  trustRoot: X509Certificate = DEV_ROOT_CA.certificate,
  cordappPackages: List<String> = emptyList()
) : AppServiceHub {

  companion object {

    @JvmStatic
    val DEFAULT_SERVICE_HUB = TestAppServiceHub()
  }

  override val identityService = InMemoryIdentityService(
    identities,
    trustRoot = trustRoot
  )

  private val dummyNotary = TestIdentity(DUMMY_NOTARY_NAME, 20)

  override val networkParameters = testNetworkParameters(notaries = listOf(NotaryInfo(dummyNotary.party, true)))

  private val persistanceAndServices = makeTestDatabaseAndMockServices(
    cordappPackages,
    identityService,
    TestIdentity.fresh("initial-org"),
    networkParameters
  )

  private val mockServices = persistanceAndServices.second

  fun <T : SerializeAsToken> createMockCordaService(serviceConstructor: (AppServiceHub) -> T): T {
    return createMockCordaService(mockServices, serviceConstructor)
  }

  override val networkParametersService: NetworkParametersService get() = mockServices.networkParametersService

  override fun loadContractAttachment(stateRef: StateRef): Attachment = mockServices.loadContractAttachment(stateRef)

  override fun withEntityManager(block: Consumer<EntityManager>) = mockServices.withEntityManager(block)

  override fun <T> withEntityManager(block: EntityManager.() -> T): T = mockServices.withEntityManager(block)

  override val attachments: AttachmentStorage get() = mockServices.attachments

  override val clock: Clock get() = mockServices.clock

  override val contractUpgradeService: ContractUpgradeService get() = mockServices.contractUpgradeService

  override val cordappProvider: CordappProvider get() = mockServices.cordappProvider

  override val database: CordaTransactionSupport
    get() = object : CordaTransactionSupport {
      override fun <T> transaction(statement: SessionScope.() -> T): T {
        return object : SessionScope {
          override val session: Session
            get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
        }.statement()
      }
    }

  override val diagnosticsService: DiagnosticsService get() = mockServices.diagnosticsService

  override val keyManagementService: KeyManagementService =
    MockKeyManagementService(identityService, PARTY_A_IDENTITY.keyPair, PARTY_B_IDENTITY.keyPair)

  override val myInfo: NodeInfo
    get() = NodeInfo(
      listOf(NetworkHostAndPort("localhost", 10001)),
      listOf(TestIdentity(DUMMY_BANK_A_NAME, 40).identity),
      3,
      1
    )
  override val networkMapCache: NetworkMapCache get() = mockServices.networkMapCache

  override val transactionVerifierService: TransactionVerifierService get() = mockServices.transactionVerifierService

  override val validatedTransactions: TransactionStorage get() = mockServices.validatedTransactions

  override val vaultService: VaultService get() = mockServices.vaultService

  override fun <T : SerializeAsToken> cordaService(type: Class<T>): T {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun jdbcSession() = mockServices.jdbcSession()

  override fun loadState(stateRef: StateRef): TransactionState<*> = mockServices.loadState(stateRef)

  override fun loadStates(stateRefs: Set<StateRef>): Set<StateAndRef<ContractState>> =
    mockServices.loadStates(stateRefs)

  override fun recordTransactions(
    statesToRecord: StatesToRecord,
    txs: Iterable<SignedTransaction>
  ) = mockServices.recordTransactions(statesToRecord, txs)

  override fun register(priority: Int, observer: ServiceLifecycleObserver) {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun registerUnloadHandler(runOnStop: () -> Unit) {
  }

  override fun <T> startFlow(flow: FlowLogic<T>): FlowHandle<T> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun <T> startTrackedFlow(flow: FlowLogic<T>): FlowProgressHandle<T> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }
}
